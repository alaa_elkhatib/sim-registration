// based on Google's CameraPreview class in API Samples
package com.simregistration.signed;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;


public class CPR2 extends Activity implements SensorEventListener{
	private Preview mPreview; 
	private ImageView mTakePicture;
	private LeftTouchView barView;
	private RightTouchView idView;
	private ImageView mFlash;
	private boolean mAutoFocus = true;
    private boolean mFlashBoolean = false;
    private SensorManager mSensorManager;
	private Sensor mAccel;
	private boolean mInitialized = false;
	private float mLastX = 0;
	private float mLastY = 0;
	private float mLastZ = 0;
	private ProgressDialog progress = null;
	private int mScreenHeight;
	private int mScreenWidth;
    private boolean mInvalidate = false;
    private Bitmap bmp = null;
    private File mIDLocation,mFormLocation;	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.cpr); // display our (only) XML layout - Views already ordered
		
		/*mIDLocation = new File(Environment.getExternalStorageDirectory(),"OCR/ID.jpg");
		try {
			bmp = BitmapFactory.decodeFile(mIDLocation.getAbsolutePath());
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		mFormLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Form.jpg");
		File parent = mFormLocation.getParentFile();
		if (parent != null) parent.mkdirs();
		
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		mScreenHeight = displaymetrics.heightPixels;
		mScreenWidth = displaymetrics.widthPixels;
		Drawable mButtonDrawable = this.getResources().getDrawable(R.drawable.camera);
        mFlash = (ImageView) findViewById(R.id.flash);
		mFlash.setOnClickListener(flashListener);
        mTakePicture = (ImageView) findViewById(R.id.startcamerapreview);
		mTakePicture.setOnClickListener(previewListener);
		// get our Views from the XML layout
		mPreview = (Preview) findViewById(R.id.preview);
		barView = (LeftTouchView) findViewById(R.id.bar_view);
		idView = (RightTouchView) findViewById(R.id.id_view);*/
	//	idView.SetBitmap(bmp);

		
	}

	private AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback(){

		public void onAutoFocus(boolean autoFocusSuccess, Camera arg1) {
			Wait.oneSec();
			mAutoFocus = true;
		}};

		public Double[] getRatio(){
			Size s = mPreview.getCameraParameters().getPreviewSize();
			double heightRatio = (double)s.height/(double)mScreenHeight;
			//Log.i(TAG,"camera pixel Height: "+mPreview.getCameraParameters().getPictureSize().height);
			//Log.i(TAG,"Height Ratio: "+heightRatio);
			double widthRatio = (double)s.width/(double)mScreenWidth;
			//Log.i(TAG,"Width Ratio: "+widthRatio);
			Double[] ratio = {heightRatio,widthRatio};
			return ratio;
		}

		private OnClickListener flashListener = new OnClickListener(){

			@Override
			public void onClick(View v) {
				if (mFlashBoolean){
					mPreview.setFlash(false);
				}
				else{
					mPreview.setFlash(true);
				}
				mFlashBoolean = !mFlashBoolean;
			}

		};

		

		private OnClickListener previewListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				//if (mAutoFocus){
					mAutoFocus = false;
					//mPreview.setCameraFocus(myAutoFocusCallback);
					progress = ProgressDialog.show(CPR2.this, getString(R.string.pleaseWait), "", true, false, null);
					Thread tGetPic = new Thread( new Runnable() {
						public void run() {
							
							saveFormPhoto(CombineBitmap(mPreview.getFormPic(),Bitmap.createScaledBitmap(bmp, (int)idView.mRightBottomPosX/2,(int)idView.mRightBottomPosY/3, false)) );
			                progress.dismiss();
						    mAutoFocus = true;
						    progress.dismiss();
						mAutoFocus = true;
						Intent intent = new Intent(getBaseContext(), CPR_Preview.class);
					    startActivityForResult(intent,1); 
						} 
					});
					tGetPic.start();
				//}
				//mView.invalidate();
				boolean pressed = false;
				//Log.i(TAG,"Is pressed? "+pressed);
				if (!mTakePicture.isPressed()){
					pressed = true;
				}
				//Log.i(TAG,"Being pressed");
				//return pressed;
			}	   
		};
		
		
	
	    
	   
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			//Log.i(TAG, "onKeyDown(" + keyCode + ")");

			// to take the pic ASAP, grab the preview frame data from here - don't wait for photo
			if (keyCode == KeyEvent.KEYCODE_BACK){
				finish();
			}
			return super.onKeyDown(keyCode, event); // pass the key along to other handlers 
		}

		private boolean saveIDPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mIDLocation.exists())
					mIDLocation.createNewFile();
				image = new FileOutputStream(mIDLocation);
			} catch (Exception ex) {
				 WebService.SaveErrorLogNoBandwidth(CPR2.this,Utility.GetUserEntity(CPR2.this).getId(),CPR2.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
					
				//if(Utility.isConnected(CPR2.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR2.this).getId(),CPR2.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			bm.compress(CompressFormat.JPEG, 100, image);
			if (image != null) {
				try {
					image.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			     }
			return true;
		}
		
		private Bitmap CombineBitmap(Bitmap c ,Bitmap s){
			Bitmap newBitmap = null;

			int w =c.getWidth();
			int h= c.getHeight();
			
			Config config = c.getConfig();
			if(config == null){
			config = Bitmap.Config.ARGB_8888;
			}

			newBitmap = Bitmap.createBitmap(w, h, config);
			Canvas newCanvas = new Canvas(newBitmap);

			newCanvas.drawBitmap(c, 0, 0, null);

			Paint paint = new Paint();
			paint.setAlpha(255);
			RectF dst= new RectF(w/2,(int)((h /7)),w,(int)(h /1.8));
			newCanvas.drawBitmap(s, null, dst, paint);
	        //newCanvas.drawBitmap(s, 0, 0, paint);

			return newBitmap;
			}

			
	
		
		private Bitmap saveFormPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mFormLocation.exists())
					mFormLocation.createNewFile();
				image = new FileOutputStream(mFormLocation);
			} catch (Exception ex) {
				 WebService.SaveErrorLogNoBandwidth(CPR2.this,Utility.GetUserEntity(CPR2.this).getId(),CPR2.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
					
				//if(Utility.isConnected(CPR2.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR2.this).getId(),CPR2.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			bm.compress(CompressFormat.JPEG, 100, image);
			if (image != null) {
				try {
					image.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			     }
			return bm;
		}
		
		/*private boolean saveExpiryPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mExpiryLocation.exists())
					mExpiryLocation.createNewFile();
				image = new FileOutputStream(mExpiryLocation);
			} catch (Exception e) {
				//Log.i(TAG,"Could not find image locatoin");
				e.printStackTrace();
			}
			Utility.RotateBitmap(bm, 90).compress(CompressFormat.JPEG, 100, image);
			
			return true;
		}
		*/
	

		public void onSensorChanged(SensorEvent event) {
			try{
			if(mPreview.mCamera!=null){

			if (mInvalidate == true){
				idView.invalidate();
				barView.invalidate();
				mInvalidate = false;
			}
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			if (!mInitialized){
				mLastX = x;
				mLastY = y;
				mLastZ = z;
				mInitialized = true;
			}
			float deltaX  = Math.abs(mLastX - x);
			float deltaY = Math.abs(mLastY - y);
			float deltaZ = Math.abs(mLastZ - z);

			if (deltaX > .5 && mAutoFocus){ //AUTOFOCUS (while it is not autofocusing)
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);
			}
			if (deltaY > .5 && mAutoFocus){ //AUTOFOCUS (while it is not autofocusing)
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);
			}
			if (deltaZ > .5 && mAutoFocus){ //AUTOFOCUS (while it is not autofocusing) */
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);
			}

			mLastX = x;
			mLastY = y;
			mLastZ = z;

		}}catch(Exception ex){
			Utility.alert(getString(R.string.restart), CPR2.this, onclick);
		}}

	


		@Override
		protected void onPause() {
			super.onPause();
			//Log.i(TAG, "onPause()");
			try{
			mSensorManager.unregisterListener(this);}
			catch(Exception ex){}
		}

		@Override
		protected void onResume() {
			super.onResume();
			try{
			mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_UI);}
			catch(Exception ex){}
			//Log.i(TAG, "onResume()");
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub

		}
		
		public void onActivityResult(int requestCode,int resultCode,Intent data)
	    {
	    	try {
	    		if (resultCode == RESULT_OK) {
	    			setResult(RESULT_OK, data);
					finish();
	    		}
	    			
	    	}catch (Exception ex) {
	    		 WebService.SaveErrorLogNoBandwidth(CPR2.this,Utility.GetUserEntity(CPR2.this).getId(),CPR2.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
					
	    		//if(Utility.isConnected(CPR2.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR2.this).getId(),CPR2.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	    	}}
		  DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
					Intent intent = new Intent(CPR2.this,Login.class);
				    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);              
					startActivity(intent);
				}};
		
		
}