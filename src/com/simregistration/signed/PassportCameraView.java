package com.simregistration.signed;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemClickListener;

import biz.smartengines.swig.MrzEngine;
import biz.smartengines.swig.MrzEngineInternalSettings;
import biz.smartengines.swig.MrzEngineSessionHelpers;
import biz.smartengines.swig.MrzEngineSessionSettings;
import biz.smartengines.swig.MrzException;
import biz.smartengines.swig.MrzRect;
import biz.smartengines.swig.MrzRectMatrix;
import biz.smartengines.swig.MrzResult;
import biz.smartengines.swig.StringVector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.simregistration.signed.entity.Offers_Entity;
import com.simregistration.signed.utility.C;

public class PassportCameraView extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.AutoFocusCallback, CompoundButton.OnCheckedChangeListener{

    private android.hardware.Camera mCamera;
    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;
    private ToggleButton mStartButton;
	private ImageView mTakePicture;
    private RelativeLayout mElementsLayout;
    private RelativeLayout mUpperZone;
    private RelativeLayout mLowerZone;
    private RectView mRectView;
    private static double aspectRatio = 6.0;         // roi aspect ratio = width/height
    private boolean bProcess = false;
    private int h_zone = 0, w_zone = 0; // size of screen to show preview
    private int h_roi = 0, w_roi = 0;   // size of roi in screen
    private static double scale = 0;
    private static boolean camera_params = false;
    private static String focus_mode;
    private static int size_mode_w;
    private static int size_mode_h;
    private Timer timer;
    private static boolean bUseTimer = false;
    private View.OnClickListener onFocus = new View.OnClickListener() {

        public void onClick(View v) {

            if(bUseTimer == false)  // focus on tap
                focusing();
        }
    };


    private class FocusTimer extends TimerTask {

        public void run() {
            focusing();
            }
    }

    private void focusing() {

        Camera.Parameters cparams = mCamera.getParameters();
        mCamera.cancelAutoFocus();
        List<String> focusModes = cparams.getSupportedFocusModes();
        if (focusModes.contains(cparams.FOCUS_MODE_AUTO))
        	cparams.setFocusMode(cparams.FOCUS_MODE_AUTO);
        mCamera.setParameters(cparams);
        mCamera.autoFocus(PassportCameraView.this);
        
        
        
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passport_cameraview);
        mTakePicture = (ImageView) findViewById(R.id.startcamerapreview);
        mTakePicture.setOnClickListener(new OnClickListener() {


			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
        mSurfaceView = (SurfaceView)findViewById(R.id.cameraView);
        mSurfaceView.setOnClickListener(onFocus);

        
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mStartButton = (ToggleButton) findViewById(R.id.StartButton);
        mStartButton.setOnCheckedChangeListener(this);
      
       
        mElementsLayout = (RelativeLayout) findViewById(R.id.ElementsLayout);
        mUpperZone = (RelativeLayout) findViewById(R.id.UpperZone);
        mLowerZone = (RelativeLayout) findViewById(R.id.LowerZone);
        mRectView = new RectView(this);
      
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

        try{
            mCamera = android.hardware.Camera.open();

            Camera.Parameters cparams = mCamera.getParameters();
            CheckCameraParams();

            cparams.setFocusMode(focus_mode);
            cparams.setPreviewSize(size_mode_w, size_mode_h);

            mCamera.setParameters(cparams);

            int w = mSurfaceView.getWidth();
            int h = mSurfaceView.getHeight();

            double p1 = (double)w/(double)size_mode_w;
            double p2 = (double)h/(double)size_mode_h;

            scale = Math.min(p1, p2);

            h_zone = (int) (scale*size_mode_h);
            w_zone = (int) (scale*size_mode_w);

            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(w_zone, h_zone);
            lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            lparams.addRule(RelativeLayout.CENTER_VERTICAL);
            mElementsLayout.setLayoutParams(lparams);
            mSurfaceView.setLayoutParams(lparams);

            mCamera.setPreviewCallback(this);
            mStartButton.setChecked(true);

        }
        catch (Exception e) {
            Log.d(C.APPNAME, "Cannot init camera: " + e.getMessage());
        }


        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            Log.d(C.APPNAME, "Cannot set preview surface: " + e.getMessage());
        }

        mCamera.startPreview();
        mCamera.setOneShotPreviewCallback(mPreviewListener);
        if(bUseTimer)
        {
            timer = new Timer();
            timer.schedule(new FocusTimer(), 1000, 3000);
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

        mStartButton.setChecked(false);

        if(bUseTimer)
            timer.cancel();

        bProcess = false;


        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mCamera.release();
    }

    private void CheckCameraParams() {


        if(camera_params == false)
        {

            Camera.Parameters cparams = mCamera.getParameters();
            List<String> focus = cparams.getSupportedFocusModes();
            List<Camera.Size> sizes = cparams.getSupportedPreviewSizes();

            focus_mode = Camera.Parameters.FOCUS_MODE_AUTO;

            focus.contains(Camera.Parameters.FOCUS_MODE_AUTO);

            if(focus.contains(focus_mode) == true)
            {

                if(focus.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                    focus_mode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
                else
                    if(focus.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
                        focus_mode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;

            }
            else
            {
                if( focus.contains(Camera.Parameters.FOCUS_MODE_INFINITY) )
                    focus_mode = Camera.Parameters.FOCUS_MODE_INFINITY;
                else
                if( focus.contains(Camera.Parameters.FOCUS_MODE_FIXED) )
                    focus_mode = Camera.Parameters.FOCUS_MODE_FIXED;
            }


            size_mode_w = size_mode_h = 0;

            for(int i=0; i< sizes.size(); i++) {

                Camera.Size size = sizes.get(i);

                if( size.width > size_mode_w )
                {
                    size_mode_w = size.width;
                    size_mode_h = size.height;
                }

            }

            bUseTimer = focus_mode == Camera.Parameters.FOCUS_MODE_AUTO;
            camera_params = true;

        }

    }


    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }


    private Camera.PreviewCallback mPreviewListener = new Camera.PreviewCallback() {
    	 @Override
    	 public void onPreviewFrame(byte[] data, Camera camera) {
    		  if( bProcess) {

    	            Log.d(C.APPNAME, "Image is recognizing");

    	            Camera.Parameters cparams = camera.getParameters();
    	            Camera.Size size = cparams.getPreviewSize();

    	            Rect rect = new Rect();
    	            rect.top = rect.left = 0;
    	            rect.bottom = size.height;
    	            rect.right = size.width;

    	            int roi_width = size.width;
    	            int roi_height = (int) (size.width / aspectRatio);

    	            int roi_x = 0;
    	            int roi_y = (size.height - roi_height)/2;

    	            int width = cparams.getPreviewSize().width;
    	            int height = cparams.getPreviewSize().height;
    	            YuvImage yuv = new YuvImage(data, cparams.getPreviewFormat(), width, height, null);

    	            ByteArrayOutputStream out = new ByteArrayOutputStream();
    	            yuv.compressToJpeg(new Rect(roi_x, roi_y, roi_width, roi_height), 50, out);

    	            byte[] bytes = out.toByteArray();
    	            final Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

    	            try {

    	               /* if(bNexus == true)
    	                    engine.FeedUncompressedImageData(bytes, size.width, size.height, size.width, 1, roi,  MrzEngine.ImageOrientation.InvertedLandscape);
    	                else
    	                    engine.FeedUncompressedImageData(bytes, size.width, size.height, size.width, 1, roi,  MrzEngine.ImageOrientation.Landscape);
    	*/
    	            } catch (Exception e) {
    	                 Log.d(C.APPNAME, "Failed to feed image to engine: " + e.getMessage());
    	            }

    	        }
    	 }
    	};
    @Override
    public void onPreviewFrame(byte[] data, android.hardware.Camera camera) {

        if( bProcess) {

            Log.d(C.APPNAME, "Image is recognizing");

            Camera.Parameters cparams = camera.getParameters();
            Camera.Size size = cparams.getPreviewSize();

            Rect rect = new Rect();
            rect.top = rect.left = 0;
            rect.bottom = size.height;
            rect.right = size.width;

            int roi_width = size.width;
            int roi_height = (int) (size.width / aspectRatio);

            int roi_x = 0;
            int roi_y = (size.height - roi_height)/2;

            int width = cparams.getPreviewSize().width;
            int height = cparams.getPreviewSize().height;
            YuvImage yuv = new YuvImage(data, cparams.getPreviewFormat(), width, height, null);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            yuv.compressToJpeg(new Rect(roi_x, roi_y, roi_width, roi_height), 50, out);

            byte[] bytes = out.toByteArray();
            final Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

            try {

               /* if(bNexus == true)
                    engine.FeedUncompressedImageData(bytes, size.width, size.height, size.width, 1, roi,  MrzEngine.ImageOrientation.InvertedLandscape);
                else
                    engine.FeedUncompressedImageData(bytes, size.width, size.height, size.width, 1, roi,  MrzEngine.ImageOrientation.Landscape);
*/
            } catch (Exception e) {
                 Log.d(C.APPNAME, "Failed to feed image to engine: " + e.getMessage());
            }

        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

       bProcess = isChecked;

       if( bProcess == true){

           h_roi = (int)(w_zone/aspectRatio);
           w_roi = w_zone;

           Rect roi = new Rect(0, (h_zone - h_roi)/2, w_zone, (h_zone + h_roi)/2);
           mRectView.SetRoiZone(roi, scale);
           mRectView.SetRects(null);

           RelativeLayout.LayoutParams params_upper = new RelativeLayout.LayoutParams(w_zone, (h_zone - h_roi)/2 );
           params_upper.addRule(RelativeLayout.CENTER_HORIZONTAL);
           params_upper.addRule(RelativeLayout.ALIGN_PARENT_TOP);
           mUpperZone.setLayoutParams(params_upper);

           RelativeLayout.LayoutParams params_lower = new RelativeLayout.LayoutParams(w_zone, (h_zone - h_roi)/2 );
           params_lower.addRule(RelativeLayout.CENTER_HORIZONTAL);
           params_lower.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
           mLowerZone.setLayoutParams(params_lower);

           RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(w_zone, h_zone);
           lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
           lparams.addRule(RelativeLayout.CENTER_VERTICAL);

           mRectView.setLayoutParams(lparams);
           mElementsLayout.addView(mRectView);
       }
        


    }

    

    public void DrawRects(MrzRectMatrix mrzRectMatrix) {

        mRectView.SetRects(mrzRectMatrix);
        mRectView.invalidate();
    }

}
