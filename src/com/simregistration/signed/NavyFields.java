package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ClearableEditText;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.IntentIntegrator;
import com.simregistration.signed.utility.IntentResult;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class NavyFields extends Activity {
    private EditText edt_Name,edt_Sim,edt_ID,edt_Address;
    private Spinner sp_Country;
    private RadioButton rb_M,rb_F,rb_B,rb_N;
    private TextView txtBirthDay,txtExpiryDate;
    private int iYear, iMonth, iDay,eYear, eMonth, eDay;
    private Button btn_NavyImage,btn_Scan,btn_Submit;
    public Bitmap navy;
    private Uri cameraTempUri;
    private final int TAKE_CAMERA_REQUEST = 1;
    private final int TAKE_NAVY_REQUEST = 2;
    private ArrayList<Countries_Entity> alCountriesDataMain;
    private ArrayList<String> alCountriesDataMainNames;
    CustomAdapter adapter;
    private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private String imgPath="";
	Button btn_AddICCID;
	private int iEditTextPos=0;
	private ArrayList<EditText> alICCID= new ArrayList<EditText>();
	LinearLayout ln_ICCID;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navy_fields_multi_iccid);
        
        ln_ICCID = (LinearLayout)findViewById(R.id.lnSim);
        btn_AddICCID = (Button)findViewById(R.id.btnAddMore);
  	    btn_AddICCID.setOnClickListener(new OnClickListener() {
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onClick(View v) {
				try {
//	      			for( int i=0;i<alICCID.size();i++){
//		      			EditText edt=alICCID.get(i);
//		      			if(edt.getText().toString().trim().equals("")){
//		      				Utility.showToast(NavyFields.this,getResources().getString(R.string.empty_iccid_no_more));
//		      				return;
//		      			}
//	      			}
					LinearLayout ln_more = new LinearLayout(NavyFields.this);
					ln_more.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					ln_more.setOrientation(LinearLayout.HORIZONTAL);
					
					
					LinearLayout.LayoutParams Editparams = new LinearLayout.LayoutParams(
					0, edt_Sim.getHeight());
					Editparams.weight = 0.7f;
					
					LinearLayout.LayoutParams Buttonparams = new LinearLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					final ImageView cancel = new ImageView(NavyFields.this);
//					cancel.setImageResource(R.drawable.cancel);
					final ClearableEditText myEditText = new ClearableEditText(NavyFields.this); // Pass it an Activity or Context
					final int sdk = android.os.Build.VERSION.SDK_INT;
					if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
						myEditText.setBackgroundDrawable( getResources().getDrawable(R.drawable.rounded) );
					} else {
						myEditText.setBackground( getResources().getDrawable(R.drawable.rounded));
					}
					myEditText.setPadding(10, 0, 10, 0);
					myEditText.setRawInputType(InputType.TYPE_CLASS_NUMBER);
					myEditText.setSingleLine(true);
					myEditText.setHint(getResources().getString(R.string.sim));
					
					
					Button myButton = new Button(NavyFields.this); // Pass it an Activity or Context
					myButton.setText(getResources().getString(R.string.scan));
					myButton.setOnClickListener(new View.OnClickListener(){

				      	public void onClick(View v){
				      		try {
				      			iEditTextPos=alICCID.indexOf(myEditText);
				      			IntentIntegrator integrator = new IntentIntegrator(NavyFields.this);
				                integrator.initiateScan();
				      		} catch (Exception ex) {
				      			WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				      			}}});
					myEditText.setLayoutParams(Editparams); 
					myButton.setLayoutParams(Buttonparams); 
					
					  Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.cancel);
					  Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, 50, 50, true);
					  cancel.setPadding(0, 10, 0, 0);
					  cancel.setImageBitmap(bMapScaled);
					  cancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							alICCID.remove(((LinearLayout)arg0.getParent()).getChildAt((((LinearLayout)arg0.getParent()).getChildCount() - 2)));
							((LinearLayout)((LinearLayout)arg0.getParent()).getParent()).removeView((View) arg0.getParent());
						}
					});
					ln_more.addView(cancel);
					ln_more.addView(myEditText);
					ln_more.addView(myButton);
					ln_ICCID.addView(ln_more);
					alICCID.add(myEditText);
					} catch (Exception ex) {
						  }
			}});
  	    
    	final Calendar c = Calendar.getInstance();
	    eYear = c.get(Calendar.YEAR) ;
	    eMonth = c.get(Calendar.MONTH)+1;
	    eDay = c.get(Calendar.DAY_OF_MONTH);
	    iYear = c.get(Calendar.YEAR) ;
	    iMonth = c.get(Calendar.MONTH)+1;
	    iDay = c.get(Calendar.DAY_OF_MONTH);
	        
	        
        dbHelper = new DatabaseHelper(NavyFields.this);
	    db = dbHelper.getWritableDatabase();
        alCountriesDataMain = new ArrayList<Countries_Entity>();
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_Name = (EditText)findViewById(R.id.edtName);
        edt_Sim = (EditText)findViewById(R.id.edtSim);
        alICCID.add(edt_Sim);
        edt_ID = (EditText)findViewById(R.id.edtID);
        sp_Country = (Spinner)findViewById(R.id.spCountry);
        edt_Address = (EditText)findViewById(R.id.edtAddress);
        txtBirthDay= (TextView)findViewById(R.id.txtBirthDay);
        txtBirthDay.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  				
  			        
  					DatePickerDialog dg = new DatePickerDialog(NavyFields.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          iYear = year;
  				          iMonth = monthOfYear+1;
  				          iDay = dayOfMonth;
  				          
  				          txtBirthDay.setText( Integer.toString(iDay)+ "/" + Integer.toString(iMonth) + "/" +
  				        		 Integer.toString(iYear) );
  				      }

  					}, iYear, iMonth-1, iDay);
  					
  					dg.show();	// Show picker dialog
  					} catch (Exception ex) {
  						WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  						
  					//	if(Utility.isConnected(NavyFields.this))
  					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

  					}}});
        txtExpiryDate= (TextView)findViewById(R.id.txtIDExpiry);
        txtExpiryDate.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  					
  					
  			
  			        
  					DatePickerDialog dg = new DatePickerDialog(NavyFields.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          eYear = year;
  				          eMonth = monthOfYear+1;
  				          eDay = dayOfMonth;
  				          
  				        txtExpiryDate.setText( Integer.toString(eDay)+ "/" + Integer.toString(eMonth) + "/" +
  				        		 Integer.toString(eYear) );
  				      }

  					}, eYear, eMonth-1, eDay);
  					
  					dg.show();	// Show picker dialog
					} catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  					
						//if(Utility.isConnected(NavyFields.this))
 						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}}});
  	  
        rb_M = (RadioButton)findViewById(R.id.rb_M);
  	    rb_F= (RadioButton)findViewById(R.id.rb_F);
  	    rb_B = (RadioButton)findViewById(R.id.rb_B);
  	    rb_N= (RadioButton)findViewById(R.id.rb_N);
  	   /* btn_NavyImage = (Button)this.findViewById(R.id.btnNavy);
  	    btn_NavyImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					int iICCID=0;
	      			for( int i=0;i<alICCID.size();i++){
	      			EditText edt=alICCID.get(i);
	      			if(!edt.getText().toString().trim().equals("")){
	      				iICCID++;
	      				for(int y =i+1;y<alICCID.size();y++){
	      					if(edt.getText().toString().trim().equals(alICCID.get(y).getText().toString().trim())){
	      						Utility.showToast(NavyFields.this,getResources().getString(R.string.dublicate_icid)+" "+(i+1)+" , "+(y+1));
			      				return;
	      					}
	      				}
	      			}else{
	      				Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_iccid));
	      				return;
	      			}
	      				
	      			}
					 if(edt_Name.getText().toString().equals(""))
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_name));
					  else if (edt_Sim.getText().toString().equals(""))
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_iccid));
					  else if(edt_ID.getText().toString().equals(""))
					   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_idnumber));	
					  else if(sp_Country.getSelectedItemPosition()==0)
						   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_country));	
					  else if(txtBirthDay.getText().toString().equals(""))
						   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_Birthday));
					  else if(txtExpiryDate.getText().toString().equals(""))
						   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_idexpiry));
					  else if (getDiffYears(iYear, iMonth, iDay)<C.AGE)
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  else if(IsExpired(txtExpiryDate.getText().toString()))
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.id_expired));
					  else{
						  final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			              intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
			              startActivityForResult(intent, TAKE_CAMERA_REQUEST);
			                
			                
			          File dir = Environment.getExternalStorageDirectory();
			          File file = File.createTempFile("photo_", null, dir);
			          cameraTempUri = Uri.fromFile(file);
			          Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			          intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			          intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraTempUri);
			          startActivityForResult(intent, TAKE_CAMERA_REQUEST);
					  }} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
		  					
						 // if(Utility.isConnected(NavyFields.this))
	  					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			      }
			}}); */
  	  btn_Submit = (Button)this.findViewById(R.id.btnSubmit);
  	  btn_Submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
						int iICCID=0;
		      			for( int i=0;i<alICCID.size();i++){
		      			EditText edt=alICCID.get(i);
		      			if(!edt.getText().toString().trim().equals("")){
		      				iICCID++;
		      				for(int y =i+1;y<alICCID.size();y++){
		      					if(edt.getText().toString().trim().equals(alICCID.get(y).getText().toString().trim())){
		      						Utility.showToast(NavyFields.this,getResources().getString(R.string.dublicate_icid)+" "+(i+1)+" , "+(y+1));
				      				return;
		      					}
		      				}
		      			}else{
		      				Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_iccid));
		      				return;
		      			}
		      				
		      			}
					  if(edt_Name.getText().toString().equals(""))
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_name));
					  else if (edt_Sim.getText().toString().equals(""))
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_iccid));
					  else if(edt_ID.getText().toString().equals(""))
					   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_idnumber));	
					  else if(sp_Country.getSelectedItemPosition()==0)
						   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_country));	
					  else if(txtBirthDay.getText().toString().equals(""))
						   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_Birthday));
					  else if(txtExpiryDate.getText().toString().equals(""))
						   Utility.showToast(NavyFields.this,getResources().getString(R.string.enter_idexpiry));
					  else if (getDiffYears(iYear, iMonth, iDay)<C.AGE)
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  else if(IsExpired(txtExpiryDate.getText().toString()))
						  Utility.showToast(NavyFields.this,getResources().getString(R.string.id_expired));
					  else{
						  Utility.alert(getResources().getString(R.string.iccid_total)+ iICCID, NavyFields.this,   new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									 Passport_Navy_Entity entity= new Passport_Navy_Entity();
					                 if(rb_M.isChecked())
					              	   entity.setGender("M");
					                 else if(rb_F.isChecked())
					              	   entity.setGender("F");
					                 
					              if(rb_B.isChecked())
					              	   entity.setNationality("B");
					                 else  if(rb_N.isChecked())
					              	  entity.setNationality("N");
					                  entity.setName(edt_Name.getText().toString());
					                  String isdns = "";
					                  for( int i=0;i<alICCID.size();i++){
							      			EditText edt=alICCID.get(i);
							      			isdns+=edt.getText().toString().trim();
							      			if(i < alICCID.size() - 1)
							      				isdns += ";";
							      	 }
					                  entity.setMSISDN(isdns);
//					                  entity.setMSISDN(edt_Sim.getText().toString());
					                  entity.setId(edt_ID.getText().toString());
					                  if(sp_Country.getSelectedItemPosition()==0)
					                 	 entity.setCountry("");
					                 else
					                  entity.setCountry(alCountriesDataMain.get(sp_Country.getSelectedItemPosition()-1).getID());
					                  entity.setAddress(edt_Address.getText().toString());
					                  String strBirthDate="",strExpityDate=""; 
					                  if(!txtBirthDay.getText().toString().equals("")){
					                  	strBirthDate+=(iYear);
					                  if((iMonth)<=9)
					                  	strBirthDate+="0";
					                  strBirthDate+=(iMonth );
					                  if(iDay<=9)
					                  	strBirthDate+="0";
					                  strBirthDate+=(iDay);
					                  entity.setBirthDay(strBirthDate);}
					                  
					                  if(!txtExpiryDate.getText().toString().equals("")){
					                  	strExpityDate+=(eYear);
					                  if((eMonth)<=9)
					                  	strExpityDate+="0";
					                  strExpityDate+=(eMonth );
					                  if(eDay<=9)
					                  	strExpityDate+="0";
					                  strExpityDate+=(eDay);
					                  entity.setExpiryDate(strExpityDate);}
					                  
					                  Intent intent = new Intent(NavyFields.this, CPR_GCC_NavyFields_Form.class);
					                  intent.putExtra("entity", entity);
					                  intent.putExtra("document", "Navy");
					                  startActivityForResult(intent, TAKE_NAVY_REQUEST);
					              }});

					  }} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
							
						 // if(Utility.isConnected(NavyFields.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   	//else
			      	   //		Utility.showToast(NavyFields.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); 
  	  btn_Scan = (Button)findViewById(R.id.btnScan);
  	  btn_Scan.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		try {
	      			iEditTextPos=0;
	      			IntentIntegrator integrator = new IntentIntegrator(NavyFields.this);
	                integrator.initiateScan();
	                //startActivityForResult(integrator, 1);

	      		} catch (Exception ex) {
	      			WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  					
	      			//if(Utility.isConnected(NavyFields.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	      		}
	      	}
	      });
  	  new FetchCountriesData().execute();}
    
    
    
    
    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Navy/", "image" + new Date().getTime() + ".png");
        File parent = file.getParentFile();
		if (parent != null) parent.mkdirs();
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
    
    
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        Intent toStart = null;
        switch(requestCode) {
            case TAKE_CAMERA_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
            		new  ResizeImage().execute(new String[]{imgPath});}
            break;
            case TAKE_NAVY_REQUEST:
                if (resultCode == RESULT_OK) {
        			setResult(RESULT_OK, data);
      				finish();
        		}
           
                break;
            case 49374:
            	if (resultCode == Activity.RESULT_OK) {
	                   IntentResult intentResult = 
	                   IntentIntegrator.parseActivityResult(requestCode, resultCode,data);
	                if (intentResult != null) {
	                	alICCID.get(iEditTextPos).setText(intentResult.getContents());
	                }
//	                	edt_Sim.setText(intentResult.getContents());} 
	             }
            	break;
           default:
               break;
        } }
    



    private class FetchCountriesData extends AsyncTask<Void, Void, Void> {
		ArrayList<Countries_Entity> alColuntriesData = new ArrayList<Countries_Entity>();
		ArrayList<String> alColuntriesDataNames = new ArrayList<String>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getCountriesContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					Countries_Entity cursorEntity = new Countries_Entity();
					cursorEntity.setID(crsr.getString(0));
					cursorEntity.setNameEng(crsr.getString(1));
					cursorEntity.setNameAr(crsr.getString(2));
					alColuntriesData.add(cursorEntity);
					alColuntriesDataNames.add(crsr.getString(1));
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  					
					//if(Utility.isConnected(NavyFields.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			if (alColuntriesData != null && alColuntriesData.size() > 0) {
				alCountriesDataMain = alColuntriesData;
				alCountriesDataMainNames=alColuntriesDataNames;
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(NavyFields.this,
						android.R.layout.simple_spinner_item, alCountriesDataMainNames);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_Country.setAdapter(
					      new NothingSelectedSpinnerAdapter(
					    		dataAdapter,
					            R.layout.country_spinner_row_nothing_selected,
					            NavyFields.this));
			      
			} }
	}
	
	
    private class ResizeImage extends AsyncTask<String, Void, Bitmap> {
		ProgressDialog progress;
		@Override
		protected void onPreExecute() {
		progress = ProgressDialog.show(NavyFields.this, getString(R.string.pleaseWait), "", true, false, null);
			super.onPreExecute();}

		@Override
		protected Bitmap doInBackground(String... params) {
			try {
				
				 Bitmap bitmap=WebService.decodeFile(1024,1024,imgPath);
        		 new File(imgPath).delete();
        		 return bitmap;
        		 }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);
			 if (bitmap != null) { 
				 Passport_Navy_Entity entity= new Passport_Navy_Entity();
                 if(rb_M.isChecked())
              	   entity.setGender("M");
                 else if(rb_F.isChecked())
              	   entity.setGender("F");
                 
                /* if(rb_B.isChecked())
              	   entity.setNationality("B");
                 else  if(rb_N.isChecked())*/
              	   entity.setNationality("N");
                  entity.setName(edt_Name.getText().toString());
                  String isdns = "";
                  for( int i=0;i<alICCID.size();i++){
		      			EditText edt=alICCID.get(i);
		      			isdns+=edt.getText().toString().trim();
		      			if(i < alICCID.size() - 1)
		      				isdns += ";";
		      	 }
                  entity.setMSISDN(isdns);
//                  entity.setMSISDN(edt_Sim.getText().toString());
                  entity.setId(edt_ID.getText().toString());
                  if(sp_Country.getSelectedItemPosition()==0)
                    	 entity.setCountry("");
                    else
                     entity.setCountry(alCountriesDataMain.get(sp_Country.getSelectedItemPosition()-1).getID());
                  entity.setAddress(edt_Address.getText().toString());
                  String strBirthDate="",strExpityDate=""; 
                  if(!txtBirthDay.getText().toString().equals("")){
                  	strBirthDate+=(iYear);
                  if((iMonth)<=9)
                  	strBirthDate+="0";
                  strBirthDate+=(iMonth );
                  if(iDay<=9)
                  	strBirthDate+="0";
                  strBirthDate+=(iDay);
                  entity.setBirthDay(strBirthDate);}
                  
                  if(!txtExpiryDate.getText().toString().equals("")){
                  	strExpityDate+=(eYear);
                  if((eMonth)<=9)
                  	strExpityDate+="0";
                  strExpityDate+=(eMonth );
                  if(eDay<=9)
                  	strExpityDate+="0";
                  strExpityDate+=(eDay);
                  entity.setExpiryDate(strExpityDate);}
                  Intent intent = new Intent(NavyFields.this, Passport_Navy_CR.class);
                  intent.putExtra("Passport_Navy_CR", entity);
                  try {
                      //Write file
                      String filename = "bitmap.png";
                      FileOutputStream stream = NavyFields.this.openFileOutput(filename, Context.MODE_PRIVATE);
                      bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                      //Cleanup
                      stream.close();
                      bitmap.recycle();
                      intent.putExtra("image", filename);
                      intent.putExtra("document", "Navy");
                  } catch (Exception ex) {
                  	WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
    					
                  	//if(Utility.isConnected(NavyFields.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

                  }
                  //intent.putExtra("BitmapImage", bitmap);
                  startActivityForResult(intent, TAKE_NAVY_REQUEST);
              
                  
                  
               }

			progress.dismiss();
			 }
	}
	
    
    
	private Cursor getCountriesContentsCursor() {
		Cursor crsr = null;
		try {
			String[] from = { C.ID,C.NAME_ENG,C.NAME_AR};
			crsr = db.query(C.COUNTRIES_TABLE, from,null, null, null, null,C.NAME_ENG+" ASC");
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(NavyFields.this,Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				
			//if(Utility.isConnected(NavyFields.this))
			//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(NavyFields.this).getId(),NavyFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
	
	public static int getDiffYears(int _year, int _month, int _day) {
	    SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
	    java.util.Date d = null;
	    java.util.Date d1 = null;
	    Calendar cal = Calendar.getInstance();
	    try {
	            d = dfDate.parse(_day+"/"+_month+"/"+_year);
	            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
	        } catch (java.text.ParseException e) {
	            e.printStackTrace();
	        }
    Calendar a = getCalendar(d);
    Calendar b = getCalendar(d1);
    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
    if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
        diff--;
    }
    return diff;
}

public static Calendar getCalendar(Date date) {
    Calendar cal = Calendar.getInstance(Locale.US);
    cal.setTime(date);
    return cal;
}
 public int getAge (int _year, int _month, int _day) {
		 
		 SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
		    java.util.Date d = null;
		    java.util.Date d1 = null;
		    Calendar cal = Calendar.getInstance();
		    try {
		            d = dfDate.parse(_day+"/"+_month+"/"+_year);
		            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
		        } catch (java.text.ParseException e) {
		            e.printStackTrace();
		        }

		    int diffInDays = (int) ((d1.getTime() - d.getTime())/ (1000 * 60 * 60 * 24));

		    
	        return diffInDays;
	    }
	/* public int getAge (int _year, int _month, int _day) {

	        GregorianCalendar cal = new GregorianCalendar();
	        int y, m, d, a;         

	        y = cal.get(Calendar.YEAR);
	        m = cal.get(Calendar.MONTH);
	        d = cal.get(Calendar.DAY_OF_MONTH);
	        cal.set(_year, _month, _day);
	        a = y - cal.get(Calendar.YEAR);
	        if ((m < cal.get(Calendar.MONTH))
	                        || ((m == cal.get(Calendar.MONTH)) && (d < cal
	                                        .get(Calendar.DAY_OF_MONTH)))) {
	                --a;
	        }
	      
	        return a;
	    }*/

	 public boolean IsExpired(String valid_until){ 
		 boolean expired =false;
		 try{ 
			 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			 String strToday = sdf.format(new Date()); 
			 
			 Date strExpiryDate = sdf.parse(valid_until);
			 Date strDate = sdf.parse(strToday);
			 if (strExpiryDate.compareTo(strDate)<0) {
				 expired = true;
			 }
			 
		}catch(Exception ex){
			 ex.getMessage();
		 }
		return expired;}
    }
