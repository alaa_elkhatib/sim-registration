package com.simregistration.signed;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Info_Entity;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.app.Activity;
import android.app.ProgressDialog;

public class MyInfo extends Activity {
    private EditText edt_ShopName,edt_FullName,edt_Email,edt_CommMobile,edt_SMSMobile,edt_Balance,
    edt_MerchantID;
    private Info_Entity alInfoDataMain;
    private ProgressDialog progress = null;	
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myinfo);  
        alInfoDataMain = new Info_Entity();
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_ShopName = (EditText)findViewById(R.id.edtShopName);
        edt_FullName = (EditText)findViewById(R.id.edtFullName);
        edt_Email = (EditText)findViewById(R.id.edtEmail);
        edt_CommMobile = (EditText)findViewById(R.id.edtCommMobileNo);
        edt_SMSMobile = (EditText)findViewById(R.id.edtSMSMobileNo);
        edt_Balance = (EditText)findViewById(R.id.edtCurrentBalance);
        edt_MerchantID = (EditText)findViewById(R.id.edtMerchantID);
        
    	if(Utility.isConnected(MyInfo.this))
         	 new GetMyInfo().execute( new String[]{}); 
         else
 	   		Utility.showToast(MyInfo.this, getString(R.string.alert_need_internet_connection));}
    
   
    private class GetMyInfo extends AsyncTask<String, Void, Integer> {
    	Info_Entity alInfoData= new  Info_Entity();
		
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(MyInfo.this, getString(R.string.pleaseWait), "", true, true, null);
			}

		@Override
		protected Integer doInBackground(String ... params) {
			int iResult=-1;
			try {
				iResult =WebService.GetMyInfo(MyInfo.this,Utility.GetUserEntity(MyInfo.this).getId(), alInfoData);
				 }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(MyInfo.this,Utility.GetUserEntity(MyInfo.this).getId(),MyInfo.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
								return null;
			}
			return iResult;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v==-1)
	  				Utility.showToast(MyInfo.this, getString(R.string.error));
				else{
					if(!alInfoData.getError().equals(""))
						Utility.showToast(MyInfo.this, alInfoData.getError());
						
				else {
					    alInfoDataMain = alInfoData;
					    edt_ShopName.setText(alInfoDataMain.getShopName());
				        edt_FullName .setText(alInfoDataMain.getFullName());
				        edt_Email .setText(alInfoDataMain.getEmail());
				        edt_CommMobile .setText(alInfoDataMain.getComm());
				        edt_SMSMobile .setText(alInfoDataMain.getSMS());
				        edt_Balance .setText(alInfoDataMain.getBalance());
				        edt_MerchantID.setText(alInfoDataMain.getMerchant());
					}}
			
				}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(MyInfo.this,Utility.GetUserEntity(MyInfo.this).getId(),MyInfo.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}}}

    }
