package com.simregistration.signed;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;

public class MyPhoneStateListener extends PhoneStateListener{
    public int signalStrengthValue =0; 
	private Context context;
   
    public MyPhoneStateListener (Context c){
    	context=c;  }
    
     @Override
     public void onSignalStrengthsChanged(SignalStrength signalStrength){
        super.onSignalStrengthsChanged(signalStrength);
		  if (signalStrength.isGsm()) {
              if (signalStrength.getGsmSignalStrength() != 99)
                  signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
              else
                  signalStrengthValue = signalStrength.getGsmSignalStrength();
          } else {
              signalStrengthValue = signalStrength.getCdmaDbm();
          }
          
     }}

       