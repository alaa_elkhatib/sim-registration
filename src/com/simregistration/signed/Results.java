package com.simregistration.signed;

import android.app.ListActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Results extends ListActivity {

    private boolean[] accepts;
    private String[] results;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle params = getIntent().getExtras();

        String[] names = params.getStringArray("names");
        String[] values = params.getStringArray("values");
        String[] lines = params.getStringArray("Lines");

        accepts = params.getBooleanArray("accepts");
        results = new String[names.length + lines.length];

        int number = 0;

        for(; number < names.length; number++)
        {
            results[number] = names[number] + ": \t" + values[number];
        }


        for(int i=0; i< lines.length; i++)
            results[number++] = String.format("Line %d: \t", i+1) + lines[i];

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, results) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView text = (TextView) view.findViewById(android.R.id.text1);

                if(position < accepts.length && position > 0)
                {
                    if(accepts[position-1] == false)
                        text.setTextColor(Color.RED);
                    else
                        text.setTextColor(Color.BLACK);
                }
                else
                {
                    text.setTextColor(Color.BLACK);
                }

view.setBackgroundColor(Color.WHITE);
                return view;
            }

        };

        setListAdapter(adapter);

    }
}
