package com.simregistration.signed;
import com.simregistration.signed.R;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;;

public class MRZNationality extends Activity {
    private EditText edt_Code;
    private TextView txtCompaire;
    private Button btn_Compaire;
    private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	  @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nationalitycode);  
        Utility.initCountryCodeMapping();
        dbHelper = new DatabaseHelper(MRZNationality.this);
	    db = dbHelper.getWritableDatabase();
	    edt_Code = (EditText)findViewById(R.id.edtCode);
	    txtCompaire= (TextView)findViewById(R.id.txtCompaire);
	    btn_Compaire = (Button)findViewById(R.id.btnCompaire);
	    btn_Compaire.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {String CompairedCode=edt_Code.getText().toString().toUpperCase();
					if(CompairedCode.length()==3 && !CompairedCode.equalsIgnoreCase("BHR")){
		    			String strCode=Utility.iso3CountryCodeToIso2CountryCode(CompairedCode);
		    			txtCompaire.setText("Country Code :" + strCode +" /n Matched Code :"+getCountriesContentsCursor(strCode));
		    			}
		    		 
				} catch (Exception ex) {
						  }
			}}); 
  	  }
    
   
		
		private String getCountriesContentsCursor(String strCode) {
			String MatchedCountry="";
			Cursor crsr = null;
			try {
				String query = "SELECT * FROM "+C.COUNTRIES_TABLE +" WHERE trim("+ C.ID+") = '"+ strCode +"'";
				crsr=  db.rawQuery(query,null);
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					MatchedCountry="ID :"+(crsr.getString(0));
					MatchedCountry=MatchedCountry+" Name :"+(crsr.getString(1));
					crsr.moveToNext();
				} crsr.close();
				}
			} catch (Exception ex) {
				}
			return MatchedCountry;
		}}
	
