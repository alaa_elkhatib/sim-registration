
package com.simregistration.signed;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;



import com.simregistration.signed.R;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.User_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class CPR_GCC_NavyFields_Form_Preview extends Activity  {
	private ImageView mMainImageViewer;
    private  File mMainLocation;/*mSigneture1Location*///;
    private ProgressDialog progress = null;
    private String DocumentType="";
    private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.passport_navy_cr_preview); 
        DocumentType = getIntent().getStringExtra("document"); 
        dbHelper = new DatabaseHelper(this);
		db = dbHelper.getWritableDatabase();
		updater = new ContentUpdater(this, db);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mMainLocation = new File(Environment.getExternalStorageDirectory(),DocumentType+"/Main.jpg");
        mMainImageViewer = (ImageView) findViewById(R.id.iv_main); 
        if(mMainLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mMainLocation.getAbsolutePath());
            mMainImageViewer.setImageBitmap(myBitmap); }
        
      
        }
    
  
    public void onCancel(View v){
    	Intent intent = new Intent(CPR_GCC_NavyFields_Form_Preview.this, CPR_GCC_NavyFields_Form.class);
		intent.putExtra("entity",  getIntent().getParcelableExtra("entity"));
		intent.putExtra("document", getIntent().getStringExtra("document"));
		startActivity(intent);
		finish();
    }
    
    
    public void onSend(View v){
    	 if(Utility.isConnected(CPR_GCC_NavyFields_Form_Preview.this))
    		 new UploadCPRImages().execute(new String[]{});
    	 else
	   		Utility.showToast(CPR_GCC_NavyFields_Form_Preview.this, getString(R.string.alert_need_internet_connection)); 
    }
    
    
    
    private class UploadCPRImages extends AsyncTask<String, Void, String> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(CPR_GCC_NavyFields_Form_Preview.this, getString(R.string.pleaseWait), "", true, false, null);

		}

		@Override
		protected String doInBackground(String... params) {
			String strResult="";
			try {
				
				
				  String strFolderName =  Utility.GetUserEntity(CPR_GCC_NavyFields_Form_Preview.this).getId()+"_"+System.currentTimeMillis()+"_"+DocumentType;	 
				  String strImage=WebService.GetImageFromFile(CPR_GCC_NavyFields_Form_Preview.this,mMainLocation.getPath());
				  if(strImage.equalsIgnoreCase(""))
						return "Error";
				  else{
				  strResult="";
				  strResult= WebService.UploadImages(CPR_GCC_NavyFields_Form_Preview.this,Utility.GetUserEntity(CPR_GCC_NavyFields_Form_Preview.this).getId(), DocumentType, strFolderName, "Main.png", strImage);
				  if(strResult.equalsIgnoreCase(CPR_GCC_NavyFields_Form_Preview.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(CPR_GCC_NavyFields_Form_Preview.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(CPR_GCC_NavyFields_Form_Preview.this.getString(R.string.no_account))|| !strResult.equalsIgnoreCase("Success"))
						return strResult;
				
				  else{
					  strResult="";
					  Passport_Navy_Entity entity = getIntent().getParcelableExtra("entity"); 
					  strResult=WebService.InsertSIMTransaction(CPR_GCC_NavyFields_Form_Preview.this,Utility.GetUserEntity(CPR_GCC_NavyFields_Form_Preview.this).getId(), entity.getMSISDN(), entity.getName(),
								 entity.getGender(), entity.getNationality(), DocumentType, entity.getId(), entity.getCountry(), entity.getAddress(), entity.getBirthDay(), entity.getExpiryDate(), strFolderName, strFolderName+"/Main.png");
						 
				
				  }}}catch (Exception ex) {
					  WebService.SaveErrorLogNoBandwidth(CPR_GCC_NavyFields_Form_Preview.this,Utility.GetUserEntity(CPR_GCC_NavyFields_Form_Preview.this).getId(),CPR_GCC_NavyFields_Form_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
						
					  //if(Utility.isConnected(Passport_Navy_CR_Preview.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Navy_CR_Preview.this).getId(),Passport_Navy_CR_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   	//else
		      	   	//	Utility.showToast(Passport_Navy_CR_Preview.this, getString(R.string.alert_need_internet_connection)); 

				return "Error";}
			return strResult;
		}

		@Override
		protected void onPostExecute(String v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v.equalsIgnoreCase("Success"))
					Utility.alert("Your transaction has been sent successfully, waiting for verification and activation", CPR_GCC_NavyFields_Form_Preview.this, onclick);
				
				else if(v.equalsIgnoreCase("Error"))
					Utility.alert(getString(R.string.error),CPR_GCC_NavyFields_Form_Preview.this,null);
				
				else if(v.equalsIgnoreCase(CPR_GCC_NavyFields_Form_Preview.this.getString(R.string.disabled)) ||  v.equalsIgnoreCase(CPR_GCC_NavyFields_Form_Preview.this.getString(R.string.locked))||  v.equalsIgnoreCase(CPR_GCC_NavyFields_Form_Preview.this.getString(R.string.no_account)))
					Utility.alert(v, CPR_GCC_NavyFields_Form_Preview.this, onclick3);
				
				else 
					Utility.alert(v, CPR_GCC_NavyFields_Form_Preview.this, null);	
				
			}catch(Exception ex){
				WebService.SaveErrorLogNoBandwidth(CPR_GCC_NavyFields_Form_Preview.this,Utility.GetUserEntity(CPR_GCC_NavyFields_Form_Preview.this).getId(),CPR_GCC_NavyFields_Form_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
				
				  //if(Utility.isConnected(Passport_Navy_CR_Preview.this))
				//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Navy_CR_Preview.this).getId(),Passport_Navy_CR_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	      	   	//else
	      	   	//	Utility.showToast(Passport_Navy_CR_Preview.this, getString(R.string.alert_need_internet_connection)); 
	        
			}}}
    
    /*
    ----------------------------- LIFE CYCLE METHODS -----------------------------
    */
 
    
   
    
    @Override
    protected void onStop(){
    	//Log.w(TAG, "onStop");       
        super.onStop(); 
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    }
    
    @Override
    protected void onStart(){
    	//Log.w(TAG, "onStart");
    	super.onStart();
    }
    
    @Override
    protected void onRestart(){
    	//Log.w(TAG, "onRestart");    	
    	super.onRestart();
    }
    
   
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			Intent intent = new Intent(CPR_GCC_NavyFields_Form_Preview.this, Sim_Registration.class);
			intent.putExtra("Passport_Navy_CR",  getIntent().getParcelableExtra("Passport_Navy_CR"));
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}};
    
    
		
		  DialogInterface.OnClickListener onclick2 = new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(CPR_GCC_NavyFields_Form_Preview.this, Passport_Navy_CR.class);
						intent.putExtra("Passport_Navy_CR",  getIntent().getParcelableExtra("Passport_Navy_CR"));
						intent.putExtra("image", getIntent().getStringExtra("image"));
						intent.putExtra("document", getIntent().getStringExtra("document"));
						startActivity(intent);
						finish();
					}};
					
					 DialogInterface.OnClickListener onclick3 = new DialogInterface.OnClickListener() {
		                  @Override
								public void onClick(DialogInterface dialog, int which) {
		                	    updater.deleteOldContentsUser();
		      			        Utility.ClearShared(CPR_GCC_NavyFields_Form_Preview.this);
								Intent intent = new Intent(CPR_GCC_NavyFields_Form_Preview.this, Login.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);}};
			    
			    
			    

					@Override
					public void onBackPressed() {
						Intent intent = new Intent(CPR_GCC_NavyFields_Form_Preview.this, Passport_Navy_CR.class);
						intent.putExtra("Passport_Navy_CR",  getIntent().getParcelableExtra("Passport_Navy_CR"));
						intent.putExtra("image", getIntent().getStringExtra("image"));
						intent.putExtra("document", getIntent().getStringExtra("document"));
						startActivity(intent);
						finish();}

    
    

  

}
