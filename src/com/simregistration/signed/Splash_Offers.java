package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.utility.Utility;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.app.Activity;
import android.content.Intent;

public class Splash_Offers extends Activity {
	WebView webView = null;
	View screener;
	int iSplashTime = 5000;
	Handler exitHandler = null;
	Runnable exitRunnable = null; 
	String url="http://83.136.56.172/SIM/FlashPosterMRS.png";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.splash_offers);  
         screener = this.findViewById(R.id.screener_pd);
         screener.setVisibility(View.VISIBLE);
         CookieSyncManager.createInstance(this);         
         CookieManager cookieManager = CookieManager.getInstance();        
         cookieManager.removeAllCookie();
         deleteDatabase("webview.db");
         deleteDatabase("webviewCache.db");
         webView = (WebView) this.findViewById(R.id.wv_internal);
         webView.clearCache(true);
		 webView.getSettings().setJavaScriptEnabled(true);
		 webView.getSettings().setDefaultTextEncodingName("utf-8");
		 webView.getSettings().setAllowFileAccess(true);
		 webView.getSettings().setUseWideViewPort(true);
		 webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); 
		 webView.getSettings().setAppCacheEnabled(false); 
		 webView.clearHistory(); 
		 webView.clearCache(true); 
		 webView.clearFormData(); 
		 webView.setInitialScale(100);
		 webView.setHorizontalScrollBarEnabled(false); 
		 webView.setVerticalScrollBarEnabled(false); 
		 exitHandler = new Handler();
	    exitRunnable = new Runnable() {
	    		public void run(){
	    		exitSplash();
	    		}}; 
		
	    		http://www.freepik.com/free-vector/wedding-card-with-roses_794737.htm#term=roses&page=3&position=0
		 webView.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					return false;
				}

				@Override
				public void onPageFinished(WebView view, String url) {
					super.onPageFinished(view, url);
					screener.setVisibility(View.GONE);
					exitHandler.postDelayed(exitRunnable, iSplashTime);
				}
			});
			
			String strHtmlCode= "<html><body><img src=\"" + url + "\" width=\"100%\" height=\"100%\"\"/></body></html>";
			if(Utility.isConnected(Splash_Offers.this))
				webView.loadData(strHtmlCode, "text/html", null);
			
			else{
				    webView.loadUrl("file:///android_asset/splash.png");
  			        exitHandler.postDelayed(exitRunnable, iSplashTime);}
    
    }
    
    private void exitSplash(){
    	finish();
    	Intent intent = getIntent();
		startActivity(new Intent(this,Login.class).putExtra("redirect", intent.getStringExtra("redirect")));}
    	
   
	
    }
