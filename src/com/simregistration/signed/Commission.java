package com.simregistration.signed;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.SIMCommission_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

public class Commission extends Activity {
    private EditText edt_TransactionID,edt_Sim,edt_ID;
    private TextView txtFromDate,txtToDate;
    private int iFYear, iFMonth, iFDay,iTYear, iTMonth, iTDay;
    private Spinner sp_IDType;
    private Button btn_Submit;
    private ProgressDialog progress = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commission);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_TransactionID = (EditText)findViewById(R.id.edtTransactionID);
        edt_Sim = (EditText)findViewById(R.id.edtSim);
        edt_ID = (EditText)findViewById(R.id.edtID);
        sp_IDType= (Spinner)findViewById(R.id.idtype_spinner);
        sp_IDType.setAdapter(ArrayAdapter.createFromResource(this, R.array.type, android.R.layout.simple_spinner_dropdown_item));
        txtFromDate= (TextView)findViewById(R.id.txtFromDate);
        txtFromDate.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  				// Initialize calendar and date picker
  					final Calendar c = Calendar.getInstance();
  			        iFYear = c.get(Calendar.YEAR) ;
  			        iFMonth = c.get(Calendar.MONTH);
  			        iFDay = c.get(Calendar.DAY_OF_MONTH);
  			        
  					DatePickerDialog dg = new DatePickerDialog(Commission.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          iFYear = year;
  				          iFMonth = monthOfYear+1;
  				          iFDay = dayOfMonth;
  				          
  				          txtFromDate.setText( Integer.toString(iFDay)+ "/" + Integer.toString(iFMonth) + "/" +
  				        		 Integer.toString(iFYear) );
  				      }

  					}, iFYear, iFMonth, iFDay);
  					
  					dg.show();	// Show picker dialog
  					} catch (Exception ex) {
  					  WebService.SaveErrorLogNoBandwidth(Commission.this,Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
  					
  						//if(Utility.isConnected(Commission.this))
  		 				///	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
  		 	   	   	
  					}}});
        
        
        txtToDate= (TextView)findViewById(R.id.txtToDate);
        txtToDate.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  				// Initialize calendar and date picker
  					final Calendar c = Calendar.getInstance();
  			        iTYear = c.get(Calendar.YEAR) ;
  			        iTMonth = c.get(Calendar.MONTH);
  			        iTDay = c.get(Calendar.DAY_OF_MONTH);
  			        
  					DatePickerDialog dg = new DatePickerDialog(Commission.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          iTYear = year;
  				          iTMonth = monthOfYear+1;
  				          iTDay = dayOfMonth;
  				          
  				          txtToDate.setText( Integer.toString(iTDay)+ "/" + Integer.toString(iTMonth) + "/" +
  				        		 Integer.toString(iTYear) );
  				      }

  					}, iTYear, iTMonth, iTDay);
  					
  					dg.show();	// Show picker dialog
  					} catch (Exception ex) {
  						 WebService.SaveErrorLogNoBandwidth(Commission.this,Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   	
  						//if(Utility.isConnected(Commission.this))
 		 				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
 		 	   	   	
  					}}});
  	  
        btn_Submit= (Button)findViewById(R.id.btnSubmit);
        btn_Submit.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  					  SIMTransaction_Entity entity= new SIMTransaction_Entity();
  					  if(edt_TransactionID.getText().toString().equals(""))
  						 entity.setTransactionID("-1");
  					  else
                      entity.setTransactionID(edt_TransactionID.getText().toString());
                      entity.setAccountID(Utility.GetUserEntity(Commission.this).getId());
                      
                      entity.setIDNumber(edt_ID.getText().toString());
                      entity.setSim(edt_Sim.getText().toString());
                      
                      if(sp_IDType.getSelectedItem().toString().equals("All"))
                      entity.setType("");
                      else   entity.setType(sp_IDType.getSelectedItem().toString());
                      
                         String strFromDate="",strToDate=""; 
                         if(!txtFromDate.getText().toString().equals("")){
                         strFromDate+=(iFYear);
                         if((iFMonth)<=9)
                        	 strFromDate+="0";
                         strFromDate+=(iFMonth );
                         if(iFDay<=9)
                        	 strFromDate+="0";
                         strFromDate+=(iFDay); }
                         entity.setFromDate(strFromDate);
                         
                         if(!txtToDate.getText().toString().equals("")){
                             strToDate+=(iTYear);
                             if((iTMonth)<=9)
                            	 strToDate+="0";
                             strFromDate+=(iTMonth );
                             if(iTDay<=9)
                            	 strToDate+="0";
                             strToDate+=(iTDay);}
                         entity.setToDate(strToDate);
                         if(Utility.isConnected(Commission.this))
                        	  new GetSimCommission().execute(new SIMTransaction_Entity[]{entity});
                    	 else
                	   		Utility.showToast(Commission.this, getString(R.string.alert_need_internet_connection)); 
                        } catch (Exception ex) {
                        	WebService.SaveErrorLogNoBandwidth(Commission.this,Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
                        	//if(Utility.isConnected(Commission.this))
     		 				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
     		 	   	   	
                        }}}); }
    
    
    private class GetSimCommission extends AsyncTask<SIMTransaction_Entity, Void, ArrayList<SIMCommission_Entity>> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Commission.this, getString(R.string.pleaseWait), "", true, true, null);
			

		}

		@Override
		protected ArrayList<SIMCommission_Entity> doInBackground( SIMTransaction_Entity ... params) {
			ArrayList<SIMCommission_Entity> list= new  ArrayList<SIMCommission_Entity>();
		    	
			try {
				WebService.GetSimCommision(Commission.this,params[0].getTransactionID(), params[0].getAccountID(),  params[0].getType(),
						 params[0].getIDNumber(),  params[0].getSim(),  params[0].getFromDate(),  params[0].getToDate(), list);
				 }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(Commission.this,Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
					// if(Utility.isConnected(Commission.this))
		 			//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		 	   	   	
			}
			return list;
		}

		@Override
		protected void onPostExecute(ArrayList<SIMCommission_Entity> v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v.size()>0){
					SIMCommission_Entity entity= v.get(0);
					if(entity.getError()!=null)
						Utility.showToast(Commission.this, entity.getError());
				else{
					 Intent intent = new Intent(Commission.this, Commission_List.class);
					 intent.putExtra("Commission",  v);
					 startActivity(intent);
				}}
			
				
				}catch(Exception ex){
					 WebService.SaveErrorLogNoBandwidth(Commission.this,Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
					 //if(Utility.isConnected(Commission.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission.this).getId(),Commission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	      	   	//else
	      	   	//	Utility.showToast(Commission.this, getString(R.string.alert_need_internet_connection)); 
	        
				}}}
    
    }
