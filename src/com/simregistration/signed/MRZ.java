package com.simregistration.signed;
import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

public class MRZ extends Activity   {
	private Button btn_Logout, btn_CPR,btn_Passport,btn_GCC;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mrz);
        
        btn_Logout = (Button)this.findViewById(R.id.btn_logout);
        btn_Logout.setVisibility(View.GONE);
       
        btn_CPR = (Button)this.findViewById(R.id.btn_cpr);
        btn_CPR.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=null;
				String mode= getIntent().getStringExtra("mode");
				if(mode.equals("scan"))
				intent = new Intent(MRZ.this,CameraView.class);
				else 
				intent = new Intent(MRZ.this,MRZCPR.class);
				intent.putExtra("type",1);
				intent.putExtra("mode",mode);
		    	startActivity(intent);
			
			}});
        
       
        
        btn_Passport = (Button)this.findViewById(R.id.btn_passport);
        btn_Passport.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=null;
				String mode= getIntent().getStringExtra("mode");
				if(mode.equals("scan"))
				intent = new Intent(MRZ.this,CameraView.class);
				else 
				intent = new Intent(MRZ.this,MRZPassport.class);
				intent.putExtra("type",2);
				intent.putExtra("mode",mode);
		    	startActivity(intent);
			}});
        
       
        btn_GCC = (Button)this.findViewById(R.id.btn_gcc);
        btn_GCC.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent ;
				String mode= getIntent().getStringExtra("mode");
				if(mode.equals("scan"))
					intent = new Intent(MRZ.this,CameraView.class);
				else 
					intent = new Intent(MRZ.this,MRZGcc.class);
				intent.putExtra("type", 3);
				intent.putExtra("mode",mode);
		    	startActivity(intent);
			}});
     
        
      
        	
       
   }
    


	
   
    
  }
