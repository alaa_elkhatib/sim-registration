package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.History_List.ViewHolder;
import com.simregistration.signed.entity.BundleSalesResponse_Entity;
import com.simregistration.signed.entity.Bundle_Entity;
import com.simregistration.signed.entity.Bundles_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;

public class Bundles extends Activity implements CustomAdapter.ListFiller {
    private EditText edt_Mobile;
    private Button btn_GetBundels,btn_Submit;
    private ArrayList<Bundles_Entity> alBundlesDataMain;
    private ProgressDialog progress = null;
    ListView lvList;
	CustomAdapter adapter;
	int selected_position = -1;
   
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bundles);  
        alBundlesDataMain = new ArrayList<Bundles_Entity>();
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_Mobile= (EditText)findViewById(R.id.edtMobile);
        btn_GetBundels = (Button)this.findViewById(R.id.btnGetBundels);
        btn_GetBundels.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  if(edt_Mobile.getText().toString().equals(""))
						  Utility.showToast(Bundles.this,getResources().getString(R.string.enter_mobileno));
					  else{
						  if(Utility.isConnected(Bundles.this))
					 	  new GetBundles().execute(new String[]{Utility.GetUserEntity(Bundles.this).getId(),edt_Mobile.getText().toString()});
	                     else
	                	   		Utility.showToast(Bundles.this, getString(R.string.alert_need_internet_connection)); 
	                      
					  }} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						}
			}}); 
        btn_Submit = (Button)this.findViewById(R.id.btnSubmit);
        btn_Submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					if(selected_position!=-1){
					AlertDialog.Builder builder = new AlertDialog.Builder(Bundles.this);
					builder.setMessage("Are you sure you want to continue?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						
								if (alBundlesDataMain != null && alBundlesDataMain.get(selected_position) != null ) {
				         			 Bundles_Entity entity = (Bundles_Entity) alBundlesDataMain.get(selected_position);
				         			
				         			if(Utility.isConnected(Bundles.this))
										  new InsertCustomerBundle().execute(new String[]{entity.getID(),entity.getMobile()});
				                	   	else
				                	   		Utility.showToast(Bundles.this, getString(R.string.alert_need_internet_connection)); 
				         		}
						}
					}).setNegativeButton("No", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
					AlertDialog alert = builder.create();
					alert.show(); }} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						}
			}}); 
    	lvList = (ListView)findViewById(R.id.lvList);
        adapter = new CustomAdapter(this,alBundlesDataMain, R.layout.bundle_item, this);
		lvList.setAdapter(adapter);
		lvList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
					// return WebService.InsertBundleSales(Bundles.this, Utility.GetUserEntity(Bundles.this).getId(), strSIMMSISDN, strPkgCode, strPackageID, strBundelID, strType, strCode, strCommand, strAction, entity)(ChangeComission.this,params[0], params[1], params[2]);
	         	  		
	         		if (alBundlesDataMain != null && alBundlesDataMain.get(pos) != null ) {
	         			 Bundles_Entity entity = (Bundles_Entity) alBundlesDataMain.get(pos);
	         			
	         			if(Utility.isConnected(Bundles.this))
							  new InsertBundleSales().execute(new String[]{entity.getMobile(),entity.getPackageCode(),entity.getPackageID(),entity.getID(),entity.getType(),entity.getCode(),entity.getCommand()
									  ,entity.getAction()});
	                	   	else
	                	   		Utility.showToast(Bundles.this, getString(R.string.alert_need_internet_connection)); 
	         		}}}); 
    
    
 

  	}
    
    private class GetBundles extends AsyncTask<String, Void, ArrayList<Bundle_Entity>> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Bundles.this, getString(R.string.pleaseWait), "", true, true, null);
			

		}

		@Override
		protected ArrayList<Bundle_Entity> doInBackground(String ... params) {
			ArrayList<Bundle_Entity> list= new  ArrayList<Bundle_Entity>();
		    	
			try {
				WebService.GetBundlesSales(Bundles.this, params[0], params[1], list);
				 }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
								return null;
			}
			return list;
		}

		@Override
		protected void onPostExecute(ArrayList<Bundle_Entity> v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v.size()>0){
					Bundle_Entity entity= v.get(0);
						if(!entity.getError().equals(""))
						Utility.showToast(Bundles.this, entity.getError());
						
						alBundlesDataMain=entity.getBundles();
						adapter.notifyDataSetChanged();
				}
			
				
				}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						
					 // if(Utility.isConnected(History.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(History.this).getId(),History.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   	//else
		      	   	//	Utility.showToast(History.this, getString(R.string.alert_need_internet_connection)); 
		        
				}}}

    
    private class InsertBundleSales extends AsyncTask<String, Void, Integer> {
    	private BundleSalesResponse_Entity entity = new BundleSalesResponse_Entity();
		   
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(Bundles.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected Integer doInBackground(String... params) {
  			try {
  				 return WebService.InsertBundleSales(Bundles.this, Utility.GetUserEntity(Bundles.this).getId(), params[0], params[1],
  						params[2], params[3], params[4], params[5], params[6], params[7], params[8], entity);
  			}catch (Exception ex) {
  			  WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());      
  			  return -1;
  			
  			}
  		}

  		@Override
  		protected void onPostExecute(Integer v) {
  			super.onPostExecute(v);
  			progress.dismiss();	
  			if(v==-1)
  				Utility.showToast(Bundles.this, getString(R.string.error));
  			else {
			if(!entity.getError().equals("")){
				if(entity.getError().equalsIgnoreCase("Invalid Code"))
					Utility.alert("Invalid Code",Bundles.this, Invalidonclick);
				else
						Utility.showToast(Bundles.this, entity.getError());}
			else 
  			Utility.alert(entity.getMsg(),Bundles.this, onclick);
  			}
  			
  		}}
    
    
    private class InsertCustomerBundle extends AsyncTask<String, Void, Integer> {
    	   
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(Bundles.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected Integer doInBackground(String... params) {
  			try {
  				 return WebService.InsertCustomer_CodeBundle(Bundles.this, Utility.GetUserEntity(Bundles.this).getId(), params[0], params[1]);
  			}catch (Exception ex) {
  			  WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());      
  			  return -1;
  			
  			}
  		}

  		@Override
  		protected void onPostExecute(Integer v) {
  			super.onPostExecute(v);
  			progress.dismiss();  			
  			if(v==-1)
  				Utility.showToast(Bundles.this, getString(R.string.error));
  			else if(v>0){
  				try {
  					LayoutInflater li = LayoutInflater.from(Bundles.this);
  					View promptsView = li.inflate(R.layout.activationcode, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(Bundles.this);
					builder.setView(promptsView);
					final EditText code = (EditText) promptsView
							.findViewById(R.id.edtActivationCode);
					builder.setMessage("Please Enter the activation code").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							if(!code.getText().toString().equals("")){
								if (alBundlesDataMain != null && alBundlesDataMain.get(selected_position) != null ) {
				         			 Bundles_Entity entity = (Bundles_Entity) alBundlesDataMain.get(selected_position);
				         			
				         			if(Utility.isConnected(Bundles.this))
										  new InsertBundleSales().execute(new String[]{entity.getMobile(),entity.getPackageCode(),entity.getPackageID(),entity.getID(),entity.getType(),entity.getCode(),entity.getCommand()
												  ,entity.getAction(),code.getText().toString()});
				                	   	else
				                	   		Utility.showToast(Bundles.this, getString(R.string.alert_need_internet_connection)); 
				         		}
						}}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
					AlertDialog alert = builder.create();
					alert.show(); } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						}}
		}}
      
      DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

   		@Override
   		public void onClick(DialogInterface dialog, int which) {
   			finish();
   		}};
   		
   	  DialogInterface.OnClickListener Invalidonclick = new DialogInterface.OnClickListener() {

     		@Override
     		public void onClick(DialogInterface dialog, int which) {
     			InterActivaionCode();
     		}};
   		
   		
	@Override
	public void fillListData(View v, int pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isEnabled(int pos) {
		return true;
	}

	@Override
	public boolean useExtGetView() {
		return true;
	}

	@Override
	public View getView(final int pos, View contextView) {
try{
			
			if (alBundlesDataMain== null || alBundlesDataMain.size() == 0)
				return  contextView;
			
				 LayoutInflater inf = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
				 contextView = inf.inflate(R.layout.bundle_item, null);
				 final ViewHolder holder = new ViewHolder();
				 holder.tv_name = (TextView) contextView.findViewById(R.id.tvBundleName);
				 holder.tv_price = (TextView) contextView.findViewById(R.id.tvPrice);
					
				 holder.cb_select = (CheckBox) contextView.findViewById(R.id.cbSelect);
				 final Bundles_Entity entity = (Bundles_Entity) alBundlesDataMain.get(pos);
			    
			      
			    if (entity != null && entity.getName() != null  && entity.getValidity() != null)
			    	 holder.tv_name.setText(entity.getName()+System.getProperty("line.separator") +entity.getValidity());
			    
			    if (entity != null && entity.getPrice() != null)
			    	 holder.tv_price.setText(entity.getPrice());
			    
			    if(selected_position==pos)
			    {
			    	holder.cb_select.setChecked(true);
			    }
			    else
			    {
			    	holder.cb_select.setChecked(false);
			    }
			
			    holder.cb_select.setOnClickListener(new View.OnClickListener() {
			        @Override
			        public void onClick(View view) {
			            if (((CheckBox) view).isChecked())
			            {
			                selected_position= pos;
			            }
			            else
			            {
			                selected_position=-1;
			            }
			            adapter.notifyDataSetChanged();
			        }
			    });
			    
				
				}catch (Exception ex){
					WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
					  
		
		}
return contextView;
	}

	
	class ViewHolder {
		private TextView tv_name,tv_price;
		private CheckBox cb_select;
	}
	@Override
	public String getFilter() {
		return "";
	}

	@Override
	public ArrayList<Bundles_Entity> getFilteredList() {
		// TODO Auto-generated method stub
		return alBundlesDataMain;
	}
    
  public void InterActivaionCode(){
	  try {
			LayoutInflater li = LayoutInflater.from(Bundles.this);
			View promptsView = li.inflate(R.layout.activationcode, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(Bundles.this);
			builder.setView(promptsView);
			final EditText code = (EditText) promptsView
					.findViewById(R.id.edtActivationCode);
			builder.setMessage("Please Enter the activation code").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					if(!code.getText().toString().equals("")){
						if (alBundlesDataMain != null && alBundlesDataMain.get(selected_position) != null ) {
		         			 Bundles_Entity entity = (Bundles_Entity) alBundlesDataMain.get(selected_position);
		         			
		         			if(Utility.isConnected(Bundles.this))
								  new InsertBundleSales().execute(new String[]{entity.getMobile(),entity.getPackageCode(),entity.getPackageID(),entity.getID(),entity.getType(),entity.getCode(),entity.getCommand()
										  ,entity.getAction(),code.getText().toString()});
		                	   	else
		                	   		Utility.showToast(Bundles.this, getString(R.string.alert_need_internet_connection)); 
		         		}
				}}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.create();
			alert.show(); } catch (Exception ex) {
				  WebService.SaveErrorLogNoBandwidth(Bundles.this,Utility.GetUserEntity(Bundles.this).getId(),Bundles.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}
  }

    }
