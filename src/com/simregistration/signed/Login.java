package com.simregistration.signed;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.User_Entity;
import com.simregistration.signed.utility.AsyncTask;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;

public class Login extends Activity {
	private EditText edtUserName,edtPassword;
	private ProgressDialog progress = null;
	private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private Button btn_Login,btn_Register,btn_ForgotPassword;
	private TextView tv_Version;
	private CheckBox cb_Remember;
	private int Logout=0;
	String app_pkg_name = "com.simregistration";
	int UNINSTALL_REQUEST_CODE = 1;
	public static MyPhoneStateListener myListener ;
	private boolean msgShown=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		dbHelper = new DatabaseHelper(Login.this);
		db = dbHelper.getWritableDatabase();
		updater = new ContentUpdater(Login.this, db);
		tv_Version = (TextView)this.findViewById(R.id.tv_version);
		tv_Version.setText("Version Name: "+GetVersion());
		cb_Remember = (CheckBox)this.findViewById(R.id.cb_remember);
		edtUserName = (EditText)this.findViewById(R.id.edtUserName);
        edtUserName.setOnEditorActionListener(new OnEditorActionListener() {
        @Override
			public boolean onEditorAction(TextView arg0, int actionId, KeyEvent arg2) {
				if(actionId==EditorInfo.IME_ACTION_GO){
					if(!edtUserName.getText().toString().equals("") && !edtPassword.getText().toString().equals("")){
                	 if(Utility.isConnected(Login.this))
                			 new LoginUser().execute(new String[]{edtUserName.getText().toString(),edtPassword.getText().toString(),Utility.getDeviceId(Login.this),Utility.getDeviceNumber(Login.this)});
                	   	else
                	   		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
                }  
                }
				return false;
			}
        });

        btn_Login = (Button)this.findViewById(R.id.btn_login);
        btn_Login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				 if(!edtUserName.getText().toString().equals("") && !edtPassword.getText().toString().equals("")){
                	 if(Utility.isConnected(Login.this))
                			 new LoginUser().execute(new String[]{edtUserName.getText().toString(),edtPassword.getText().toString(),Utility.getDeviceId(Login.this),Utility.getDeviceNumber(Login.this)});
                	   	else
                	   		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
               
				 }}});
        
        btn_Register = (Button)this.findViewById(R.id.btn_register);
        btn_Register.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Login.this,Register.class);
		    	startActivity(intent);}});
        
        btn_ForgotPassword = (Button)this.findViewById(R.id.btn_forgotpassword);
        btn_ForgotPassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Login.this,ForgotPassword.class);
		    	startActivity(intent);}});
        
    	edtPassword = (EditText)this.findViewById(R.id.edtPassword);
		edtPassword.setOnEditorActionListener(new OnEditorActionListener() {
	        @Override
				public boolean onEditorAction(TextView arg0, int actionId, KeyEvent arg2) {
					if(actionId==EditorInfo.IME_ACTION_GO){
	                 if(!edtUserName.getText().toString().equals("") && !edtPassword.getText().toString().equals("")){
	                	 if(Utility.isConnected(Login.this))
                			 new LoginUser().execute(new String[]{edtUserName.getText().toString(),edtPassword.getText().toString(),Utility.getDeviceId(Login.this),Utility.getDeviceNumber(Login.this)});
                	   	else
                	   		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
	                 }  
	                }
					return false;
				}
	        });
		
		 
    	edtPassword = (EditText)this.findViewById(R.id.edtPassword);
		edtPassword.setOnEditorActionListener(new OnEditorActionListener() {
	        @Override
				public boolean onEditorAction(TextView arg0, int actionId, KeyEvent arg2) {
					if(actionId==EditorInfo.IME_ACTION_GO){
	                 if(!edtUserName.getText().toString().equals("") && !edtPassword.getText().toString().equals("")){
	                	 if(Utility.isConnected(Login.this))
                			 new LoginUser().execute(new String[]{edtUserName.getText().toString(),edtPassword.getText().toString(),Utility.getDeviceId(Login.this),Utility.getDeviceNumber(Login.this)});
                	   	else
                	   		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
	                 }  
	                }
					return false;
				}
	        });
		
		Logout= getIntent().getIntExtra("logout", 0);
		
		//new FetchUserData().execute(new Void[]{null});
		if( isPackageExisted(app_pkg_name)){
		Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);  
		intent.setData(Uri.parse("package:" + app_pkg_name));  
		intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
		startActivityForResult(intent, UNINSTALL_REQUEST_CODE);}
		///new GetVersionCode().execute(new Void[]{null});
         
	}

	@Override
	public void onResume() {
	    super.onResume();  // Always call the superclass method first
	    if(!msgShown)
	    new GetVersionCode().execute(new Void[]{null});
	}
	
	private class GetVersionCode extends AsyncTask<Void, String, String> {
	    @Override
	    protected String doInBackground(Void... voids) {

	        String newVersion = null;
	        try {
	            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + Login.this.getPackageName() + "&hl=it")
	                    .timeout(30000)
	                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
	                    .referrer("http://www.google.com")
	                    .get()
	                    .select("div[itemprop=softwareVersion]")
	                    .first()
	                    .ownText();
	            return newVersion;
	        } catch (Exception e) {
	            return newVersion;
	        }
	    }

	    @Override
	    protected void onPostExecute(final String onlineVersion) {
	        super.onPostExecute(onlineVersion);
	        if (onlineVersion != null && !onlineVersion.isEmpty()) {
	            if (GetVersion().compareTo(onlineVersion)<0) {
	            	 msgShown=true;
	            	  Utility.alertnoBack(getResources().getString(R.string.latest), Login.this,   new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								try {
								    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + Login.this.getPackageName() )));
								} catch (android.content.ActivityNotFoundException anfe) {
								    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + Login.this.getPackageName() )));
								}
								Log.d("update", "Current version " + GetVersion() + "playstore version " + onlineVersion);
								 }});

				 
	            }
	            else 
	            	new FetchUserData().execute(new Void[]{null});
	        }
	        else 
            	new FetchUserData().execute(new Void[]{null});
	    }}

  private class LoginUser extends AsyncTask<String, Void, Integer> {
    	private ArrayList<User_Entity> alUserData= new ArrayList<User_Entity>();
    	int Auto=0;
    	String strUserName,strPassword;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Login.this, getString(R.string.pleaseWait), "", true, false, null);

		}

		@Override
		protected Integer doInBackground(String... params) {
			int iRes=-1;
			try {
				if(params.length==5)
					Auto=Integer.parseInt(params[4]);
				  strUserName=params[0];
				  strPassword=params[1];
				  WebService.Login(Login.this,params[0], params[1], params[2],params[3], alUserData);
				 if(alUserData.size()>0){
						User_Entity user = (User_Entity)alUserData.get(0);
						if(user.getError()==null){
							String strVersion=GetVersion();
							WebService.AddAppVersion(Login.this,user.getId(), strVersion)	;}
				 }
			}catch (Exception ex) {
				 WebService.SaveErrorLogNoBandwidth(Login.this,Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
				 //if(Utility.isConnected(Login.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
        	   //	else
        	   //		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
           
			}
			return iRes;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
			if(alUserData.size()>0){
				User_Entity user = (User_Entity)alUserData.get(0);
				if(user.getError()!=null)
				   Utility.alert(user.getError(),Login.this, null);
				else {
					Utility.SaveUserEntity(Login.this, alUserData.get(0));
					//C.alUserMainData=alUserData;
					//C.USER_ID=C.alUserMainData.get(0).getId();
					
					if(user.getDisabled().equals("1"))
						 Utility.alert(Login.this.getString(R.string.disabled),Login.this, null);
					else if(user.getLocked().equals("1"))
						 Utility.alert(Login.this.getString(R.string.locked),Login.this, null);
					else{
						Calendar c = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String strDate = sdf.format(c.getTime());
						int remember=0;
						if(cb_Remember.isChecked()|Auto==1){
							remember=1;
							user.setAuto(remember);
							Utility.SaveUserEntity(Login.this, user);}
					    updater.deleteOldContentsUser();
						updater.saveContentsForUser(user.getId(), user.getName(), user.getShopName(), 
								user.getLocation(), user.getMobile(), user.getEmail(), user.getLandMark(),
								user.getCRNumber(), user.getDate(), user.getLocked(), user.getDisabled(), user.getCommission(), 
								user.getLang(),strDate,strUserName,strPassword,remember,user.getType(),user.getBundleSales().toString());
						
						
				    Intent  intent = new Intent(Login.this,Home.class);
				    Intent intent2 = getIntent();
					if(intent2.getStringExtra("redirect")!=null){
				    intent.putExtra("redirect", intent2.getStringExtra("redirect"));}
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			    	startActivity(intent);}}
			}
			else
				Utility.showToast(Login.this, getString(R.string.error)); 
			
			}catch(Exception ex){
				WebService.SaveErrorLogNoBandwidth(Login.this,Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				
				// if(Utility.isConnected(Login.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
        	   	//else
        	   	//	Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
           
			}}}
  
  
 /* private class Countires extends AsyncTask<Void, Void, Integer> {
  	private ArrayList<Countries_Entity> alCountriesData= new ArrayList<Countries_Entity>();
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Login.this, getString(R.string.pleaseWait), "", true, true, null);

		}

		@Override
		protected Integer doInBackground(Void... params) {
			int iRes=-1;
			try {
				  WebService.GetCountries(Login.this,alCountriesData,C.getLastModificationDate(Login.this));
			}catch (Exception ex) {
				WebService.SaveErrorLogNoBandwidth(Login.this,Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				
				//if(Utility.isConnected(Login.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			return iRes;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
			if(alCountriesData.size()>0){
				updater.deleteOldContentsCountries();
				for (int i=0;i<alCountriesData.size();i++){
				Countries_Entity countries = (Countries_Entity)alCountriesData.get(i);
				if(countries.getStatus().equals("1"))
				updater.saveContentsForCountries(countries.getID(), countries.getNameEng(), countries.getNameAr());
				else if(countries.getStatus().equals("2"))
					updater.updateContentsForCountries(countries.getID(), countries.getNameEng(), countries.getNameAr());
				else if(countries.getStatus().equals("3"))
					updater.deleteContentsForCountries(countries.getID());
				}	
				    Calendar c = Calendar.getInstance();     
			        C.storeLastModificationDate(Login.this, c.get(Calendar.YEAR)+"-"+ c.get(Calendar.MONTH)+"-"+c.get(Calendar.DAY_OF_MONTH));
				
			}
			else
				Utility.showToast(Login.this, getString(R.string.error)); 
			
			}catch(Exception ex){
				WebService.SaveErrorLogNoBandwidth(Login.this,Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				
				// if(Utility.isConnected(Login.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
      	   	//else
      	   	//	Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
         
			}}}*/

  
  
  private   Cursor getUserContentsCursor() {
		Cursor crsr = null;
		try {
			crsr = db.rawQuery("Select * from User", null);
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(Login.this,Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
			
			//if(Utility.isConnected(Login.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		}
		return crsr;
	}
	
	
	
	   class FetchUserData extends AsyncTask<Void, Void, Void> {
			ArrayList<User_Entity> alUserData = new ArrayList<User_Entity>();
			String strLogin_DateTime;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();}
			
			@Override
			protected Void doInBackground(Void... params) {
				try {	
					Cursor crsr = getUserContentsCursor();
					crsr.moveToFirst();
					if(crsr!=null){
					while (!crsr.isAfterLast()) {
						
						User_Entity cursorEntity = new User_Entity();
						cursorEntity.setId(crsr.getString(0));
						cursorEntity.setName(crsr.getString(1));
						cursorEntity.setShopName(crsr.getString(2));
						cursorEntity.setLocation(crsr.getString(3));
						cursorEntity.setMobile(crsr.getString(4));
						cursorEntity.setEmail(crsr.getString(5));
						cursorEntity.setLandMark(crsr.getString(6));
						cursorEntity.setCRNumber(crsr.getString(7));
						cursorEntity.setDate(crsr.getString(8));
						cursorEntity.setLocked(crsr.getString(9));
						cursorEntity.setDisabled(crsr.getString(10));
						cursorEntity.setCommission(crsr.getString(11));
						cursorEntity.setLang(crsr.getString(12));
						strLogin_DateTime=crsr.getString(13);
						cursorEntity.setUserName(crsr.getString(14));
						cursorEntity.setPassword(crsr.getString(15));
						cursorEntity.setAuto(Integer.parseInt(crsr.getString(16)));
						cursorEntity.setType(crsr.getString(17));
						cursorEntity.setBundleSales(Boolean.parseBoolean(crsr.getString(18)));
						
						alUserData.add(cursorEntity);
						crsr.moveToNext();
					} crsr.close();
					}
					
					  }catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(Login.this,Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
							
						//  if(Utility.isConnected(Login.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Login.this).getId(),Login.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				}
				return null;
			}

			@Override
			protected void onPostExecute(Void v) {
				super.onPostExecute(v);
				try{
					if(alUserData.size()>0){
						Calendar c = Calendar.getInstance();
						 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						 String strCurrentDate = sdf.format(c.getTime());
						 Date CurrentDate = sdf.parse(strCurrentDate);
						 Date Login_DateTime = sdf.parse(strLogin_DateTime);
						 int minutes=minutesDiff(Login_DateTime,CurrentDate);
						 
						User_Entity user=alUserData.get(0);
						Utility.SaveUserEntity(Login.this, user);
						
					   // C.alUserMainData=alUserData;
						//C.USER_ID=user.getId();
						
					   if(Logout==0 && user.getAuto()==1){
						   Intent  intent = new Intent(Login.this,Home.class);
						    Intent intent2 = getIntent();
							if(intent2.getStringExtra("redirect")!=null){
						    intent.putExtra("redirect", intent2.getStringExtra("redirect"));}
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					    	startActivity(intent);
						  /* if(Utility.isConnected(Login.this))
	                			 new LoginUser().execute(new String[]{user.getUserName(),user.getPassword(),Utility.getDeviceId(Login.this),Utility.getDeviceNumber(Login.this),Integer.toString(user.getAuto())});
	                	   	else
	                	   		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
						  */ }
					
						else if (Logout==1 && user.getAuto()==1){
							edtUserName.setText(user.getUserName());
							edtPassword.setText(user.getPassword());
							if(user.getAuto()==1)
								cb_Remember.setChecked(true);}
						
						else  if(minutes<=120){
							 /* if(Utility.isConnected(Login.this))
		                			 new LoginUser().execute(new String[]{user.getUserName(),user.getPassword(),Utility.getDeviceId(Login.this),Utility.getDeviceNumber(Login.this),Integer.toString(user.getAuto())});
		                	   	else
		                	   		Utility.showToast(Login.this, getString(R.string.alert_need_internet_connection)); 
							  */ 
							  Intent  intent = new Intent(Login.this,Home.class);
							    Intent intent2 = getIntent();
								if(intent2.getStringExtra("redirect")!=null){
							    intent.putExtra("redirect", intent2.getStringExtra("redirect"));}
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						    	startActivity(intent);}
					
						else {
						
						}}}catch(Exception ex){}
				}
			
			
			
		}
		
	   
	
	

/*    private class FetchUserData extends AsyncTask<Void, Void, Void> {
		ArrayList<User_Entity> alUserData = new ArrayList<User_Entity>();
		String strLogin_DateTime;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getUserContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					
					User_Entity cursorEntity = new User_Entity();
					cursorEntity.setId(crsr.getString(0));
					cursorEntity.setName(crsr.getString(1));
					cursorEntity.setShopName(crsr.getString(2));
					cursorEntity.setLocation(crsr.getString(3));
					cursorEntity.setMobile(crsr.getString(4));
					cursorEntity.setEmail(crsr.getString(5));
					cursorEntity.setLandMark(crsr.getString(6));
					cursorEntity.setCRNumber(crsr.getString(7));
					cursorEntity.setDate(crsr.getString(8));
					cursorEntity.setLocked(crsr.getString(9));
					cursorEntity.setDisabled(crsr.getString(10));
					cursorEntity.setCommission(crsr.getString(11));
					cursorEntity.setLang(crsr.getString(12));
					strLogin_DateTime=crsr.getString(13);
					alUserData.add(cursorEntity);
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception exception) {
				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			try{
			Calendar c = Calendar.getInstance();
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 String strCurrentDate = sdf.format(c.getTime());
			 Date CurrentDate = sdf.parse(strCurrentDate);
			 Date Login_DateTime = sdf.parse(strLogin_DateTime);
			 int minutes=minutesDiff(Login_DateTime,CurrentDate);
			 if(minutes<=30){
				    C.alUserMainData=alUserData;
					C.USER_ID=C.alUserMainData.get(0).getId();
					updater.updateLoginDateContentsForUser(strCurrentDate);
				    Intent  intent = new Intent(Login.this,SimRegsitration.class);
				    Intent intent2 = getIntent();
					if(intent2.getStringExtra("redirect")!=null){
				    intent.putExtra("redirect", intent2.getStringExtra("redirect"));}
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			    	startActivity(intent);}
			 else 
				 updater.deleteOldContentsUser();
			}catch(Exception ex){}
			}
		
		
		
	}
	 */
    public static int minutesDiff(Date Login_DateTime, Date CurrentDate)
    {
        if( Login_DateTime == null || CurrentDate == null ) return 0;

        long result = ((CurrentDate.getTime()/60000) - (Login_DateTime.getTime()/60000));
        return (int) result;
    }
   
    
    
    public String GetVersion(){
    	Context context = getApplicationContext(); // or activity.getApplicationContext()
    	PackageManager packageManager = context.getPackageManager();
    	String packageName = context.getPackageName();

    	String myVersionName = "not available"; // initialize String

    	try {
    	    myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
    	} catch (PackageManager.NameNotFoundException e) {
    	    e.printStackTrace();
    	}
		return myVersionName;
    }
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UNINSTALL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d("TAG", "onActivityResult: user accepted the (un)install");
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("TAG", "onActivityResult: user canceled the (un)install");
            } else if (resultCode == RESULT_FIRST_USER) {
                Log.d("TAG", "onActivityResult: failed to (un)install");
            }
        }
    }
    
    public boolean isPackageExisted(String targetPackage){
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = getPackageManager();        
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if(packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }
 
}
