package com.simregistration.signed;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.simregistration.signed.R.color;
//import com.simregistration.signed.GCCFields.ResizeImage;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.DateArrayAdapter;
import com.simregistration.signed.utility.DateNumericAdapter;
import com.simregistration.signed.utility.HijriCalendar;
import com.simregistration.signed.utility.IntentIntegrator;
import com.simregistration.signed.utility.IntentResult;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.OnWheelChangedListener;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;
import com.simregistration.signed.utility.WheelView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class MRZGcc extends Activity {
    Button btn_logout,btn_Scan;
    EditText edt_Name,edt_Sim,edt_ID,edt_Address;
    Spinner sp_Country;
	TextView txtBirthDay,txtExpiryDate;
	private RadioButton rb_M,rb_F;
	Button btn_GCCImage,edit,submit;
	String firstname ="",surname="";
	private final int TAKE_CAMERA_REQUEST = 1;
	private String imgPath="";
	private int eYear, eMonth, eDay;
	private ArrayList<Countries_Entity> alCountriesDataMain;
    private ArrayList<String> alCountriesDataMainNames;
    private SQLiteDatabase db;
    private DatabaseHelper dbHelper;
    String our_nationality = "";
    int exp_year,exp_month,exp_day;
    int editflag = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        exp_year=exp_month=exp_day=0;
        eYear= eMonth= eDay = 0;
        setContentView(R.layout.gcc_fields);  
        btn_logout =(Button)findViewById(R.id.btn_logout);
        btn_logout.setVisibility(View.GONE);
        edt_Name = (EditText)findViewById(R.id.edtName);
        edt_Sim = (EditText)findViewById(R.id.edtSim);
        edt_ID = (EditText)findViewById(R.id.edtID);
        sp_Country = (Spinner)findViewById(R.id.spCountry);
        edt_Address = (EditText)findViewById(R.id.edtAddress);
        txtBirthDay= (TextView)findViewById(R.id.txtBirthDay);
        rb_M = (RadioButton)findViewById(R.id.rb_M);
  	    rb_F= (RadioButton)findViewById(R.id.rb_F);
  	    btn_GCCImage = (Button)this.findViewById(R.id.btnGCC);
  	    txtExpiryDate= (TextView)findViewById(R.id.txtIDExpiry);
  	    edit= (Button)findViewById(R.id.edit);
  	    submit=(Button)findViewById(R.id.btnSubmit);
  	    
  	    
  	  if(!getIntent().getStringExtra("mode").trim().equalsIgnoreCase("manual")){
  		  edt_Name.setEnabled(false);
    	  edt_ID.setEnabled(false);
    	  rb_M.setEnabled(false);
    	  rb_F.setEnabled(false);
    	  txtBirthDay.setEnabled(false);
    	  txtExpiryDate.setEnabled(false);
    	  edit.setVisibility(View.VISIBLE);
      }
//  
  	  edit.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
		 if(editflag == 0){
			 	editflag = 1;
			 	submit.setEnabled(false);
				edit.setText(getResources().getString(R.string.doneediting));
				edt_Name.setEnabled(true);
				edt_ID.setEnabled(true);
				sp_Country.setEnabled(true);
				txtBirthDay.setEnabled(true);
				rb_M.setEnabled(true);
				rb_F.setEnabled(true);
				txtExpiryDate.setEnabled(true);
				try{
					String[] fields = txtBirthDay.getText().toString().split("/");
					eYear= Integer.parseInt(fields[2]);
					eMonth= Integer.parseInt(fields[1]);
					eDay= Integer.parseInt(fields[0]);
				}catch(Exception e){
					
				}
				try{
					String[] fields = txtExpiryDate.getText().toString().split("/");
					exp_year= Integer.parseInt(fields[2]);
					exp_month= Integer.parseInt(fields[1]);
					exp_day= Integer.parseInt(fields[0]);
				}catch(Exception e){
					
				}
				
		 }else{
			 editflag = 0;
			 	submit.setEnabled(true);
				edit.setText(getResources().getString(R.string.edit));
				edt_Name.setEnabled(false);
				edt_ID.setEnabled(false);
				sp_Country.setEnabled(false);
				txtBirthDay.setEnabled(false);
				rb_M.setEnabled(false);
				rb_F.setEnabled(false);
				txtExpiryDate.setEnabled(false);
		 }
			
		}
	});
  	
  	dbHelper = new DatabaseHelper(MRZGcc.this);
    db = dbHelper.getWritableDatabase();
    alCountriesDataMain = new ArrayList<Countries_Entity>();
  	
  	  
  	    
  	  btn_Scan = (Button)findViewById(R.id.btnScan);
  	  btn_Scan.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		try {
	      			IntentIntegrator integrator = new IntentIntegrator(MRZGcc.this);
	                integrator.initiateScan();
	                //startActivityForResult(integrator, 1);

	      		} catch (Exception ex) {
	      			WebService.SaveErrorLogNoBandwidth(MRZGcc.this,Utility.GetUserEntity(MRZGcc.this).getId(),MRZGcc.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
	      			//if(Utility.isConnected(GCCFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	      		}
	      	}
	      });
  	  
  	  	btn_GCCImage.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				
				 
//				  if(edt_Name.getText().toString().equals(""))
//					  Utility.showToast(MRZGcc.this,getResources().getString(R.string.enter_name));
//				  else if (edt_Sim.getText().toString().equals(""))
//					  Utility.showToast(MRZGcc.this,getResources().getString(R.string.enter_iccid));
//				  else if(edt_ID.getText().toString().equals(""))
//				   Utility.showToast(MRZGcc.this,getResources().getString(R.string.enter_idnumber));	
//				  else if(sp_Country.getSelectedItemPosition()==0)
//					   Utility.showToast(MRZGcc.this,getResources().getString(R.string.enter_country));	
//				
//				  else if(txtBirthDay.getText().toString().equals(""))
//					   Utility.showToast(MRZGcc.this,getResources().getString(R.string.enter_Birthday));
//			
//				  
//				  else if(txtExpiryDate.getText().toString().equals(""))
//					   Utility.showToast(MRZGcc.this,getResources().getString(R.string.enter_idexpiry));
//				  
//					  
//				
//				  else if(IsExpired(txtExpiryDate.getText().toString()))
//					  Utility.showToast(MRZGcc.this,getResources().getString(R.string.id_expired));
//				  else{
					  
					  final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					
		                intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
		                startActivityForResult(intent, TAKE_CAMERA_REQUEST);
		                
		                
//				  }
			} catch (Exception ex) {
					  WebService.SaveErrorLogNoBandwidth(MRZGcc.this,Utility.GetUserEntity(MRZGcc.this).getId(),MRZGcc.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						
					  //if(Utility.isConnected(GCCFields.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   //	else
		      	   //		Utility.showToast(GCCFields.this, getString(R.string.alert_need_internet_connection)); 
		        
		       
		      }
		}}); 
  	  	
  	  txtExpiryDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				try {
					if(exp_year==exp_month && exp_month==exp_day && exp_year == 0 && getIntent().getStringExtra("mode").trim().equalsIgnoreCase("manual")){
						Calendar calendar = Calendar.getInstance();
						exp_year = calendar.get(Calendar.YEAR);
						exp_month = calendar.get(Calendar.MONTH);
						exp_day = calendar.get(Calendar.DAY_OF_MONTH);
					}
			        
					DatePickerDialog dg = new DatePickerDialog(MRZGcc.this, new DatePickerDialog.OnDateSetListener() {
						
						@Override
						public void onDateSet(DatePicker view, int year, 
	                            int monthOfYear, int dayOfMonth) {
						
							exp_year = year;
							exp_month = monthOfYear + 1;
							exp_day = dayOfMonth;
				          
				        txtExpiryDate.setText( Integer.toString(exp_day)+ "/" + Integer.toString(exp_month) + "/" +
				        		 Integer.toString(exp_year) );
				      }

					}, exp_year, exp_month - 1, exp_day);
					
					dg.show();	// Show picker dialog
					} catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(MRZGcc.this,Utility.GetUserEntity(MRZGcc.this).getId(),MRZGcc.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						//if(Utility.isConnected(GCCFields.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}}});
  	  
  	  
  	txtBirthDay.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			try {
				if(eYear==eMonth && eMonth==eDay && eYear == 0 && getIntent().getStringExtra("mode").trim().equalsIgnoreCase("manual")){
					Calendar calendar = Calendar.getInstance();
					eYear = calendar.get(Calendar.YEAR);
					eMonth = calendar.get(Calendar.MONTH);
					eDay = calendar.get(Calendar.DAY_OF_MONTH);
				}
		        
				DatePickerDialog dg = new DatePickerDialog(MRZGcc.this, new DatePickerDialog.OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, 
                            int monthOfYear, int dayOfMonth) {
					
			          eYear = year;
			          eMonth = monthOfYear+1;
			          eDay = dayOfMonth;
			          
			          txtBirthDay.setText( Integer.toString(eDay)+ "/" + Integer.toString(eMonth) + "/" +
			        		 Integer.toString(eYear) );
			      }

				}, eYear, eMonth-1, eDay);
				
				dg.show();	// Show picker dialog
				} catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(MRZGcc.this,Utility.GetUserEntity(MRZGcc.this).getId(),MRZGcc.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					//if(Utility.isConnected(GCCFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				}
			}
		});

        
        
        
  	if(!getIntent().getStringExtra("mode").trim().equalsIgnoreCase("manual")){
        Bundle params = getIntent().getExtras();

        String[] names = params.getStringArray("names");
        String[] values = params.getStringArray("values");
        String[] lines = params.getStringArray("Lines");
        String mrz_nationality_id ="";
        
        String IDnumber = "";
        String IDnumber2 = "";
        for(int i=0;i<11;i++){
        	if(names[i].trim().equalsIgnoreCase("ID Number")){
        		IDnumber = values[i];
        	}else if(names[i].trim().equalsIgnoreCase("Document Number")){
        		IDnumber2 = values[i];
        	}else if(names[i].trim().equalsIgnoreCase("Sex")){
        		if(values[i].equalsIgnoreCase("f")){
        			rb_F.setChecked(true);
        		}else{
        			rb_M.setChecked(true);
        		}
        	}else if(names[i].trim().equalsIgnoreCase("Name")){
        		firstname = values[i];
        	}else if(names[i].trim().equalsIgnoreCase("Surname")){
        		surname = values[i];
        	}else if(names[i].trim().equalsIgnoreCase("Expiry date")){
        		txtExpiryDate.setText(values[i].replace(" ","/"));
        	}else if(names[i].trim().equalsIgnoreCase("Birth date")){
        		txtBirthDay.setText(values[i].replace(" ","/"));
        	}else if(names[i].trim().equalsIgnoreCase("Country")){
        		mrz_nationality_id = values[i];
        	}
        }
        edt_Name.setText(firstname +" "+ surname);
        
        if(mrz_nationality_id.equalsIgnoreCase("KWT")){
        	our_nationality = "KW";
        	edt_ID.setText(IDnumber);
        }else if(mrz_nationality_id.contains("OM")){
        	our_nationality = "OM";
        }else if(mrz_nationality_id.contains("QA")){
        	our_nationality = "QA";
        }else if(mrz_nationality_id.contains("SA")){
        	our_nationality = "SA";
        }else if(mrz_nationality_id.equalsIgnoreCase("ARE")){
        	our_nationality = "AE";
        	edt_ID.setText(IDnumber2);
        }else{
        	our_nationality = "NON";
        }
  	}
        new FetchCountriesData().execute();
        
    }
    
    public int getIndexOfCountry(String id){
    	for(int i =0;i<alCountriesDataMain.size();i++ ){
    		if(alCountriesDataMain.get(i).getID().trim().equalsIgnoreCase(id.trim())){
    			return i;
    		}
    	}
    	return -1;
    }
    
    private class FetchCountriesData extends AsyncTask<Void, Void, Void> {
		ArrayList<Countries_Entity> alColuntriesData = new ArrayList<Countries_Entity>();
		ArrayList<String> alColuntriesDataNames = new ArrayList<String>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getCountriesGCCContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					Countries_Entity cursorEntity = new Countries_Entity();
					cursorEntity.setID(crsr.getString(0));
					cursorEntity.setNameEng(crsr.getString(1));
					cursorEntity.setNameAr(crsr.getString(2));
					alColuntriesData.add(cursorEntity);
					alColuntriesDataNames.add(crsr.getString(1));
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(MRZGcc.this,Utility.GetUserEntity(MRZGcc.this).getId(),MRZGcc.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
					//if(Utility.isConnected(GCCFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			if (alColuntriesData != null && alColuntriesData.size() > 0) {
				alCountriesDataMain.clear();
				for(Countries_Entity country : alColuntriesData){
					alCountriesDataMain.add(country);
				}
//				alCountriesDataMain = alColuntriesData;
				alCountriesDataMainNames=alColuntriesDataNames;
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MRZGcc.this,
						android.R.layout.simple_spinner_item, alCountriesDataMainNames);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_Country.setAdapter(
					      new NothingSelectedSpinnerAdapter(
					    		dataAdapter,
					            R.layout.country_spinner_row_nothing_selected,
					            MRZGcc.this));
                if(!getIntent().getStringExtra("mode").trim().equalsIgnoreCase("manual")){
                	sp_Country.setSelection(getIndexOfCountry(our_nationality) + 1);
                	sp_Country.setEnabled(false);
                }
                
                
			      
			} }
	}
	private Cursor getCountriesGCCContentsCursor() {
		Cursor crsr = null;
		try {
			String[] from = { C.ID,C.NAME_ENG,C.NAME_AR};
			crsr = db.query(C.COUNTRIES_TABLE, from,C.NAME_ENG +" = 'United Arab Emirates' OR "+ C.NAME_ENG+" = 'Kuwait'  OR "+ C.NAME_ENG+" = 'Saudi Arabia'  OR " + C.NAME_ENG+" ='Oman'  OR "  + C.NAME_ENG+" ='Qatar'   "   , null, null, null,C.NAME_ENG+" ASC");
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(MRZGcc.this,Utility.GetUserEntity(MRZGcc.this).getId(),MRZGcc.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
			//if(Utility.isConnected(GCCFields.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
    public boolean IsExpired(String valid_until){ 
   	 boolean expired =false;
   	 try{ 
   		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
   		 String strToday = sdf.format(new Date()); 
   		 
   		 Date strExpiryDate = sdf.parse(valid_until);
   		 Date strDate = sdf.parse(strToday);
   		 if (strExpiryDate.compareTo(strDate)<0) {
   			 expired = true;
   		 }
   		 
   	}catch(Exception ex){
   		 ex.getMessage();
   	 }
   	return expired;
   	}
    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/GCC/", "image" + new Date().getTime() + ".png");
        File parent = file.getParentFile();
		if (parent != null) parent.mkdirs();
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
    
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case TAKE_CAMERA_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
            		btn_GCCImage.setBackgroundColor(getResources().getColor(R.color.dark_gray));
            		btn_GCCImage.setTextColor(getResources().getColor(R.color.green_light));
//            		new  ResizeImage().execute(new String[]{imgPath});
            		}
            break;
            
            case 49374:
            	if (resultCode == Activity.RESULT_OK) {
	                   IntentResult intentResult = 
	                   IntentIntegrator.parseActivityResult(requestCode, resultCode,data);
	                if (intentResult != null) {
	                	edt_Sim.setText(intentResult.getContents());} 
	             }
           
            	break;
           default:
               break;
        } }
    }
