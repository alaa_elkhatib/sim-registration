package com.simregistration.signed;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import biz.smartengines.swig.MrzRectMatrix;

import java.util.ArrayList;
import java.util.List;


public class RectView extends View {

    private Paint paint = new Paint();
    private List<Rect> rects = new ArrayList<Rect>();
    private int x_roi = 0, y_roi = 0;
    private double scale = 0;


    public RectView(Context context) {

        super(context);
        paint.setColor(Color.YELLOW);
        this.setBackgroundColor(Color.argb(0, 0, 0, 0));
    }

    public void SetRoiZone(Rect roi, double _scale)
    {
        x_roi = roi.left;
        y_roi = roi.top;
        scale = _scale;
    }

    public void SetRects(MrzRectMatrix mrzRectMatrix) {

        rects.clear();

        if(mrzRectMatrix == null)
            return;

        for(int i=0; i< mrzRectMatrix.size(); i++) {

            for(int j=0; j< mrzRectMatrix.get(i).size(); j++) {

                int l = (int)(x_roi/scale) + mrzRectMatrix.get(i).get(j).getX();
                int t = (int)(y_roi/scale) + mrzRectMatrix.get(i).get(j).getY();
                int r = l + mrzRectMatrix.get(i).get(j).getWidth();
                int b = t + mrzRectMatrix.get(i).get(j).getHeight();

                l= (int)(l*scale);
                t= (int)(t*scale);
                r= (int)(r*scale);
                b= (int)(b*scale);

                Rect rect = new Rect(l, t, r, b);
                rects.add(rect);

            }
        }
    }

    @Override
    public void onDraw(Canvas canvas) {

        for(int i=0; i< rects.size(); i++) {

            Rect rect = rects.get(i);

            canvas.drawLine(rect.left, rect.top, rect.right, rect.top, paint);
            canvas.drawLine(rect.left, rect.top, rect.left, rect.bottom, paint);
            canvas.drawLine(rect.right, rect.top, rect.right, rect.bottom, paint);
            canvas.drawLine(rect.left, rect.bottom, rect.right, rect.bottom, paint);


        }

    }



}
