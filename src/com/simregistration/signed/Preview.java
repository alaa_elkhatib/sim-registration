package com.simregistration.signed;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import com.simregistration.signed.utility.Utility;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

class Preview extends SurfaceView implements SurfaceHolder.Callback {	
	//public final String TAG = "TESTESTESTEST"; // used for LogCat filtering

	private SurfaceHolder mHolder;
	static Camera mCamera;
	Camera.Parameters mParameters;
	byte[] mBuffer;
	final static int SUPPORTED_WIDTH = 640;
    final static int SUPPORTED_HEIGHT = 480;

	// this constructor used when requested as an XML resource
	public Preview(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public Preview(Context context) {
		super(context);
		init();
	}

	public void init() {
		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);}

	
	public Bitmap onPreviewFrame ( int x, int y, int width, int height, byte[] data, Camera arg1) {
		
		 int w = mParameters.getPreviewSize().width;
		   int h = mParameters.getPreviewSize().height;
		   int format = mParameters.getPreviewFormat();
		   YuvImage image = new YuvImage(data, format, w, h, null);

		   ByteArrayOutputStream out = new ByteArrayOutputStream();
		   Rect area = new Rect(x, y, width, height);
		   image.compressToJpeg(area, 100, out);
		   Bitmap bm = BitmapFactory.decodeByteArray(out.toByteArray(), 0, out.size());
		 
		   
		   
	/*	Bitmap b = null;
		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			
			YuvImage yuvimage = new YuvImage(data,ImageFormat.NV21,arg1.getParameters().getPreviewSize().width,arg1.getParameters().getPreviewSize().height,null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			yuvimage.compressToJpeg(new Rect(x,y,arg1.getParameters().getPreviewSize().width,arg1.getParameters().getPreviewSize().height), 100, baos);
			b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
			
				} catch (Exception e) {
			e.printStackTrace();}
		 finally {
		}
*/
		return bm;
	}
	public Bitmap getPic(int x, int y, int width, int height,byte[] buffer) {
		System.gc(); 
		Bitmap b = null;
		Size s = mParameters.getPreviewSize();
        YuvImage yuvimage = new YuvImage(buffer, ImageFormat.NV21, s.width,  s.height, null);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(x, y, width, height), 100, outStream); // make JPG
		b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
		if (b != null) {
			//Log.i(TAG, "getPic() WxH:" + b.getWidth() + "x" + b.getHeight());
		} else {
			//Log.i(TAG, "getPic(): Bitmap is null..");
		}
		yuvimage = null;
		outStream = null;
		System.gc();
		return b;
	}
	

	/*public Bitmap getPic(Rect rec,byte[] buffer) {
		System.gc(); 
		Bitmap b = null;
		Size s = mParameters.getPreviewSize();
        YuvImage yuvimage = new YuvImage(buffer, ImageFormat.NV21, s.width,  s.height, null);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(rec, 100, outStream); // make JPG
		b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
		if (b != null) {
			//Log.i(TAG, "getPic() WxH:" + b.getWidth() + "x" + b.getHeight());
		} else {
			//Log.i(TAG, "getPic(): Bitmap is null..");
		}
		yuvimage = null;
		outStream = null;
		System.gc();
		return b;
	}*/
	
/*	public Bitmap getPic(int x, int y, int width, int height,int c_width, int c_height) {
		System.gc(); 
		Bitmap b = null;
		Size s = mParameters.getPreviewSize();
        YuvImage yuvimage = new YuvImage(mBuffer, ImageFormat.NV21, c_width, c_height, null);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(x, y, width, height), 100, outStream); // make JPG
		b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
		if (b != null) {
			//Log.i(TAG, "getPic() WxH:" + b.getWidth() + "x" + b.getHeight());
		} else {
			//Log.i(TAG, "getPic(): Bitmap is null..");
		}
		yuvimage = null;
		outStream = null;
		System.gc();
		return b;
	}*/
	
	public Bitmap getPic(int x, int y, int width, int height,YuvImage yuvimage) {
		System.gc(); 
		Bitmap b = null;
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(x, y, width, height), 100, outStream); // make JPG
		b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
		if (b != null) {
			//Log.i(TAG, "getPic() WxH:" + b.getWidth() + "x" + b.getHeight());
		} else {
			//Log.i(TAG, "getPic(): Bitmap is null..");
		}
		outStream = null;
		System.gc();
		return b;
	}
	
	public Bitmap getFormPic() {
		System.gc(); 
		Bitmap b = null;
		Size s = mParameters.getPreviewSize();
		
        YuvImage yuvimage = new YuvImage(mBuffer, ImageFormat.NV21, s.width, s.height, null);
        
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, s.width, s.height), 100, outStream); // make JPG
		b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
		if (b != null) {
			//Log.i(TAG, "getPic() WxH:" + b.getWidth() + "x" + b.getHeight());
		} else {
			//Log.i(TAG, "getPic(): Bitmap is null..");
		}
		yuvimage = null;
		outStream = null;
		System.gc();
		return b;
	}

	private void updateBufferSize() {
		mBuffer = null;
		System.gc();
		// prepare a buffer for copying preview data to
		int h = mCamera.getParameters().getPreviewSize().height;
		int w = mCamera.getParameters().getPreviewSize().width;
		int bitsPerPixel = ImageFormat.getBitsPerPixel( mCamera.getParameters().getPreviewFormat() );
		mBuffer = new byte[w * h * bitsPerPixel/2];
		//Log.i("surfaceCreated", "buffer length is " + mBuffer.length + " bytes");
	}

	 /**
     * Works in API level >= 10.
     *
     * @return Front camera handle.
     */
    Camera getFrontFacingCamera()  {
        final Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int cameraIndex = 0; cameraIndex < Camera.getNumberOfCameras(); cameraIndex++) {
            Camera.getCameraInfo(cameraIndex, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                try {
                    return Camera.open(cameraIndex);
                } catch (final RuntimeException e) {
                    e.printStackTrace();
                }
               
            }
        }
		return null;
       
    }
    
    /**
     * Works in API level >= 7 at Samsung Galaxy S.
     *
     * @param Camera handle.
     */
    void setFrontCamera(Camera camera) {
        final Camera.Parameters parameters = camera.getParameters();
        parameters.set("camera-id", 2);
        try {
            camera.setParameters(parameters);
        } catch (final RuntimeException e) {
            // If we can't set front camera it means that device hasn't got "camera-id". Maybe it's not Galaxy S.
            e.printStackTrace();
        }
    }

    
  /*  public void surfaceCreated(SurfaceHolder holder) {
        if (android.os.Build.VERSION.SDK_INT >= 10) {
            mCamera = getFrontFacingCamera();
        } else {
            mCamera = Camera.open();
            setFrontCamera(mCamera);
        }
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (final IOException e) {
        	if(mCamera!=null)
            mCamera.release();
            mCamera = null;
            e.printStackTrace();
        }
    }
    */
	public void surfaceCreated(SurfaceHolder holder) {
		//Log.i(TAG,"Surface created called");
		// The Surface has been created, acquire the camera and tell it where to draw.
		try {
			  if (!getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) { } else {
				     int cameraId = findCamera();
				      if (cameraId < 0) {
				         } else {
				        	 mCamera = Camera.open(cameraId);
				      }}}
				      
				
		catch (RuntimeException exception) {
			//Log.i(TAG, "Exception on Camera.open(): " + exception.toString());
			Toast.makeText(getContext(), "Camera broken, quitting :(", Toast.LENGTH_LONG).show();
			// TODO: exit program
		}

		try {
			if(mCamera!=null){
			mCamera.setPreviewDisplay(holder);
			updateBufferSize();
			mCamera.addCallbackBuffer(mBuffer); // where we'll store the image data
			mCamera.setPreviewCallbackWithBuffer(new PreviewCallback() {
				public synchronized void onPreviewFrame(byte[] data, Camera c) {

					if (mCamera != null) { // there was a race condition when onStop() was called..
						mCamera.addCallbackBuffer(mBuffer); // it was consumed by the call, add it back
					}
				}
			});}
		} catch (Exception ex) {
		
			//Log.e(TAG, "Exception trying to set preview");
			if(mCamera!=null)
			mCamera.release();
			mCamera = null;
			// TODO: add more exception handling logic here
		}
	}

    
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        // Because the CameraDevice object is not a shared resource, it's very
        // important to release it when the activity is paused.
    	if(mCamera!=null){
        mCamera.stopPreview();
        mCamera.release();}
        mCamera = null;
    }
    
    
	/*public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		//Log.i(TAG,"SurfaceDestroyed being called");
		if(mCamera!=null){
		mCamera.stopPreview();
		mCamera.release();}
		mCamera = null;
        mBuffer=null;
	}
*/
	// FYI: not called for each frame of the camera preview
	// gets called on my phone when keyboard is slid out
	// requesting landscape orientation prevents this from being called as camera tilts
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		//Log.i(TAG, "Preview: surfaceChanged() - size now " + w + "x" + h);
		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		try {
			//mCamera.setDisplayOrientation(90);
			mParameters = mCamera.getParameters();
			List<String> focusModes = mParameters.getSupportedFocusModes();
			if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
			{
				mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
			}
			mParameters.set("orientation","landscape");
			
			// Size optimalSize=getOptimalPreviewSize(mParameters.getSupportedPreviewSizes(),  getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
			// mParameters.setPreviewSize(optimalSize.width, optimalSize.height);
	          
			List<Size> sizes = mParameters.getSupportedPreviewSizes();
			Size size1 = null;
			for (int i=0;i<sizes.size();i++){
				if(i==0)
					size1=sizes.get(i);
				else {
					if((sizes.get(i).height>size1.height) &&  (sizes.get(i).width>size1.width)) 
						size1=sizes.get(i);
				}
					
				
			}
			mParameters.setPreviewSize(size1.width, size1.height);
			mCamera.setParameters(mParameters); // apply the changes
		    mCamera.setPreviewDisplay(mHolder);
		    updateBufferSize();
		} catch (Exception e) {
			// older phone - doesn't support these calls
		}

		//updateBufferSize(); // then use them to calculate

		//Log.i(TAG, "Preview: checking it was set: " + p.width + "x" + p.height); // DEBUG
		mCamera.startPreview();
	}
	
	/*
	 public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	        // Now that the size is known, set up the camera parameters and begin the preview.
	        final Camera.Parameters parameters = mCamera.getParameters();
	        parameters.setPreviewSize(SUPPORTED_WIDTH, SUPPORTED_HEIGHT);
	        mCamera.setParameters(parameters);
	        mCamera.startPreview();
	    }*/
	 
	 
	 

	public Parameters getCameraParameters(){
		return mCamera.getParameters();
	}

	
	;
 
	public void setCameraFocus(AutoFocusCallback autoFocus){
		if (mCamera.getParameters().getFocusMode().equals(mCamera.getParameters().FOCUS_MODE_AUTO) ||
		        mCamera.getParameters().getFocusMode().equals(mCamera.getParameters().FOCUS_MODE_MACRO)){
			try{
		    mCamera.autoFocus(autoFocus);}catch(Exception ex){
		    	
		    }}
	
	}

	
	public void setFlash(boolean flash){
		if (flash){
			mParameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
			mCamera.setParameters(mParameters);
		}
		else{
			mParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
			mCamera.setParameters(mParameters);
		}
	}
	
	public void refreshCamera(Camera camera) {
		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}
		mCamera =camera;
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
		} catch (Exception e) {
			Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
		}
	}


	 private int findCamera() {
		    int cameraId = -1;
		    // Search for the front facing camera
		    int numberOfCameras = Camera.getNumberOfCameras();
		    for (int i = 0; i < numberOfCameras; i++) {
		      CameraInfo info = new CameraInfo();
		      Camera.getCameraInfo(i, info);
		      if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
			       // Log.d(DEBUG_TAG, "Camera found");
			        cameraId = i;
			        break;
			      }
		      if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
		       // Log.d(DEBUG_TAG, "Camera found");
		        cameraId = i;
		        break;
		      }
		    }
		    return cameraId;
		  }
	
	 
	 
	/* private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
		    final double ASPECT_TOLERANCE = 0.05;
		    double targetRatio = (double) w/h;

		    if (sizes==null) return null;

		    Camera.Size optimalSize = null;

		    double minDiff = Double.MAX_VALUE;

		    int targetHeight = h;

		    // Find size
		    for (Camera.Size size : sizes) {
		        double ratio = (double) size.width / size.height;
		        if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
		        if (Math.abs(size.height - targetHeight) < minDiff) {
		            optimalSize = size;
		            minDiff = Math.abs(size.height - targetHeight);
		        }
		    }

		    if (optimalSize == null) {
		        minDiff = Double.MAX_VALUE;
		        for (Camera.Size size : sizes) {
		            if (Math.abs(size.height - targetHeight) < minDiff) {
		                optimalSize = size;
		                minDiff = Math.abs(size.height - targetHeight);
		            }
		        }
		    }
		    return optimalSize;
		}*/
}
