// based on Google's CameraPreview class in API Samples
package com.simregistration.signed;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.CameraPreview;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class Passport_Bar extends Activity implements SensorEventListener {
	//public final String TAG = "TESTESTESTESTEST"; 

	private CameraPreview mPreview; 
	private ImageView mTakePicture;
	private CPRIDTouchView passportbar;
	private ImageView mFlash;
	private boolean mAutoFocus = true;
    private boolean mFlashBoolean = false;
    private SensorManager mSensorManager;
	private Sensor mAccel;
	private boolean mInitialized = false;
	private float mLastX = 0;
	private float mLastY = 0;
	private float mLastZ = 0;
	private ProgressDialog progress = null;
	private int mScreenHeight;
	private int mScreenWidth;
    private boolean mInvalidate = false;
    private File mPassportBarLocation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.passport_id); // display our (only) XML layout - Views already ordered
		mPassportBarLocation = new File(Environment.getExternalStorageDirectory(),"Passport/PassportBar.jpg");
		File parent = mPassportBarLocation.getParentFile();
		if (parent != null) parent.mkdirs();
		
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		//DisplayMetrics displaymetrics = new DisplayMetrics();
		//getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		//mScreenHeight = displaymetrics.heightPixels;
		//mScreenWidth = displaymetrics.widthPixels;
		//GetRealSize();
		Drawable mButtonDrawable = this.getResources().getDrawable(R.drawable.camera);
        mFlash = (ImageView) findViewById(R.id.flash);
		mFlash.setOnClickListener(flashListener);
        mTakePicture = (ImageView) findViewById(R.id.startcamerapreview);
		mTakePicture.setOnClickListener(previewListener);
		// get our Views from the XML layout
		mPreview = (CameraPreview) findViewById(R.id.preview);
		passportbar = (CPRIDTouchView) findViewById(R.id.cpr_id_view);
		
	}

	private AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback(){

		public void onAutoFocus(boolean autoFocusSuccess, Camera arg1) {
			Wait.oneSec();
			mAutoFocus = true;
		}};

		public Double[] getRatio(){
			GetSize();
			Size s = mPreview.getCameraParameters().getPreviewSize();
			double heightRatio = (double)s.height/((double)mScreenHeight);
			//Log.i(TAG,"camera pixel Height: "+mPreview.getCameraParameters().getPictureSize().height);
			//Log.i(TAG,"Height Ratio: "+heightRatio);
			double widthRatio = (double)s.width/(double)mScreenWidth;
			//Log.i(TAG,"Width Ratio: "+widthRatio);
			Double[] ratio = {heightRatio,widthRatio};
			return ratio;
		}

		private OnClickListener flashListener = new OnClickListener(){

			@Override
			public void onClick(View v) {
				if (mFlashBoolean){
					mPreview.setFlash(false);
				}
				else{
					mPreview.setFlash(true);
				}
				mFlashBoolean = !mFlashBoolean;
			}

		};

		

		private OnClickListener previewListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				mPreview.camera.setOneShotPreviewCallback(new Camera.PreviewCallback() {
		             @Override
		             public void onPreviewFrame(final byte[] data, Camera camera) {
		            	//if (mAutoFocus){
							mAutoFocus = false;
							//mPreview.setCameraFocus(myAutoFocusCallback);
							progress = ProgressDialog.show(Passport_Bar.this, getString(R.string.pleaseWait), "", true, false, null);
							Thread tGetPic = new Thread( new Runnable() {
								public void run() {
								
									Double[] ratio = getRatio();
									int id_left = (int) (ratio[1]*(double)passportbar.getmLeftTopPosX());
									int id_top = (int) (ratio[0]*(double)passportbar.getmLeftTopPosY());
				                    int id_right = (int)(ratio[1]*(double)passportbar.getmRightBottomPosX());
				                    int id_bottom = (int)(ratio[0]*(double)passportbar.getmRightBottomPosY());
				                   savePassportBarPhoto(mPreview.onPreviewFrame(id_left,id_top,id_right,id_bottom,data,mPreview.camera));
				                    Passport_Bar.this.runOnUiThread(new Runnable() {
					                    public void run() {
					                    	  progress.dismiss(); }
					                });
					              
									mAutoFocus = true;
									Intent intent = new Intent(Passport_Bar.this, Passport_Bar_Preview.class);
									 String filename = getIntent().getStringExtra("image");   
									intent.putExtra("image", getIntent().getStringExtra("image"));
									startActivity(intent);
									finish();
									} 
							});
							tGetPic.start();
						//}
						//mView.invalidate();
						boolean pressed = false;
						//Log.i(TAG,"Is pressed? "+pressed);
						if (!mTakePicture.isPressed()){
							pressed = true;
						}
						//Log.i(TAG,"Being pressed");
						//return pressed;
					}});
				
				 
				}	   
		};
		
		
	
	    
	   
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			//Log.i(TAG, "onKeyDown(" + keyCode + ")");

			// to take the pic ASAP, grab the preview frame data from here - don't wait for photo
			if (keyCode == KeyEvent.KEYCODE_BACK){
				Intent intent = new Intent(Passport_Bar.this, Passport.class);
				startActivity(intent);
				finish();
			}
			return super.onKeyDown(keyCode, event); // pass the key along to other handlers 
		}

	
		private void savePassportBarPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mPassportBarLocation.exists())
					mPassportBarLocation.createNewFile();
				image = new FileOutputStream(mPassportBarLocation);
			} catch (Exception ex) {
				WebService.SaveErrorLogNoBandwidth(Passport_Bar.this,Utility.GetUserEntity(Passport_Bar.this).getId(),Passport_Bar.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
				//if(Utility.isConnected(Passport_Bar.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Bar.this).getId(),Passport_Bar.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			bm.compress(CompressFormat.JPEG, 100, image);	
			bm.recycle();
			bm=null;
		}
		
	
	

		public void onSensorChanged(SensorEvent event) {
			try{
				if(mPreview.camera!=null){
			if (mInvalidate == true){
				passportbar.invalidate();
			
				mInvalidate = false;
			}
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			if (!mInitialized){
				mLastX = x;
				mLastY = y;
				mLastZ = z;
				mInitialized = true;
			}
			float deltaX  = Math.abs(mLastX - x);
			float deltaY = Math.abs(mLastY - y);
			float deltaZ = Math.abs(mLastZ - z);

			if (deltaX > .5 && mAutoFocus){ //AUTOFOCUS (while it is not autofocusing)
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);
			}
			if (deltaY > .5 && mAutoFocus){ //AUTOFOCUS (while it is not autofocusing)
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);
			}
			if (deltaZ > .5 && mAutoFocus){ //AUTOFOCUS (while it is not autofocusing) */
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);
			}

			mLastX = x;
			mLastY = y;
			mLastZ = z;

		}}catch(Exception ex){
			
		}}

		// extra overrides to better understand app lifecycle and assist debugging
		@Override
		protected void onDestroy() {
			super.onDestroy();
			//Log.i(TAG, "onDestroy()");
		}

	



		@Override
		protected void onPause() {
			super.onPause();
			//Log.i(TAG, "onPause()");
			try{
			mSensorManager.unregisterListener(this);}
			catch(Exception ex){}
		}

		@Override
		protected void onResume() {
			super.onResume();
			try{
			mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_UI);}
			catch(Exception ex){}
			//Log.i(TAG, "onResume()");
		}


		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub

		}
	

		public void GetRealSize(){
			 int width = 0, height = 0;
			    final DisplayMetrics metrics = new DisplayMetrics();
			    Display display = getWindowManager().getDefaultDisplay();
			    Method mGetRawH = null, mGetRawW = null;

			    try {
			        // For JellyBean 4.2 (API 17) and onward
			        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
			            display.getRealMetrics(metrics);

			            width = metrics.widthPixels;
			            height = metrics.heightPixels;
			            mScreenHeight=height;
			            mScreenWidth = width;
			        } else {
			            mGetRawH = Display.class.getMethod("getRawHeight");
			            mGetRawW = Display.class.getMethod("getRawWidth");

			            try {
			                width = (Integer) mGetRawW.invoke(display);
			                height = (Integer) mGetRawH.invoke(display);
			                mScreenHeight=height;
				            mScreenWidth = width;
			            } catch (IllegalArgumentException e) {
			                // TODO Auto-generated catch block
			                e.printStackTrace();
			            } catch (IllegalAccessException e) {
			                // TODO Auto-generated catch block
			                e.printStackTrace();
			            } catch (InvocationTargetException e) {
			                // TODO Auto-generated catch block
			                e.printStackTrace();
			            }
			        }
			    } catch (NoSuchMethodException e3) {  
			        e3.printStackTrace();
			    }
		}
		
		
		public void GetSize(){
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			mScreenHeight = displaymetrics.heightPixels;
			mScreenWidth = displaymetrics.widthPixels;
			Log.d("PassportBar", "Height"+mScreenHeight+" Width"+mScreenWidth);
	
		}
		
}