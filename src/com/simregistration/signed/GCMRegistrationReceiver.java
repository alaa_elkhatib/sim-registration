package com.simregistration.signed;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.AsyncTask;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class GCMRegistrationReceiver extends BroadcastReceiver {
	private static int NOTIFICATION_ID = 1;
	private static final String EXTRA_MESSAGE = "message";
	private static  String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final String TAG = "GCM";
	private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	String regid;
	Context context = null;
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		this.context=context;
		dbHelper = new DatabaseHelper(context);
		db = dbHelper.getWritableDatabase();
		updater = new ContentUpdater(context, db);
		Log.w("C2DM", "Registration Receiver called");
		if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
			Log.w("C2DM", "Received registration ID");
			regid = intent.getStringExtra("registration_id"); 
			
			setRegistrationId(regid);
			}
		
		else if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) {
			sendNotification(intent.getExtras().getString("SIMMSISDN"),intent.getExtras().getString("TranDateTime"),
					intent.getExtras().getString("data"),	intent.getExtras().getString("tranID"),
					intent.getExtras().getString("VerificationStatus"),intent.getExtras().getString("ActivationStatus"));}
	}

	private void setRegistrationId(String regId) {
		try{
		/*final SharedPreferences prefs = getGCMPreferences();
		int appVersion = getAppVersion(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();*/
		if(Utility.GetUserEntity(context)!=null && !regId.equals("") ){
			if(!Utility.GetUserEntity(context).getId().equals(""))
		new TokenLoader().execute(new String[] {regId,Utility.GetUserEntity(context).getId()});}
	}catch(Exception ex){}}
	
	private SharedPreferences getGCMPreferences() {
		return context.getSharedPreferences(C.APPNAME, Context.MODE_PRIVATE);
	}
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private void sendNotification(String strSIM,String strDateTime,String strMsg,String strTransID,String strVarificatoin,String strActivation) {
			if(strMsg!=null){
		    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			CharSequence tickerText = strMsg; // ticker-text
			long when = System.currentTimeMillis();
            CharSequence contentTitle = "SIM Registration";
			CharSequence contentText = strMsg;
			Intent notificationIntent = new Intent(context, Splash_adv.class); ;
			if(Utility.GetUserEntity(context)!=null){
				if(!Utility.GetUserEntity(context).getId().equals(""))
		        notificationIntent = new Intent(context, Notifications.class);}
			
			else  {
			notificationIntent = new Intent(context, Splash_adv.class);
			notificationIntent.putExtra("redirect","true");}
			
			notificationIntent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
			 
			PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
			Notification notification = new Notification(R.drawable.ic_launcher, tickerText, when);
			notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
			notification.flags = Notification.FLAG_AUTO_CANCEL;	
			mNotificationManager.notify(NOTIFICATION_ID++, notification);
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String strDate = sdf.format(c.getTime());
			updater.saveContentsForNotificaiton(strSIM, strDateTime, strMsg,strTransID,strVarificatoin,strActivation,strDate);

			}}
	

	
	public  class TokenLoader extends AsyncTask<String, Void, Integer>
	{
		
		@Override
		protected Integer doInBackground(String... arg0)
		{
			return WebService.AddToken(context,arg0[0],arg0[1]);
		}
		}
	
	
	
	
}
