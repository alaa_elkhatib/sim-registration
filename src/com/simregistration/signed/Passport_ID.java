// based on Google's CameraPreview class in API Samples
package com.simregistration.signed;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.YuvImage;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;


public class Passport_ID extends Activity implements SensorEventListener {
	//public final String TAG = "TESTESTESTESTEST"; 

	private Preview mPreview; 
	private ImageView mTakePicture;
	private CPRIDTouchView cpridView;
	private ImageView mFlash;
	private boolean mAutoFocus = true;
    private boolean mFlashBoolean = false;
    private SensorManager mSensorManager;
	private Sensor mAccel;
	private boolean mInitialized = false;
	private float mLastX = 0;
	private float mLastY = 0;
	private float mLastZ = 0;
	private ProgressDialog progress = null;
	private int mScreenHeight;
	private int mScreenWidth;
    private boolean mInvalidate = false;
    private File mCPRIDLocation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.cpr_id); // display our (only) XML layout - Views already ordered
		mCPRIDLocation = new File(Environment.getExternalStorageDirectory(),"Passport/PassportBar.jpg");
		File parent = mCPRIDLocation.getParentFile();
		if (parent != null) parent.mkdirs();
		
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		mScreenHeight = displaymetrics.heightPixels;
		mScreenWidth = displaymetrics.widthPixels;
		Drawable mButtonDrawable = this.getResources().getDrawable(R.drawable.camera);
        mFlash = (ImageView) findViewById(R.id.flash);
		mFlash.setOnClickListener(flashListener);
        mTakePicture = (ImageView) findViewById(R.id.startcamerapreview);
		mTakePicture.setOnClickListener(previewListener);
		// get our Views from the XML layout
		mPreview = (Preview) findViewById(R.id.preview);
		mPreview.setOnClickListener(focusListener);
		 getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		cpridView = (CPRIDTouchView) findViewById(R.id.cpr_id_view);
		
	}

	private AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback(){

		public void onAutoFocus(boolean autoFocusSuccess, Camera arg1) {
			Wait.oneSec();
			mAutoFocus = true;
		}};

		public Double[] getRatio(){
			Size s = mPreview.getCameraParameters().getPreviewSize();
			double heightRatio = (double)s.height/(double)mScreenHeight;
			//Log.i(TAG,"camera pixel Height: "+mPreview.getCameraParameters().getPictureSize().height);
			//Log.i(TAG,"Height Ratio: "+heightRatio);
			double widthRatio = (double)s.width/(double)mScreenWidth;
			//Log.i(TAG,"Width Ratio: "+widthRatio);
			Double[] ratio = {heightRatio,widthRatio};
			return ratio;
		}

		private OnClickListener flashListener = new OnClickListener(){

			@Override
			public void onClick(View v) {
				if (mFlashBoolean){
					mPreview.setFlash(false);
				}
				else{
					mPreview.setFlash(true);
				}
				mFlashBoolean = !mFlashBoolean;
			}

		};
		
		private OnClickListener focusListener = new OnClickListener(){

			@Override
			public void onClick(View v) {
				//mPreview.refreshCamera(mPreview.mCamera);
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);}};


		private OnClickListener previewListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
			 Preview.mCamera.setOneShotPreviewCallback(new Camera.PreviewCallback() {
             @Override
             public void onPreviewFrame(final byte[] data, Camera camera) {
            	 
            	    mAutoFocus = false;
					//mPreview.setCameraFocus(myAutoFocusCallback);
					progress = ProgressDialog.show(Passport_ID.this, getString(R.string.pleaseWait), "", true, false, null);
					Thread tGetPic = new Thread( new Runnable() {
						public void run() {
						
							Double[] ratio = getRatio();
							int id_left = (int) (ratio[1]*(double)cpridView.getmLeftTopPosX());
							int id_top = (int) (ratio[0]*(double)cpridView.getmLeftTopPosY());
		                    int id_right = (int)(ratio[1]*(double)cpridView.getmRightBottomPosX());
		                    int id_bottom = (int)(ratio[0]*(double)cpridView.getmRightBottomPosY());
		                    saveCPRIDPhoto(mPreview.getPic(id_left,id_top,id_right,id_bottom,data));
		    				
		                     Passport_ID.this.runOnUiThread(new Runnable() {
			                    public void run() {
			                    	  progress.dismiss(); }
			                });
			              
		                  	
							mAutoFocus = true;
							Intent intent = new Intent(Passport_ID.this, Passport_Bar_Preview.class);
							intent.putExtra("image", getIntent().getStringExtra("image"));
							
							startActivity(intent);
							finish();
							} 
					});
					tGetPic.start();
					boolean pressed = false;
					//Log.i(TAG,"Is pressed? "+pressed);
					if (!mTakePicture.isPressed()){
						pressed = true;}}});}};
		
		
	
	    
	   
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			//Log.i(TAG, "onKeyDown(" + keyCode + ")");

			// to take the pic ASAP, grab the preview frame data from here - don't wait for photo
			if (keyCode == KeyEvent.KEYCODE_BACK){
				finish();
			}
			return super.onKeyDown(keyCode, event); // pass the key along to other handlers 
		}

	
		private void saveCPRIDPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mCPRIDLocation.exists())
					mCPRIDLocation.createNewFile();
				image = new FileOutputStream(mCPRIDLocation);
			} catch (Exception ex) {
				 WebService.SaveErrorLogNoBandwidth(Passport_ID.this,Utility.GetUserEntity(Passport_ID.this).getId(),Passport_ID.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   	
				//if(Utility.isConnected(CPR_ID.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR_ID.this).getId(),CPR_ID.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	
			}
			bm.compress(CompressFormat.JPEG, 100, image);	
			bm.recycle();
			bm=null;
		}
		
	
	

		public void onSensorChanged(SensorEvent event) {
			try{
				if(mPreview.mCamera!=null){
			if (mInvalidate == true){
				cpridView.invalidate();
			
				mInvalidate = false;
			}
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			if (!mInitialized){
				mLastX = x;
				mLastY = y;
				mLastZ = z;
				mInitialized = true;
			}
			float deltaX  = Math.abs(mLastX - x);
			float deltaY = Math.abs(mLastY - y);
			float deltaZ = Math.abs(mLastZ - z);
			if (mAutoFocus && (deltaX > .5 || deltaY > .5 || deltaZ > .5)){
				mAutoFocus = false;
				mPreview.setCameraFocus(myAutoFocusCallback);}
		
			mLastX = x;
			mLastY = y;
			mLastZ = z;

		}}catch(Exception ex){
			Utility.alert(getString(R.string.restart), Passport_ID.this, onclick);
			}}

		// extra overrides to better understand app lifecycle and assist debugging
		@Override
		protected void onDestroy() {
			super.onDestroy();
			//Log.i(TAG, "onDestroy()");
		}

	



		@Override
		protected void onPause() {
			super.onPause();
			//Log.i(TAG, "onPause()");
			try{
			mSensorManager.unregisterListener(this);}
			catch(Exception ex){}
		}

		@Override
		protected void onResume() {
			super.onResume();
			try{
			mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_UI);}
			catch(Exception ex){}
			//Log.i(TAG, "onResume()");
		}


		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub

		}
	
		  DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
					Intent intent = new Intent(Passport_ID.this,Login.class);
				    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);              
					startActivity(intent);
				}};
				
			
		
		
}