package com.simregistration.signed;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Locale;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;


import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Splash extends Activity {
	int iSplashTime = 1000;
	Handler exitHandler = null;
	Runnable exitRunnable = null;
	CustomAdapter adapter;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
       // Backup("/data/data/com.simregistration/databases/simregistration",android.os.Environment.getExternalStorageDirectory() + "/Android/data/db");
       exitHandler = new Handler();
    	exitRunnable = new Runnable() {
    		public void run(){
    			
    		exitSplash();
    		}}; 
    		exitHandler.postDelayed(exitRunnable, iSplashTime);}

    private void exitSplash(){
    	/*  Intent i=new Intent(this,com.google.zxing.client.android.CaptureActivity.class);
    	  i.setAction(com.google.zxing.client.android.Intents.Scan.ACTION);
    	  i.putExtra("SCAN_MODE","ONE_D_QRCODE_MODE");
    	  i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    	  startActivityForResult(i,1);*/
    	
    	finish();
    	Intent intent = getIntent();
		startActivity(new Intent(this,Login.class).putExtra("redirect", intent.getStringExtra("redirect")));}
    	
    
   
    public static void Backup(String InputPath,String OutputPath)  { 
    	try{
        //Open your local db as the input stream 
        String inFileName = InputPath; 
        File dbFile = new File(inFileName); 
        FileInputStream fis = new FileInputStream(dbFile); 
        String outFileName =  OutputPath; 
        //Open the empty db as the output stream 
        OutputStream output = new FileOutputStream(outFileName); 
        //transfer bytes from the inputfile to the outputfile 
        byte[] buffer = new byte[1024]; 
        int length; 
        while ((length = fis.read(buffer))>0){ 
            output.write(buffer, 0, length); 
        } 
        //Close the streams 
        output.flush(); 
        output.close(); 
        fis.close();
        
    	}catch(Exception ex){
    		ex.getMessage();
    	}}
    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	   if (requestCode == 0) {
    	      if (resultCode == RESULT_OK) {
    	         String contents = intent.getStringExtra("SCAN_RESULT");
    	         String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
    	         // Handle successful scan
    	      } else if (resultCode == RESULT_CANCELED) {
    	         // Handle cancel
    	      }
    	   }
    	}
 
	
    	 
}
