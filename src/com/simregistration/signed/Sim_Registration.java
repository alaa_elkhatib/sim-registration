package com.simregistration.signed;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;

public class Sim_Registration extends Activity   {
	private Button btn_Logout, btn_CPR,btn_CPRManual,btn_Navy,btn_Passport,btn_PassportManual,btn_CR,btn_GCC
	,btn_MRZ,btn_SaudiNavy;
	//private ImageButton btn_Manual;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sim_activation);
        
        btn_Logout = (Button)this.findViewById(R.id.btn_logout);
        btn_Logout.setVisibility(View.GONE);
       
      /*  btn_Manual = (ImageButton)this.findViewById(R.id.btn_manual);
        btn_Manual.setVisibility(View.VISIBLE);
        btn_Manual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					AssetManager assetManager = getAssets();

			    InputStream in = null;
			    OutputStream out = null;
			    File file = new File(getFilesDir(), "manual.docx");
			    try {
			        in = assetManager.open("manual.docx");
			        out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

			        Utility.copyFile(in, out);
			        in.close();
			        in = null;
			        out.flush();
			        out.close();
			        out = null;
			    } catch (Exception e) {
			        Log.e("tag", e.getMessage());
			    }

			    Intent intent = new Intent(Intent.ACTION_VIEW);
			    intent.setDataAndType(
			            Uri.parse("file://" + getFilesDir() + "/manual.docx"),
			            "application/docx");

			    startActivity(intent);
}catch(Exception ex){
            		ex.getMessage();
            	}}});
        */
        btn_CPR = (Button)this.findViewById(R.id.btn_cpr);
        btn_CPR.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,CameraView.class);
				intent.putExtra("type", 4);
				startActivity(intent);
//				Intent intent = new Intent(Sim_Registration.this,CPR_ID.class);
//				startActivityForResult(intent,1);
			}});
        
        btn_CPRManual = (Button)this.findViewById(R.id.btn_cpr_manual);
        btn_CPRManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,CPRFields.class);
				intent.putExtra("type", 7);
				startActivityForResult(intent,1);
			}});
        
        btn_Navy = (Button)this.findViewById(R.id.btn_navy);
        btn_Navy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,NavyFields.class);
		    	startActivity(intent);
			}});
        
        btn_Passport = (Button)this.findViewById(R.id.btn_passport);
        btn_Passport.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,CameraView_Passport.class);
				intent.putExtra("type", 5);
				startActivity(intent);
//				Intent intent = new Intent(Sim_Registration.this,Passport.class);
//		    	startActivity(intent);
			}});
        
        btn_PassportManual = (Button)this.findViewById(R.id.btn_passport_manual);
        btn_PassportManual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				Intent intent = new Intent(Sim_Registration.this,PassportFields.class);
//		    	startActivity(intent);
				Intent intent = new Intent(Sim_Registration.this,CPRFields.class);
				intent.putExtra("type", 6);
				startActivity(intent);
			}});
        
        
        btn_CR = (Button)this.findViewById(R.id.btn_cr);
        btn_CR.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,CPRFields.class);
				intent.putExtra("type", 8);
				startActivity(intent);
//				Intent intent = new Intent(Sim_Registration.this,CRFields.class);
//		    	startActivity(intent);
			}});
        
        btn_GCC = (Button)this.findViewById(R.id.btn_gcc);
        btn_GCC.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,GCCFields.class);
		    	startActivity(intent);
			}});
        
        btn_SaudiNavy = (Button)this.findViewById(R.id.btn_saudinavy);
        btn_SaudiNavy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,GCCFields.class);
		    	startActivity(intent);
			}});
        
        
        btn_MRZ = (Button)this.findViewById(R.id.btn_mrz);
        btn_MRZ.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Sim_Registration.this,MRZOptions.class);
		    	startActivity(intent);
			}});
        
        if(Utility.GetUserEntity(this).getType().equals("1")){
        	btn_PassportManual.setVisibility(View.GONE);
        	btn_CPRManual.setVisibility(View.GONE); }
        	
       
   }
    

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        //btn_Manual.setVisibility(View.VISIBLE);
    }
	
    @Override
    public void onBackPressed() {
    	 //btn_Manual.setVisibility(View.GONE);
        super.onBackPressed();
    }
    
  }
