package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParserFactory;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ClearableEditText;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.DateArrayAdapter;
import com.simregistration.signed.utility.DateNumericAdapter;
import com.simregistration.signed.utility.HijriCalendar;
import com.simregistration.signed.utility.IntentIntegrator;
import com.simregistration.signed.utility.IntentResult;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.OnWheelChangedListener;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;
import com.simregistration.signed.utility.WheelView;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class GCCFields extends Activity {
    private EditText edt_Name,edt_Sim,edt_ID,edt_Address;
    private Spinner sp_Country,sp_IDType;
    private RadioButton rb_M,rb_F,rb_B,rb_N,rb_Saudi,rb_Navy,rb_Oman;
    private TextView txtBirthDay,txtExpiryDate;
    private int ehYear,ehMonth,ehDay,egYear,egMonth,egDay;
    private int hYear,hMonth,hDay,gYear,gMonth,gDay;
    private Button btn_GCCImage,btn_Scan,btn_Submit;
    public Bitmap passport;
    private Uri cameraTempUri;
    private final int TAKE_CAMERA_REQUEST = 1;
    private final int TAKE_GCC_REQUEST = 2;
    private ArrayList<Countries_Entity> alCountriesDataMain;
    private ArrayList<String> alCountriesDataMainNames;
    CustomAdapter adapter;
    private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private Calendar c;
	private String Bdate="Hijri",Edate="Hijri",imgPath="";
	Button btn_AddICCID;
	private int iEditTextPos=0;
	private ArrayList<EditText> alICCID= new ArrayList<EditText>();
	LinearLayout ln_ICCID;
	TableRow tr_Country;
	//private ImageButton btn_Manual;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gcc_fields); 
        c = Calendar.getInstance();
       /* btn_Manual = (ImageButton)this.findViewById(R.id.btn_manual);
        btn_Manual.setVisibility(View.VISIBLE);
        btn_Manual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					AssetManager assetManager = getAssets();

			    InputStream in = null;
			    OutputStream out = null;
			    File file = new File(getFilesDir(), "manual.docx");
			    try {
			        in = assetManager.open("manual.docx");
			        out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

			        Utility.copyFile(in, out);
			        in.close();
			        in = null;
			        out.flush();
			        out.close();
			        out = null;
			    } catch (Exception e) {
			        Log.e("tag", e.getMessage());
			    }

			    Intent intent = new Intent(Intent.ACTION_VIEW);
			    intent.setDataAndType(
			            Uri.parse("file://" + getFilesDir() + "/manual.docx"),
			            "application/docx");

			    startActivity(intent);
				}catch(Exception ex){
            		ex.getMessage();
            	}}});*/
        ln_ICCID = (LinearLayout)findViewById(R.id.lnSim);
        btn_AddICCID = (Button)findViewById(R.id.btnAddMore);
  	    btn_AddICCID.setOnClickListener(new OnClickListener() {
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onClick(View v) {
				try {
//	      			for( int i=0;i<alICCID.size();i++){
//		      			EditText edt=alICCID.get(i);
//		      			if(edt.getText().toString().trim().equals("")){
//		      				Utility.showToast(GCCFields.this,getResources().getString(R.string.empty_iccid_no_more));
//		      				return;
//		      			}
//	      			}
					LinearLayout ln_more = new LinearLayout(GCCFields.this);
					ln_more.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					ln_more.setOrientation(LinearLayout.HORIZONTAL);
					
					
					LinearLayout.LayoutParams Editparams = new LinearLayout.LayoutParams(
					0, edt_Sim.getHeight());
					Editparams.weight = 0.7f;
					
					LinearLayout.LayoutParams Buttonparams = new LinearLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					final ImageView cancel = new ImageView(GCCFields.this);
//					cancel.setImageResource(R.drawable.cancel);
					final ClearableEditText myEditText = new ClearableEditText(GCCFields.this); // Pass it an Activity or Context
					final int sdk = android.os.Build.VERSION.SDK_INT;
					if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
						myEditText.setBackgroundDrawable( getResources().getDrawable(R.drawable.rounded) );
					} else {
						myEditText.setBackground( getResources().getDrawable(R.drawable.rounded));
					}
					myEditText.setPadding(10, 0, 10, 0);
					myEditText.setRawInputType(InputType.TYPE_CLASS_NUMBER);
					myEditText.setSingleLine(true);
					myEditText.setHint(getResources().getString(R.string.sim));
					
					
					Button myButton = new Button(GCCFields.this); // Pass it an Activity or Context
					myButton.setText(getResources().getString(R.string.scan));
					myButton.setOnClickListener(new View.OnClickListener(){

				      	public void onClick(View v){
				      		try {
				      			List<PackageInfo> packages = getPackageManager().getInstalledPackages(0);
				      			for (PackageInfo packageInfo : packages) {
				      				if(packageInfo.packageName.equals("com.google.zxing.client.android"))
				      			    System.out.println(packageInfo.versionName);
				      			}
				      			iEditTextPos=alICCID.indexOf(myEditText);
				      			IntentIntegrator integrator = new IntentIntegrator(GCCFields.this);
				                integrator.initiateScan();
				      		} catch (Exception ex) {
				      			WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				      			}}});
					myEditText.setLayoutParams(Editparams); 
					myButton.setLayoutParams(Buttonparams); 
					
					  Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.cancel);
					  Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, 50, 50, true);
					  cancel.setPadding(0, 10, 0, 0);
					  cancel.setImageBitmap(bMapScaled);
					  cancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							alICCID.remove(((LinearLayout)arg0.getParent()).getChildAt((((LinearLayout)arg0.getParent()).getChildCount() - 2)));
							((LinearLayout)((LinearLayout)arg0.getParent()).getParent()).removeView((View) arg0.getParent());
						}
					});
					ln_more.addView(cancel);
					ln_more.addView(myEditText);
					ln_more.addView(myButton);
					ln_ICCID.addView(ln_more);
					alICCID.add(myEditText);
					} catch (Exception ex) {
						  }
			}});
  	    final Calendar c = Calendar.getInstance();
	    egYear = c.get(Calendar.YEAR) ;
	    egMonth = c.get(Calendar.MONTH)+1;
	    egDay = c.get(Calendar.DAY_OF_MONTH);
	    gYear = c.get(Calendar.YEAR) ;
	    gMonth = c.get(Calendar.MONTH)+1;
	    gDay = c.get(Calendar.DAY_OF_MONTH);
	    HijriCalendar hijriCalendar =new HijriCalendar(c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH));    
		hDay=hijriCalendar.getHijriDay();
		hMonth=hijriCalendar.getHijriMonth();
		hYear=hijriCalendar.getHijriYear();
		ehDay=hijriCalendar.getHijriDay();
		ehMonth=hijriCalendar.getHijriMonth();
		ehYear=hijriCalendar.getHijriYear();
        dbHelper = new DatabaseHelper(GCCFields.this);
	    db = dbHelper.getWritableDatabase();
        alCountriesDataMain = new ArrayList<Countries_Entity>();
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_Name = (EditText)findViewById(R.id.edtName);
        edt_Sim = (EditText)findViewById(R.id.edtSim);
        alICCID.add(edt_Sim);
        edt_ID = (EditText)findViewById(R.id.edtID);
        sp_Country = (Spinner)findViewById(R.id.spCountry);
        sp_IDType = (Spinner)findViewById(R.id.spIDType);
        sp_IDType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                    	HijriSelected();
                  	    tr_Country.setVisibility(View.GONE);
                    break;
                    case 1:
                    	GregorianSelected();
                    	tr_Country.setVisibility(View.GONE);
                        break;
                    case 2:
                    	GregorianSelected();
                    	tr_Country.setVisibility(View.GONE);
                        break;
                    case 3:
                    	GregorianSelected();
                    	tr_Country.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

               // sometimes you need nothing here
            }
        });
        edt_Address = (EditText)findViewById(R.id.edtAddress);
        txtBirthDay= (TextView)findViewById(R.id.txtBirthDay);
        txtBirthDay.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  					if(sp_IDType.getSelectedItemPosition()==0 ){
  						int Day=0,Month=0,Year=0;
						//if(!Bdate.equals("Hijri")){
						//HijriCalendar hijriCalendar =new HijriCalendar(c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH));    
						//Day=hijriCalendar.getHijriDay();
						//Month=hijriCalendar.getHijriMonth();
						//Year=hijriCalendar.getHijriYear();}					
						//else
							if(Bdate.equals("Hijri")){
							Day=hDay;
    						Month=hMonth;
    						Year=hYear;
						}
						HijriDataPicker(Year,Month, Day,"Bdate");
						
					
					/*AlertDialog.Builder bld = new AlertDialog.Builder(GCCFields.this);
    				bld.setNeutralButton(getString(R.string.hijri), new android.content.DialogInterface.OnClickListener() {

    					@Override
    					public void onClick(DialogInterface dialog, int which) {
    						int Day=0,Month=0,Year=0;
    						if(!Bdate.equals("Hijri")){
    						HijriCalendar hijriCalendar =new HijriCalendar(c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH));    
    						Day=hijriCalendar.getHijriDay();
    						Month=hijriCalendar.getHijriMonth();
    						Year=hijriCalendar.getHijriYear();}					
    						else if(Bdate.equals("Hijri")){
    							Day=hDay;
        						Month=hMonth;
        						Year=hYear;
    						}
    						HijriDataPicker(Year,Month, Day,"Bdate");
    						
    					}});
    				
    				bld.setPositiveButton(getString(R.string.gregorian), new android.content.DialogInterface.OnClickListener() {

    					@Override
    					public void onClick(DialogInterface dialog, int which) {
    						int Day=0,Month=0,Year=0;
    						if(!Bdate.equals("Gregorian")){
    						 Year = c.get(Calendar.YEAR) ;
    					     Month = c.get(Calendar.MONTH)+1;
    						 Day = c.get(Calendar.DAY_OF_MONTH);}					
    						else if(Bdate.equals("Gregorian")){
    							Day=gDay;
        						Month=gMonth;
        						Year=gYear;
    						}
    						GregorianDataPicker(Year,Month, Day,"Bdate");}});	
    				bld.create().show();
  					*/}
  					else {
  						Bdate="Gregorian";
  	  					DatePickerDialog dg = new DatePickerDialog(GCCFields.this, new DatePickerDialog.OnDateSetListener() {
  	  						
  	  						@Override
  	  						public void onDateSet(DatePicker view, int year, 
  	  	                            int monthOfYear, int dayOfMonth) {
  	  						
  	  				          gYear = year;
  	  				          gMonth = monthOfYear+1;
  	  				          gDay = dayOfMonth;
  	  				          
  	  				          txtBirthDay.setText( Integer.toString(gDay)+ "/" + Integer.toString(gMonth) + "/" +
  	  				        		 Integer.toString(gYear) );
  	  				      }

  	  					}, gYear, gMonth-1, gDay);
  	  					
  	  					dg.show();	// Show picker dialog
  						
  					}} catch (Exception activityException) {
    				Log.e("helloandroid dialing example", "Call failed", activityException);}}});
  	  
        txtExpiryDate= (TextView)findViewById(R.id.txtIDExpiry);
        txtExpiryDate.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  					if(sp_IDType.getSelectedItemPosition()==0){
  						int Day=0,Month=0,Year=0;
						//if(!Edate.equals("Hijri")){
						//HijriCalendar hijriCalendar =new HijriCalendar(c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH));    
						//Day=hijriCalendar.getHijriDay();
						//Month=hijriCalendar.getHijriMonth();
						//Year=hijriCalendar.getHijriYear();}					
						//else 
							if(Edate.equals("Hijri")){
							Day=ehDay;
    						Month=ehMonth;
    						Year=ehYear;
						}
						HijriDataPicker(Year,Month, Day,"Edate");
						
					/*
					AlertDialog.Builder bld = new AlertDialog.Builder(GCCFields.this);
    				bld.setNeutralButton(getString(R.string.hijri), new android.content.DialogInterface.OnClickListener() {

    					@Override
    					public void onClick(DialogInterface dialog, int which) {
    						int Day=0,Month=0,Year=0;
    						if(!Edate.equals("Hijri")){
    						HijriCalendar hijriCalendar =new HijriCalendar(c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH));    
    						Day=hijriCalendar.getHijriDay();
    						Month=hijriCalendar.getHijriMonth();
    						Year=hijriCalendar.getHijriYear();}					
    						else if(Edate.equals("Hijri")){
    							Day=ehDay;
        						Month=ehMonth;
        						Year=ehYear;
    						}
    						HijriDataPicker(Year,Month, Day,"Edate");
    						
    					}});
    				
    				bld.setPositiveButton(getString(R.string.gregorian), new android.content.DialogInterface.OnClickListener() {

    					@Override
    					public void onClick(DialogInterface dialog, int which) {
    						int Day=0,Month=0,Year=0;
    						if(!Edate.equals("Gregorian")){
    						 Year = c.get(Calendar.YEAR) ;
    					     Month = c.get(Calendar.MONTH)+1;
    						 Day = c.get(Calendar.DAY_OF_MONTH);}					
    						else if(Edate.equals("Gregorian")){
    							Day=egDay;
        						Month=egMonth;
        						Year=egYear;
    						}
    						GregorianDataPicker(Year,Month, Day,"Edate");}});	
    				bld.create().show();
  					*/}
  					else {
  						Edate="Gregorian";
  	  					DatePickerDialog dg = new DatePickerDialog(GCCFields.this, new DatePickerDialog.OnDateSetListener() {
  	  						
  	  						@Override
  	  						public void onDateSet(DatePicker view, int year, 
  	  	                            int monthOfYear, int dayOfMonth) {
  	  						
  	  				          egYear = year;
  	  				          egMonth = monthOfYear+1;
  	  				          egDay = dayOfMonth;
  	  				          
  	  				        txtExpiryDate.setText( Integer.toString(egDay)+ "/" + Integer.toString(egMonth) + "/" +
  	  				        		 Integer.toString(egYear) );
  	  				      }

  	  					}, egYear, egMonth-1, egDay);
  	  					
  	  					dg.show();	// Show picker dialog
  						
  						
  					}
  					} catch (Exception activityException) {
    				Log.e("helloandroid dialing example", "Call failed", activityException);}}});
        rb_M = (RadioButton)findViewById(R.id.rb_M);
  	    rb_F= (RadioButton)findViewById(R.id.rb_F);
  	    rb_B = (RadioButton)findViewById(R.id.rb_B);
  	    rb_N= (RadioButton)findViewById(R.id.rb_N);
  	    rb_Saudi = (RadioButton)findViewById(R.id.rb_Saudi);
  	    rb_Saudi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if(isChecked) {
            	   Edate="Hijri";
            	   Bdate="Hijri";
            	  tr_Country.setVisibility(View.GONE);
                  if(!txtBirthDay.getText().toString().equals("")){
                	HijriCalendar hijriCalendar =new HijriCalendar(gYear,gMonth,gDay);    
                	hDay=hijriCalendar.getHijriDay();
      				hMonth=hijriCalendar.getHijriMonth();
      				hYear=hijriCalendar.getHijriYear();
      				txtBirthDay.setText( Integer.toString(hDay)+ "/" + Integer.toString(hMonth) + "/" +
			        		 Integer.toString(hYear) );
			      }
                  if(!txtExpiryDate.getText().toString().equals("")){
                  	HijriCalendar hijriCalendar =new HijriCalendar(egYear,egMonth,egDay);    
                  		ehDay=hijriCalendar.getHijriDay();
        				ehMonth=hijriCalendar.getHijriMonth();
        				ehYear=hijriCalendar.getHijriYear();
        				txtExpiryDate.setText( Integer.toString(ehDay)+ "/" + Integer.toString(ehMonth) + "/" +
  			        		 Integer.toString(ehYear) );
  			      }
             
              } }});
  	    
	    rb_Navy= (RadioButton)findViewById(R.id.rb_Navy);
	    rb_Navy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if(isChecked) {
            	  Bdate="Gregorian";
            	  Edate="Gregorian";
            	  tr_Country.setVisibility(View.VISIBLE);
              	  if(!txtBirthDay.getText().toString().equals("")){
            	  int[]  date=Utility.islToChr(hYear, hMonth, hDay, 2);
            	  gYear = date[2];
			      gMonth =date[1];
			      gDay = date[0]+1;
			      txtBirthDay.setText( Integer.toString(gDay)+ "/" + Integer.toString(gMonth) + "/" +
			        		 Integer.toString(gYear) );
			      }
              	if(!txtExpiryDate.getText().toString().equals("")){
              	  int[]  date=Utility.islToChr(ehYear, ehMonth, ehDay, 2);
              	  egYear = date[2];
  			      egMonth =date[1];
  			      egDay = date[0]+1;
  			      txtExpiryDate.setText( Integer.toString(egDay)+ "/" + Integer.toString(egMonth) + "/" +
  			        		 Integer.toString(egYear) );
  			      }
              
            }}});
	    
	    rb_Oman= (RadioButton)findViewById(R.id.rb_Oman);
	    rb_Oman.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if(isChecked) {
            	  Bdate="Gregorian";
            	  Edate="Gregorian";
            	  tr_Country.setVisibility(View.GONE);
              	  if(!txtBirthDay.getText().toString().equals("")){
            	  int[]  date=Utility.islToChr(hYear, hMonth, hDay, 2);
            	  gYear = date[2];
			      gMonth =date[1];
			      gDay = date[0]+1;
			      txtBirthDay.setText( Integer.toString(gDay)+ "/" + Integer.toString(gMonth) + "/" +
			        		 Integer.toString(gYear) );
			      }
              	if(!txtExpiryDate.getText().toString().equals("")){
              	  int[]  date=Utility.islToChr(ehYear, ehMonth, ehDay, 2);
              	  egYear = date[2];
  			      egMonth =date[1];
  			      egDay = date[0]+1;
  			      txtExpiryDate.setText( Integer.toString(egDay)+ "/" + Integer.toString(egMonth) + "/" +
  			        		 Integer.toString(egYear) );
  			      }
              
            }}});
	    
	    tr_Country = (TableRow)this.findViewById(R.id.trCountry);
  	    btn_Submit = (Button)this.findViewById(R.id.btnSubmit);
  	    btn_Submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
						int iICCID=0;
		      			for( int i=0;i<alICCID.size();i++){
		      			EditText edt=alICCID.get(i);
		      			if(!edt.getText().toString().trim().equals("")){
		      				iICCID++;
		      				for(int y =i+1;y<alICCID.size();y++){
		      					if(edt.getText().toString().trim().equals(alICCID.get(y).getText().toString().trim())){
		      						Utility.showToast(GCCFields.this,getResources().getString(R.string.dublicate_icid)+" "+(i+1)+" , "+(y+1));
				      				return;
		      					}
		      				}
		      			}else{
		      				Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_iccid));
		      				return;
		      			}
		      				
		      			}
		      			if(edt_Name.getText().toString().equals("")){
						  Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_name));
		      			}else if (edt_Sim.getText().toString().equals("")){
						  Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_iccid));
		      			}else if(edt_ID.getText().toString().equals("")){
					   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_idnumber));	
		      			}else if(sp_Country.getSelectedItemPosition()==0 && sp_IDType.getSelectedItemPosition()==3  ){
						   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_country));	
					
		      			} else if(txtBirthDay.getText().toString().equals("")){
						   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_Birthday));
				
					  
		      			}else if(txtExpiryDate.getText().toString().equals("")){
						   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_idexpiry));
					  
		      			}else if((sp_IDType.getSelectedItemPosition()!=0 || Bdate.equals("Gregorian")) &&  getDiffYears(gYear,gMonth,gDay)<C.AGE){	  
						 Utility.showToast(GCCFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  
		      			}else if(Bdate.equals("Hijri") && sp_IDType.getSelectedItemPosition()==0  && !CheckHijriDate()){
							  Utility.showToast(GCCFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  
					}else if((sp_IDType.getSelectedItemPosition()!=0 || Edate.equals("Gregorian")) &&   IsExpired(txtExpiryDate.getText().toString())){	  
						Utility.showToast(GCCFields.this,getResources().getString(R.string.id_expired));
					
					}else if(Edate.equals("Hijri") && sp_IDType.getSelectedItemPosition()==0 &&  IsExpiredHijriDate()){
						Utility.showToast(GCCFields.this,getResources().getString(R.string.id_expired));
		      		  
					  
					}else{
						  Utility.alert(getResources().getString(R.string.iccid_total)+ iICCID, GCCFields.this,   new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									String strDocument="";
									  if(sp_IDType.getSelectedItemPosition()!=3 )
										  strDocument="GCC";
						              else
						                	 strDocument="Navy";
	  
									 Passport_Navy_Entity entity= new Passport_Navy_Entity();
					                 if(rb_M.isChecked())
					              	   entity.setGender("M");
					                 else if(rb_F.isChecked())
					              	   entity.setGender("F");
					                 
					              //if(rb_B.isChecked())
					              //	   entity.setNationality("B");
					               //  else  if(rb_N.isChecked())
					              	  entity.setNationality("N");
					                  entity.setName(edt_Name.getText().toString());
					                  String isdns = "";
					                  for( int i=0;i<alICCID.size();i++){
							      			EditText edt=alICCID.get(i);
							      			isdns+=edt.getText().toString().trim();
							      			if(i < alICCID.size() - 1)
							      				isdns += ";";
							      	 }
					                  entity.setMSISDN(isdns);
//					                  entity.setMSISDN(edt_Sim.getText().toString());
					                  entity.setId(edt_ID.getText().toString());
					                  if(sp_IDType.getSelectedItemPosition()==0 )
					                 	 entity.setCountry("SA");
					                  else if(sp_IDType.getSelectedItemPosition()==1 )
						                 	 entity.setCountry("OM");
					                  
					                  else if(sp_IDType.getSelectedItemPosition()==2 )
						                 	 entity.setCountry("QA");
					                
					                 else
					                  entity.setCountry(alCountriesDataMain.get(sp_Country.getSelectedItemPosition()-1).getID());
					                  entity.setAddress(edt_Address.getText().toString());
					                  String strBirthDate="",strExpityDate=""; 
					                  if(Bdate.equals("Gregorian") || sp_IDType.getSelectedItemPosition()!=0   ){
					                  if(!txtBirthDay.getText().toString().equals("")){
					                   	strBirthDate+=(gYear);
					                   if((gMonth)<=9)
					                   	strBirthDate+="0";
					                   strBirthDate+=(gMonth );
					                   if(gDay<=9)
					                   	strBirthDate+="0";
					                   strBirthDate+=(gDay);
					                   entity.setBirthDay(strBirthDate);}}
					                  
					                  else if(Bdate.equals("Hijri")){
					                      if(!txtBirthDay.getText().toString().equals("")){
					                   	   int[]  date=Utility.islToChr(hYear, hMonth, hDay, 2);
					                       	strBirthDate+=(date[2]);
					                       if((date[1])<=9)
					                       	strBirthDate+="0";
					                       strBirthDate+=(date[1] );
					                       if(date[0]<=9)
					                       	strBirthDate+="0";
					                       strBirthDate+=(date[0]);
					                       entity.setBirthDay(strBirthDate);}}
					                  
					                  
					                  if(Edate.equals("Gregorian")|| sp_IDType.getSelectedItemPosition()!=0  ){
						                  if(!txtExpiryDate.getText().toString().equals("")){
						                	  strExpityDate+=(egYear);
						                   if((egMonth)<=9)
						                   strExpityDate+="0";
						                   strExpityDate+=(egMonth );
						                   if(egDay<=9)
						                	strExpityDate+="0";
						                   strExpityDate+=(egDay);
						                   entity.setExpiryDate(strExpityDate);}}
						                  
					                  else if(Edate.equals("Hijri")){
						                      if(!txtExpiryDate.getText().toString().equals("")){
						                   	   int[]  date=Utility.islToChr(ehYear, ehMonth, ehDay, 2);
						                   	   strExpityDate+=(date[2]);
						                       if((date[1])<=9)
						                    	strExpityDate+="0";
						                        strExpityDate+=(date[1] );
						                       if(date[0]<=9)
						                    	strExpityDate+="0";
						                       strExpityDate+=(date[0]);
						                       entity.setExpiryDate(strExpityDate);}}
						                  
					                  Intent intent = new Intent(GCCFields.this, CPR_GCC_NavyFields_Form.class);
					                  intent.putExtra("entity", entity);
					                  intent.putExtra("document", strDocument);
					                  startActivityForResult(intent, TAKE_GCC_REQUEST);
					              }});

					  }} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
							
						 // if(Utility.isConnected(GCCFields.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   	//else
			      	   //		Utility.showToast(GCCFields.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); 
  	  /*  btn_GCCImage = (Button)this.findViewById(R.id.btnGCC);
  	    btn_GCCImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					int iICCID=0;
	      			for( int i=0;i<alICCID.size();i++){
	      			EditText edt=alICCID.get(i);
	      			if(!edt.getText().toString().trim().equals("")){
	      				iICCID++;
	      				for(int y =i+1;y<alICCID.size();y++){
	      					if(edt.getText().toString().trim().equals(alICCID.get(y).getText().toString().trim())){
	      						Utility.showToast(GCCFields.this,getResources().getString(R.string.dublicate_icid)+" "+(i+1)+" , "+(y+1));
			      				return;
	      					}
	      				}
	      			}else{
	      				Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_iccid));
	      				return;
	      			}
	      				
	      			}
					 
					  if(edt_Name.getText().toString().equals(""))
						  Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_name));
					  else if (edt_Sim.getText().toString().equals(""))
						  Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_iccid));
					  else if(edt_ID.getText().toString().equals(""))
					   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_idnumber));	
					  else if(sp_Country.getSelectedItemPosition()==0)
						   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_country));	
					
					  else if(txtBirthDay.getText().toString().equals(""))
						   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_Birthday));
				
					  
					  else if(txtExpiryDate.getText().toString().equals(""))
						   Utility.showToast(GCCFields.this,getResources().getString(R.string.enter_idexpiry));
					  
						  
					else if(Bdate.equals("Hijri") && !CheckHijriDate())
							  Utility.showToast(GCCFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  
					else if(Bdate.equals("Gregorian") && !txtBirthDay.getText().toString().equals("") && getDiffYears(gYear,gMonth,gDay)<C.AGE)
								  Utility.showToast(GCCFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  
					  else if(IsExpired(txtExpiryDate.getText().toString()))
						  Utility.showToast(GCCFields.this,getResources().getString(R.string.id_expired));
					  else{
						  Utility.alert(getResources().getString(R.string.iccid_total)+ iICCID, GCCFields.this,   new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
									  intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
						              startActivityForResult(intent, TAKE_CAMERA_REQUEST);   
								}});
						  
						
			                
			                
					  }} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
							
						  //if(Utility.isConnected(GCCFields.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   //	else
			      	   //		Utility.showToast(GCCFields.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); */
  	  btn_Scan = (Button)findViewById(R.id.btnScan);
  	  btn_Scan.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		try {
	      			iEditTextPos=0;
	      			IntentIntegrator integrator = new IntentIntegrator(GCCFields.this);
	                integrator.initiateScan();
	                //startActivityForResult(integrator, 1);

	      		} catch (Exception ex) {
	      			WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
	      			//if(Utility.isConnected(GCCFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	      		}
	      	}
	      });
  	  new FetchCountriesData().execute();}
    
    
    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/GCC/", "image" + new Date().getTime() + ".png");
        File parent = file.getParentFile();
		if (parent != null) parent.mkdirs();
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
    
    
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case TAKE_CAMERA_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
            		//new  ResizeImage().execute(new String[]{imgPath});
            		}
            break;
            case TAKE_GCC_REQUEST:
                if (resultCode == RESULT_OK) {
        			setResult(RESULT_OK, data);
      				finish();
        		}
                break;
            case 49374:
            	if (resultCode == Activity.RESULT_OK) {
	                   IntentResult intentResult = 
	                   IntentIntegrator.parseActivityResult(requestCode, resultCode,data);
	                if (intentResult != null) {
	                	alICCID.get(iEditTextPos).setText(intentResult.getContents());
	                }
//	                	edt_Sim.setText(intentResult.getContents());} 
	             }
           
            	break;
           default:
               break;
        } }
    

	private class FetchCountriesData extends AsyncTask<Void, Void, Void> {
		ArrayList<Countries_Entity> alColuntriesData = new ArrayList<Countries_Entity>();
		ArrayList<String> alColuntriesDataNames = new ArrayList<String>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getCountriesContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					Countries_Entity cursorEntity = new Countries_Entity();
					cursorEntity.setID(crsr.getString(0).trim());
					cursorEntity.setNameEng(crsr.getString(1));
					//cursorEntity.setNameAr(crsr.getString(2));
					if(cursorEntity.getID().equals("US")){
					alColuntriesData.add(0,cursorEntity);
					alColuntriesDataNames.add(0,crsr.getString(1));}
					else {
						alColuntriesData.add(cursorEntity);
						alColuntriesDataNames.add(crsr.getString(1));
					}
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
					//if(Utility.isConnected(GCCFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			if (alColuntriesData != null && alColuntriesData.size() > 0) {
				alCountriesDataMain = alColuntriesData;
				alCountriesDataMainNames=alColuntriesDataNames;
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(GCCFields.this,
						android.R.layout.simple_spinner_item, alCountriesDataMainNames);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_Country.setAdapter(
					      new NothingSelectedSpinnerAdapter(
					    		dataAdapter,
					            R.layout.country_spinner_row_nothing_selected,
					            GCCFields.this));
			      
			} }
	}
	
	
	
	/*private class ResizeImage extends AsyncTask<String, Void, Bitmap> {
		ProgressDialog progress;
		@Override
		protected void onPreExecute() {
		progress = ProgressDialog.show(GCCFields.this, getString(R.string.pleaseWait), "", true, false, null);
			super.onPreExecute();}

		@Override
		protected Bitmap doInBackground(String... params) {
			try {
				
				 Bitmap bitmap=WebService.decodeFile(1024,1024,imgPath);
        		 new File(imgPath).delete();
        		 return bitmap;
        		 }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);
			 if (bitmap != null) { 
               Passport_Navy_Entity entity= new Passport_Navy_Entity();
               if(rb_M.isChecked())
            	   entity.setGender("M");
               else if(rb_F.isChecked())
            	   entity.setGender("F");
               
               if(rb_B.isChecked())
            	   entity.setNationality("B");
               else  if(rb_N.isChecked())
            	   entity.setNationality("N");
                entity.setName(edt_Name.getText().toString());
                String isdns = "";
                for( int i=0;i<alICCID.size();i++){
		      			EditText edt=alICCID.get(i);
		      			isdns+=edt.getText().toString().trim();
		      			if(i < alICCID.size() - 1)
		      				isdns += ";";
		      	 }
                entity.setMSISDN(isdns);
//                entity.setMSISDN(edt_Sim.getText().toString());
                entity.setId(edt_ID.getText().toString());
                if(sp_Country.getSelectedItemPosition()==0)
               	 entity.setCountry("");
               else
                entity.setCountry(alCountriesDataMain.get(sp_Country.getSelectedItemPosition()-1).getID());
                entity.setAddress(edt_Address.getText().toString());
                String strBirthDate="",strExpityDate=""; 
               if(Bdate.equals("Gregorian")){
               if(!txtBirthDay.getText().toString().equals("")){
                	strBirthDate+=(gYear);
                if((gMonth)<=9)
                	strBirthDate+="0";
                strBirthDate+=(gMonth );
                if(gDay<=9)
                	strBirthDate+="0";
                strBirthDate+=(gDay);
                entity.setBirthDay(strBirthDate);}}
               
               if(Bdate.equals("Hijri")){
                   if(!txtBirthDay.getText().toString().equals("")){
                	   int[]  date=Utility.islToChr(hYear, hMonth, hDay, 2);
                    	strBirthDate+=(date[2]);
                    if((date[1])<=9)
                    	strBirthDate+="0";
                    strBirthDate+=(date[1] );
                    if(date[0]<=9)
                    	strBirthDate+="0";
                    strBirthDate+=(date[0]);
                    entity.setBirthDay(strBirthDate);}}
                
                if(!txtExpiryDate.getText().toString().equals("")){
                	strExpityDate+=(eYear);
                if((eMonth)<=9)
                	strExpityDate+="0";
                strExpityDate+=(eMonth );
                if(eDay<=9)
                	strExpityDate+="0";
                strExpityDate+=(eDay);
                entity.setExpiryDate(strExpityDate);}
                
                Intent intent = new Intent(GCCFields.this, Passport_Navy_CR.class);
                intent.putExtra("Passport_Navy_CR", entity);
                try {
                    //Write file
                    String filename = "bitmap.png";
                    FileOutputStream stream = GCCFields.this.openFileOutput(filename, Context.MODE_PRIVATE);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    stream.close();
                    bitmap.recycle();
                    intent.putExtra("image", filename);
                    intent.putExtra("document", "GCC");
                } catch (Exception ex) {
                	WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}
               startActivityForResult(intent, TAKE_GCC_REQUEST);
            }

			progress.dismiss();
			 }
	}*/
	
	
	
	
	/*private Cursor getCountriesGCCContentsCursor() {
		Cursor crsr = null;
		try {
			String[] from = { C.ID,C.NAME_ENG,C.NAME_AR};
			crsr = db.query(C.COUNTRIES_TABLE, from,C.NAME_ENG +" = 'United Arab Emirates' OR "+ C.NAME_ENG+" = 'Kuwait'  OR "+ C.NAME_ENG+" = 'Saudi Arabia'  OR " + C.NAME_ENG+" ='Oman'  OR "  + C.NAME_ENG+" ='Qatar'   "   , null, null, null,C.NAME_ENG+" ASC");
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
			//if(Utility.isConnected(GCCFields.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}*/
	
	private Cursor getCountriesContentsCursor() {
		Cursor crsr = null;
		try {
			String[] from = { C.ID,C.NAME};
			crsr = db.query(C.COUNTRIES_TABLE, from,null, null, null, null,C.NAME+" ASC");
		} catch (Exception ex) {
			 WebService.SaveErrorLogNoBandwidth(GCCFields.this,Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				
			//if(Utility.isConnected(GCCFields.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(GCCFields.this).getId(),GCCFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
	
	public static int getDiffYears(int _year, int _month, int _day) {
	    SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
	    java.util.Date d = null;
	    java.util.Date d1 = null;
	    Calendar cal = Calendar.getInstance();
	    try {
	            d = dfDate.parse(_day+"/"+_month+"/"+_year);
	            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
	        } catch (java.text.ParseException e) {
	            e.printStackTrace();
	        }
    Calendar a = getCalendar(d);
    Calendar b = getCalendar(d1);
    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
    if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
        diff--;
    }
    return diff;
}

public static Calendar getCalendar(Date date) {
    Calendar cal = Calendar.getInstance(Locale.US);
    cal.setTime(date);
    return cal;
}
 public int getAge (int _year, int _month, int _day) {
		 
		 SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
		    java.util.Date d = null;
		    java.util.Date d1 = null;
		    Calendar cal = Calendar.getInstance();
		    try {
		            d = dfDate.parse(_day+"/"+_month+"/"+_year);
		            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
		        } catch (java.text.ParseException e) {
		            e.printStackTrace();
		        }

		    int diffInDays = (int) ((d1.getTime() - d.getTime())/ (1000 * 60 * 60 * 24));

		    
	        return diffInDays;
	    }
	/* public int getAge (int _year, int _month, int _day) {

	        GregorianCalendar cal = new GregorianCalendar();
	        int y, m, d, a;         

	        y = cal.get(Calendar.YEAR);
	        m = cal.get(Calendar.MONTH);
	        d = cal.get(Calendar.DAY_OF_MONTH);
	        cal.set(_year, _month, _day);
	        a = y - cal.get(Calendar.YEAR);
	        if ((m < cal.get(Calendar.MONTH))
	                        || ((m == cal.get(Calendar.MONTH)) && (d < cal
	                                        .get(Calendar.DAY_OF_MONTH)))) {
	                --a;
	        }
	       
	        return a;
	    }*/
	public boolean IsExpired(String valid_until){ 
	 boolean expired =false;
	 try{ 
		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		 String strToday = sdf.format(new Date()); 
		 
		 Date strExpiryDate = sdf.parse(valid_until);
		 Date strDate = sdf.parse(strToday);
		 if (strExpiryDate.compareTo(strDate)<0) {
			 expired = true;
		 }
		 
	}catch(Exception ex){
		 ex.getMessage();
	 }
	return expired;}
	
	public void HijriDataPicker(int  hijri_year,int  hijri_month, int hijri_day,final String strType){
		int sel_year = hijri_year ;
		int sel_month =  hijri_month-1;
		final int sel_day = hijri_day ;
		final int minimum_year = 623;
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = this.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.date_layout, null);
		dialogBuilder.setView(dialogView);
        Calendar calendar = Calendar.getInstance();
        final WheelView month = (WheelView) dialogView.findViewById(R.id.month);
	    final WheelView year = (WheelView) dialogView.findViewById(R.id.year);
	    final WheelView day = (WheelView) dialogView.findViewById(R.id.day);
	    final TextView date = (TextView) dialogView.findViewById(R.id.txtDate);
	    
	    String months[] =  new String[] {"1", "2", "3", "4","5", "6", "7", "8","9", "10", "11", "12"};
	    OnWheelChangedListener listener = new OnWheelChangedListener() {
	        
			@Override
			public void onChanged(WheelView wheel, int oldValue) {
				  updateHijriDays(year, month.getCurrentItem()+1, day,sel_day);
				  date.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
				 
				
			}
        };

	 
        month.setViewAdapter(new DateArrayAdapter(this, months, sel_month));
        month.setCurrentItem(sel_month);
        month.addChangingListener(listener);
    
        // year
        int curYear = sel_year;
        year.setViewAdapter(new DateNumericAdapter(this, minimum_year, 2500, curYear-minimum_year));
        year.setCurrentItem(curYear-minimum_year);
        year.addChangingListener(listener);
        
        
        //day
        day.setViewAdapter(new DateNumericAdapter(this, 1, 31,sel_day));
        day.setCurrentItem(sel_day);
        day.addChangingListener(listener);
        date.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
        dialogBuilder.setNegativeButton("Cancel", new android.content.DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {}
		});
		
        dialogBuilder.setPositiveButton("Ok", new android.content.DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			//hYear,hMonth,hDay
			if(strType.equals("Bdate")){
			hYear=year.getCurrentItem()+minimum_year;
			hMonth= month.getCurrentItem()+1;
			hDay=day.getCurrentItem()+1;
			Bdate="Hijri";
			txtBirthDay.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
			}
			else {
				ehYear=year.getCurrentItem()+minimum_year;
				ehMonth= month.getCurrentItem()+1;
				ehDay=day.getCurrentItem()+1;
				Edate="Hijri";
				txtExpiryDate.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
					
			}}
		});  
	        
		AlertDialog alertDialog = dialogBuilder.create();
		alertDialog.show();
		
	}

	
	
	public void GregorianDataPicker(int  hijri_year,int  hijri_month, int hijri_day,final String strType){
		int sel_year = hijri_year ;
		int sel_month =  hijri_month-1;
		final int sel_day = hijri_day-1 ;
		final int minimum_year = 623;
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = this.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.date_layout, null);
		dialogBuilder.setView(dialogView);
        Calendar calendar = Calendar.getInstance();
        final WheelView month = (WheelView) dialogView.findViewById(R.id.month);
	    final WheelView year = (WheelView) dialogView.findViewById(R.id.year);
	    final WheelView day = (WheelView) dialogView.findViewById(R.id.day);
	    final TextView date = (TextView) dialogView.findViewById(R.id.txtDate);
	    
	    String months[] =  new String[] {"Jan", "Feb", "Mar", "Apr","May", "Jun", "Jul", "Aug","Sep", "Oct", "Nov", "Dec"};
	    OnWheelChangedListener listener = new OnWheelChangedListener() {
	        
			@Override
			public void onChanged(WheelView wheel, int oldValue) {
				  updateDays(year, month, day,sel_day);
				  date.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
				 
				
			}
        };

	 
        month.setViewAdapter(new DateArrayAdapter(this, months, sel_month));
        month.setCurrentItem(sel_month);
        month.addChangingListener(listener);
    
        // year
        int curYear = sel_year;
        year.setViewAdapter(new DateNumericAdapter(this, minimum_year, 2500, curYear-minimum_year));
        year.setCurrentItem(curYear-minimum_year);
        year.addChangingListener(listener);
        
        
        //day
        day.setViewAdapter(new DateNumericAdapter(this, 1, 31,sel_day));
        day.setCurrentItem(sel_day);
        day.addChangingListener(listener);
        date.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
        dialogBuilder.setNegativeButton("Cancel", new android.content.DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {}
		});
		
        dialogBuilder.setPositiveButton("Ok", new android.content.DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			//hYear,hMonth,hDay
			if(strType.equals("Bdate")){
			gYear=year.getCurrentItem()+minimum_year;
			gMonth= month.getCurrentItem()+1;
			gDay=day.getCurrentItem()+1;
			Bdate="Gregorian";
			txtBirthDay.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
			}
			else {
				egYear=year.getCurrentItem()+minimum_year;
				egMonth= month.getCurrentItem()+1;
				egDay=day.getCurrentItem()+1;
				Edate="Gregorian";
				txtExpiryDate.setText(Integer.toString(day.getCurrentItem()+1)+"/"+ Integer.toString( month.getCurrentItem()+1)+"/"+Integer.toString(year.getCurrentItem()+minimum_year));
				}
			}
		});  
	        
		AlertDialog alertDialog = dialogBuilder.create();
		alertDialog.show();
		
	}
	
	
	 void updateDays(WheelView year, WheelView month, WheelView day, int sel_day) {
	        Calendar calendar = Calendar.getInstance();
	        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + year.getCurrentItem());
	        calendar.set(Calendar.MONTH, month.getCurrentItem());
	        
	        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	        day.setViewAdapter(new DateNumericAdapter(this, 1, maxDays, sel_day));
	        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
	        day.setCurrentItem(curDay - 1, true);
	    }
	 
	 
	 void updateHijriDays(WheelView year, int month, WheelView day, int sel_day) {
		 int maxDays=0;
		    if(month==12 || month==10 || month==8 || month==6 || month==4 || month==2)
		    	  maxDays=30;
		    else
		    	 maxDays=29;
	        day.setViewAdapter(new DateNumericAdapter(this, 1, maxDays, sel_day));
	        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
	        day.setCurrentItem(curDay - 1, true);
	    }
	 
	 public boolean CheckHijriDate(){
		 boolean above=true;
		 int[] date =null;
		  if(Bdate.equals("Hijri")){
			  date=Utility.islToChr(hYear, hMonth, hDay, 2);
			  
		  if(Bdate.equals("Hijri") && !txtBirthDay.getText().toString().equals("") && getDiffYears(date[2], date[1], date[0])<C.AGE){
				  Utility.showToast(GCCFields.this,getResources().getString(R.string.age_above)+" "+C.AGE);
				  above=false;}}
		return above;
		 
	}
	 
	 public boolean IsExpiredHijriDate(){
		 boolean expired =false;
		 int[] date=null;
		 try{ 
			 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			 String strToday = sdf.format(new Date()); 
			 date=Utility.islToChr(ehYear, ehMonth, ehDay, 2);
			 Date strExpiryDate =sdf.parse(date[0]+"/"+date[1]+"/"+date[2]);
			 Date strDate = sdf.parse(strToday);
			 if (strExpiryDate.compareTo(strDate)<0) {
				 expired = true;
			 }
			 
		}catch(Exception ex){
			 ex.getMessage();
		 }
		return expired;
		
		 
	}
	  @Override
	    public void onResume() {
	        super.onResume();  // Always call the superclass method first
	       // btn_Manual.setVisibility(View.VISIBLE);
	    }
	  
	  
	  private void HijriSelected(){
	   Edate="Hijri";
   	   Bdate="Hijri";
         if(!txtBirthDay.getText().toString().equals("")){
       	HijriCalendar hijriCalendar =new HijriCalendar(gYear,gMonth,gDay);    
       	hDay=hijriCalendar.getHijriDay();
				hMonth=hijriCalendar.getHijriMonth();
				hYear=hijriCalendar.getHijriYear();
				txtBirthDay.setText( Integer.toString(hDay)+ "/" + Integer.toString(hMonth) + "/" +
	        		 Integer.toString(hYear) );
	      }
         if(!txtExpiryDate.getText().toString().equals("")){
         	HijriCalendar hijriCalendar =new HijriCalendar(egYear,egMonth,egDay);    
         		ehDay=hijriCalendar.getHijriDay();
				ehMonth=hijriCalendar.getHijriMonth();
				ehYear=hijriCalendar.getHijriYear();
				txtExpiryDate.setText( Integer.toString(ehDay)+ "/" + Integer.toString(ehMonth) + "/" +
		        		 Integer.toString(ehYear) );
		      } }
	  
	  private void GregorianSelected(){
		  Bdate="Gregorian";
    	  Edate="Gregorian";
      	  if(!txtBirthDay.getText().toString().equals("")){
    	  int[]  date=Utility.islToChr(hYear, hMonth, hDay, 2);
    	  gYear = date[2];
	      gMonth =date[1];
	      gDay = date[0]+1;
	      txtBirthDay.setText( Integer.toString(gDay)+ "/" + Integer.toString(gMonth) + "/" +
	        		 Integer.toString(gYear) );
	      }
      	if(!txtExpiryDate.getText().toString().equals("")){
      	  int[]  date=Utility.islToChr(ehYear, ehMonth, ehDay, 2);
      	  egYear = date[2];
		      egMonth =date[1];
		      egDay = date[0]+1;
		      txtExpiryDate.setText( Integer.toString(egDay)+ "/" + Integer.toString(egMonth) + "/" +
		        		 Integer.toString(egYear) );
		      }
      
  }
    }
