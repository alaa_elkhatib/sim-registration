package com.simregistration.signed;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.jsoup.Jsoup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.ErrorLog_Entity;
import com.simregistration.signed.utility.AsyncTask;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Home extends Activity  implements
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener {
	private Button btn_Logout,btn_Language, btn_SimRegistration,btn_Bundles,btn_TopUp,btn_Options,btn_Offers;
	double latitude,longitude,iLat=0.0,iLng=0.0 ;	
	String strLocation="";
	private LocationClient mLocationClient;
	private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private TextView tv_Version;
	 private ProgressDialog progress = null;	
	//public static ConnectionClassManager mConnectionClassManager;
	//public static DeviceBandwidthSampler mDeviceBandwidthSampler;
	//public static ConnectionQuality mConnectionClass = ConnectionQuality.UNKNOWN;
	//public static ConnectionClassStateChangeListener mListener;

	private  MyPhoneStateListener myListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        dbHelper = new DatabaseHelper(Home.this);
		db = dbHelper.getWritableDatabase();
		updater = new ContentUpdater(Home.this, db);
        Intent regIntent = new Intent("com.google.android.c2dm.intent.REGISTER");   
    	regIntent.putExtra("app", PendingIntent.getBroadcast(Home.this, 0, new Intent(), 0));        // Identify role account server will use to send     
    	regIntent.putExtra("sender", "500925520490");  
        startService(regIntent);
         mLocationClient = new LocationClient(this, this, this);	
        mLocationClient.connect();
        myListener = new MyPhoneStateListener(this);
        TelephonyManager telManager  = ( TelephonyManager )getSystemService(Context.TELEPHONY_SERVICE);
		telManager.listen(myListener ,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
      
       // mConnectionClassManager = ConnectionClassManager.getInstance();
       // mDeviceBandwidthSampler = DeviceBandwidthSampler.getInstance();
       // mListener = new ConnectionChangedListener();

    	tv_Version = (TextView)this.findViewById(R.id.tv_version);
    	tv_Version.setText("Version Name: "+GetVersion());
	    btn_Logout = (Button)this.findViewById(R.id.btn_logout);
        btn_Logout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Utility.isConnected(Home.this))
					 new AuditTrailLoader().execute(new Void[] {null});
				//if(Utility.GetUserEntity(SimRegistration.this).getAuto()==0)
				updater.deleteOldContentsUser();
			    Utility.ClearShared(Home.this);
				Intent intent = new Intent(Home.this,Login.class);
				//intent.putExtra("logout", 1);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		    	startActivity(intent);}});
        
       /* btn_Language = (Button)this.findViewById(R.id.btn_language);
        btn_Language.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(C.LANGUAGE.equals("en"))
					C.SetLanguage(SimRegistration.this,"ar");
				else
					C.SetLanguage(SimRegistration.this,"en");
				
				btn_Logout.setText(getString(R.string.logout));
				btn_Language.setText(getString(R.string.language));}});
        */

       
        btn_SimRegistration = (Button)this.findViewById(R.id.btn_simregistration);
        btn_SimRegistration.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Home.this,Sim_Registration.class);
		    	startActivity(intent);
			}});
        
        btn_Bundles = (Button)this.findViewById(R.id.btn_bundle);
        btn_Bundles.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Home.this,Bundles.class);
		    	startActivity(intent);
			}});
        
        
        btn_TopUp = (Button)this.findViewById(R.id.btn_topup);
        btn_TopUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Home.this,TopUp.class);
		    	startActivity(intent);
			}});
        
        btn_Options = (Button)this.findViewById(R.id.btn_options);
        btn_Options.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Home.this,Options.class);
		    	startActivity(intent);
			}});
        
        btn_Offers = (Button)this.findViewById(R.id.btn_offers);
        btn_Offers.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Home.this,Offers.class);
		    	startActivity(intent);
			}});
             
        if(!Utility.GetUserEntity(this).getBundleSales())
        	btn_Bundles.setVisibility(View.GONE);
        
        Intent intent = getIntent();
		if(intent.getStringExtra("redirect")!=null){
		startActivity(new Intent(Home.this,Notifications.class));}
		new UpdateVersion().execute(new String[]{null});
		
	     mLocationClient.connect();
	     if(Utility.isConnected(Home.this))
         	 new GetAgeSetting().execute( new Void[]{}); 
	     new GetBarCodeVersionCode().execute(new Void[]{null});
   }
    
    @Override
    protected void onStop() {
       // Disconnect the client.
       mLocationClient.disconnect();
       //mConnectionClassManager.register(mListener);
       super.onStop();
    }
    
    @Override
    protected void onResume() {
    	 //mConnectionClassManager.remove(mListener);
    	//startZxing(this,"com.google.zxing.client.android");
    	if(Utility.isConnected(Home.this))
   	     new SendErrorLogData().execute(new Void[]{null});
       super.onResume();
    }

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		 displayCurrentLocation();
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

	  public void displayCurrentLocation() {
		  try{
	      // Get the current location's latitude & longitude
	      Location currentLocation = mLocationClient.getLastLocation();
	      String msg = "Current Location: " +
	      Double.toString(currentLocation.getLatitude()) + "," +
	      Double.toString(currentLocation.getLongitude());
	      if(Utility.isConnected(Home.this))
	      new GetAddressTask().execute(currentLocation);
	   }catch(Exception ex){}}

	  private class GetAddressTask extends AsyncTask<Location, Void, String>{
	      

	      /*
	       * When the task finishes, onPostExecute() displays the address. 
	       */
	      @Override
	      protected void onPostExecute(String address) {
	      }
	      @Override
	      protected String doInBackground(Location... params) {
	     try {
	         Geocoder geocoder = new Geocoder(Home.this, Locale.getDefault());
	         Location loc = params[0];
	         if(loc!=null){
	         List<Address> addresses = null;
	            addresses = geocoder.getFromLocation(Double.parseDouble(String.format("%.6f",loc.getLatitude())),
	            		Double.parseDouble(String.format("%.6f",loc.getLongitude())), 1);
	        
	         if (addresses != null && addresses.size() > 0) {
	             Address address = addresses.get(0);
	             String strCountry=address.getCountryName();
	             String strCountryCode= address.getCountryCode();
	             String strCity= address.getAdminArea();
	             String strStreet= address.getSubLocality();
	             String strArea= address.getLocality();
	     	     WebService.LocationInformation(Home.this,Utility.GetUserEntity(Home.this).getId(), strCountry, strCountryCode, strCity,strArea, strStreet, String.format("%.6f",loc.getLongitude()),String.format("%.6f",loc.getLatitude()));       
	     	     mLocationClient.disconnect();
	     	     return strCountryCode;}
	         else {
		            return "No address found";
	         } }}catch(Exception ex){
	        	 try{
	        		 WebService.SaveErrorLogNoBandwidth(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						
		        	// if(Utility.isConnected(SimRegistration.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(SimRegistration.this).getId(),SimRegistration.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	        	   //	else
	        	   //		Utility.showToast(SimRegistration.this, getString(R.string.alert_need_internet_connection)); 
	          
	            
	         }catch(Exception e){}}
			return ""; 
	      }
	   }// AsyncTask class

	  
		public void startZxing(Context context, String packageName) {
		    Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
		    if (intent == null) {
		    	Utility.alert(getString(R.string.install), Home.this, onclick);
		    } 
		}
    
		
		  DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent	 intent = new Intent(Intent.ACTION_VIEW);
				        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				        intent.setData(Uri.parse("market://details?id=com.google.zxing.client.android"));
				        startActivity(intent);
				}};
				
				  public String GetVersion(){
				    	Context context = getApplicationContext(); // or activity.getApplicationContext()
				    	PackageManager packageManager = context.getPackageManager();
				    	String packageName = context.getPackageName();

				    	String myVersionName = "not available"; // initialize String

				    	try {
				    	    myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
				    	} catch (PackageManager.NameNotFoundException e) {
				    	    e.printStackTrace();
				    	}
						return myVersionName;
				    }
				  
				  private class UpdateVersion extends AsyncTask<String, Void, Integer> {
				    		@Override
						protected void onPreExecute() {
							super.onPreExecute();
						
						}

						@Override
						protected Integer doInBackground(String... params) {
							int iRes=-1;
							try {
								
									String strVersion=GetVersion();
									WebService.AddAppVersion(Home.this,Utility.GetUserEntity(Home.this).getId(), strVersion)	;
								 
							}catch (Exception ex) {
								 WebService.SaveErrorLogNoBandwidth(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
									
								//if(Utility.isConnected(SimRegistration.this))
								//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(SimRegistration.this).getId(),SimRegistration.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

							}
							return iRes;
						}

						@Override
						protected void onPostExecute(Integer v) {
							super.onPostExecute(v);
							}}
				  
				  
					private class AuditTrailLoader extends AsyncTask<Void, Void, String>
					{
						
						@Override
						protected String doInBackground(Void... arg0)
						{try{
							return WebService.AuditTrail(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.getClass().getName(),"7", Utility.getDeviceId(Home.this)+" Logout");}catch(Exception ex){}
						return "";
						}
						}
					
					
					
	private class SendErrorLogData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}
		  @Override
		   protected Void doInBackground(Void... params) {
							try {	
								Cursor crsr = getErrorLogContentsCursor();
								crsr.moveToFirst();
								if(crsr!=null){
								while (!crsr.isAfterLast()) {
									ErrorLog_Entity cursorEntity = new ErrorLog_Entity();
									cursorEntity.setUserID(crsr.getString(0));
									cursorEntity.setClassName(crsr.getString(1));
									cursorEntity.setStackTrace(crsr.getString(2));
									cursorEntity.setDeviceInfo(crsr.getString(3));
									cursorEntity.setDateTime(crsr.getString(4));
									cursorEntity.setSignal(crsr.getString(5));
									cursorEntity.setBandwidth(crsr.getString(6));
									cursorEntity.setID(crsr.getString(7));
									cursorEntity.setSignalID(crsr.getString(8));
									
									int iRes=WebService.AddErrorLog2(Home.this,crsr.getString(0), crsr.getString(1), crsr.getString(2), crsr.getString(3),
											crsr.getString(4),crsr.getString(5),crsr.getString(6));
									if(iRes==1){
										updater.deleteErrorLog(crsr.getString(7));
										updater.deleteSignal(crsr.getString(8));
									}
									crsr.moveToNext();
								} crsr.close();
								} }catch (Exception ex) {}
							return null;
						}

						@Override
						protected void onPostExecute(Void v) { }
					}
				  
				  
	private Cursor getErrorLogContentsCursor() {
		Cursor crsr = null;
		try {
			String sql="Select "+ C.USERID+","+ C.CLASSNAME+","+C.STACKTRACE+","+C.DEVICEINFO+","+C.DATETIME+","+C.SIGNAL+","+C.BANDWIDTH+", ErrorLog.ID , Signal.ID From " + C.ERRORLOG_TABLE +"  Inner Join "+ C.SIGNAL_TABLE+" on ErrorLog.ID= Signal.Error_FK"  ;
				crsr = db.rawQuery(sql , null);
		} catch (Exception ex) {
			 WebService.SaveErrorLogNoBandwidth(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   						
			//if(Utility.isConnected(SimRegistration.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(SimRegistration.this).getId(),SimRegistration.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
	  
	
  private class GetAgeSetting extends AsyncTask<Void, Void, Integer>{
	      
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Home.this, getString(R.string.pleaseWait), "", true, true, null);
			}
	      @Override
	      protected Integer doInBackground(Void... params) {
	    	  int iResult=-1;
	     try {
	    	 iResult = WebService.GetAgeSetting(Home.this,Utility.GetUserEntity(Home.this).getId());       
	     	     return iResult;
	          }catch(Exception ex){
	        	 try{ }catch(Exception e){}}
			return iResult; 
	      }
	  	@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			try{
				if(v==-1)
	  				Utility.showToast(Home.this, getString(R.string.error));
				else
					C.AGE=v;
				 new Countires().execute(new Void[]{null});		
				}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}}}// AsyncTask class
  
  
  public class Countires extends AsyncTask<Void, Void, Integer> {
	  	private ArrayList<Countries_Entity> alCountriesData= new ArrayList<Countries_Entity>();
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			
			}

			@Override
			protected Integer doInBackground(Void... params) {
				int iRes=-1;
				try {
					  WebService.GetCountriesWithDate(Home.this,Utility.GetUserEntity(Home.this).getId(),alCountriesData,C.getLastModificationDate(Home.this));
				}catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
					//if(Utility.isConnected(context))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				}
				return iRes;
			}

			@Override
			protected void onPostExecute(Integer v) {
				super.onPostExecute(v);
				progress.dismiss();
				try{
				if(alCountriesData.size()>0){
					for (int i=0;i<alCountriesData.size();i++){
					Countries_Entity countries = (Countries_Entity)alCountriesData.get(i);
					if(countries.getAction().equals("1"))
					updater.saveContentsForCountries(countries.getID(), countries.getNameEng());
					else if(countries.getAction().equals("2"))
						updater.updateContentsForCountries(countries.getID(), countries.getNameEng());
					else if(countries.getAction().equals("3"))
						updater.deleteContentsForCountries(countries.getID());
					}	
					
					Calendar calendar = Calendar.getInstance();
					SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
					C.storeLastModificationDate(Home.this, format.format(calendar.getTime()));
					
				}
				
				}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(Home.this,Utility.GetUserEntity(Home.this).getId(),Home.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
					// if(Utility.isConnected(context))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	      	   	//else
	      	   	//	Utility.showToast(context, getString(R.string.alert_need_internet_connection)); 
	         
				}}}
  

  private class GetBarCodeVersionCode extends AsyncTask<Void, String, String> {
	  String currentVersion="";
		@Override
		protected void onPreExecute() {
			List<PackageInfo> packages = getPackageManager().getInstalledPackages(0);
			for (PackageInfo packageInfo : packages) {
				if(packageInfo.packageName.equals("com.google.zxing.client.android"))
					currentVersion=packageInfo.versionName;
			}
		}
	    @Override
	    protected String doInBackground(Void... voids) {

	        String newVersion = null;
	        try {
	            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.google.zxing.client.android &hl=it")
	                    .timeout(30000)
	                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
	                    .referrer("http://www.google.com")
	                    .get()
	                    .select("div[itemprop=softwareVersion]")
	                    .first()
	                    .ownText();
	            return newVersion;
	        } catch (Exception e) {
	            return newVersion;
	        }
	    }

	    @Override
	    protected void onPostExecute(final String onlineVersion) {
	        super.onPostExecute(onlineVersion);
	        if (onlineVersion != null && !onlineVersion.isEmpty()) {
	            if (currentVersion.compareTo(onlineVersion)<0) {
	            	  Utility.alertnoBack(getResources().getString(R.string.latest), Home.this,   new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								try {
								    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.zxing.client.android")));
								} catch (android.content.ActivityNotFoundException anfe) {
								    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.zxing.client.android" )));
								}
								Log.d("update", "Current version " + GetVersion() + "playstore version " + onlineVersion);
								 }});

				 
	            }
	        }
	      
	    }}
  }
