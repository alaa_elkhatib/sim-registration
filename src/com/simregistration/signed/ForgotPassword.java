package com.simregistration.signed;
import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

public class ForgotPassword extends Activity {
    private EditText edt_CRNumber,edt_CPRNumber;
    private Button btn_Submit;
    private ProgressDialog progress = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_CRNumber = (EditText)findViewById(R.id.edtCRNumber);
        edt_CPRNumber = (EditText)findViewById(R.id.edtCPRNumber);
        
  	    btn_Submit = (Button)this.findViewById(R.id.btnSubmit);
  	    btn_Submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  if(edt_CRNumber.getText().toString().equals("")||edt_CPRNumber.getText().toString().equals(""))
						  Utility.showToast(ForgotPassword.this,getResources().getString(R.string.fill));
					  
					  else{
						 if(Utility.isConnected(ForgotPassword.this))
							  new ForgotUserPassword().execute(new String[]{edt_CPRNumber.getText().toString(), edt_CRNumber.getText().toString(),Utility.getDeviceId(ForgotPassword.this)});
	                	   	else
	                	   		Utility.showToast(ForgotPassword.this, getString(R.string.alert_need_internet_connection)); }
	               
					  } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(ForgotPassword.this,Utility.GetUserEntity(ForgotPassword.this).getId(),ForgotPassword.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
							
						 // if(Utility.isConnected(ForgotPassword.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(ForgotPassword.this).getId(),ForgotPassword.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   	//else
			      	   	//	Utility.showToast(ForgotPassword.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); }
  	    
  	  private class ForgotUserPassword extends AsyncTask<String, Void, String> {
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(ForgotPassword.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected String doInBackground(String... params) {
  			try {
  				 return WebService.ForgotPassword(ForgotPassword.this,params[0], params[1], params[2]);
  			}catch (Exception ex) {
  				WebService.SaveErrorLogNoBandwidth(ForgotPassword.this,Utility.GetUserEntity(ForgotPassword.this).getId(),ForgotPassword.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  				//if(Utility.isConnected(ForgotPassword.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(ForgotPassword.this).getId(),ForgotPassword.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

  				return "";
  				
  			}
  		}

  		@Override
  		protected void onPostExecute(String v) {
  			super.onPostExecute(v);
  			progress.dismiss();
  			Utility.alert(v,ForgotPassword.this, onclick);}}
      
      DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

   		@Override
   		public void onClick(DialogInterface dialog, int which) {
   			finish();
   		}};
  	}
    
    

    
