package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.utility.Utility;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.app.Activity;

public class Offer_Details extends Activity {
	WebView webView = null;
	View screener;
	private ArrayList<String> alImages;
	private String strDescription;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.offer_details);  
         this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
         screener = this.findViewById(R.id.screener_pd);
         screener.setVisibility(View.VISIBLE);
         alImages=getIntent().getStringArrayListExtra("Images");
         strDescription=getIntent().getStringExtra("Description");
         webView = (WebView) this.findViewById(R.id.wv_internal);
		 webView.getSettings().setJavaScriptEnabled(true);
		 webView.getSettings().setBuiltInZoomControls(true);
		 webView.getSettings().setDefaultTextEncodingName("utf-8");
		 webView.getSettings().setAppCacheEnabled(true);
		 webView.getSettings().setAllowFileAccess(true);
		 webView.getSettings().setUseWideViewPort(true);
		 webView.setInitialScale(100);
		 String strHtmlCode="";
		 if(!strDescription.equals(""))
			 strHtmlCode = "<h1>"+strDescription+"</h1></b>";
		 for(int i=0;i<alImages.size();i++){
			 strHtmlCode=strHtmlCode+ "<img src="+alImages.get(i)+"></b>";}

		 webView.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					return false;
				}

				@Override
				public void onPageFinished(WebView view, String url) {
					super.onPageFinished(view, url);
					screener.setVisibility(View.GONE);
				}
			});
			webView.setOnKeyListener(new OnKeyListener(){

	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                  if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
	                        handler.sendEmptyMessage(1);
	                        return true;
	                    }
	                    return false; }});
			if(Utility.isConnected(Offer_Details.this))
			webView.loadDataWithBaseURL("", "<html><body>"+strHtmlCode+"</body></html>", "text/html", "UTF-8", "");
			else 
				Utility.showToast(Offer_Details.this, getString(R.string.alert_need_internet_connection)); 
    
    }
    
    private Handler handler = new Handler(){
	    @Override
	    public void handleMessage(Message message) {
	        switch (message.what) {
	            case 1:{
	                webViewGoBack();
	            }break;
	        }
	    }
	};
	 private void webViewGoBack(){
		 webView.goBack();
	    }
	
    }
