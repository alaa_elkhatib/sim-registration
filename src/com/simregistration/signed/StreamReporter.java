package com.simregistration.signed;

import biz.smartengines.swig.MrzRectMatrix;
import biz.smartengines.swig.MrzResult;
import biz.smartengines.swig.StreamReporterInterface;


public class StreamReporter extends StreamReporterInterface {

    private CameraView cameraView;

    public void SetParentView(CameraView _cameraView){
        cameraView = _cameraView;
    }

    public void SnapshotProcessed(MrzResult mrzResult, boolean done) {

        if(done)
        {
            cameraView.ShowResult(mrzResult);
        }
    }

    public void SnapshotRejected() {

    }

    public void SymbolRectsFound(MrzRectMatrix mrzRectMatrix) {

        //cameraView.DrawRects(mrzRectMatrix);
    }

}
