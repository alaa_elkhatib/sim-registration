package com.simregistration.signed;
import com.simregistration.signed.R;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

public class ChangePassword extends Activity {
    private EditText edt_CurrentPassword,edt_NewPassword,edt_ConfirmdPassword;
    private Button btn_Submit;
    private ProgressDialog progress = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_CurrentPassword = (EditText)findViewById(R.id.edtCurrentPassword);
        edt_NewPassword = (EditText)findViewById(R.id.edtNewPassword);
        edt_ConfirmdPassword = (EditText)findViewById(R.id.edtConfirmPassword);
        
  	    btn_Submit = (Button)this.findViewById(R.id.btnSubmit);
  	    btn_Submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  if(edt_CurrentPassword.getText().toString().equals("")||edt_NewPassword.getText().toString().equals("")
							  ||edt_ConfirmdPassword.getText().toString().equals(""))
						  Utility.showToast(ChangePassword.this,getResources().getString(R.string.fill));
					  else  if(!edt_NewPassword.getText().toString().equals(edt_ConfirmdPassword.getText().toString()))
						  Utility.showToast(ChangePassword.this,getResources().getString(R.string.passwords_dontmatch));
					  else if(edt_CurrentPassword.getText().toString().length()==8 && edt_NewPassword.getText().toString().length()==8
							   && edt_ConfirmdPassword.getText().toString().length()==8){
						 if(Utility.isConnected(ChangePassword.this))
							  new ChangeUserPassword().execute(new String[]{Utility.GetUserEntity(ChangePassword.this).getId(),edt_CurrentPassword.getText().toString(), edt_NewPassword.getText().toString()});
	                	   	else
	                	   		Utility.showToast(ChangePassword.this, getString(R.string.alert_need_internet_connection)); }
	               
					  } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(ChangePassword.this,Utility.GetUserEntity(ChangePassword.this).getId(),ChangePassword.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());
					        
						 // if(Utility.isConnected(ChangePassword.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(ChangePassword.this).getId(),ChangePassword.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   	//else
			      	   	//	Utility.showToast(ChangePassword.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); }
  	    
    
  
    
  	  private class ChangeUserPassword extends AsyncTask<String, Void, String> {
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(ChangePassword.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected String doInBackground(String... params) {
  			try {
  				 return WebService.ChangePassword(ChangePassword.this,params[0], params[1], params[2]);
  			}catch (Exception ex) {
  			     WebService.SaveErrorLogNoBandwidth(ChangePassword.this,Utility.GetUserEntity(ChangePassword.this).getId(),ChangePassword.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
  				//if(Utility.isConnected(ChangePassword.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(ChangePassword.this).getId(),ChangePassword.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	   	 
  				return "";
  			  	
  			}
  			
  		}

  		@Override
  		protected void onPostExecute(String v) {
  			super.onPostExecute(v);
  			progress.dismiss();
  			Utility.alert(v,ChangePassword.this, onclick);}}
      
      DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

   		@Override
   		public void onClick(DialogInterface dialog, int which) {
   			finish();
   		}};
   		
   
  	}
    
    

    
