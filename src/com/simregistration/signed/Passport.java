
package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
public class Passport extends Activity  {
	 private Uri cameraTempUri;
	    private final int TAKE_CAMERA_REQUEST = 1;
	    private String defaultCameraPackage;
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.iccid);     
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        try{
         File dir = new File(Environment.getExternalStorageDirectory(),"Passport");
    		if (dir != null) dir.mkdirs();	
        File file = File.createTempFile("photo_", null, dir);
        cameraTempUri = Uri.fromFile(file);
        List<ApplicationInfo> list = getPackageManager().getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (int n=0;n<list.size();n++) {
            if((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM)==1)
            {
                Log.d("TAG", "Installed Applications  : " + list.get(n).loadLabel(getPackageManager()).toString());
                Log.d("TAG", "package name  : " + list.get(n).packageName);
                if(list.get(n).loadLabel(getPackageManager()).toString().equalsIgnoreCase("Camera")) {
                    defaultCameraPackage = list.get(n).packageName;
                    break;
                }
            }
        }
        
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.setPackage(defaultCameraPackage); 
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraTempUri);
        startActivityForResult(intent, TAKE_CAMERA_REQUEST);
        Utility.showToast2(this, getString(R.string.scan_passport)); 
		}catch(Exception ex){}}

		  protected void onActivityResult (int requestCode, int resultCode, Intent data) {
		        switch(requestCode) {
		        case TAKE_CAMERA_REQUEST:
	            	if (resultCode == Activity.RESULT_OK) {
	                    //Bitmap originalBitmap = (Bitmap) data.getExtras().get("data");
	                    Uri photoUri = cameraTempUri;
	                    if (photoUri != null) {
	                       Bitmap bitmap = BitmapUtils.getBitmap(Passport.this, photoUri, 4); 
	                       
	                        Intent intent = new Intent(Passport.this, Passport_Bar.class);
	                        try {
	                            //Write file
	                            String filename = "bitmap.png";
	                            FileOutputStream stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
	                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

	                            //Cleanup
	                            stream.close();
	                            bitmap.recycle();
	                            intent.putExtra("image", filename);
	                        } catch (Exception ex) {
	                        	WebService.SaveErrorLogNoBandwidth(Passport.this,Utility.GetUserEntity(Passport.this).getId(),Passport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
	            				
	                        	//if(Utility.isConnected(Passport.this))
	           					// new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport.this).getId(),Passport.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	                            ex.printStackTrace();
	                        }
	                    	startActivity(intent);
							finish();
	                    }}
	        	else if (resultCode == Activity.RESULT_CANCELED) {
	        		finish();}
		
	            break;
		           
		           default:
		               break;
		        } }
		  
			
			@Override
			public boolean onKeyDown(int keyCode, KeyEvent event) {
				//Log.i(TAG, "onKeyDown(" + keyCode + ")");

				// to take the pic ASAP, grab the preview frame data from here - don't wait for photo
				if (keyCode == KeyEvent.KEYCODE_BACK){
					finish();
				}
				return super.onKeyDown(keyCode, event); // pass the key along to other handlers 
			}


  

}
