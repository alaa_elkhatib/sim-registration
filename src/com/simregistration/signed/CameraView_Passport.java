package com.simregistration.signed;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import biz.smartengines.swig.MrzEngine;
import biz.smartengines.swig.MrzEngineInternalSettings;
import biz.smartengines.swig.MrzEngineSessionHelpers;
import biz.smartengines.swig.MrzEngineSessionSettings;
import biz.smartengines.swig.MrzException;
import biz.smartengines.swig.MrzRect;
import biz.smartengines.swig.MrzRectMatrix;
import biz.smartengines.swig.MrzResult;
import biz.smartengines.swig.StringVector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CameraView_Passport extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.AutoFocusCallback, CompoundButton.OnCheckedChangeListener{

    private android.hardware.Camera mCamera;
    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;
    private ToggleButton mStartButton;

    private RelativeLayout mElementsLayout;
    private RelativeLayout mUpperZone;
    private RelativeLayout mLowerZone;

    private RectView mRectView;

    private static final String mModuleName = "MRZ Recognition DEMO";
    private static double aspectRatio = 6.0;         // roi aspect ratio = width/height

    private boolean bProcess = false;

    private int h_zone = 0, w_zone = 0; // size of screen to show preview
    private int h_roi = 0, w_roi = 0;   // size of roi in screen

    private static double scale = 0;

    private static boolean bInitEngine = false;
    public static MrzEngine engine;

    private static MrzEngineSessionHelpers helpers;
    private static MrzEngineSessionSettings sessionSettings;
    private static StreamReporter_Passport reporter;

    private static boolean camera_params = false;

    private static String focus_mode;
    private static int size_mode_w;
    private static int size_mode_h;

    private static boolean bMrpSupport = true;
    private static boolean bTd1Support = false;
    private static boolean bTd2Support = false;
    private static boolean bCnisSupport = false;
    private static boolean bM3zSupport = false;
    private static boolean bVisaSupport = false;
    private static boolean bRussianVisaSupport = false;

    private Timer timer;
    private static boolean bUseTimer = false;

    private boolean bNexus = Build.MODEL.contains("Nexus 5X");
    static {
        Log.w("MYAPP", "About to load library.");
        System.loadLibrary("mrzjniInterface");
        Log.w("MYAPP", "Library loaded.");
    } 
    private class InitCore extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            System.loadLibrary("mrzjniInterface");

            String configPath = CameraView_Passport.this.getApplicationContext().getFilesDir().getAbsolutePath() + File.separator + "mrzapi_engine.zip";

            try {

                MrzEngineInternalSettings internalSettings = MrzEngineInternalSettings.createFromFilesystem(configPath);

                sessionSettings = new MrzEngineSessionSettings();
                sessionSettings.set_should_postprocess(true);

                helpers = new MrzEngineSessionHelpers();

                reporter = new StreamReporter_Passport();
                reporter.SetParentView(CameraView_Passport.this);

                engine = new MrzEngine(internalSettings);
                bInitEngine = true;

            }
            catch (MrzException e) {

                Log.d(mModuleName, "Cannot init engine: " + e.getMessage());
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(bInitEngine && bProcess)
                StartSession();

        }
    }

    private View.OnClickListener onFocus = new View.OnClickListener() {

        public void onClick(View v) {

            if(bUseTimer == false)  // focus on tap
                focusing();
        }
    };



    private class FocusTimer extends TimerTask {

        public void run() {

            focusing();

        }
    }


    private void focusing() {

//        Camera.Parameters cparams = mCamera.getParameters();
//        mCamera.cancelAutoFocus();
//
//        cparams.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//        mCamera.setParameters(cparams);
//        mCamera.autoFocus(CameraView.this);
//        
        
        
        Camera.Parameters cparams = mCamera.getParameters();
        mCamera.cancelAutoFocus();
        List<String> focusModes = cparams.getSupportedFocusModes();
        if (focusModes.contains(cparams.FOCUS_MODE_AUTO))
        	cparams.setFocusMode(cparams.FOCUS_MODE_AUTO);
        mCamera.setParameters(cparams);
        mCamera.autoFocus(CameraView_Passport.this);
        
        
        
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_view);

        CheckParams();

        mSurfaceView = (SurfaceView)findViewById(R.id.cameraView);
        mSurfaceView.setOnClickListener(onFocus);

        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mStartButton = (ToggleButton) findViewById(R.id.StartButton);
        mStartButton.setOnCheckedChangeListener(this);
      
       
        mElementsLayout = (RelativeLayout) findViewById(R.id.ElementsLayout);
        mUpperZone = (RelativeLayout) findViewById(R.id.UpperZone);
        mLowerZone = (RelativeLayout) findViewById(R.id.LowerZone);

        mRectView = new RectView(this);

        // check if we need to copy dir or not - only once if new version

        int versionCode = 0;//BuildConfig.VERSION_CODE;
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);
        int versionCurrent = sPref.getInt("iVersionCode", -1);

        if(versionCode != versionCurrent )
        {
            sPref.edit().putInt("iVersionCode", versionCode).commit();
            CopyAssetsDir("data");
        }


        if(bInitEngine == false)
        {
            InitCore init = new InitCore();
            init.execute();
        }
        else
        {
            reporter.SetParentView(this);
        }
      

    }

    private void CopyAssetsDir(String start_dir) {

        AssetManager assetManager = getAssets();

        String cut_dir = "";
        int slash_pos = start_dir.indexOf(File.separatorChar);

        if( slash_pos > 0)
            cut_dir = start_dir.substring(slash_pos + 1);

        final String output_dir = this.getApplicationContext().getFilesDir().getAbsolutePath() + File.separator + cut_dir;

        File newDir = new File(output_dir);
        newDir.mkdir();

        String[] files;

        try {

            files = assetManager.list(start_dir);

            for(String file : files) {

                String input_name =  start_dir + File.separator + file;
                String output_name = output_dir + File.separator + file;

                boolean isFile = input_name.lastIndexOf('.') > 0;

                if( isFile ) {
                    CopyFileFromAsset(input_name, output_name);
                }
                else {
                    CopyAssetsDir(input_name);
                }

            }

        } catch (IOException e) {
            Log.d(mModuleName, "Cannot find assets files: " + e.getMessage());
        }
    }

    private void CopyFileFromAsset(String input, String output) throws IOException {

        File file = new File(output);

        AssetManager assetManager = getAssets();

        InputStream in = assetManager.open(input);
        OutputStream out = new FileOutputStream(output);

        int length = 0;
        byte[] buffer = new byte[1024];

        while ( ( length = in.read(buffer) )  > 0) {

            out.write(buffer, 0, length);
        }

        in.close();
        out.close();

    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

        try{
            mCamera = android.hardware.Camera.open();

            Camera.Parameters cparams = mCamera.getParameters();
            CheckCameraParams();

            cparams.setFocusMode(focus_mode);
            cparams.setPreviewSize(size_mode_w, size_mode_h);

            mCamera.setParameters(cparams);

            int w = mSurfaceView.getWidth();
            int h = mSurfaceView.getHeight();

            double p1 = (double)w/(double)size_mode_w;
            double p2 = (double)h/(double)size_mode_h;

            scale = Math.min(p1, p2);

            h_zone = (int) (scale*size_mode_h);
            w_zone = (int) (scale*size_mode_w);

            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(w_zone, h_zone);
            lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            lparams.addRule(RelativeLayout.CENTER_VERTICAL);
            mElementsLayout.setLayoutParams(lparams);
            mSurfaceView.setLayoutParams(lparams);

            mCamera.setPreviewCallback(this);
            mStartButton.setChecked(true);

        }
        catch (Exception e) {
            Log.d(mModuleName, "Cannot init camera: " + e.getMessage());
        }


        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            Log.d(mModuleName, "Cannot set preview surface: " + e.getMessage());
        }

        mCamera.startPreview();

        if(bUseTimer)
        {
            timer = new Timer();
            timer.schedule(new FocusTimer(), 1000, 3000);
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

        mStartButton.setChecked(false);

        if(bUseTimer)
            timer.cancel();

        bProcess = false;

        if(bInitEngine)
        {

            try{
                engine.TerminateSession();
            }
            catch(MrzException e){
                Log.d(mModuleName, "Cannot terminate session: " + e.getMessage());
            }


        }

        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mCamera.release();
    }

    private void CheckCameraParams() {


        if(camera_params == false)
        {

            Camera.Parameters cparams = mCamera.getParameters();
            List<String> focus = cparams.getSupportedFocusModes();
            List<Camera.Size> sizes = cparams.getSupportedPreviewSizes();

            focus_mode = Camera.Parameters.FOCUS_MODE_AUTO;

            focus.contains(Camera.Parameters.FOCUS_MODE_AUTO);

            if(focus.contains(focus_mode) == true)
            {

                if(focus.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                    focus_mode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
                else
                    if(focus.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
                        focus_mode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;

            }
            else
            {
                if( focus.contains(Camera.Parameters.FOCUS_MODE_INFINITY) )
                    focus_mode = Camera.Parameters.FOCUS_MODE_INFINITY;
                else
                if( focus.contains(Camera.Parameters.FOCUS_MODE_FIXED) )
                    focus_mode = Camera.Parameters.FOCUS_MODE_FIXED;
            }


            size_mode_w = size_mode_h = 0;

            for(int i=0; i< sizes.size(); i++) {

                Camera.Size size = sizes.get(i);

                if( size.width > size_mode_w )
                {
                    size_mode_w = size.width;
                    size_mode_h = size.height;
                }

            }

            bUseTimer = focus_mode == Camera.Parameters.FOCUS_MODE_AUTO;
            camera_params = true;

        }

    }


    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }


    @Override
    public void onPreviewFrame(byte[] bytes, android.hardware.Camera camera) {

        if(bInitEngine && bProcess) {

            Log.d(mModuleName, "Image is recognizing");

            Camera.Parameters cparams = camera.getParameters();
            Camera.Size size = cparams.getPreviewSize();

            Rect rect = new Rect();
            rect.top = rect.left = 0;
            rect.bottom = size.height;
            rect.right = size.width;

            int roi_width = size.width;
            int roi_height = (int) (size.width / aspectRatio);

            int roi_x = 0;
            int roi_y = (size.height - roi_height)/2;

            MrzRect roi = new MrzRect(roi_x, roi_y, roi_width, roi_height);

            try {

                if(bNexus == true)
                    engine.FeedUncompressedImageData(bytes, size.width, size.height, size.width, 1, roi,  MrzEngine.ImageOrientation.InvertedLandscape);
                else
                    engine.FeedUncompressedImageData(bytes, size.width, size.height, size.width, 1, roi,  MrzEngine.ImageOrientation.Landscape);

            } catch (MrzException e) {
                 Log.d(mModuleName, "Failed to feed image to engine: " + e.getMessage());
            }

        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

       bProcess = isChecked;

       if( bProcess == true){

           if(bInitEngine)
               StartSession();

           h_roi = (int)(w_zone/aspectRatio);
           w_roi = w_zone;

           Rect roi = new Rect(0, (h_zone - h_roi)/2, w_zone, (h_zone + h_roi)/2);
           mRectView.SetRoiZone(roi, scale);
           mRectView.SetRects(null);

           RelativeLayout.LayoutParams params_upper = new RelativeLayout.LayoutParams(w_zone, (h_zone - h_roi)/2 );
           params_upper.addRule(RelativeLayout.CENTER_HORIZONTAL);
           params_upper.addRule(RelativeLayout.ALIGN_PARENT_TOP);
           mUpperZone.setLayoutParams(params_upper);

           RelativeLayout.LayoutParams params_lower = new RelativeLayout.LayoutParams(w_zone, (h_zone - h_roi)/2 );
           params_lower.addRule(RelativeLayout.CENTER_HORIZONTAL);
           params_lower.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
           mLowerZone.setLayoutParams(params_lower);

           RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(w_zone, h_zone);
           lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
           lparams.addRule(RelativeLayout.CENTER_VERTICAL);

           mRectView.setLayoutParams(lparams);
           mElementsLayout.addView(mRectView);
       }
        else {

           RelativeLayout.LayoutParams params_upper = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
           mUpperZone.setLayoutParams(params_upper);

           RelativeLayout.LayoutParams params_lower = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
           mLowerZone.setLayoutParams(params_lower);

           mElementsLayout.removeView(mRectView);
       }


    }

    private void StartSession() {

        sessionSettings.set_mrp_support_enabled(true);
        sessionSettings.set_td1_support_enabled(false);
        sessionSettings.set_td2_support_enabled(false);
        sessionSettings.set_cnis_support_enabled(false);
        sessionSettings.set_m3z_support_enabled(false);

        sessionSettings.set_mrva_support_enabled(false);
        sessionSettings.set_mrvb_support_enabled(false);

        //sessionSettings.set_mrvrus_support_enabled(bRussianVisaSupport);

        try {
            engine.InitializeSession(reporter, helpers, sessionSettings);
        }
        catch (MrzException e){
            Log.d(mModuleName, "Feed init session: " + e.getMessage());
        }

        aspectRatio = helpers.get_optimal_aspect_ratio();


    }


    

    protected void CheckParams(){

        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);

        bMrpSupport = sPref.getBoolean("bMrpSupport", true);
        bTd1Support = sPref.getBoolean("bTd1Support", true);
        bTd2Support = sPref.getBoolean("bTd2Support", true);
        bCnisSupport = sPref.getBoolean("bCnisSupport", true);
        bM3zSupport = sPref.getBoolean("bM3zSupport", true);
        bVisaSupport = sPref.getBoolean("bVisaSupport", true);
        bRussianVisaSupport = sPref.getBoolean("bRussianVisaSupport", true);

        if( bMrpSupport == false && bTd1Support == false && bTd2Support == false && bCnisSupport == false && bM3zSupport == false && bVisaSupport == false && bRussianVisaSupport == false )
            bMrpSupport = true;

        SharedPreferences.Editor ed = sPref.edit();
        ed.putBoolean("bMrpSupport", bMrpSupport);
        ed.putBoolean("bTd1Support", bTd1Support);
        ed.putBoolean("bTd2Support", bTd2Support);
        ed.putBoolean("bCnisSupport", bCnisSupport);
        ed.putBoolean("bM3zSupport", bM3zSupport);
        ed.putBoolean("bVisaSupport", bVisaSupport);
        ed.putBoolean("bRussianVisaSupport", bRussianVisaSupport);

        ed.commit();


    }


    public void ShowResult(MrzResult result){

        mStartButton.setChecked(false);
        Intent intent =null;
        if(getIntent().getIntExtra("type", 0) == 1){
        	intent = new Intent(this, MRZCPR.class);
        	}
        else if(getIntent().getIntExtra("type", 0) == 3){
        	intent = new Intent(this, MRZGcc.class);
        	intent.putExtra("mode", "scan");
        }else if(getIntent().getIntExtra("type", 0) == 2){
        	intent = new Intent(this, MRZPassport.class);
        }else if(getIntent().getIntExtra("type", 0) == 4 ){
        	intent = new Intent(this,CPRFields.class);
        	intent.putExtra("type", getIntent().getIntExtra("type", 0));
        	
        }else if( getIntent().getIntExtra("type", 0) == 5){
        	intent = new Intent(this,PassportFields.class);
        	intent.putExtra("type", getIntent().getIntExtra("type", 0));
        	
        }
        Bundle params = new Bundle();

        String[] names = new String[11];
        String[] values = new String[11];
        boolean[] accepts = new boolean[11];

        names[0] = "Document Type";
        values[0] = result.getDocType().getAsString();
        accepts[0] = result.getDocType().isAccepted();

        names[1] = "Country";
        values[1] = result.getCountry().getAsString();
        accepts[1] = result.getCountry().isAccepted();

        names[2] = "Document Number";
        values[2] = result.getDocNumFormatted().getAsString();
        accepts[2] = result.getDocNumFormatted().isAccepted();

        names[3] = "Surname";
        values[3] = result.getSecondName().getAsString();
        accepts[3] = result.getSecondName().isAccepted();

        names[4] = "Name";
        values[4] = result.getFirstName().getAsString();
        accepts[4] = result.getFirstName().isAccepted();

        names[5] = "Birth date";
        values[5] = result.getBirthdate().getAsString();
        accepts[5] = result.getBirthdate().isAccepted();

        names[6] = "Nationality";
        values[6] = result.getNationality().getAsString();
        accepts[6] = result.getNationality().isAccepted();

        names[7] = "Sex";
        values[7] = result.getSex().getAsString();
        accepts[7] = result.getSex().isAccepted();

        names[8] = "Expiry date";
        values[8] = result.getExpidate().getAsString();
        accepts[8] = result.getExpidate().isAccepted();

        names[9] = "Optional data";
        values[9] = result.getOptData2().getAsString();
        accepts[9] = result.getOptData2().isAccepted();

        names[10] = "ID Number";
        values[10] = result.getOptData1().getAsString();
        accepts[10] = result.getOptData1().isAccepted();
        
        
        params.putStringArray("names", names);
        params.putStringArray("values", values);
        params.putBooleanArray("accepts", accepts);

        StringVector vector = result.getMrzLines();
        String[] lines = new String[(int)vector.size()];

        for(int i=0; i< vector.size(); i++)
            lines[i] = vector.get(i);

        params.putStringArray("Lines", lines);

        intent.putExtras(params);
        if(getIntent().getStringExtra("mode")!=null)
        	intent.putExtra("mode",getIntent().getStringExtra("mode"));
        startActivity(intent);
    }


    public void DrawRects(MrzRectMatrix mrzRectMatrix) {

        mRectView.SetRects(mrzRectMatrix);
        mRectView.invalidate();
    }

}
