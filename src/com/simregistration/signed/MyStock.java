package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Denomination_Entity;
import com.simregistration.signed.entity.Stock_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.entity.Stocks_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ListView;
import android.app.Activity;
import android.app.ProgressDialog;

public class MyStock extends Activity  implements CustomAdapter.ListFiller {
	private ArrayList<Stocks_Entity> alStockDataMain;
	ListView lvList;
	CustomAdapter adapter;
    private ProgressDialog progress = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mystock_list);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
		lvList = (ListView)findViewById(R.id.lvList);
		alStockDataMain =new ArrayList<Stocks_Entity>();// getIntent().getParcelableArrayListExtra("Commission");
		adapter = new CustomAdapter(this,alStockDataMain, R.layout.mystock_item, this);
		lvList.setAdapter(adapter);
		 if(Utility.isConnected(MyStock.this))
       	  new GetMyStock().execute(new String[]{Utility.GetUserEntity(MyStock.this).getId()});
   	 else
	   		Utility.showToast(MyStock.this, getString(R.string.alert_need_internet_connection)); 
   }
    
 
    private class GetMyStock extends AsyncTask<String, Void, Integer> {
    	ArrayList<Stock_Entity> alStockData= new  ArrayList<Stock_Entity>();
		
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(MyStock.this, getString(R.string.pleaseWait), "", true, true, null);
			}

		@Override
		protected Integer doInBackground(String ... params) {
			int iResult=-1;
			try {
				iResult =WebService.GetMyStock(MyStock.this, params[0], alStockData);
				 }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(MyStock.this,Utility.GetUserEntity(MyStock.this).getId(),MyStock.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
								return null;
			}
			return iResult;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v==-1)
	  				Utility.showToast(MyStock.this, getString(R.string.error));
				else{
				if(alStockData.size()>0){
					Stock_Entity entity= alStockData.get(0);
						if(!entity.getError().equals(""))
						Utility.showToast(MyStock.this, entity.getError());
						
				else {
					    alStockDataMain = alStockData.get(0).getStocks();
					    adapter.notifyDataSetChanged();}}
			
				}}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(MyStock.this,Utility.GetUserEntity(MyStock.this).getId(),MyStock.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}}}


	@Override
	public void fillListData(View v, int pos) {
		
	}


	@Override
	public boolean isEnabled(int pos) {
		return false;
	}


	@Override
	public boolean useExtGetView() {
		return true;
	}


	@Override
	public View getView(int pos, View contextView) {
try{
			
			if (alStockDataMain== null || alStockDataMain.size() == 0)
				return  contextView;
			
				 LayoutInflater inf = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
				 contextView = inf.inflate(R.layout.mystock_item, null);
				 final ViewHolder holder = new ViewHolder();
				 holder.tv_denomination = (TextView) contextView.findViewById(R.id.tvDenomination);
				 holder.tv_qty= (TextView) contextView.findViewById(R.id.tvQty);
				 holder.tv_amount = (TextView) contextView.findViewById(R.id.tvAmount);
				  final Stocks_Entity entity = (Stocks_Entity) alStockDataMain.get(pos);
			    
			      
			    if (entity != null && entity.getDenomination() != null)
					holder.tv_denomination.setText(entity.getDenomination());
			    
			    if (entity != null && entity.getAmount() != null)
					holder.tv_amount.setText(entity.getAmount());
			    
			    
			    if (entity != null && entity.getQty() != null)
					holder.tv_qty.setText(entity.getQty());
			    
			
				}catch (Exception ex){
					  WebService.SaveErrorLogNoBandwidth(MyStock.this,Utility.GetUserEntity(MyStock.this).getId(),MyStock.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
						
					// if(Utility.isConnected(Commission_List.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	      	   	//else
	      	   		//Utility.showToast(Commission_List.this, getString(R.string.alert_need_internet_connection)); 
	        
				
		}
return contextView;
	}

	
	class ViewHolder {
		private TextView tv_denomination,tv_amount,tv_qty;
	}
	

	@Override
	public String getFilter() {
		return "";
	}


	@Override
	public ArrayList<Stocks_Entity> getFilteredList() {
		return alStockDataMain;
	}
    
    }
