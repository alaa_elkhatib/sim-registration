
package com.simregistration.signed;
import java.io.File;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class CPR_Preview extends Activity  {
	private ImageView mFormImageViewer/*,mIDNoImageViewer,mNationalityImageViewer,mNameImageViewer,mExpiryImageViewer*/;
    private  File mFormLocation,mIDInfoLocation,mIDInfoLocation2,mSIMLocation/*,mSigneture1Location/*,mBarLocation,mIDNoLocation,mNationalityLocation,mNameLocation/*,mExpiryLocation,
    mStampLocation,mSigneture1Location,mSigneture2Location*/;
    private ProgressDialog progress = null;
	private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.cpr_preview);  
        dbHelper = new DatabaseHelper(this);
		db = dbHelper.getWritableDatabase();
		updater = new ContentUpdater(this, db);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mFormLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Main.jpg");
        mFormImageViewer = (ImageView) findViewById(R.id.iv_form); 
        if(mFormLocation.exists()){
        	final BitmapFactory.Options options = new BitmapFactory.Options();
        	//options.inSampleSize = 2;
            Bitmap myBitmap = BitmapFactory.decodeFile(mFormLocation.getAbsolutePath()/*,options*/);
            mFormImageViewer.setImageBitmap(myBitmap); }
        //mSigneture1Location = new File(Environment.getExternalStorageDirectory(),"OCR/Signeture1.jpg");
        
        
       /* mStampLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Stamp.jpg");
     
        mSigneture2Location = new File(Environment.getExternalStorageDirectory(),"OCR/Signeture2.jpg");
      */   
    
        mIDInfoLocation = new File(Environment.getExternalStorageDirectory(),"OCR/CPRID2.jpg");
        
        mIDInfoLocation2 = new File(Environment.getExternalStorageDirectory(),"OCR/CPRID2.jpg");
        
      //  mSIMLocation = new File(Environment.getExternalStorageDirectory(),"OCR/SIM.jpg");
        
        
        /*
              mBarLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Bar.jpg");
             mNationalityLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Nationality.jpg");
        mNationalityImageViewer = (ImageView) findViewById(R.id.iv_nationality);
        if(mNationalityLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mNationalityLocation.getAbsolutePath());
            mNationalityImageViewer.setImageBitmap(myBitmap);}
        
        mNameLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Name.jpg");
        mNameImageViewer = (ImageView) findViewById(R.id.iv_name); 
        if(mNameLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mNameLocation.getAbsolutePath());
            mNameImageViewer.setImageBitmap(myBitmap);}
        
      mExpiryLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Expiry.jpg");
        mExpiryImageViewer = (ImageView) findViewById(R.id.iv_expiry);
        if(mExpiryLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mExpiryLocation.getAbsolutePath());
            mExpiryImageViewer.setImageBitmap(myBitmap);}*/
        }
    
   
    
   
    
    public void onCancel(View v){
    	Intent intent = new Intent(CPR_Preview.this, CPR_ID2.class);
    	intent.putExtra("ICCID", getIntent().getStringExtra("ICCID"));
		startActivity(intent);
    	this.finish();
    }
    
    
    public void onSend(View v){
    	 if(Utility.isConnected(CPR_Preview.this))
    		 new UploadCPRImages().execute(new String[]{});
    	 else
	   		Utility.showToast(CPR_Preview.this, getString(R.string.alert_need_internet_connection)); 
    }
    
    
    
    private class UploadCPRImages extends AsyncTask<String, Void, String> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(CPR_Preview.this, getString(R.string.pleaseWait), "", true, false, null);

		}

		@Override
		protected String doInBackground(String... params) {
			String strResult=CPR_Preview.this.getString(R.string.disabled);
			try {
				 
				 String strFolderName =  Utility.GetUserEntity(CPR_Preview.this).getId()+"_"+System.currentTimeMillis()+"_CPR";	 
				 File f = new File(mFormLocation.getPath());
				 if (f.exists()) { 
				  String strImage=WebService.GetImageFromFile(CPR_Preview.this,mFormLocation.getPath());
				  if(strImage.equalsIgnoreCase(""))
						return "Error GetImageFromFile";	  
				  else{
				  strResult= WebService.UploadImages(CPR_Preview.this,Utility.GetUserEntity(CPR_Preview.this).getId(), "CPR", strFolderName, "Main.png", strImage);
				  if(strResult==null)
					  return strResult;
				  else if(strResult.equalsIgnoreCase(CPR_Preview.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(CPR_Preview.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(CPR_Preview.this.getString(R.string.no_account)) || !strResult.equalsIgnoreCase("Success"))
						return strResult;
				  else{
				  strImage="";
				  f = new File(mIDInfoLocation.getPath());
				  if (f.exists()) { 
				  strImage=WebService.GetImageFromFile(CPR_Preview.this,mIDInfoLocation.getPath());
				  if(strImage.equalsIgnoreCase(""))
						return "Error GetImageFromFile";
				  else{
				  strResult="";
				  strResult=WebService.UploadImages(CPR_Preview.this,Utility.GetUserEntity(CPR_Preview.this).getId(), "CPR", strFolderName, "ID.png", strImage);
				  if(strResult==null)
					  return strResult;
				  else if(strResult.equalsIgnoreCase(CPR_Preview.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(CPR_Preview.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(CPR_Preview.this.getString(R.string.no_account)) || !strResult.equalsIgnoreCase("Success"))
						return strResult;
				  else{
			      strResult="";
				  strResult=WebService.InsertSIMTransaction(CPR_Preview.this,Utility.GetUserEntity(CPR_Preview.this).getId(),"CPR",getIntent().getStringExtra("ICCID"),strFolderName,strFolderName+"/Main.png");
				  
				
				  }}}
				  
				  else return mIDInfoLocation.getPath()+" doesnt exists";
				  }}}
				 else return mFormLocation.getPath()+" doesnt exists";	  }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(CPR_Preview.this,Utility.GetUserEntity(CPR_Preview.this).getId(),CPR_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
					 // if(Utility.isConnected(CPR_Preview.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR_Preview.this).getId(),CPR_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   	//else
		      	   	//	Utility.showToast(CPR_Preview.this, getString(R.string.alert_need_internet_connection)); 

				return "Error";
			}
			return strResult;
		}

		@Override
		protected void onPostExecute(String v) {
			super.onPostExecute(v);
			progress.dismiss();
		
			 
			try{
				if(v.equalsIgnoreCase("Success"))
					Utility.alert("Your transaction has been sent successfully, waiting for verification and activation", CPR_Preview.this, onclick);
				
				else if(v.equalsIgnoreCase("Error"))
					Utility.alert(getString(R.string.error),CPR_Preview.this,null);
				
				else if(v.equalsIgnoreCase(CPR_Preview.this.getString(R.string.disabled)) ||  v.equalsIgnoreCase(CPR_Preview.this.getString(R.string.locked))||  v.equalsIgnoreCase(CPR_Preview.this.getString(R.string.no_account)))
					Utility.alert(v, CPR_Preview.this, onclick3);
				else 
					Utility.alert(v, CPR_Preview.this, null);
			}catch(Exception ex){
				 WebService.SaveErrorLogNoBandwidth(CPR_Preview.this,Utility.GetUserEntity(CPR_Preview.this).getId(),CPR_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
					
				// if(Utility.isConnected(CPR_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR_Preview.this).getId(),CPR_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
      	   //	else
      	   //		Utility.showToast(CPR_Preview.this, getString(R.string.alert_need_internet_connection)); 
        
			
			}}}
    
    /*
    ----------------------------- LIFE CYCLE METHODS -----------------------------
    */
 
    
   
    
  
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			finish();
		}};
    
    
		
	  DialogInterface.OnClickListener onclick2 = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(CPR_Preview.this, CPR_ID2.class);
					intent.putExtra("ICCID", getIntent().getStringExtra("ICCID"));
					startActivity(intent);
					finish();
				}};
				
				
				
	 DialogInterface.OnClickListener onclick3 = new DialogInterface.OnClickListener() {
                  @Override
						public void onClick(DialogInterface dialog, int which) {
                	    updater.deleteOldContentsUser();
      			        Utility.ClearShared(CPR_Preview.this);
						Intent intent = new Intent(CPR_Preview.this, Login.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);}};
		    
		    
  

		/*private void saveCPRIDPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mIDInfoLocation2.exists())
					mIDInfoLocation2.createNewFile();
				image = new FileOutputStream(mIDInfoLocation2);
			} catch (Exception ex) {
				if(Utility.isConnected(CPR_Preview.this))
					 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR_Preview.this).getId(),CPR_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	
			}
			bm.compress(CompressFormat.JPEG, 100, image);
			bm.recycle();
			bm=null;
		}*/
		
		@Override
		public void onBackPressed() {
			Intent intent = new Intent(CPR_Preview.this, CPR_ID2.class);
			intent.putExtra("ICCID", getIntent().getStringExtra("ICCID"));
			startActivity(intent);
			finish();}

    
		
    

  

}
