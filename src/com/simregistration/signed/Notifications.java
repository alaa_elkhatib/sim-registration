package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Notification_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;

public class Notifications extends Activity  implements CustomAdapter.ListFiller {
	private ArrayList<Notification_Entity> alNotificationDataMain;
	ListView lvList;
	CustomAdapter adapter;
    private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private ProgressDialog progress = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_list);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
       // this.findViewById(R.id.btn_manual).setVisibility(View.GONE);
		lvList = (ListView)findViewById(R.id.lvList);
		alNotificationDataMain = new ArrayList<Notification_Entity>();
		adapter = new CustomAdapter(this,alNotificationDataMain, R.layout.notification_item, this);
		lvList.setAdapter(adapter); 
		lvList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				
         		if (alNotificationDataMain != null && alNotificationDataMain.get(pos) != null ) {
         			 Intent intent = new Intent(Notifications.this, Notification_Details.class);
					 intent.putExtra("Notification",  alNotificationDataMain.get(pos));
					 startActivity(intent);}}}); 
		dbHelper = new DatabaseHelper(Notifications.this);
	    db = dbHelper.getWritableDatabase();
	    new FetchNotificationData().execute();}
    

	private class FetchNotificationData extends AsyncTask<Void, Void, Void> {
		ArrayList<Notification_Entity> alNotificationData = new ArrayList<Notification_Entity>();
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Notifications.this, getString(R.string.pleaseWait), "", true, true, null);

		}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getNotificationContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					Notification_Entity cursorEntity = new Notification_Entity();
					cursorEntity.setSIM(crsr.getString(0));
					cursorEntity.setDateTime(crsr.getString(1));
					cursorEntity.setMsg(crsr.getString(2));
					cursorEntity.setTransID(crsr.getString(3));
					cursorEntity.setVarification(crsr.getString(4));
					cursorEntity.setActivation(crsr.getString(5));
					cursorEntity.setDateTime(crsr.getString(6));
					alNotificationData.add(cursorEntity);
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(Notifications.this,Utility.GetUserEntity(Notifications.this).getId(),Notifications.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				//	if(Utility.isConnected(Notifications.this))
				//			 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Notifications.this).getId(),Notifications.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			progress.dismiss();
			if (alNotificationData != null && alNotificationData.size() > 0) {
				alNotificationDataMain = alNotificationData;
				adapter.notifyDataSetChanged();
			} }
	}
	
	
	private Cursor getNotificationContentsCursor() {
		Cursor crsr = null;
		try {
			String sql="Select "+ C.SIM+","+C.DATETIME+","+C.MESSAGE+","+C.TRANSID+","+C.VARIFICATION+","+C.ACTIVATION +" From " + C.NOTIFICATION_TABLE +" Order By date("+C.NDATE+") Desc Limit 100"  ;
			//String[] from = { C.SIM,C.DATETIME,C.MESSAGE,C.TRANSID,C.VARIFICATION,C.ACTIVATION};
			crsr = db.rawQuery("Select "+ C.SIM+","+C.DATETIME+","+C.MESSAGE+","+C.TRANSID+","+C.VARIFICATION+","+C.ACTIVATION+","+C.NDATE +" From " + C.NOTIFICATION_TABLE +" Order By datetime("+C.NDATE+") Desc Limit 100"  , null);
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(Notifications.this,Utility.GetUserEntity(Notifications.this).getId(),Notifications.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
			//if(Utility.isConnected(Notifications.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Notifications.this).getId(),Notifications.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
	  
 


	@Override
	public void fillListData(View v, int pos) {
		
	}


	@Override
	public boolean isEnabled(int pos) {
		return true;
	}


	@Override
	public boolean useExtGetView() {
		return true;
	}


	@Override
	public View getView(int pos, View contextView) {
try{
			
			if (alNotificationDataMain== null || alNotificationDataMain.size() == 0)
				return  contextView;
			
				 LayoutInflater inf = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
				 contextView = inf.inflate(R.layout.notification_item, null);
				 final ViewHolder holder = new ViewHolder();
				 holder.tv_sim = (TextView) contextView.findViewById(R.id.tvSIM);
				 holder.tv_msg= (TextView) contextView.findViewById(R.id.tvMessage);
				 holder.ln_notification= (TableRow) contextView.findViewById(R.id.lnNotification);
				 
				 final Notification_Entity entity = (Notification_Entity) alNotificationDataMain.get(pos);
			    
			      
			    if (entity != null && entity.getSIM() != null)
					holder.tv_sim.setText(entity.getSIM());
			    
			    
			    if (entity != null && entity.getMsg() != null)
					holder.tv_msg.setText(entity.getMsg());
			    

			    if (entity != null && entity.getVarification() != null){
			    	if( entity.getVarification().equals("P"))
			    		holder.ln_notification.setBackgroundColor(Color.rgb( 255, 153, 0));
			    		
			    		else if(entity.getVarification().equals("R"))
			    			holder.ln_notification.setBackgroundColor(Color.RED);
			    		
			    		else if(entity.getVarification().equals("A")){
			    			   if (entity != null && entity.getActivation() != null){
			    				   if( entity.getActivation().equals("P"))
			   			    		holder.ln_notification.setBackgroundColor(Color.rgb( 255, 153, 0));
			    				   
			    				   else if(entity.getActivation().equals("R"))
						    			holder.ln_notification.setBackgroundColor(Color.RED);
			    				   
			    				   else if(entity.getActivation().equals("A"))
						    			holder.ln_notification.setBackgroundColor(Color.GREEN);
			    				   
			    				   
			    			   }}
			    		
					holder.tv_msg.setText(entity.getMsg());}
			    
			    
			    
			    
				
			
				
				}catch (Exception ex){
					WebService.SaveErrorLogNoBandwidth(Notifications.this,Utility.GetUserEntity(Notifications.this).getId(),Notifications.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  					
					//if(Utility.isConnected(Notifications.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Notifications.this).getId(),Notifications.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
return contextView;
	}

	
	class ViewHolder {
		private TextView tv_sim,tv_msg;
		private  TableRow ln_notification;
	}
	

	@Override
	public String getFilter() {
		return "";
	}


	@Override
	public ArrayList<Notification_Entity> getFilteredList() {
		return alNotificationDataMain;
	}
    
    }
