package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.IntentIntegrator;
import com.simregistration.signed.utility.IntentResult;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

public class MRZPassport extends Activity {
    private EditText edt_Name,edt_Sim,edt_ID,edt_Address;
    private Spinner sp_Country;
    private RadioButton rb_M,rb_F,rb_B,rb_N;
    private TextView txtBirthDay,txtExpiryDate;
    private int iYear, iMonth, iDay,eYear, eMonth, eDay;
    private Button btn_PassportImage,btn_Scan,btn_Edit;
    private Uri cameraTempUri;
    private final int TAKE_CAMERA_REQUEST = 1;
    private final int TAKE_PASSPORT_REQUEST = 2;
    private ArrayList<Countries_Entity> alCountriesDataMain;
    private ArrayList<String> alCountriesDataMainNames;
    CustomAdapter adapter;
    private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private String imgPath="";
	private Map<String, Locale> localeMap;
	//private ImageButton btn_Manual;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mrzpassport);  
      /*  btn_Manual = (ImageButton)this.findViewById(R.id.btn_manual);
        btn_Manual.setVisibility(View.VISIBLE);
        btn_Manual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					AssetManager assetManager = getAssets();

			    InputStream in = null;
			    OutputStream out = null;
			    File file = new File(getFilesDir(), "manual.docx");
			    try {
			        in = assetManager.open("manual.docx");
			        out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

			        Utility.copyFile(in, out);
			        in.close();
			        in = null;
			        out.flush();
			        out.close();
			        out = null;
			    } catch (Exception e) {
			        Log.e("tag", e.getMessage());
			    }

			    Intent intent = new Intent(Intent.ACTION_VIEW);
			    intent.setDataAndType(
			            Uri.parse("file://" + getFilesDir() + "/manual.docx"),
			            "application/docx");

			    startActivity(intent);
					}catch(Exception ex){
            		ex.getMessage();
            	}}});*/
        Utility.initCountryCodeMapping();
        final Calendar c = Calendar.getInstance();
	    eYear = c.get(Calendar.YEAR) ;
	    eMonth = c.get(Calendar.MONTH)+1;
	    eDay = c.get(Calendar.DAY_OF_MONTH);
	    iYear = c.get(Calendar.YEAR) ;
	    iMonth = c.get(Calendar.MONTH)+1;
	    iDay = c.get(Calendar.DAY_OF_MONTH);
        dbHelper = new DatabaseHelper(MRZPassport.this);
	    db = dbHelper.getWritableDatabase();
        alCountriesDataMain = new ArrayList<Countries_Entity>();
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_Name = (EditText)findViewById(R.id.edtName);
        edt_Sim = (EditText)findViewById(R.id.edtSim);
        edt_ID = (EditText)findViewById(R.id.edtID);
        sp_Country = (Spinner)findViewById(R.id.spCountry);
        edt_Address = (EditText)findViewById(R.id.edtAddress);
        txtBirthDay= (TextView)findViewById(R.id.txtBirthDay);
        txtBirthDay.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  			
  					DatePickerDialog dg = new DatePickerDialog(MRZPassport.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          iYear = year;
  				          iMonth = monthOfYear+1;
  				          iDay = dayOfMonth;
  				          
  				          txtBirthDay.setText( Integer.toString(iDay)+ "/" + Integer.toString(iMonth) + "/" +
  				        		 Integer.toString(iYear) );
  				      }

  					}, iYear, iMonth-1, iDay);
  					
  					dg.show();	// Show picker dialog
					} catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
  	
						//if(Utility.isConnected(PassportFields.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}}});
  	  
        txtExpiryDate= (TextView)findViewById(R.id.txtIDExpiry);
        txtExpiryDate.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  				
  			        
  					DatePickerDialog dg = new DatePickerDialog(MRZPassport.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          eYear = year;
  				          eMonth = monthOfYear+1;
  				          eDay = dayOfMonth;
  				          
  				        txtExpiryDate.setText( Integer.toString(eDay)+ "/" + Integer.toString(eMonth) + "/" +
  				        		 Integer.toString(eYear) );
  				      }

  					}, eYear, eMonth-1, eDay);
  					
  					dg.show();	// Show picker dialog
					} catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
					  	
						//if(Utility.isConnected(PassportFields.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}}});
        rb_M = (RadioButton)findViewById(R.id.rb_M);
  	    rb_F= (RadioButton)findViewById(R.id.rb_F);
  	    rb_B = (RadioButton)findViewById(R.id.rb_B);
  	    rb_N= (RadioButton)findViewById(R.id.rb_N);
  	    btn_PassportImage = (Button)this.findViewById(R.id.btnPassport);
  	    btn_PassportImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  /*if(edt_Name.getText().toString().equals(""))
						  Utility.showToast(MRZPassport.this,getResources().getString(R.string.enter_name));
					  else if (edt_Sim.getText().toString().equals(""))
						  Utility.showToast(MRZPassport.this,getResources().getString(R.string.enter_iccid));
					  else if(edt_ID.getText().toString().equals(""))
					   Utility.showToast(MRZPassport.this,getResources().getString(R.string.enter_idnumber));	
					  else if(sp_Country.getSelectedItemPosition()==0)
						   Utility.showToast(MRZPassport.this,getResources().getString(R.string.enter_country));	
					  else if(txtBirthDay.getText().toString().equals(""))
						   Utility.showToast(MRZPassport.this,getResources().getString(R.string.enter_Birthday));
					  else if(txtExpiryDate.getText().toString().equals(""))
						   Utility.showToast(MRZPassport.this,getResources().getString(R.string.enter_idexpiry));
					  else if (getDiffYears(iYear, iMonth, iDay)<C.AGE)
						  Utility.showToast(MRZPassport.this,getResources().getString(R.string.age_above)+" "+C.AGE);
					  else if(IsExpired(txtExpiryDate.getText().toString()))
						  Utility.showToast(MRZPassport.this,getResources().getString(R.string.id_expired));
					  else{*/
						  final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							
			                intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
			                startActivityForResult(intent, TAKE_CAMERA_REQUEST);
			                
			                
					 
			                
			         /* File dir = Environment.getExternalStorageDirectory();
			          File file = File.createTempFile("photo_", null, dir);
			          cameraTempUri = Uri.fromFile(file);
			          Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			          intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			          intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraTempUri);
			          startActivityForResult(intent, TAKE_CAMERA_REQUEST);}
					  */} catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
						  	
						 // if(Utility.isConnected(PassportFields.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   //	else
			      	   //		Utility.showToast(PassportFields.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); 
  	  btn_Scan = (Button)findViewById(R.id.btnScan);
  	  btn_Scan.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		try {
	      			IntentIntegrator integrator = new IntentIntegrator(MRZPassport.this);
	                integrator.initiateScan();
	                //startActivityForResult(integrator, 1);

	      		} catch (Exception ex) {
	      			WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
	      		  	
	      			//if(Utility.isConnected(PassportFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	      		}
	      	}
	      });
  	  
  	btn_Edit = (Button)findViewById(R.id.btnEdit);
  	btn_Edit.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      	   Enable_DisableControls(true);
	      		}
	      });
  	
  	  new FetchCountriesData().execute();}
    
    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Passport/", "image" + new Date().getTime() + ".png");
        File parent = file.getParentFile();
		if (parent != null) parent.mkdirs();
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
    
    
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        Intent toStart = null;
        switch(requestCode) {
            case TAKE_CAMERA_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
            		//new  ResizeImage().execute(new String[]{imgPath});
            		btn_PassportImage.setTextColor(getResources().getColor(R.color.green_light));
            		btn_PassportImage.setBackgroundColor(getResources().getColor(R.color.dark_gray));
              	}
              	
            break;
            case TAKE_PASSPORT_REQUEST:
                if (resultCode == RESULT_OK) {
        			setResult(RESULT_OK, data);
      				finish();
        		}
                break;
            case 49374:
            	if (resultCode == Activity.RESULT_OK) {
	                   IntentResult intentResult = 
	                   IntentIntegrator.parseActivityResult(requestCode, resultCode,data);
	                if (intentResult != null) {
	                	edt_Sim.setText(intentResult.getContents());} 
	             }
           
            	break;
           default:
               break;
        } }
    

    private class ResizeImage extends AsyncTask<String, Void, Bitmap> {
		ProgressDialog progress;
		@Override
		protected void onPreExecute() {
		progress = ProgressDialog.show(MRZPassport.this, getString(R.string.pleaseWait), "", true, false, null);
			super.onPreExecute();}

		@Override
		protected Bitmap doInBackground(String... params) {
			try {
				
				 Bitmap bitmap=WebService.decodeFile(1024,1024,imgPath);
        		 new File(imgPath).delete();
        		 return bitmap;
        		 }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);
			 if (bitmap != null) { 
				  Passport_Navy_Entity entity= new Passport_Navy_Entity();
                  if(rb_M.isChecked())
               	   entity.setGender("M");
                  else if(rb_F.isChecked())
               	   entity.setGender("F");
                  
               if(rb_B.isChecked())
               	   entity.setNationality("B");
                  else  if(rb_N.isChecked())
               	  entity.setNationality("N");
                   entity.setName(edt_Name.getText().toString());
                   entity.setMSISDN(edt_Sim.getText().toString());
                   entity.setId(edt_ID.getText().toString());
                   if(sp_Country.getSelectedItemPosition()==0)
                  	 entity.setCountry("");
                  else
                   entity.setCountry(alCountriesDataMain.get(sp_Country.getSelectedItemPosition()-1).getID());
                   entity.setAddress(edt_Address.getText().toString());
                   String strBirthDate="",strExpityDate=""; 
                   if(!txtBirthDay.getText().toString().equals("")){
                   	strBirthDate+=(iYear);
                   if((iMonth)<=9)
                   	strBirthDate+="0";
                   strBirthDate+=(iMonth );
                   if(iDay<=9)
                   	strBirthDate+="0";
                   strBirthDate+=(iDay);
                   entity.setBirthDay(strBirthDate);}
                   
                   if(!txtExpiryDate.getText().toString().equals("")){
                   	strExpityDate+=(eYear);
                   if((eMonth)<=9)
                   	strExpityDate+="0";
                   strExpityDate+=(eMonth );
                   if(eDay<=9)
                   	strExpityDate+="0";
                   strExpityDate+=(eDay);
                   entity.setExpiryDate(strExpityDate);}
                   
                   Intent intent = new Intent(MRZPassport.this, Passport_Navy_CR.class);
                   intent.putExtra("Passport_Navy_CR", entity);
                   try {
                       //Write file
                       String filename = "bitmap.png";
                       FileOutputStream stream = MRZPassport.this.openFileOutput(filename, Context.MODE_PRIVATE);
                       bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                       //Cleanup
                       stream.close();
                       bitmap.recycle();
                       intent.putExtra("image", filename);
                       intent.putExtra("document", "Passport");
                   } catch (Exception ex) {
                   	WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
                     	
                   	//if(Utility.isConnected(PassportFields.this))
							// new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

                   }
                   //intent.putExtra("BitmapImage", bitmap);
                   startActivityForResult(intent, TAKE_PASSPORT_REQUEST);
               }

			progress.dismiss();
			 }
	}
    
	private class FetchCountriesData extends AsyncTask<Void, Void, Void> {
		ArrayList<Countries_Entity> alColuntriesData = new ArrayList<Countries_Entity>();
		ArrayList<String> alColuntriesDataNames = new ArrayList<String>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getCountriesContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					Countries_Entity cursorEntity = new Countries_Entity();
					cursorEntity.setID(crsr.getString(0));
					cursorEntity.setNameEng(crsr.getString(1));
					cursorEntity.setNameAr(crsr.getString(2));
					alColuntriesData.add(cursorEntity);
					alColuntriesDataNames.add(crsr.getString(1));
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
				  	
					//if(Utility.isConnected(PassportFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			if (alColuntriesData != null && alColuntriesData.size() > 0) {
				alCountriesDataMain = alColuntriesData;
				alCountriesDataMainNames=alColuntriesDataNames;
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MRZPassport.this,
						android.R.layout.simple_spinner_item, alCountriesDataMainNames);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_Country.setAdapter(
					      new NothingSelectedSpinnerAdapter(
					    		dataAdapter,
					            R.layout.country_spinner_row_nothing_selected,
					            MRZPassport.this));
			      
			} 
			if(getIntent().getStringExtra("mode").equalsIgnoreCase("scan")){
			   btn_Edit.setVisibility(View.VISIBLE);
			   Enable_DisableControls(false);
			   FillDate();
			   }}
	}
	
	
	private Cursor getCountriesContentsCursor() {
		Cursor crsr = null;
		try {
			String[] from = { C.ID,C.NAME_ENG,C.NAME_AR};
			crsr = db.query(C.COUNTRIES_TABLE, from,null, null, null, null,C.NAME_ENG+" ASC");
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(MRZPassport.this,Utility.GetUserEntity(MRZPassport.this).getId(),MRZPassport.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
		  	
			//if(Utility.isConnected(PassportFields.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
	
	
	public static int getDiffYears(int _year, int _month, int _day) {
	    SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
	    java.util.Date d = null;
	    java.util.Date d1 = null;
	    Calendar cal = Calendar.getInstance();
	    try {
	            d = dfDate.parse(_day+"/"+_month+"/"+_year);
	            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
	        } catch (java.text.ParseException e) {
	            e.printStackTrace();
	        }
    Calendar a = getCalendar(d);
    Calendar b = getCalendar(d1);
    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
    if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
        diff--;
    }
    return diff;
}

public static Calendar getCalendar(Date date) {
    Calendar cal = Calendar.getInstance(Locale.US);
    cal.setTime(date);
    return cal;
}
 public int getAge (int _year, int _month, int _day) {
		 
		 SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
		    java.util.Date d = null;
		    java.util.Date d1 = null;
		    Calendar cal = Calendar.getInstance();
		    try {
		            d = dfDate.parse(_day+"/"+_month+"/"+_year);
		            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
		        } catch (java.text.ParseException e) {
		            e.printStackTrace();
		        }

		    int diffInDays = (int) ((d1.getTime() - d.getTime())/ (1000 * 60 * 60 * 24));

		    
	        return diffInDays;
	    }
	/* public int getAge (int _year, int _month, int _day) {

	        GregorianCalendar cal = new GregorianCalendar();
	        int y, m, d, a;         

	        y = cal.get(Calendar.YEAR);
	        m = cal.get(Calendar.MONTH);
	        d = cal.get(Calendar.DAY_OF_MONTH);
	        cal.set(_year, _month, _day);
	        a = y - cal.get(Calendar.YEAR);
	        if ((m < cal.get(Calendar.MONTH))
	                        || ((m == cal.get(Calendar.MONTH)) && (d < cal
	                                        .get(Calendar.DAY_OF_MONTH)))) {
	                --a;
	        }
	       
	        return a;
	    }*/
	public boolean IsExpired(String valid_until){ 
		boolean expired =false;
		 try{ 
			 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			 String strToday = sdf.format(new Date()); 
			 
			 Date strExpiryDate = sdf.parse(valid_until);
			 Date strDate = sdf.parse(strToday);
			 if (strExpiryDate.compareTo(strDate)<0) {
				 expired = true;
			 }
			 
		}catch(Exception ex){
			 ex.getMessage();
		 }
		return expired;}
	
	
	private void Enable_DisableControls(boolean blEnable){
		edt_Name.setEnabled(blEnable);
  		rb_M.setEnabled(blEnable);
  		rb_F.setEnabled(blEnable);
  		rb_B.setEnabled(blEnable);
  		rb_N.setEnabled(blEnable);
  		edt_ID.setEnabled(blEnable);
  		txtBirthDay.setEnabled(blEnable);
  		txtExpiryDate.setEnabled(blEnable);
  		sp_Country.setEnabled(blEnable);
		
	}
	 private void FillDate(){
	  Bundle params = getIntent().getExtras();
      String[] names = params.getStringArray("names");
      String[] values = params.getStringArray("values");
      String strFirstName="",strSureName="",strCountry="";
      for(int i=0; i < names.length; i++)
      {
    	  if(names[i].equals("Document Type"))
    		   Utility.alert(values[i], MRZPassport.this, null);
    		 
    	  
    	  if(names[i].equals("Surname"))
    		  strSureName=values[i];
      	  
      	  if(names[i].equals("Name"))
      		strFirstName=values[i];
    	  
    	  if(names[i].equals("Sex")){
    		  if(values[i].equalsIgnoreCase("M"))
    			  rb_M.setChecked(true);
    		  else 
    			  rb_F.setChecked(true);}
    	  
    	  if(names[i].equals("Country")){
      		  if(values[i].equalsIgnoreCase("BHR"))
      			  rb_B.setChecked(true);
      		  else 
      			  rb_N.setChecked(true);}
      	  
    	  if(names[i].equals("Nationality")){
      		  if(values[i].length()==3 && !values[i].equalsIgnoreCase("BHR"))
      			  strCountry=Utility.iso3CountryCodeToIso2CountryCode(values[i]);
      		  else {
      			  if(values[i].equalsIgnoreCase("BHR"))
      				  strCountry="BAH";
      			  else 
      			  strCountry=values[i];}
      		  for(int j=0;j<alCountriesDataMain.size();j++){
      			  Countries_Entity country = alCountriesDataMain.get(j);
      			  if(country.getID().trim().equalsIgnoreCase(strCountry.trim()))
      				  sp_Country.setSelection(j+1);  }}
    	  
    	  if(names[i].equals("Document Number"))
    		  edt_ID.setText(values[i]);
    	  
    	 
    	  if(names[i].equals("Birth date")){
     		 String strBirthDate=values[i];
     		  String[] iDate=strBirthDate.split(" ");
     		  iYear = Integer.parseInt(iDate[2]);
 		      iMonth = Integer.parseInt(iDate[1]);
 		      iDay =  Integer.parseInt(iDate[0]);
 		     txtBirthDay.setText( Integer.toString(iDay)+ "/" + Integer.toString(iMonth) + "/" +
 		        		 Integer.toString(iYear) );
     	  }
    	  
    	  if(names[i].equals("Expiry date")){
    		 String strExpiryDate=values[i];
    		  String[] eDate=strExpiryDate.split(" ");
    		  eYear = Integer.parseInt(eDate[2]);
		      eMonth = Integer.parseInt(eDate[1]);
		      eDay =  Integer.parseInt(eDate[0]);
		      txtExpiryDate.setText( Integer.toString(eDay)+ "/" + Integer.toString(eMonth) + "/" +
		        		 Integer.toString(eYear) );
    	  }
    	   }
      edt_Name.setText(strFirstName +" "+ strSureName);

}

	    @Override
	    public void onResume() {
	        super.onResume();  // Always call the superclass method first
	        //btn_Manual.setVisibility(View.VISIBLE);
	    }
		
		
    }
