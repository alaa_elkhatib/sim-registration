package com.simregistration.signed;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ClearableEditText;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.IntentIntegrator;
import com.simregistration.signed.utility.IntentResult;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

public class MRZCPR extends Activity {
    private EditText edt_Name,edt_Sim,edt_ID,edt_Address;
    LinearLayout ln_ICCID;
    private Spinner sp_Country;
    private RadioButton rb_M,rb_F,rb_B,rb_N;
    private TextView txtBirthDay,txtExpiryDate;
    private int iYear, iMonth, iDay,eYear, eMonth, eDay;
    private Button btn_CPRImage,btn_Scan,btn_Edit,btn_Submit,btn_AddICCID;
    private Uri cameraTempUri;
    private final int TAKE_CAMERA_REQUEST = 1;
    private ArrayList<Countries_Entity> alCountriesDataMain;
    private ArrayList<String> alCountriesDataMainNames;
    CustomAdapter adapter;
    private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
	private String imgPath="";
	private ProgressDialog progress = null;
	private ArrayList<EditText> alICCID= new ArrayList<EditText>();
	private int iEditTextPos=0;
	private Bitmap CPR;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mrzcpr);  
        Utility.initCountryCodeMapping();
        final Calendar c = Calendar.getInstance();
	    eYear = c.get(Calendar.YEAR) ;
	    eMonth = c.get(Calendar.MONTH)+1;
	    eDay = c.get(Calendar.DAY_OF_MONTH);
	    iYear = c.get(Calendar.YEAR) ;
	    iMonth = c.get(Calendar.MONTH)+1;
	    iDay = c.get(Calendar.DAY_OF_MONTH);
        dbHelper = new DatabaseHelper(MRZCPR.this);
	    db = dbHelper.getWritableDatabase();
        alCountriesDataMain = new ArrayList<Countries_Entity>();
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        ln_ICCID = (LinearLayout)findViewById(R.id.lnSim);
     
        edt_Name = (EditText)findViewById(R.id.edtName);
        edt_Sim = (EditText)findViewById(R.id.edtSim);
        alICCID.add(edt_Sim);
        edt_ID = (EditText)findViewById(R.id.edtID);
        sp_Country = (Spinner)findViewById(R.id.spCountry);
        edt_Address = (EditText)findViewById(R.id.edtAddress);
        txtBirthDay= (TextView)findViewById(R.id.txtBirthDay);
        txtBirthDay.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  			
  					DatePickerDialog dg = new DatePickerDialog(MRZCPR.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          iYear = year;
  				          iMonth = monthOfYear+1;
  				          iDay = dayOfMonth;
  				          
  				          txtBirthDay.setText( Integer.toString(iDay)+ "/" + Integer.toString(iMonth) + "/" +
  				        		 Integer.toString(iYear) );
  				      }

  					}, iYear, iMonth-1, iDay);
  					
  					dg.show();	// Show picker dialog
					} catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
  	
						//if(Utility.isConnected(PassportFields.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}}});
  	  
        txtExpiryDate= (TextView)findViewById(R.id.txtIDExpiry);
        txtExpiryDate.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View arg0) {
  				try {
  				
  			        
  					DatePickerDialog dg = new DatePickerDialog(MRZCPR.this, new DatePickerDialog.OnDateSetListener() {
  						
  						@Override
  						public void onDateSet(DatePicker view, int year, 
  	                            int monthOfYear, int dayOfMonth) {
  						
  				          eYear = year;
  				          eMonth = monthOfYear+1;
  				          eDay = dayOfMonth;
  				          
  				        txtExpiryDate.setText( Integer.toString(eDay)+ "/" + Integer.toString(eMonth) + "/" +
  				        		 Integer.toString(eYear) );
  				      }

  					}, eYear, eMonth-1, eDay);
  					
  					dg.show();	// Show picker dialog
					} catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
					  	
						//if(Utility.isConnected(PassportFields.this))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}}});
        rb_M = (RadioButton)findViewById(R.id.rb_M);
  	    rb_F= (RadioButton)findViewById(R.id.rb_F);
  	    rb_B = (RadioButton)findViewById(R.id.rb_B);
  	    rb_N= (RadioButton)findViewById(R.id.rb_N);
  	    btn_AddICCID = (Button)findViewById(R.id.btnAddMore);
  	    btn_AddICCID.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					
					
					
					LinearLayout ln_more = new LinearLayout(MRZCPR.this);
					ln_more.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					ln_more.setOrientation(LinearLayout.HORIZONTAL);
					
					
					LinearLayout.LayoutParams Editparams = new LinearLayout.LayoutParams(
					0, edt_Sim.getHeight());
					Editparams.weight = 0.7f;
					
					LinearLayout.LayoutParams Buttonparams = new LinearLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						
					final ClearableEditText myEditText = new ClearableEditText(MRZCPR.this); // Pass it an Activity or Context
					final int sdk = android.os.Build.VERSION.SDK_INT;
					if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
						myEditText.setBackgroundDrawable( getResources().getDrawable(R.drawable.rounded) );
					} else {
						myEditText.setBackground( getResources().getDrawable(R.drawable.rounded));
					}
					myEditText.setPadding(10, 0, 10, 0);
					myEditText.setRawInputType(InputType.TYPE_CLASS_NUMBER);
					myEditText.setSingleLine(true);
					myEditText.setHint(getResources().getString(R.string.sim));
					
					
					Button myButton = new Button(MRZCPR.this); // Pass it an Activity or Context
					myButton.setText(getResources().getString(R.string.scan));
					myButton.setOnClickListener(new View.OnClickListener(){

				      	public void onClick(View v){
				      		try {
				      			iEditTextPos=alICCID.indexOf(myEditText);
				      			IntentIntegrator integrator = new IntentIntegrator(MRZCPR.this);
				                integrator.initiateScan();
				      		} catch (Exception ex) {
				      			WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				      			}}});
					myEditText.setLayoutParams(Editparams); 
					myButton.setLayoutParams(Buttonparams); 
					ln_more.addView(myEditText);
					ln_more.addView(myButton);
					ln_ICCID.addView(ln_more);
					alICCID.add(myEditText);
					} catch (Exception ex) {
						  }
			}}); 
  	    btn_CPRImage = (Button)this.findViewById(R.id.btnCPR);
  	    btn_CPRImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					 File dir = Environment.getExternalStorageDirectory();
   			         File file = File.createTempFile("photo_", null, dir);
   			         cameraTempUri = Uri.fromFile(file);
   			         Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
   			         intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
   			         intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraTempUri);
   			         startActivityForResult(intent, TAKE_CAMERA_REQUEST);
   					
			       } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
						  	
						 // if(Utility.isConnected(PassportFields.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   //	else
			      	   //		Utility.showToast(PassportFields.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); 
  	  btn_Scan = (Button)findViewById(R.id.btnScan);
  	  btn_Scan.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		try {
	      			iEditTextPos=0;
	      			IntentIntegrator integrator = new IntentIntegrator(MRZCPR.this);
	                integrator.initiateScan();

	      		} catch (Exception ex) {
	      			WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
	      			}}});
  	  
  	btn_Edit = (Button)findViewById(R.id.btnEdit);
  	btn_Edit.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      	   Enable_DisableControls(true);
	      		}
	      });
	btn_Submit = (Button)findViewById(R.id.btnSubmit);
	btn_Submit.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		int iICCID=0;
	      		for( int i=0;i<alICCID.size();i++){
	      			EditText edt=alICCID.get(i);
	      			if(!edt.getText().toString().trim().equals(""))
	      				iICCID++;}
	      		if(edt_Name.getText().toString().equals(""))
				  Utility.showToast(MRZCPR.this,getResources().getString(R.string.enter_name));
			  else if (iICCID==0)
				  Utility.showToast(MRZCPR.this,getResources().getString(R.string.enter_iccid));
			  else if(edt_ID.getText().toString().equals(""))
			   Utility.showToast(MRZCPR.this,getResources().getString(R.string.enter_idnumber));	
			  else if(sp_Country.getSelectedItemPosition()==0)
				   Utility.showToast(MRZCPR.this,getResources().getString(R.string.enter_country));	
			  else if(txtBirthDay.getText().toString().equals(""))
				   Utility.showToast(MRZCPR.this,getResources().getString(R.string.enter_Birthday));
			  else if(txtExpiryDate.getText().toString().equals(""))
				   Utility.showToast(MRZCPR.this,getResources().getString(R.string.enter_idexpiry));
			  else if (getDiffYears(iYear, iMonth, iDay)<C.AGE)
				  Utility.showToast(MRZCPR.this,getResources().getString(R.string.age_above)+" "+C.AGE);
			  else if(IsExpired(txtExpiryDate.getText().toString()))
				  Utility.showToast(MRZCPR.this,getResources().getString(R.string.id_expired));
			  else if(CPR==null)
				  Utility.showToast(MRZCPR.this,getResources().getString(R.string.select_picture));
			 
			  else {
				  Utility.alert(getResources().getString(R.string.iccid_total)+ iICCID, MRZCPR.this,   new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							 String strGender="",setNationality="",strCountry=""
									  ,strBirthDate="",strExpityDate="",cpr_image="";
							  if(rb_M.isChecked())
								  strGender="M";
				                 else if(rb_F.isChecked())
				                	 strGender="F";
							  if(rb_B.isChecked())
								  setNationality="B";
				                 else  if(rb_N.isChecked())
				                	 setNationality="N";
							  if(sp_Country.getSelectedItemPosition()==0)
								  strCountry="";
				                 else
				                	 strCountry=(alCountriesDataMain.get(sp_Country.getSelectedItemPosition()-1).getID());
							  
			                  if(!txtBirthDay.getText().toString().equals("")){
			                  	strBirthDate+=(iYear);
			                  if((iMonth)<=9)
			                  	strBirthDate+="0";
			                  strBirthDate+=(iMonth );
			                  if(iDay<=9)
			                  	strBirthDate+="0";
			                  strBirthDate+=(iDay);}
			                  
			                  if(!txtExpiryDate.getText().toString().equals("")){
			                  	strExpityDate+=(eYear);
			                  if((eMonth)<=9)
			                  	strExpityDate+="0";
			                  strExpityDate+=(eMonth );
			                  if(eDay<=9)
			                  	strExpityDate+="0";
			                  strExpityDate+=(eDay);}
			                  if(CPR!=null){
	                			  ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
	                			  CPR.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
	                			  byte[] byteArray = byteArrayOutputStream .toByteArray();
	                			  cpr_image= Base64.encodeToString(byteArray, Base64.DEFAULT);
	                		  }
							 if(Utility.isConnected(MRZCPR.this))
								 new UploadMRZCPR().execute(new String[] {edt_Sim.getText().toString(),edt_Name.getText().toString()
										 ,strGender,setNationality,edt_ID.getText().toString(),strCountry,edt_Address.getText().toString()
										 ,strBirthDate,strExpityDate,cpr_image});
					      	  	else
					      	  		Utility.showToast(MRZCPR.this, getString(R.string.alert_need_internet_connection)); 
					        
						}});

				 
	      		}}
	      });
  	
  	  new FetchCountriesData().execute();}
    
    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Passport/", "image" + new Date().getTime() + ".png");
        File parent = file.getParentFile();
		if (parent != null) parent.mkdirs();
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
    
    
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        Intent toStart = null;
        switch(requestCode) {
            case TAKE_CAMERA_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
            		 Uri photoUri = cameraTempUri;
                     if (photoUri != null) {
                     CPR = BitmapUtils.getBitmap(MRZCPR.this, photoUri, 12);
                      if(CPR!=null){
                    	btn_CPRImage.setTextColor(getResources().getColor(R.color.green_light));
                  		btn_CPRImage.setBackgroundColor(getResources().getColor(R.color.dark_gray));
                        }
              	}}
              	
            break;
            case 49374:
            	if (resultCode == Activity.RESULT_OK) {
	                   IntentResult intentResult = 
	                   IntentIntegrator.parseActivityResult(requestCode, resultCode,data);
	                   
	                if (intentResult != null) {
	                	alICCID.get(iEditTextPos).setText(intentResult.getContents());
	                	} 
	             }
           
            	break;
           default:
               break;
        } }
    

    
    
	private class FetchCountriesData extends AsyncTask<Void, Void, Void> {
		ArrayList<Countries_Entity> alColuntriesData = new ArrayList<Countries_Entity>();
		ArrayList<String> alColuntriesDataNames = new ArrayList<String>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();}

		@Override
		protected Void doInBackground(Void... params) {
			try {	
				Cursor crsr = getCountriesContentsCursor();
				crsr.moveToFirst();
				if(crsr!=null){
				while (!crsr.isAfterLast()) {
					Countries_Entity cursorEntity = new Countries_Entity();
					cursorEntity.setID(crsr.getString(0));
					cursorEntity.setNameEng(crsr.getString(1));
					cursorEntity.setNameAr(crsr.getString(2));
					alColuntriesData.add(cursorEntity);
					alColuntriesDataNames.add(crsr.getString(1));
					crsr.moveToNext();
				} crsr.close();
				} }catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
				  	
					//if(Utility.isConnected(PassportFields.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			super.onPostExecute(v);
			if (alColuntriesData != null && alColuntriesData.size() > 0) {
				alCountriesDataMain = alColuntriesData;
				alCountriesDataMainNames=alColuntriesDataNames;
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MRZCPR.this,
						android.R.layout.simple_spinner_item, alCountriesDataMainNames);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_Country.setAdapter(
					      new NothingSelectedSpinnerAdapter(
					    		dataAdapter,
					            R.layout.country_spinner_row_nothing_selected,
					            MRZCPR.this));
			      
			} 
			if(getIntent().getStringExtra("mode").equalsIgnoreCase("scan")){
			   btn_Edit.setVisibility(View.VISIBLE);
			   Enable_DisableControls(false);
			   FillDate();
			   }}
	}
	
	
	private Cursor getCountriesContentsCursor() {
		Cursor crsr = null;
		try {
			String[] from = { C.ID,C.NAME_ENG,C.NAME_AR};
			crsr = db.query(C.COUNTRIES_TABLE, from,null, null, null, null,C.NAME_ENG+" ASC");
		} catch (Exception ex) {
			WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
		  	
			//if(Utility.isConnected(PassportFields.this))
			//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportFields.this).getId(),PassportFields.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

		}
		return crsr;
	}
	
	
	public static int getDiffYears(int _year, int _month, int _day) {
	    SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
	    java.util.Date d = null;
	    java.util.Date d1 = null;
	    Calendar cal = Calendar.getInstance();
	    try {
	            d = dfDate.parse(_day+"/"+_month+"/"+_year);
	            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
	        } catch (java.text.ParseException e) {
	            e.printStackTrace();
	        }
    Calendar a = getCalendar(d);
    Calendar b = getCalendar(d1);
    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
    if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
        diff--;
    }
    return diff;
}

public static Calendar getCalendar(Date date) {
    Calendar cal = Calendar.getInstance(Locale.US);
    cal.setTime(date);
    return cal;
}
 public int getAge (int _year, int _month, int _day) {
		 
		 SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
		    java.util.Date d = null;
		    java.util.Date d1 = null;
		    Calendar cal = Calendar.getInstance();
		    try {
		            d = dfDate.parse(_day+"/"+_month+"/"+_year);
		            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
		        } catch (java.text.ParseException e) {
		            e.printStackTrace();
		        }

		    int diffInDays = (int) ((d1.getTime() - d.getTime())/ (1000 * 60 * 60 * 24));

		    
	        return diffInDays;
	    }
	/* public int getAge (int _year, int _month, int _day) {

	        GregorianCalendar cal = new GregorianCalendar();
	        int y, m, d, a;         

	        y = cal.get(Calendar.YEAR);
	        m = cal.get(Calendar.MONTH);
	        d = cal.get(Calendar.DAY_OF_MONTH);
	        cal.set(_year, _month, _day);
	        a = y - cal.get(Calendar.YEAR);
	        if ((m < cal.get(Calendar.MONTH))
	                        || ((m == cal.get(Calendar.MONTH)) && (d < cal
	                                        .get(Calendar.DAY_OF_MONTH)))) {
	                --a;
	        }
	       
	        return a;
	    }*/
	public boolean IsExpired(String valid_until){ 
		boolean expired =false;
		 try{ 
			 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			 String strToday = sdf.format(new Date()); 
			 
			 Date strExpiryDate = sdf.parse(valid_until);
			 Date strDate = sdf.parse(strToday);
			 if (strExpiryDate.compareTo(strDate)<0) {
				 expired = true;
			 }
			 
		}catch(Exception ex){
			 ex.getMessage();
		 }
		return expired;}
	
	
	private void Enable_DisableControls(boolean blEnable){
		edt_Name.setEnabled(blEnable);
  		rb_M.setEnabled(blEnable);
  		rb_F.setEnabled(blEnable);
  		rb_B.setEnabled(blEnable);
  		rb_N.setEnabled(blEnable);
  		edt_ID.setEnabled(blEnable);
  		txtBirthDay.setEnabled(blEnable);
  		txtExpiryDate.setEnabled(blEnable);
  		sp_Country.setEnabled(blEnable);
		
	}
	 private void FillDate(){
	  Bundle params = getIntent().getExtras();
      String[] names = params.getStringArray("names");
      String[] values = params.getStringArray("values");
      String strFirstName="",strSureName="",strCountry="";
     
      for(int i=0; i < names.length; i++)
      {
    	  if(names[i].equals("Surname"))
    		  strSureName=values[i];
      	  
      	  if(names[i].equals("Name"))
      		strFirstName=values[i];
    	  
    	  if(names[i].equals("Sex")){
    		  if(values[i].equalsIgnoreCase("M"))
    			  rb_M.setChecked(true);
    		  else 
    			  rb_F.setChecked(true);}
    	  
    	  if(names[i].equals("Country")){
      		  if(values[i].equalsIgnoreCase("BHR"))
      			  rb_B.setChecked(true);
      		  else 
      			  rb_N.setChecked(true);}
      	  
    	  if(names[i].equals("Nationality")){
      		  if(values[i].length()==3 && !values[i].equalsIgnoreCase("BHR"))
      			  strCountry=Utility.iso3CountryCodeToIso2CountryCode(values[i]);
      		  else {
      			  if(values[i].equalsIgnoreCase("BHR"))
      				  strCountry="BAH";
      			  else 
      			  strCountry=values[i];}
      		  for(int j=0;j<alCountriesDataMain.size();j++){
      			  Countries_Entity country = alCountriesDataMain.get(j);
      			  if(country.getID().trim().equalsIgnoreCase(strCountry.trim()))
      				  sp_Country.setSelection(j+1);  }}
    	  
    	  if(names[i].equals("Document Number"))
    		  edt_ID.setText(values[i]);
    	  
    	 
    	  if(names[i].equals("Birth date")){
     		 String strBirthDate=values[i];
     		  String[] iDate=strBirthDate.split(" ");
     		  iYear = Integer.parseInt(iDate[2]);
 		      iMonth = Integer.parseInt(iDate[1]);
 		      iDay =  Integer.parseInt(iDate[0]);
 		     txtBirthDay.setText( Integer.toString(iDay)+ "/" + Integer.toString(iMonth) + "/" +
 		        		 Integer.toString(iYear) );
     	  }
    	  
    	  if(names[i].equals("Expiry date")){
    		 String strExpiryDate=values[i];
    		  String[] eDate=strExpiryDate.split(" ");
    		  eYear = Integer.parseInt(eDate[2]);
		      eMonth = Integer.parseInt(eDate[1]);
		      eDay =  Integer.parseInt(eDate[0]);
		      txtExpiryDate.setText( Integer.toString(eDay)+ "/" + Integer.toString(eMonth) + "/" +
		        		 Integer.toString(eYear) );
    	  }
    	   }
      edt_Name.setText(strFirstName +" "+ strSureName);

}
	 private class UploadMRZCPR extends AsyncTask<String, Void, String> {
	    	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				progress = ProgressDialog.show(MRZCPR.this, getString(R.string.pleaseWait), "", true, false, null);

			}

			@Override
			protected String doInBackground(String... params) {
				String strResult=MRZCPR.this.getString(R.string.disabled);
				try {
					 String strFolderName =  Utility.GetUserEntity(MRZCPR.this).getId()+"_"+System.currentTimeMillis()+"_CPRMRZ";	
					  strResult= WebService.UploadImages(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(), "CPRMRZ", strFolderName, "ID.png", params[9]);
					  if(strResult==null)
						  return strResult;
					  else if(strResult.equalsIgnoreCase(MRZCPR.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(MRZCPR.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(MRZCPR.this.getString(R.string.no_account)) || !strResult.equalsIgnoreCase("Success"))
							return strResult;
					  else{
				      strResult="";
				      strResult=WebService.InsertSIMTransaction(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(), params[0],params[1],
				    		  params[2], params[3],"CPRMRZ",params[4], params[5], params[6],params[7],params[8], strFolderName, strFolderName+"/ID.png");
					
					   }
					  
					  }catch (Exception ex) {
						 WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
						
					return "Error";
				}
				return strResult;
			}

			@Override
			protected void onPostExecute(String v) {
				super.onPostExecute(v);
				progress.dismiss();
				try{
					if(v.equalsIgnoreCase("Success"))
						Utility.alert("Your transaction has been sent successfully, waiting for verification and activation", MRZCPR.this, null);
					
					else if(v.equalsIgnoreCase("Error"))
						Utility.alert(getString(R.string.error),MRZCPR.this,null);
					
					else if(v.equalsIgnoreCase(MRZCPR.this.getString(R.string.disabled)) ||  v.equalsIgnoreCase(MRZCPR.this.getString(R.string.locked))||  v.equalsIgnoreCase(MRZCPR.this.getString(R.string.no_account)))
						Utility.alert(v, MRZCPR.this, null);
					else 
						Utility.alert(v, MRZCPR.this, null);
				}catch(Exception ex){
					 WebService.SaveErrorLogNoBandwidth(MRZCPR.this,Utility.GetUserEntity(MRZCPR.this).getId(),MRZCPR.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   			
						
					}}}
	    
		
    }
