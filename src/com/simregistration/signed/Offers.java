package com.simregistration.signed;
import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.simregistration.signed.entity.Offers_Entity;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;

public class Offers extends Activity {
    private ArrayList<Offers_Entity> alOffersDataMain;
	private ProgressDialog progress = null;	
	private Gallery galary_offers;
	private OffersAdapter OfferAdapter = null;
	ImageLoader imageLoader = ImageLoader.getInstance();
	private DisplayImageOptions options;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offers);  
       this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
       galary_offers = (Gallery) findViewById(R.id.gallery);
      
       galary_offers.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Offers_Entity entity = (Offers_Entity) alOffersDataMain.get(arg2);
				Intent intent = new Intent(Offers.this, Offer_Details.class);
				intent.putExtra("Images", entity.getImages());
				intent.putExtra("Description", entity.getDescription());
				startActivity(intent);
			}
		});
       options = new DisplayImageOptions.Builder()
		.cacheInMemory()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
    	if(Utility.isConnected(Offers.this))
          	 new GetOffers().execute( new String[]{Utility.GetUserEntity(Offers.this).getId()}); 
          else
  	   		Utility.showToast(Offers.this, getString(R.string.alert_need_internet_connection));
      }
    
    private class GetOffers extends AsyncTask<String, Void, Integer> {
    	ArrayList<Offers_Entity> alOffersData= new  ArrayList<Offers_Entity>();
		
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Offers.this, getString(R.string.pleaseWait), "", true, true, null);
			}

		@Override
		protected Integer doInBackground(String ... params) {
			int iResult=-1;
			try {
				iResult =WebService.GetOffers(Offers.this, params[0], alOffersData);
				 }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(Offers.this,Utility.GetUserEntity(Offers.this).getId(),Offers.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
								return null;
			}
			return iResult;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v==-1)
	  				Utility.showToast(Offers.this, getString(R.string.error));
				else{
				if(alOffersData.size()>0){
					alOffersDataMain = alOffersData; 
					OfferAdapter = new OffersAdapter(alOffersDataMain);
				    galary_offers.setAdapter(OfferAdapter);}}
			
				}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(Offers.this,Utility.GetUserEntity(Offers.this).getId(),Offers.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}}}

    
    private class OffersAdapter extends BaseAdapter {

		ArrayList<Offers_Entity> alOffersData = null;

		public OffersAdapter(ArrayList<Offers_Entity> alOffersData) {
			this.alOffersData = alOffersData;
			}

		@Override
		public int getCount() {
			return alOffersData.size();
		}

		@Override
		public Object getItem(int arg0) {
			return alOffersData.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(Offers.this).inflate(R.layout.offer_item, null);
				holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
				holder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
				holder.iv_offer = (ImageView) convertView.findViewById(R.id.iv_offer);
				holder.pd = convertView.findViewById(R.id.screener_pd);
				holder.pd.setVisibility(View.VISIBLE);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			 final Offers_Entity entity = (Offers_Entity) alOffersData.get(position);
			    

			  if (entity != null && entity.getName() != null)
			    	 holder.tv_name.setText(entity.getName());
			  
			  if (entity != null && entity.getDescription() != null)
			    	 holder.tv_description.setText(entity.getDescription());
			  
			try {
				 if (entity != null && entity.getImage() != null){
		     	    imageLoader.displayImage(entity.getImage(),holder.iv_offer, options, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							holder.pd.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							holder.pd.setVisibility(View.GONE);
						    holder.iv_offer.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
						
						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							holder.pd.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
							// TODO Auto-generated method stub
							
						}});
		     	}
				    else 
				    	holder.iv_offer.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
					
				    }catch(Exception ex){}

			return convertView;
		}

		class ViewHolder {
			private ImageView iv_offer;
			private TextView tv_name,tv_description;
			private View pd;
		}
	}
    
	
    }
