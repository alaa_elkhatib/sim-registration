package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;

public class History_List extends Activity  implements CustomAdapter.ListFiller {
	private ArrayList<SIMTransaction_Entity> alSimTransactionDataMain;
	ListView lvList;
	CustomAdapter adapter;
    private ProgressDialog progress = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_list);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
		lvList = (ListView)findViewById(R.id.lvList);
		alSimTransactionDataMain = getIntent().getParcelableArrayListExtra("Hisotry");
		adapter = new CustomAdapter(this,alSimTransactionDataMain, R.layout.history_item, this);
		lvList.setAdapter(adapter);
		lvList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
					
	         		if (alSimTransactionDataMain != null && alSimTransactionDataMain.get(pos) != null ) {
	         			 Intent intent = new Intent(History_List.this, History_Details.class);
						 intent.putExtra("Hisotry",  alSimTransactionDataMain.get(pos));
						 startActivity(intent);}}}); }
    
    
 


	@Override
	public void fillListData(View v, int pos) {
		
	}


	@Override
	public boolean isEnabled(int pos) {
		return true;
	}


	@Override
	public boolean useExtGetView() {
		return true;
	}


	@Override
	public View getView(int pos, View contextView) {
try{
			
			if (alSimTransactionDataMain== null || alSimTransactionDataMain.size() == 0)
				return  contextView;
			
				 LayoutInflater inf = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
				 contextView = inf.inflate(R.layout.history_item, null);
				 final ViewHolder holder = new ViewHolder();
				 holder.tv_mobileno = (TextView) contextView.findViewById(R.id.tvMobileNo);
				 holder.tv_documenttype = (TextView) contextView.findViewById(R.id.tvDocumentType);
				 holder.tv_varificationstatus= (TextView) contextView.findViewById(R.id.tvVarificationStatus);
				 holder.tv_varificationdatetime= (TextView) contextView.findViewById(R.id.tvVarificationDateTime);		  
				 final SIMTransaction_Entity entity = (SIMTransaction_Entity) alSimTransactionDataMain.get(pos);
			    
			      
			    if (entity != null && entity.getContractNo() != null)
					holder.tv_mobileno.setText(entity.getSim());
			    
			    if (entity != null && entity.getDocument_Type() != null)
					holder.tv_documenttype.setText(entity.getDocument_Type());
			    
			    if (entity != null && entity.getDocument_Type() != null)
					holder.tv_varificationstatus.setText(entity.getVarification_Status());
			    
			    if (entity != null && entity.getVarification_DateTime() != null)
					holder.tv_varificationdatetime.setText(entity.getDateTime());
			    
				
			
				
				}catch (Exception ex){
					WebService.SaveErrorLogNoBandwidth(History_List.this,Utility.GetUserEntity(History_List.this).getId(),History_List.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					
					 // if(Utility.isConnected(History_List.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(History_List.this).getId(),History_List.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   //	else
		      	   //		Utility.showToast(History_List.this, getString(R.string.alert_need_internet_connection)); 
		        
					  
		
		}
return contextView;
	}

	
	class ViewHolder {
		private TextView tv_mobileno,tv_documenttype,tv_varificationstatus,tv_varificationdatetime;
	}
	

	@Override
	public String getFilter() {
		return "";
	}


	@Override
	public ArrayList<SIMTransaction_Entity> getFilteredList() {
		return alSimTransactionDataMain;
	}
    
    }
