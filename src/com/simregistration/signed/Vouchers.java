package com.simregistration.signed;
import java.util.ArrayList;
import com.simregistration.signed.entity.Denomination_Entity;
import com.simregistration.signed.entity.Result_Entity;
import com.simregistration.signed.utility.NothingSelectedSpinnerAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;

public class Vouchers extends Activity {
    private EditText edt_CustomerNumber;
    private Spinner sp_Denomination;
    private Button btn_Submit;
    private ArrayList<Denomination_Entity> alDenominationDataMain;
    private ArrayList<String> alDenominationDataMainNames;
	private ProgressDialog progress = null;	
	
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vouchers);  
        edt_CustomerNumber = (EditText)findViewById(R.id.edtCustomerNumber);
        sp_Denomination = (Spinner)findViewById(R.id.spDenomination);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        btn_Submit = (Button)findViewById(R.id.btnSubmit);
    	btn_Submit.setOnClickListener(new View.OnClickListener(){

  	      	public void onClick(View v){
  	      	
  	      	try {
  	      	  if(edt_CustomerNumber.getText().toString().equals("") || sp_Denomination.getSelectedItemPosition()==0 || sp_Denomination.getSelectedItemPosition()==-1 )
				  Utility.showToast(Vouchers.this,getResources().getString(R.string.fill));
			  else if (edt_CustomerNumber.getText().toString().length()==8){
			
				AlertDialog.Builder builder = new AlertDialog.Builder(Vouchers.this);
				builder.setMessage("Are you sure you want to continue?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
								if(Utility.isConnected(Vouchers.this))
									  new InsertVocuher().execute(new String[]{edt_CustomerNumber.getText().toString(),
											  alDenominationDataMain.get(0).getDenominations().get(sp_Denomination.getSelectedItemPosition()-1).getID()});
			                	   	else
			                	   		Utility.showToast(Vouchers.this, getString(R.string.alert_need_internet_connection)); }
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();} } catch (Exception ex) {
					  WebService.SaveErrorLogNoBandwidth(Vouchers.this,Utility.GetUserEntity(Vouchers.this).getId(),Vouchers.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					}} });
    	
    	if(Utility.isConnected(Vouchers.this))
          	 new GetDenominations().execute( new String[]{Utility.GetUserEntity(Vouchers.this).getId()}); 
          else
  	   		Utility.showToast(Vouchers.this, getString(R.string.alert_need_internet_connection));
      }
    
    private class GetDenominations extends AsyncTask<String, Void, Integer> {
    	ArrayList<Denomination_Entity> alDenominationsData= new  ArrayList<Denomination_Entity>();
		ArrayList<String> alDenominationDataNames = new ArrayList<String>();
		
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Vouchers.this, getString(R.string.pleaseWait), "", true, true, null);
			}

		@Override
		protected Integer doInBackground(String ... params) {
			int iResult=-1;
			try {
				iResult =WebService.GetDenominations(Vouchers.this, params[0], alDenominationsData,alDenominationDataNames);
				 }catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(Vouchers.this,Utility.GetUserEntity(Vouchers.this).getId(),Vouchers.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
								return null;
			}
			return iResult;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v==-1)
	  				Utility.showToast(Vouchers.this, getString(R.string.error));
				else{
				if(alDenominationsData.size()>0){
					Denomination_Entity entity= alDenominationsData.get(0);
						if(!entity.getError().equals(""))
						Utility.showToast(Vouchers.this, entity.getError());
						
				else {
					    alDenominationDataMain = alDenominationsData;
					    alDenominationDataMainNames=alDenominationDataNames;
						ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Vouchers.this,
									android.R.layout.simple_spinner_item, alDenominationDataMainNames);
				        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				        sp_Denomination.setAdapter(
									    new NothingSelectedSpinnerAdapter(
									    dataAdapter,
									    R.layout.denomination_spinner_row_nothing_selected,
									    Vouchers.this));}}
			
				}}catch(Exception ex){
					WebService.SaveErrorLogNoBandwidth(Vouchers.this,Utility.GetUserEntity(Vouchers.this).getId(),Vouchers.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
				}}}

    
    private class InsertVocuher extends AsyncTask<String, Void, Integer> {
    	private Result_Entity entity = new Result_Entity();
		   
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(Vouchers.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected Integer doInBackground(String... params) {
  			try {
  				 return WebService.InsertVoucher(Vouchers.this, Utility.GetUserEntity(Vouchers.this).getId(), params[0], params[1],entity);
  			}catch (Exception ex) {
  			  WebService.SaveErrorLogNoBandwidth(Vouchers.this,Utility.GetUserEntity(Vouchers.this).getId(),Vouchers.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());      
  			  return -1;
  			
  			}
  		}

  		@Override
  		protected void onPostExecute(Integer v) {
  			super.onPostExecute(v);
  			progress.dismiss();	
  			if(v==-1)
  				Utility.showToast(Vouchers.this, getString(R.string.error));
  			else 
  				Utility.alert(entity.getDescription(),Vouchers.this, null);
			
  			
  		}}
	
    }
