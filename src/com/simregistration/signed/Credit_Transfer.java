package com.simregistration.signed;
import com.simregistration.signed.entity.Result_Entity;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;

public class Credit_Transfer extends Activity {
    private EditText edt_CustomerNumber,edt_Amount;
    private Button btn_Submit;
    private ProgressDialog progress = null;	
	
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_transfer);  
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_CustomerNumber = (EditText)findViewById(R.id.edtCustomerNumber);
        edt_Amount = (EditText)findViewById(R.id.edtAmount);
        btn_Submit = (Button)findViewById(R.id.btnSubmit);
    	btn_Submit.setOnClickListener(new View.OnClickListener(){

  	      	public void onClick(View v){
  	      	
  	      	try {
  	      	  if(edt_CustomerNumber.getText().toString().equals("") || edt_Amount.getText().toString().equals(""))
				  Utility.showToast(Credit_Transfer.this,getResources().getString(R.string.fill));
			  else if (edt_CustomerNumber.getText().toString().length()==8){
			
				AlertDialog.Builder builder = new AlertDialog.Builder(Credit_Transfer.this);
				builder.setMessage("Are you sure you want to continue?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
								if(Utility.isConnected(Credit_Transfer.this))
									  new InsertCreditTransfer().execute(new String[]{edt_CustomerNumber.getText().toString(),
											  edt_Amount.getText().toString()});
			                	   	else
			                	   		Utility.showToast(Credit_Transfer.this, getString(R.string.alert_need_internet_connection)); 
			         	  
					}}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();} } catch (Exception ex) {
					  WebService.SaveErrorLogNoBandwidth(Credit_Transfer.this,Utility.GetUserEntity(Credit_Transfer.this).getId(),Credit_Transfer.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
					}} });
    	
      }
    
   
    
    private class InsertCreditTransfer extends AsyncTask<String, Void, Integer> {
    	private Result_Entity entity = new Result_Entity();
		   
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(Credit_Transfer.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected Integer doInBackground(String... params) {
  			try {
  				 return WebService.InsertCreditTransfer(Credit_Transfer.this, Utility.GetUserEntity(Credit_Transfer.this).getId(), params[0], params[1],entity);
  			}catch (Exception ex) {
  			  WebService.SaveErrorLogNoBandwidth(Credit_Transfer.this,Utility.GetUserEntity(Credit_Transfer.this).getId(),Credit_Transfer.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());      
  			  return -1;
  			
  			}
  		}

  		@Override
  		protected void onPostExecute(Integer v) {
  			super.onPostExecute(v);
  			progress.dismiss();	
  			if(v==-1)
  				Utility.showToast(Credit_Transfer.this, getString(R.string.error));
  			else 
  				Utility.alert(entity.getDescription(),Credit_Transfer.this, null);
			
  			
  		}}
	
    }
