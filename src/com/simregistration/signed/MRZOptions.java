package com.simregistration.signed;
import com.simregistration.signed.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

public class MRZOptions extends Activity   {
	private Button btn_Logout, btn_Manual,btn_Scan;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mrzoptions);
        
        btn_Logout = (Button)this.findViewById(R.id.btn_logout);
        btn_Logout.setVisibility(View.GONE);
       
        btn_Manual = (Button)this.findViewById(R.id.btn_manaual);
        btn_Manual.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent intent= new Intent(MRZOptions.this,MRZ.class);
				intent.putExtra("mode","manual");
				startActivity(intent);
			}});
    
        btn_Scan = (Button)this.findViewById(R.id.btn_scan);
        btn_Scan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent= new Intent(MRZOptions.this,MRZ.class);
				intent.putExtra("mode","scan");
				startActivity(intent);
			}});
        
       
      
      
        	
       
   }
    


	
   
    
  }
