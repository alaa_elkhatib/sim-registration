package com.simregistration.signed;
import com.simregistration.signed.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

public class Options extends Activity  {
	private Button btn_Logout,btn_History,btn_Comission,btn_Notifications,btn_ChangePassword
	,btn_ChangeComission,btn_MyStock,btn_MyInfo;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options);
        btn_Logout = (Button)this.findViewById(R.id.btn_logout);
        btn_Logout.setVisibility(View.GONE);
       
        
        btn_History = (Button)this.findViewById(R.id.btn_history);
        btn_History.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,History.class);
		    	startActivity(intent);
			}});
        
        btn_Comission = (Button)this.findViewById(R.id.btn_comission);
        btn_Comission.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,Commission_List.class);
		    	startActivity(intent);
			}});
        
        btn_Notifications = (Button)this.findViewById(R.id.btn_notifications);
        btn_Notifications.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,Notifications.class);
		    	startActivity(intent);
			}});
        
        btn_ChangePassword = (Button)this.findViewById(R.id.btn_changepassword);
        btn_ChangePassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,ChangePassword.class);
		    	startActivity(intent);
			}});
        
        btn_ChangeComission = (Button)this.findViewById(R.id.btn_changecomission);
        btn_ChangeComission.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,ChangeComission.class);
		    	startActivity(intent);
			}});
        
        btn_MyStock= (Button)this.findViewById(R.id.btn_mystock);
        btn_MyStock.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,MyStock.class);
		    	startActivity(intent);
			}});
        
        btn_MyInfo= (Button)this.findViewById(R.id.btn_myinfo);
        btn_MyInfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Options.this,MyInfo.class);
		    	startActivity(intent);
			}});
        
        
        
       
   }
    
 
	  
  }
