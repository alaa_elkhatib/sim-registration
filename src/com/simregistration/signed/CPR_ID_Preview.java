
package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
public class CPR_ID_Preview extends Activity  {
	private ImageView mIDImageViewer;
    private  File mIDLocation,mIDLocation2;
    private ProgressDialog progress = null;
    
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.cpr_id_preview);  
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  
        mIDLocation = new File(Environment.getExternalStorageDirectory(),"OCR/CPRID.jpg");
        mIDLocation2 = new File(Environment.getExternalStorageDirectory(),"OCR/CPRID2.jpg");
        mIDImageViewer = (ImageView) findViewById(R.id.iv_id);
        new LoadImage().execute(new String[]{""});}
    

    public void onCancel(View v){
    	Intent intent = new Intent(CPR_ID_Preview.this,CPR_ID.class);
		startActivity(intent);
    	finish();
    }
    
    
    public void onSend(View v){
    	Intent intent = new Intent(CPR_ID_Preview.this,ICCIDScan.class);
    	startActivity(intent);
    	finish();
    }
    
   
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			setResult(RESULT_OK, null);
			finish();
		}};
    
    
 
    
		@Override
		public void onBackPressed() {
			Intent intent = new Intent(CPR_ID_Preview.this,CPR_ID.class);
			startActivity(intent);
			finish();}


  
		  
	    private class LoadImage extends AsyncTask<String, Void, String> {
	    	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				progress = ProgressDialog.show(CPR_ID_Preview.this, getString(R.string.pleaseWait), "", true, false, null);

			}

			@Override
			protected String doInBackground(String... params) {
				String strResult="";
				try {
					  saveCPRIDPhoto(Utility.createContrast(Utility.doBrightness(BitmapFactory.decodeFile(mIDLocation.getAbsolutePath()),40),35));
					 	
				}catch (Exception ex) {
					 WebService.SaveErrorLogNoBandwidth(CPR_ID_Preview.this,Utility.GetUserEntity(CPR_ID_Preview.this).getId(),CPR_ID_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
	  					
					//if(Utility.isConnected(CPR_ID_Preview.this))
	 				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR_ID_Preview.this).getId(),CPR_ID_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	 	
					return "Error";
				   	   	
				}
				return strResult;
			}

			@Override
			protected void onPostExecute(String v) {
				super.onPostExecute(v);
				progress.dismiss();
				try{

			        if(mIDLocation2.exists()){
			            mIDImageViewer.setImageBitmap(BitmapFactory.decodeFile(mIDLocation2.getAbsolutePath())); }
				}catch(Exception ex){}}}
	    
		private void saveCPRIDPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mIDLocation2.exists())
					mIDLocation2.createNewFile();
				image = new FileOutputStream(mIDLocation2);
			} catch (Exception ex) {
				 WebService.SaveErrorLogNoBandwidth(CPR_ID_Preview.this,Utility.GetUserEntity(CPR_ID_Preview.this).getId(),CPR_ID_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
	  				
				//if(Utility.isConnected(CPR_ID_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(CPR_ID_Preview.this).getId(),CPR_ID_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	
			}
			bm.compress(CompressFormat.JPEG, 100, image);	
			bm.recycle();
			bm=null;
		}

}
