
package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.User_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class Passport_Preview extends Activity  {
	private ImageView mFormImageViewer/*,mIDNoImageViewer,mNationalityImageViewer,mNameImageViewer,mExpiryImageViewer*/;
    private  File mFormLocation,mIDInfoLocation,mSIMLocation/*,mSigneture1Location/*,mBarLocation,mIDNoLocation,mNationalityLocation,mNameLocation/*,mExpiryLocation,
    mStampLocation,mSigneture1Location,mSigneture2Location*/;
    private ProgressDialog progress = null;
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.cpr_preview);  
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mFormLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Main.jpg");
        mFormImageViewer = (ImageView) findViewById(R.id.iv_form); 
        if(mFormLocation.exists()){
        	final BitmapFactory.Options options = new BitmapFactory.Options();
        	//options.inSampleSize = 2;
            Bitmap myBitmap = BitmapFactory.decodeFile(mFormLocation.getAbsolutePath()/*,options*/);
            mFormImageViewer.setImageBitmap(myBitmap); }
        //mSigneture1Location = new File(Environment.getExternalStorageDirectory(),"OCR/Signeture1.jpg");
        
        
       /* mStampLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Stamp.jpg");
     
        mSigneture2Location = new File(Environment.getExternalStorageDirectory(),"OCR/Signeture2.jpg");
      */   
    
        mIDInfoLocation = new File(Environment.getExternalStorageDirectory(),"OCR/CPRID.jpg");
        
      //  mSIMLocation = new File(Environment.getExternalStorageDirectory(),"OCR/SIM.jpg");
        
        
        /*
              mBarLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Bar.jpg");
             mNationalityLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Nationality.jpg");
        mNationalityImageViewer = (ImageView) findViewById(R.id.iv_nationality);
        if(mNationalityLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mNationalityLocation.getAbsolutePath());
            mNationalityImageViewer.setImageBitmap(myBitmap);}
        
        mNameLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Name.jpg");
        mNameImageViewer = (ImageView) findViewById(R.id.iv_name); 
        if(mNameLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mNameLocation.getAbsolutePath());
            mNameImageViewer.setImageBitmap(myBitmap);}
        
      mExpiryLocation = new File(Environment.getExternalStorageDirectory(),"OCR/Expiry.jpg");
        mExpiryImageViewer = (ImageView) findViewById(R.id.iv_expiry);
        if(mExpiryLocation.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(mExpiryLocation.getAbsolutePath());
            mExpiryImageViewer.setImageBitmap(myBitmap);}*/
        }
    
   
    
   
    
    public void onCancel(View v){
    	Intent intent = new Intent(Passport_Preview.this, CPR_ID2.class);
		startActivity(intent);
    	this.finish();
    }
    
    
    public void onSend(View v){
    	 if(Utility.isConnected(Passport_Preview.this))
    		 new UploadCPRImages().execute(new String[]{});
    	 else
	   		Utility.showToast(Passport_Preview.this, getString(R.string.alert_need_internet_connection)); 
    }
    
    
    
    private class UploadCPRImages extends AsyncTask<String, Void, String> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Passport_Preview.this, getString(R.string.pleaseWait), "", true, false, null);

		}

		@Override
		protected String doInBackground(String... params) {
			String strResult="";
			try {
				
				/*
				
				
				
				
				
				
				  String strFolderName =  C.USER_ID+"_"+System.currentTimeMillis()+"_CPR";	
				  
				  String strImage=WebService.GetImageFromFile(mFormLocation.getPath());
				  strResult= WebService.UploadImages(C.USER_ID, "CPR", strFolderName, "Main.png", strImage);
				 
				  saveCPRIDPhoto(Utility.createContrast(Utility.doBrightness(BitmapFactory.decodeFile(mIDInfoLocation.getAbsolutePath()),40),50));
				  strImage=WebService.GetImageFromFile(mIDInfoLocation.getPath());
				  strResult=WebService.UploadImages(C.USER_ID, "CPR", strFolderName, "ID.png", strImage);
				 
				  
				  strImage=WebService.GetImageFromFile(mSIMLocation.getPath());
				  strResult=WebService.UploadImages(C.USER_ID, "CPR", strFolderName, "SIM.png", strImage);
			
				  strResult=WebService.InsertSIMTransaction(C.USER_ID,"CPR",getIntent().getStringExtra("ICCID"),strFolderName,strFolderName+"/Main.png");
				  
				
			*/}catch (Exception ex) {
				WebService.SaveErrorLogNoBandwidth(Passport_Preview.this,Utility.GetUserEntity(Passport_Preview.this).getId(),Passport_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
				
				//if(Utility.isConnected(Passport_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Preview.this).getId(),Passport_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				return "Error";
			}
			return strResult;
		}

		@Override
		protected void onPostExecute(String v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v.equals("Success"))
					Utility.alert("Your transaction has been sent successfully, waiting for verification and activation", Passport_Preview.this, onclick);
				
				else if(v.equals("Error"))
					Utility.showToast(Passport_Preview.this,getString(R.string.error));
				else 
					Utility.alert(v, Passport_Preview.this, null);
		}catch(Exception ex){
			WebService.SaveErrorLogNoBandwidth(Passport_Preview.this,Utility.GetUserEntity(Passport_Preview.this).getId(),Passport_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
			
				 //if(Utility.isConnected(Passport_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Preview.this).getId(),Passport_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
      	   	//else
      	   	//	Utility.showToast(Passport_Preview.this, getString(R.string.alert_need_internet_connection)); 
        
			
			}}}
    
    /*
    ----------------------------- LIFE CYCLE METHODS -----------------------------
    */
 
    
   
    
  
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			setResult(RESULT_OK, null);
			finish();
		}};
    
    
		
	  DialogInterface.OnClickListener onclick2 = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(Passport_Preview.this, CPR_ID2.class);
					startActivity(intent);
					finish();
				}};
		    
		    
  

		private void saveCPRIDPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mIDInfoLocation.exists())
					mIDInfoLocation.createNewFile();
				image = new FileOutputStream(mIDInfoLocation);
			} catch (Exception ex) {
				WebService.SaveErrorLogNoBandwidth(Passport_Preview.this,Utility.GetUserEntity(Passport_Preview.this).getId(),Passport_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   					
				
				//if(Utility.isConnected(Passport_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Preview.this).getId(),Passport_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			bm.compress(CompressFormat.JPEG, 100, image);
			bm.recycle();
			bm=null;
		}
		
		@Override
		public void onBackPressed() {
			Intent intent = new Intent(Passport_Preview.this, CPR_ID2.class);
			startActivity(intent);
			finish();}

    
		
    

  

}
