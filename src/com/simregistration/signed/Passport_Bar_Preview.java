
package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;

import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
public class Passport_Bar_Preview extends Activity  {
	private ImageView mBarImageViewer;
    private  File mBarLocation,mBarLocation2;
	private ProgressDialog progress = null;
    
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.cpr_id_preview);  
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mBarLocation = new File(Environment.getExternalStorageDirectory(),"Passport/PassportBar.jpg");
        mBarLocation2 = new File(Environment.getExternalStorageDirectory(),"Passport/PassportBar2.jpg");
        mBarImageViewer = (ImageView) findViewById(R.id.iv_id);
        new LoadImage().execute(new String[]{""});}
    

    public void onCancel(View v){
    	Intent intent = new Intent(Passport_Bar_Preview.this,Passport_Bar.class);
    	intent.putExtra("image", getIntent().getStringExtra("image"));	
		startActivity(intent);
    	finish();
    }
    
    
    public void onSend(View v){
    	Intent intent = new Intent(Passport_Bar_Preview.this,ICCIDScan_Passport.class);
    	intent.putExtra("image", getIntent().getStringExtra("image"));
    	startActivity(intent);
    	finish();
    }
    
   
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			setResult(RESULT_OK, null);
			finish();
		}};
    
    
 
    
		@Override
		public void onBackPressed() {
			Intent intent = new Intent(Passport_Bar_Preview.this,Passport_Bar.class);
			intent.putExtra("image", getIntent().getStringExtra("image"));   	
			startActivity(intent);
			finish();}


	    private class LoadImage extends AsyncTask<String, Void, String> {
	    	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				progress = ProgressDialog.show(Passport_Bar_Preview.this, getString(R.string.pleaseWait), "", true, false, null);

			}

			@Override
			protected String doInBackground(String... params) {
				String strResult="";
				try {
					savePassportBarPhoto(Utility.createContrast(Utility.doBrightness(BitmapFactory.decodeFile(mBarLocation.getAbsolutePath()),40),35));
					 	
				}catch (Exception ex) {
					WebService.SaveErrorLogNoBandwidth(Passport_Bar_Preview.this,Utility.GetUserEntity(Passport_Bar_Preview.this).getId(),Passport_Bar_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
  					
					//if(Utility.isConnected(Passport_Bar_Preview.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Bar_Preview.this).getId(),Passport_Bar_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					return "Error";
				
				}
				return strResult;
			}

			@Override
			protected void onPostExecute(String v) {
				super.onPostExecute(v);
				progress.dismiss();
				try{

			        if(mBarLocation2.exists()){
			        	mBarImageViewer.setImageBitmap(BitmapFactory.decodeFile(mBarLocation2.getAbsolutePath())); }
				}catch(Exception ex){}}}
	    
		private void savePassportBarPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mBarLocation2.exists())
					mBarLocation2.createNewFile();
				image = new FileOutputStream(mBarLocation2);
			} catch (Exception ex) {
				WebService.SaveErrorLogNoBandwidth(Passport_Bar_Preview.this,Utility.GetUserEntity(Passport_Bar_Preview.this).getId(),Passport_Bar_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   						
				//if(Utility.isConnected(Passport_Bar_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Passport_Bar_Preview.this).getId(),Passport_Bar_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			bm.compress(CompressFormat.JPEG, 100, image);	
			bm.recycle();
			bm=null;
		}
  

}
