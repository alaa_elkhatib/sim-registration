
package com.simregistration.signed;
import java.io.File;
import java.io.FileOutputStream;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.IntentIntegrator;
import com.simregistration.signed.utility.IntentResult;
import com.simregistration.signed.utility.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
public class ICCIDScan extends Activity  {
    
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.iccid);  
        IntentIntegrator integrator = new IntentIntegrator(ICCIDScan.this);
        integrator.initiateScan(); }

		  protected void onActivityResult (int requestCode, int resultCode, Intent data) {
		        switch(requestCode) {
		           	case 49374:
		            	if (resultCode == Activity.RESULT_OK) {
			                   IntentResult intentResult = 
			                   IntentIntegrator.parseActivityResult(requestCode, resultCode,data);
			                if (intentResult != null) {
			                	if(!intentResult.getContents().equals("")){
			                	Intent intent = new Intent(ICCIDScan.this, CPR_ID2.class);
			                	intent.putExtra("ICCID", intentResult.getContents());
								startActivity(intent);
								finish();}
			                	else{
			                		Utility.alert(getString(R.string.rescan), ICCIDScan.this, onclick);
			                	}}
			             }
		            	else if (resultCode == Activity.RESULT_CANCELED) {
		            		Intent intent = new Intent(ICCIDScan.this,CPR_ID_Preview.class);
		    				startActivity(intent);
		    				finish(); }
		            	break;
		           
		           default:
		               break;
		        } }
		  
			@Override
			public void onBackPressed() {
				Intent intent = new Intent(ICCIDScan.this,CPR_ID_Preview.class);
				startActivity(intent);
				finish();}



  
			  DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						IntentIntegrator integrator = new IntentIntegrator(ICCIDScan.this);
				        integrator.initiateScan();
					}};

}
