package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Notification_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;

public class Notification_Details extends Activity  {
	private  Notification_Entity alNotificationDataMain;
    private EditText edt_TransactionID,edt_Sim,edt_Msg,edt,edt_TransactionDateTime,edt_VarificationStatus,edt_ActivationStatus;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_details);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        alNotificationDataMain = getIntent().getParcelableExtra("Notification");
		edt_TransactionID = (EditText)findViewById(R.id.edtTransactionID);
		if(alNotificationDataMain!=null && alNotificationDataMain.getTransID()!=null)
		edt_TransactionID.setText(alNotificationDataMain.getTransID());
		
		edt_Sim = (EditText)findViewById(R.id.edtSim);
		if(alNotificationDataMain!=null && alNotificationDataMain.getSIM()!=null)
		edt_Sim.setText(alNotificationDataMain.getSIM());
		
		edt_Msg = (EditText)findViewById(R.id.edtMessage);
		if(alNotificationDataMain!=null && alNotificationDataMain.getMsg()!=null)
		edt_Msg.setText(alNotificationDataMain.getMsg());
		
		
		edt_TransactionDateTime= (EditText)findViewById(R.id.edtTransactionDateTime);
		if(alNotificationDataMain!=null && alNotificationDataMain.getDateTime()!=null)		
		edt_TransactionDateTime.setText(alNotificationDataMain.getDateTime());
		
		edt_VarificationStatus= (EditText)findViewById(R.id.edtVarificationStatus);
		if(alNotificationDataMain!=null && alNotificationDataMain.getVarification()!=null)	{
		if(alNotificationDataMain.getVarification().equals("P"))
		edt_VarificationStatus.setText("Pending");
		
		else if(alNotificationDataMain.getVarification().equals("R"))
			edt_VarificationStatus.setText("Rejected");
		
		else if(alNotificationDataMain.getVarification().equals("A"))
			edt_VarificationStatus.setText("Approved");}
		
		
		edt_ActivationStatus= (EditText)findViewById(R.id.edtActivationStatus);
		if(alNotificationDataMain!=null && alNotificationDataMain.getActivation()!=null)	{	
			edt_ActivationStatus.setText(alNotificationDataMain.getActivation());}
			
		
		
    }}
    
    
 


	
    
    
