package com.simregistration.signed;
import com.simregistration.signed.R;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

public class ChangeComission extends Activity {
    private EditText edt_NewComission,edt_Password;
    private Button btn_Submit;
    private ProgressDialog progress = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changecomission);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_NewComission = (EditText)findViewById(R.id.edtNewComission);
        edt_Password = (EditText)findViewById(R.id.edtPassword);
        
  	    btn_Submit = (Button)this.findViewById(R.id.btnSubmit);
  	    btn_Submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  if(edt_NewComission.getText().toString().equals("")||edt_Password.getText().toString().equals(""))
						  Utility.showToast(ChangeComission.this,getResources().getString(R.string.fill));
					//  else if(edt_Password.getText().toString().length()==8){
					  else{ if(Utility.isConnected(ChangeComission.this))
							  new ChangeUserComission().execute(new String[]{Utility.GetUserEntity(ChangeComission.this).getId(),edt_NewComission.getText().toString(), edt_Password.getText().toString()});
	                	   	else
	                	   		Utility.showToast(ChangeComission.this, getString(R.string.alert_need_internet_connection)); }
	               
					  } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(ChangeComission.this,Utility.GetUserEntity(ChangeComission.this).getId(),ChangeComission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());
				        	
						  //if(Utility.isConnected(ChangeComission.this))
							//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(ChangeComission.this).getId(),ChangeComission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   //	else
			      	   	//	Utility.showToast(ChangeComission.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); }
  	    
    
  
    
  	  private class ChangeUserComission extends AsyncTask<String, Void, String> {
      	@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			progress = ProgressDialog.show(ChangeComission.this, getString(R.string.pleaseWait), "", true, false, null);

  		}

  		@Override
  		protected String doInBackground(String... params) {
  			try {
  				 return WebService.ChangeComission(ChangeComission.this,params[0], params[1], params[2]);
  			}catch (Exception ex) {
  			  WebService.SaveErrorLogNoBandwidth(ChangeComission.this,Utility.GetUserEntity(ChangeComission.this).getId(),ChangeComission.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());      
  				//if(Utility.isConnected(ChangeComission.this))
 				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(ChangeComission.this).getId(),ChangeComission.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
 	   	   	
  				return "";
  			
  			}
  		}

  		@Override
  		protected void onPostExecute(String v) {
  			super.onPostExecute(v);
  			progress.dismiss();
  			Utility.alert(v,ChangeComission.this, onclick);}}
      
      DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

   		@Override
   		public void onClick(DialogInterface dialog, int which) {
   			finish();
   		}};
   		
   
  	}
    
    

    
