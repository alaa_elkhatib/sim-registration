package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;

public class History_Details extends Activity  {
	private  SIMTransaction_Entity alSimTransactionDataMain;
    private EditText edt_TransactionID,edt_Sim,edt_Name,edt_Gender,edt_Nationality,edt_IDType,edt_IDNumber,edt_Country,
    edt_Address,edt_BirthDay,edt_Contract,
    edt_ComissionAmount,edt_TransactionDateTime,edt_VarificationDateTime,edt_VarificationRejected,edt_ActivationStatus,edt_TabsError,edt_ContractNo,edt_TabsDateTime;
    private TableRow ln_TabsError,ln_TabsDateTime,ln_VarificationRejected;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_details);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
		alSimTransactionDataMain = getIntent().getParcelableExtra("Hisotry");
		edt_TransactionID = (EditText)findViewById(R.id.edtTransactionID);
		edt_TransactionID.setText(alSimTransactionDataMain.getIDNumber());
		
		edt_Sim = (EditText)findViewById(R.id.edtSim);
		edt_Sim.setText(alSimTransactionDataMain.getSim());
		
		edt_Name = (EditText)findViewById(R.id.edtName);
		edt_Name.setText(alSimTransactionDataMain.getName());
		
		edt_Gender = (EditText)findViewById(R.id.edtGender);
		edt_Gender.setText(alSimTransactionDataMain.getGender());
		
		edt_Nationality = (EditText)findViewById(R.id.edtNatinality);
		edt_Nationality.setText(alSimTransactionDataMain.getNationality());
		
		
		edt_IDType = (EditText)findViewById(R.id.edtIDType);
		edt_IDType.setText(alSimTransactionDataMain.getType());
		
		edt_IDNumber = (EditText)findViewById(R.id.edtIDNumber);
		edt_IDNumber.setText(alSimTransactionDataMain.getType());
	
		edt_Country= (EditText)findViewById(R.id.edtCountry);
		edt_Country.setText(alSimTransactionDataMain.getCountry());
		
		
		edt_Address= (EditText)findViewById(R.id.edtAddress);
		edt_Address.setText(alSimTransactionDataMain.getAddress());
		
		edt_BirthDay= (EditText)findViewById(R.id.edtBirthDay);
		edt_BirthDay.setText(alSimTransactionDataMain.getBirthDay());
		
		edt_ComissionAmount= (EditText)findViewById(R.id.edtCommissionAmount);
		edt_ComissionAmount.setText(alSimTransactionDataMain.getAmount());
		
		edt_TransactionDateTime= (EditText)findViewById(R.id.edtTransactionDateTime);
		edt_TransactionDateTime.setText(alSimTransactionDataMain.getDateTime());
		
		edt_VarificationDateTime= (EditText)findViewById(R.id.edtVarificationDateTime);
		edt_VarificationDateTime.setText(alSimTransactionDataMain.getVarification_DateTime());
		
		if(alSimTransactionDataMain.getVarification_Status().contains("error")){
	    ln_VarificationRejected= (TableRow)findViewById(R.id.ln_VarificationRejected);
	    ln_VarificationRejected.setVisibility(View.VISIBLE);
		edt_VarificationRejected= (EditText)findViewById(R.id.edtVarificationRejectedReason);
		edt_VarificationRejected.setText(alSimTransactionDataMain.getVarification_RejectedReason()); }
		
		edt_ActivationStatus= (EditText)findViewById(R.id.edtActivationStatus);
		edt_ActivationStatus.setText(alSimTransactionDataMain.getActivationStatus());
		
		edt_Contract= (EditText)findViewById(R.id.edtContractNo);
		edt_Contract.setText(alSimTransactionDataMain.getContractNo());
		
		if(alSimTransactionDataMain.getActivationStatus().contains("error")){
			ln_TabsError= (TableRow)findViewById(R.id.ln_TabsError);
			ln_TabsError.setVisibility(View.VISIBLE);
			edt_TabsError= (EditText)findViewById(R.id.edtTabsError);
			edt_TabsError.setText(alSimTransactionDataMain.getTab_Error());
			
			ln_TabsError= (TableRow)findViewById(R.id.ln_TabsError);
			ln_TabsError.setVisibility(View.VISIBLE);
			
			ln_TabsDateTime= (TableRow)findViewById(R.id.ln_TabsDateTime);
			ln_TabsDateTime.setVisibility(View.VISIBLE);
			
			edt_TabsDateTime= (EditText)findViewById(R.id.edtTabsDatetime);
			edt_TabsDateTime.setText(alSimTransactionDataMain.getTabs_DateTime());
			
			
			
		}
			
		
		
		 }
    
    
 


	
    
    }
