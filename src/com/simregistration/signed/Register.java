package com.simregistration.signed;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.Register_Entity;
import com.simregistration.signed.entity.User_Entity;
import com.simregistration.signed.utility.BitmapUtils;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;

public class Register extends Activity {
    private EditText edt_FullName,edt_ShopName,edt_Location,edt_Mobile,edt_Email,edt_LandMark,edt_CRNumber,edt_CPRNumber
    ,edt_SMSNumber,edt_WebUser,edt_USSD;
    private RadioButton rb_Arabic,rb_English;
    private Button btn_CRImage,btn_CPRImage,btn_Register;
    public Bitmap cr,cpr;
    private Uri cameraTempUri;
    private final int TAKE_CR_REQUEST = 1;
    private final int TAKE_CPR_REQUEST = 2;
	private ProgressDialog progress = null;
   
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regsiter);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
        edt_FullName = (EditText)findViewById(R.id.edtFullName);
        edt_ShopName = (EditText)findViewById(R.id.edtShopName);
        edt_Location = (EditText)findViewById(R.id.edtLocation);
        edt_Mobile = (EditText)findViewById(R.id.edtMobile);
        edt_Email = (EditText)findViewById(R.id.edtEmail);
        edt_LandMark= (EditText)findViewById(R.id.edtNearestLandMark);
        edt_CRNumber = (EditText)findViewById(R.id.edtCRNumber);
        edt_CPRNumber = (EditText)findViewById(R.id.edtCPRNumber);
        edt_SMSNumber = (EditText)findViewById(R.id.edtSMSNumber);
        edt_WebUser = (EditText)findViewById(R.id.edtWebUserName);
        edt_USSD = (EditText)findViewById(R.id.edtUSSD);
       
        rb_Arabic = (RadioButton)findViewById(R.id.rb_A);
        rb_English= (RadioButton)findViewById(R.id.rb_E);
  	    
  	    btn_CRImage = (Button)this.findViewById(R.id.btnCRImage);
  	    btn_CRImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  File dir = Environment.getExternalStorageDirectory();
			          File file = File.createTempFile("photo_", null, dir);
			          cameraTempUri = Uri.fromFile(file);
			          Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			          intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			          intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraTempUri);
			          startActivityForResult(intent, TAKE_CR_REQUEST);
					  } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(Register.this,Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  				
						 // if(Utility.isConnected(Register.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   	//else
			      	   	//	Utility.showToast(Register.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); 
  	    
  	  btn_CPRImage = (Button)this.findViewById(R.id.btnCPRImage);
	    btn_CPRImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					  File dir = Environment.getExternalStorageDirectory();
			          File file = File.createTempFile("photo_", null, dir);
			          cameraTempUri = Uri.fromFile(file);
			          Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			          intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			          intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraTempUri);
			          startActivityForResult(intent, TAKE_CPR_REQUEST);
					  } catch (Exception ex) {
						  WebService.SaveErrorLogNoBandwidth(Register.this,Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  				
							
						  //if(Utility.isConnected(Register.this))
						//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			      	   	//else
			      	   	//	Utility.showToast(Register.this, getString(R.string.alert_need_internet_connection)); 
			        
			       
			      }
			}}); 
	    
	   
        
	    btn_Register = (Button)findViewById(R.id.btnRegister);
	    btn_Register.setOnClickListener(new View.OnClickListener(){

	      	public void onClick(View v){
	      		try {
	      			 if(edt_FullName.getText().toString().equals(""))
	      					  Utility.showToast(Register.this,getResources().getString(R.string.enter_fullname));
	      			 
	      			 else if(edt_ShopName.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_shopname));
	      			 
	      			 else if(edt_Location.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_location));
	      			 else if(edt_Mobile.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_comissionno));
	      			 
	      			 else if( edt_CRNumber.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_crno));
	      			 else if(edt_CPRNumber.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_cprno));
	      			 
	      			 else if(edt_SMSNumber.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_notificationno));
	      			 
	      			 else if(!edt_Email.getText().toString().trim().equals("") && !isEmailValid(edt_Email.getText().toString().trim())) 
	 						Utility.showToast(Register.this,getString(R.string.validemail));
	      			 
	      			 else if(edt_LandMark.getText().toString().equals(""))
	      				  Utility.showToast(Register.this,getResources().getString(R.string.enter_landmark));
	      			 
	 					else{
	                	 if(Utility.isConnected(Register.this)){
	                		 
	                		 String cr_image = "",cpr_image="",lang="EN"; 
	                		/* if(rb_Arabic.isChecked())
	                			 lang="AR";
	                		 else lang="EN";*/
	                		  if(cr!=null){
	                			  ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
	                			  cr.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
	                			  byte[] byteArray = byteArrayOutputStream .toByteArray();
	                			  cr_image= Base64.encodeToString(byteArray, Base64.DEFAULT);
	                		  }
	                		  if(cpr!=null){
	                			  ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
	                			  cpr.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
	                			  byte[] byteArray = byteArrayOutputStream .toByteArray();
	                			  cpr_image = Base64.encodeToString(byteArray, Base64.DEFAULT);
	                		  }
	                			 new RegisterUser().execute(new String[]{edt_FullName.getText().toString(), edt_ShopName.getText().toString(),edt_Location.getText().toString(),edt_Mobile.getText().toString()
	        	      					 ,edt_Email.getText().toString().trim() ,edt_LandMark.getText().toString(),edt_CRNumber.getText().toString(),edt_CPRNumber.getText().toString(),edt_SMSNumber.getText().toString(),lang,cr_image,cpr_image,Utility.getDeviceId(Register.this),edt_WebUser.getText().toString(),edt_USSD.getText().toString()});}
	                	   	else
	                	   		Utility.showToast(Register.this, getString(R.string.alert_need_internet_connection)); 
	               
					 }

	      		} catch (Exception ex) {
	      		  WebService.SaveErrorLogNoBandwidth(Register.this,Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  				
					
	      			//if(Utility.isConnected(Register.this))
      				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

	      		}
	      	}
	      });}
    
    
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case TAKE_CR_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
                    //Bitmap originalBitmap = (Bitmap) data.getExtras().get("data");
                    Uri photoUri = cameraTempUri;
                    if (photoUri != null) {
                       cr = BitmapUtils.getBitmap(Register.this, photoUri, 4);
                      /* int ration=cr.getWidth()/cr.getHeight();
                       cr =Bitmap.createScaledBitmap(cr, 1600, 1600/ration, false);
                      */ btn_CRImage.setBackgroundResource(R.drawable.btn_default_pressed_holo_light);
                       btn_CRImage.setTextColor(Color.WHITE);
                       }	}
            break;
            case TAKE_CPR_REQUEST:
            	if (resultCode == Activity.RESULT_OK) {
                    //Bitmap originalBitmap = (Bitmap) data.getExtras().get("data");
                    Uri photoUri = cameraTempUri;
                    if (photoUri != null) {
                       cpr = BitmapUtils.getBitmap(Register.this, photoUri, 12); 
                       btn_CPRImage.setBackgroundResource(R.drawable.btn_default_pressed_holo_light);
                       btn_CPRImage.setTextColor(Color.WHITE);}	}
            break;
           default:
               break;
        } }
    

    private class RegisterUser extends AsyncTask<String, Void, Integer> {
    	Register_Entity register =new  Register_Entity();
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Register.this, getString(R.string.pleaseWait), "", true, false, null);

		}

		@Override
		protected Integer doInBackground(String... params) {
			int iRes=-1;
			try {
				/*iRes= WebService.Register(params[0], params[1], params[2], params[3], params[4], params[5],
						params[6], params[7], params[8], params[9], params[10],params[11],params[12], register);*/
				
				iRes= WebService.Register2(Register.this,params[0], params[1], params[2], params[3], params[4], params[5],
						params[6], params[7], params[8], params[9], params[10],params[11],params[12],register, params[13],params[14]);
			}catch (Exception ex) {
				  WebService.SaveErrorLogNoBandwidth(Register.this,Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  				
					
				//if(Utility.isConnected(Register.this))
 				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Register.this).getId(),Register.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

				return -1;
			}
			return iRes;
		}

		@Override
		protected void onPostExecute(Integer v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v>0){
					if(register.getStatus().equals("0"))
						 Utility.alert(register.getDescription(), Register.this, onclick);
					
						else  Utility.alert(register.getDescription(), Register.this, null);}
				else Utility.showToast(Register.this, getString(R.string.error)); 
					}catch(Exception ex){}}}
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

 		@Override
 		public void onClick(DialogInterface dialog, int which) {
 			finish();
 		}};
 		


public static boolean isEmailValid(String email) {
	 Pattern pattern;
	    Matcher matcher;
	    final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
	    pattern = Pattern.compile(EMAIL_PATTERN);
	    matcher = pattern.matcher(email);
	    return matcher.matches();}


	
    }
