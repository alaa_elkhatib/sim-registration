package com.simregistration.signed;

import com.simregistration.signed.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout.LayoutParams;

public class FormTouchView extends View {

	//private final String TAG = "TESTTESTTESTESTTESTESTEST";

	private Drawable mLeftTopIcon;
	private Drawable mRightTopIcon;
	private Drawable mLeftBottomIcon;
	private Drawable mRightBottomIcon;



	private float mLeftTopPosX = 50;
	private float mLeftTopPosY = 120;

	private float mRightTopPosX = 450;
	private float mRightTopPosY = 120;

	private float mLeftBottomPosX = 50;
	private float mLeftBottomPosY = 300;

	private float mRightBottomPosX = 450;
	private float mRightBottomPosY = 300;
	private float mPosX;
	private float mPosY;

	private float mLastTouchX;
	private float mLastTouchY;

	private Paint topLine;
	private Paint bottomLine;
	private Paint leftLine;
	private Paint rightLine;

	private Rect buttonRec;
	
	private int mCenter;

	private static final int INVALID_POINTER_ID = -1;
	private int mActivePointerId = INVALID_POINTER_ID;

	private ScaleGestureDetector mScaleDetector;
	private float mScaleFactor = 1.f;
	
	private Paint ocrText;
	private String ocrTextString = "";
	private Bitmap bitmap;
	public void setOcrTextString(String ocrTextString){
		this.ocrTextString = ocrTextString;
	}
	
	private Paint recognizedText;
	private String recognizedTextString = "";
	WindowManager manager;
	Display display ;
    private Rect Rec;
	private Paint mPaint ;
	public void setRecognizedTextString(String recognizedTextString) {
		this.recognizedTextString = recognizedTextString;
	}

	public FormTouchView(Context context){
		super(context);
		init(context);
	}

	public FormTouchView(Context context, AttributeSet attrs){
		super (context,attrs);
		init(context);
	}

	public FormTouchView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	
	public void SetBitmap(Bitmap bit){
		bitmap=bit;
		invalidate();
	}

	private void init(Context context) {


		 manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 display = manager.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		mLeftTopPosX = 20;
		mLeftTopPosY = 20;

		mRightTopPosX = width/2;
		mRightTopPosY =  20;

	    mLeftBottomPosX = 20;
		mLeftBottomPosY =100;// (width/2)-300;

		mRightBottomPosX = width/2;
		mRightBottomPosY = 100;//(width/2)-300;
		
		topLine = new Paint();
		bottomLine = new Paint();
		leftLine = new Paint();
		rightLine = new Paint();
		recognizedText = new Paint();
		ocrText = new Paint();
		mPaint= new Paint();
		setLineParameters(Color.WHITE,4);

		mLeftTopIcon = context.getResources().getDrawable(R.drawable.corners);
		
		mCenter = mLeftTopIcon.getMinimumHeight()/2;
		mLeftTopIcon.setBounds((int)mLeftTopPosX, (int)mLeftTopPosY,
				mLeftTopIcon.getIntrinsicWidth()+(int)mLeftTopPosX,
				mLeftTopIcon.getIntrinsicHeight()+(int)mLeftTopPosY);

		mRightTopIcon = context.getResources().getDrawable(R.drawable.corners);
		mRightTopIcon.setBounds((int)mRightTopPosX, (int)mRightTopPosY,
				mRightTopIcon.getIntrinsicWidth()+(int)mRightTopPosX,
				mRightTopIcon.getIntrinsicHeight()+(int)mRightTopPosY);

		mLeftBottomIcon = context.getResources().getDrawable(R.drawable.corners);
		mLeftBottomIcon.setBounds((int)mLeftBottomPosX, (int)mLeftBottomPosY,
				mLeftBottomIcon.getIntrinsicWidth()+(int)mLeftBottomPosX,
				mLeftBottomIcon.getIntrinsicHeight()+(int)mLeftBottomPosY);

		mRightBottomIcon = context.getResources().getDrawable(R.drawable.corners);
		mRightBottomIcon.setBounds((int)mRightBottomPosX, (int)mRightBottomPosY,
				mRightBottomIcon.getIntrinsicWidth()+(int)mRightBottomPosX,
				mRightBottomIcon.getIntrinsicHeight()+(int)mRightBottomPosY);
		// Create our ScaleGestureDetector
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());

	}

	

	private void setLineParameters(int color, float width){

		topLine.setColor(color);
		topLine.setStrokeWidth(width);

		bottomLine.setColor(color);
		bottomLine.setStrokeWidth(width);

		leftLine.setColor(color);
		leftLine.setStrokeWidth(width);

		rightLine.setColor(color);
		rightLine.setStrokeWidth(width);
		
		mPaint.setColor(color);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(width);
	
	}

	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.save();
		
		mLeftTopPosX=(int)canvas.getWidth()/10;
	    mLeftTopPosY=mLeftTopPosX/5;
		mRightBottomPosX=canvas.getWidth()-mLeftTopPosX;
		mRightBottomPosY=canvas.getHeight()-mLeftTopPosY;
		Rec = new Rect((int)mLeftTopPosX,(int) mLeftTopPosY,(int)mRightBottomPosX,(int)mRightBottomPosY);
		canvas.drawRect(Rec,mPaint); 
	  
	
		canvas.restore();
	}


	
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			mScaleFactor *= detector.getScaleFactor();

			// Don't let the object get too small or too large.
			mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));

			invalidate();
			return true;
		}
	}

	public float getmLeftTopPosX(){
		return mLeftTopPosX;
	}
	public float getmLeftTopPosY(){
		return mLeftTopPosY;
	}
	
	public float getmRightBottomPosY() {
		return mRightBottomPosY+50;
	}
	public float getmRightBottomPosX() {
		return mRightBottomPosX;
	}
	public void setRec(Rect rec) {
		this.buttonRec = rec;

	}
	public Rect getRect() {
		return Rec;
	}
	

	public void setInvalidate() {
		invalidate();
		
	}
}
