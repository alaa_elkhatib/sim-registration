package  com.simregistration.signed.utility;


/**
 * Wheel changed listener interface.
 * <p>The onChanged() method is called whenever current wheel positions is changed:
 * <li> New Wheel position is set
 * <li> Wheel view is scrolled
 */
public interface OnWheelChangedListener {

 void onChanged(WheelView wheel, int oldValue);
}
