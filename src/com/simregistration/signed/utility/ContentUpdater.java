package com.simregistration.signed.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ContentUpdater {
	private Context context;
	private SQLiteDatabase db;
	public ContentUpdater(Context context, SQLiteDatabase db) {
		this.context = context;
		this.db = db;
	}

	public synchronized boolean saveContentsForNotificaiton(String strSIM,String strDateTime,
			String strMessage,String strTransID,String strVarification,String strActivation,String strDate) {
		boolean result = false;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.SIM, strSIM);
				vals.put(C.DATETIME, strDateTime);
				vals.put(C.MESSAGE, strMessage);
				vals.put(C.TRANSID, strTransID);
				vals.put(C.VARIFICATION, strVarification);
				vals.put(C.ACTIVATION, strActivation);
				vals.put(C.NDATE, strDate);
				long rid = db.insert(C.NOTIFICATION_TABLE, null, vals);
				result = true;
		} catch (Exception ex) {
		}
		return result;
	}

	
	public synchronized boolean saveContentsForCountries(String strID,String strName_Eng) {
		boolean result = false;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.ID, strID);
				vals.put(C.NAME, strName_Eng);
				long rid = db.insert(C.COUNTRIES_TABLE, null, vals);
				result = true;
		} catch (Exception ex) {
		}
		return result;
	}
	
	public synchronized void deleteContentsForCountries(String strID) {
		
		try {

			String sql = "DELETE FROM " + C.COUNTRIES_TABLE+" Where "+C.ID+"='"+strID+"'";
			db.execSQL(sql);
			
		} catch (Exception ex) {
		}
	}
	
	public synchronized boolean updateContentsForCountries(String strID,String strName_Eng) {
		boolean result = false;
		try {

			Cursor c=db.rawQuery("SELECT * FROM " + C.COUNTRIES_TABLE+" WHERE "+C.ID+"='"+strID+"'", null);
			if(c.moveToFirst())
			{
				ContentValues vals = new ContentValues();
				vals.put(C.NAME, strName_Eng);
				long rid = db.update(C.COUNTRIES_TABLE, vals, C.ID+" = '"+strID+"'",null);
			}
			else
			{
				ContentValues vals = new ContentValues();
				vals.put(C.ID, strID);
				vals.put(C.NAME, strName_Eng);
				long rid = db.insert(C.COUNTRIES_TABLE, null, vals);
				result = true;
			}
			
		
				result = true;
		} catch (Exception ex) {
		}
		return result;
	}
	
	
	
	public synchronized void deleteOldContentsCountries() {

		try {
			String sql = "DELETE FROM " + C.COUNTRIES_TABLE;
			db.execSQL(sql);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	
	
	
	public synchronized boolean saveContentsForUser(String strID,String strName,String strShopName,String strLocation,String strMobile,
		String strEmail,String strLandmark,String strCR_Number,String strDate,String strLocked,String strDisabled,String strCommission
		,String strLang,String strLogin_DateTime,String strUserName,String strPassword, int iAuto,String strType,String strBundleSales) {
		boolean result = false;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.ID, strID);
				vals.put(C.NAME, strName);
				vals.put(C.SHOPNAME, strShopName);	
				vals.put(C.LOCATION, strLocation);
				vals.put(C.MOBILE, strMobile);
				vals.put(C.EMAIL, strEmail);
				vals.put(C.LANDMARK, strLandmark);
				vals.put(C.CR_NUMBER, strCR_Number);
				vals.put(C.DATE, strDate);
				vals.put(C.LOCKED, strLocked);
				vals.put(C.DISABLED, strDisabled);
				vals.put(C.COMMISSION, strCommission);
				vals.put(C.LANG, strLang);
				vals.put(C.LOGIN_DATETIME, strLogin_DateTime);
				vals.put(C.USERNAME, strUserName);
				vals.put(C.PASSWORD, strPassword);
				vals.put(C.AUTO, iAuto);
				vals.put(C.TYPE, strType);
				vals.put(C.BUNDLESALES, strBundleSales);
				long rid = db.insert(C.USER_TABLE, null, vals);
				result = true;
		} catch (Exception ex) {
		}
		return result;
	}
	
	
	public synchronized void deleteOldContentsUser() {

		try {
			String sql = "DELETE FROM " + C.USER_TABLE;
			db.execSQL(sql);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	public synchronized boolean updateLoginDateContentsForUser(String strLogin_DateTime) {
			boolean result = false;
			try {

					ContentValues vals = new ContentValues();
					vals.put(C.LOGIN_DATETIME, strLogin_DateTime);
					long rid = db.update(C.USER_TABLE, vals, null, null);
					result = true;
			} catch (Exception ex) {
			}
			return result;
		}
	
	public synchronized boolean updateAutoLoginContentsForUser(int iAuto) {
		boolean result = false;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.AUTO, iAuto);
				long rid = db.update(C.USER_TABLE, vals, null, null);
				result = true;
		} catch (Exception ex) {
		}
		return result;
	}
	
	
	public synchronized long saveContentsErrorLog(String strUserID,String strClassName,String strStackTrace,
			String strDeviceInfo,String strDateTime) {
		long id=0;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.USERID, strUserID);
				vals.put(C.CLASSNAME, strClassName);
				vals.put(C.STACKTRACE, strStackTrace);
				vals.put(C.DEVICEINFO, strDeviceInfo);
				vals.put(C.DATETIME, strDateTime);
				vals.put(C.BANDWIDTH, "0");
				id = db.insert(C.ERRORLOG_TABLE, null, vals);
				} catch (Exception ex) {
		}
		return id;
	}
	
	public synchronized long saveContentsErrorLogBandwidth(String strID,String strBandwidth) {
		long id=0;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.BANDWIDTH, strBandwidth);
				id = db.update(C.ERRORLOG_TABLE, vals, C.ID+" = " +strID, null);
				} catch (Exception ex) {
		}
		return id;
	}

	
	public synchronized void deleteErrorLog(String strErrorID) {

		try {
			String sql = "DELETE FROM " + C.ERRORLOG_TABLE+ " where "+ C.ID+" = "+strErrorID;
			db.execSQL(sql);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	
	public synchronized boolean saveContentsSignal(String strSignal,String strErrorFK) {
		boolean result = false;
		try {

				ContentValues vals = new ContentValues();
				vals.put(C.SIGNAL, strSignal);
				vals.put(C.ERRORFK, strErrorFK);
				long rid = db.insert(C.SIGNAL_TABLE, null, vals);
				if(rid!=-1)
					result=true;
				} catch (Exception ex) {
		}
		return result;
	}
	
	public synchronized void deleteSignal(String strSignalID) {

		try {
			String sql = "DELETE FROM " + C.SIGNAL_TABLE+ " where "+ C.ID+" = "+strSignalID;
			db.execSQL(sql);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	
	
	
}


