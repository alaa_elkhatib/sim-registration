package com.simregistration.signed.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import com.simregistration.signed.Login;
import com.simregistration.signed.R;
import com.simregistration.signed.Register;
import com.simregistration.signed.entity.Countries_Entity;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static int DATABASE_VERSION =11;
	private ContentUpdater updater;
	Context context;       

	public DatabaseHelper(Context context) {
		super(context, C.DATABASE_NAME, null, DATABASE_VERSION);
		
		this.context = context;
		    if(!checkdatabase()){ 
		    	this.getReadableDatabase();
		        copydatabase(); }
	}

	@Override
	public void onCreate(SQLiteDatabase db) {}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		updater = new ContentUpdater(context, db);
	   try{
		db.execSQL("CREATE TABLE IF NOT EXISTS User  (ID text,Name text,ShopName text,Location text,Mobile text,Email text,Landmark text,CR_Number text,Date text,Locked text,Disabled text,Commission text,Lang text,Login_DateTime text,UserName text,Password text,auto integer DEFAULT 0,type integer DEFAULT 1)");
	   }catch(Exception ex){}
	    try{db.execSQL("Delete From Notification where  date(DateTime) < '2015-05-24'");
	    }catch(Exception ex){}
	    try{db.execSQL("ALTER TABLE Notification ADD COLUMN NDate text");
	    }catch(Exception ex){}
	    try{db.execSQL("UPDATE "+C.NOTIFICATION_TABLE +" SET " + C.NDATE+ " = "+C.DATETIME);
	    }catch(Exception ex){}    
	    try{db.execSQL("update Notification set NDate = substr(NDate , 7,4) || '-' || substr(NDate ,4,2)  || '-' || substr(NDate , 1,2)");
	    }catch(Exception ex){}
	    try{db.execSQL("ALTER TABLE User ADD COLUMN type integer DEFAULT 1 ");
	    }catch(Exception ex){}
	    try{db.execSQL("Delete * From User");
	    }catch(Exception ex){}
	    try{db.execSQL("CREATE TABLE ErrorLog (ID integer PRIMARY KEY AUTOINCREMENT,ClassName text,StackTrace text,DeviceInfo text,DateTime text,Bandwidth text DEFAULT '0')");
	    }catch(Exception ex){}
	    try{db.execSQL("CREATE TABLE Signal (ID integer PRIMARY KEY AUTOINCREMENT,Signal text,Error_FK integer)");
	    }catch(Exception ex){}
	   
	    try{db.execSQL("ALTER TABLE User ADD COLUMN BundleSales text DEFAULT false ");
	    }catch(Exception ex){}
	    
	    try{db.execSQL("Drop TABLE Countries");
	    }catch(Exception ex){}
	    
	    try{db.execSQL("CREATE TABLE IF NOT EXISTS Countries  (ID text,Name text)");
	    }catch(Exception ex){}
	   
	  
		Log.w(C.TAG,
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data"); 
		
	}
	
	
	private void copydatabase()  { 
		 try{
	    //Open your local db as the input stream 
	    InputStream myinput =context.getAssets().open(C.DATABASE_NAME); 
	    //Open the empty db as the output stream 
	    OutputStream myoutput = new FileOutputStream(C.DATABASE_PATH + C.DATABASE_NAME); 
	 
	    // transfer byte to inputfile to outputfile 
	    byte[] buffer = new byte[1024]; 
	    int length; 
	    while ((length = myinput.read(buffer))>0) 
	    { 
	        myoutput.write(buffer,0,length); 
	    } 
	 
	    //Close the streams 
	    myoutput.flush(); 
	    myoutput.close(); 
	    myinput.close(); 
	 
		 }catch(Exception ex){
			 ex.getMessage();
		 }} 
	private boolean checkdatabase() { 
	    //SQLiteDatabase checkdb = null; 
	    boolean checkdb = false; 
	    try{ 
	        String myPath = C.DATABASE_PATH + C.DATABASE_NAME; 
	        File dbfile = new File(myPath); 
	        checkdb = dbfile.exists(); 
	    } 
	    catch(SQLiteException e){ 
	        System.out.println("Database doesn't exist"); 
	    } 
	 
	    return checkdb; 
	} 
	
	/* public class Countires extends AsyncTask<Void, Void, Integer> {
		  	private ArrayList<Countries_Entity> alCountriesData= new ArrayList<Countries_Entity>();
				@Override
				protected void onPreExecute() {
					super.onPreExecute();
				
				}

				@Override
				protected Integer doInBackground(Void... params) {
					int iRes=-1;
					try {
						  WebService.GetCountriesWithDate(context,"1",alCountriesData,C.getLastModificationDate(context));
					}catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(context,Utility.GetUserEntity(context).getId(),context.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						
						//if(Utility.isConnected(context))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

					}
					return iRes;
				}

				@Override
				protected void onPostExecute(Integer v) {
					super.onPostExecute(v);
					try{
					if(alCountriesData.size()>0){
						updater.deleteOldContentsCountries();
						for (int i=0;i<alCountriesData.size();i++){
						Countries_Entity countries = (Countries_Entity)alCountriesData.get(i);
						if(countries.getAction().equals("1"))
						updater.saveContentsForCountries(countries.getID(), countries.getNameEng(), countries.getNameAr());
						else if(countries.getAction().equals("2"))
							updater.updateContentsForCountries(countries.getID(), countries.getNameEng(), countries.getNameAr());
						else if(countries.getAction().equals("3"))
							updater.deleteContentsForCountries(countries.getID());
						}	
						    Calendar c = Calendar.getInstance();     
					        C.storeLastModificationDate(context, c.get(Calendar.YEAR)+""+ c.get(Calendar.MONTH)+""+c.get(Calendar.DAY_OF_MONTH)+""+c.get(Calendar.HOUR_OF_DAY)+""+c.get(Calendar.MINUTE)+""+c.get(Calendar.SECOND));
						
					}
					
					}catch(Exception ex){
						WebService.SaveErrorLogNoBandwidth(context,Utility.GetUserEntity(context).getId(),context.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   				
						
						// if(Utility.isConnected(context))
						//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   	//else
		      	   	//	Utility.showToast(context, getString(R.string.alert_need_internet_connection)); 
		         
					}}}
*/
}
