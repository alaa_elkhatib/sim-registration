package com.simregistration.signed.utility;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class WifiReceiver extends BroadcastReceiver {
	public WifiManager mainWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();
	private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
    long id ;
    
    public WifiReceiver (long iID){
    	id=iID;}
    
	  public void onReceive(Context c, Intent intent) {
		  dbHelper = new DatabaseHelper(c);
		  db = dbHelper.getWritableDatabase();
		  updater = new ContentUpdater(c, db);
		  WifiInfo wifiInfo = mainWifi.getConnectionInfo();
          int level=WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 5);
          sb.append("Bars =" +level); 
          updater.saveContentsSignal(Integer.toString(level), Long.toString(id));
          c.getApplicationContext().unregisterReceiver(this);}
}
