package com.simregistration.signed.utility;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.gson.Gson;
import com.simregistration.signed.entity.User_Entity;

public class Utility {
	private static Map<String, Locale> localeMap;
	public static boolean isConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		boolean connected = ni != null && ni.isConnected();
		return connected;
	}

	

	public static void showToast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
	public static void showToast2(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	public static void alert(String message, final Activity activity,android.content.DialogInterface.OnClickListener onclick) {
		AlertDialog.Builder bld = new AlertDialog.Builder(activity);
		bld.setMessage(message);
		bld.setNeutralButton("OK", onclick);
		final AlertDialog alertDialog =bld.create();
		alertDialog.show();
		alertDialog.setCancelable(false);
		alertDialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                    KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	alertDialog.dismiss();
                	activity.setResult(-1, null);
                	activity.finish();
                }
                return true;
            }
        });


	}
	public static void alertnoBack(String message, final Activity activity,android.content.DialogInterface.OnClickListener onclick) {
		AlertDialog.Builder bld = new AlertDialog.Builder(activity);
		bld.setMessage(message);
		bld.setNeutralButton("OK", onclick);
		final AlertDialog alertDialog =bld.create();
		alertDialog.show();
		alertDialog.setCancelable(false);
	}


	public String getDeviceName() {
		  String manufacturer = Build.MANUFACTURER;
		  String model = Build.MODEL;
		  if (model.startsWith(manufacturer)) {
		    return capitalize(model);
		  } else {
		    return capitalize(manufacturer) + " " + model;
		  }
		}


		private String capitalize(String s) {
		  if (s == null || s.length() == 0) {
		    return "";
		  }
		  char first = s.charAt(0);
		  if (Character.isUpperCase(first)) {
		    return s;
		  } else {
		    return Character.toUpperCase(first) + s.substring(1);
		  }
		}

	
	
	
	
	public static String getDeviceId(Context context) {
		try {
			final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String deviceid = tm.getDeviceId();
			if (deviceid == null)
				deviceid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
			return deviceid;

		} catch (Exception e) {
		//	Log.e(C.TAG, "An exception was thrown", e);
		}

		return "";
	}
	
	public static String getDeviceNumber(Context context) {
		try {
			final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String devicenumber = tm.getLine1Number();
			if (devicenumber == null)
				devicenumber = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
			return devicenumber;

		} catch (Exception e) {
		//	Log.e(C.TAG, "An exception was thrown", e);
		}

		return "";
	}
	

	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	
	
	public static boolean isNotEmpty(CharSequence str) {
		return !isEmpty(str);
	}

	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}
	
	
	public static Bitmap createContrast(Bitmap src, double value) {
	    // image size
	    int width = src.getWidth();
	    int height = src.getHeight();
	    // create output bitmap
	    Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
	    // color information
	    int A, R, G, B;
	    int pixel;
	    // get contrast value
	    double contrast = Math.pow((100 + value) / 100, 2);
	 
	    // scan through all pixels
	    for(int x = 0; x < width; ++x) {
	        for(int y = 0; y < height; ++y) {
	            // get pixel color
	            pixel = src.getPixel(x, y);
	            A = Color.alpha(pixel);
	            // apply filter contrast for every channel R, G, B
	            R = Color.red(pixel);
	            R = (int)(((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
	            if(R < 0) { R = 0; }
	            else if(R > 255) { R = 255; }
	 
	            G = Color.red(pixel);
	            G = (int)(((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
	            if(G < 0) { G = 0; }
	            else if(G > 255) { G = 255; }
	 
	            B = Color.red(pixel);
	            B = (int)(((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
	            if(B < 0) { B = 0; }
	            else if(B > 255) { B = 255; }
	 
	            // set new pixel color to output bitmap
	            bmOut.setPixel(x, y, Color.argb(A, R, G, B));
	        }
	    }
	 
	    // return final image
	    return bmOut;
	}
	
	
	public static Bitmap doBrightness(Bitmap src, int value) {
	    // image size
	    int width = src.getWidth();
	    int height = src.getHeight();
	    // create output bitmap
	    Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
	    // color information
	    int A, R, G, B;
	    int pixel;
	 
	    // scan through all pixels
	    for(int x = 0; x < width; ++x) {
	        for(int y = 0; y < height; ++y) {
	            // get pixel color
	            pixel = src.getPixel(x, y);
	            A = Color.alpha(pixel);
	            R = Color.red(pixel);
	            G = Color.green(pixel);
	            B = Color.blue(pixel);
	 
	            // increase/decrease each channel
	            R += value;
	            if(R > 255) { R = 255; }
	            else if(R < 0) { R = 0; }
	 
	            G += value;
	            if(G > 255) { G = 255; }
	            else if(G < 0) { G = 0; }
	 
	            B += value;
	            if(B > 255) { B = 255; }
	            else if(B < 0) { B = 0; }
	 
	            // apply new pixel color to output bitmap
	            bmOut.setPixel(x, y, Color.argb(A, R, G, B));
	        }
	    }
	 
	    // return final image
	    return bmOut;
	}
	
	
	

	/*public static class ErrorLogLoader extends AsyncTask<String, Void, Integer>
	{
		
		@Override
		protected Integer doInBackground(String... arg0)
		{try{
			return WebService.AddErrorLog(arg0[0],arg0[1],arg0[2],arg0[3]);
			}catch(Exception ex){}
		return -1;
		}
		}
	*/
	
	
	public static void SaveUserEntity(Context cont,User_Entity user){
		try{
		 SharedPreferences  mPrefs = cont.getSharedPreferences("myPrefs",cont.MODE_PRIVATE);
		 Editor prefsEditor = mPrefs.edit();
	     Gson gson = new Gson();
	     String json = gson.toJson(user);
	     prefsEditor.putString("User", json);
	     prefsEditor.commit();}catch(Exception ex){
	    	 ex.getMessage();
	     }
	}
	
	
	public static User_Entity GetUserEntity(Context cont){
		 User_Entity obj= new User_Entity();
		 SharedPreferences  mPrefs =cont.getSharedPreferences("myPrefs",cont.MODE_PRIVATE);
		  Gson gson = new Gson();
		  String json = mPrefs.getString("User", "");
		   obj = gson.fromJson(json, User_Entity.class);
		   if(obj!=null)
		   return obj;
		   else{
			   return  new User_Entity();
		   }
	}
	
	public static void ClearShared(Activity act){
		 SharedPreferences  mPrefs =act.getSharedPreferences("myPrefs",act.MODE_PRIVATE);
		 Editor prefsEditor = mPrefs.edit();
		 prefsEditor.clear();
		 prefsEditor.commit();
		
	}
	
	//islamic to christian calendar
		public static int[] islToChr(int y, int m, int d, int diff) {
			
			int jd=intPart((11*y+3)/30)+354*y+30*m-intPart((m-1)/2)+d+1948440-385-diff;	
			if (jd> 2299160 )
			{
				int l=jd+68569;
				int n=intPart((4*l)/146097);
				l=l-intPart((146097*n+3)/4);
				int i=intPart((4000*(l+1))/1461001);
				l=l-intPart((1461*i)/4)+31;
				int j=intPart((80*l)/2447);
				d=l-intPart((2447*j)/80);
				l=intPart(j/11);
				m=j+2-12*l;
				y=100*(n-49)+i+l;
			}	
			else	
			{
				int j=jd+1402;
				int k=intPart((j-1)/1461);
				int l=j-1461*k;
				int n=intPart((l-1)/365)-intPart(l/1461);
				int i=l-365*n+30;
				j=intPart((80*i)/2447);
				d=i-intPart((2447*j)/80);
				i=intPart(j/11);
				m=j+2-12*i;
				y=4*k+n+i-4716;
			}

			int[] res = new int [3];
			res[0] = d;
			res[1] = m;
			res[2] = y;
			return res;
		}

		
		static int intPart(int floatNum){
			if ((float)floatNum < -0.0000001){
			 return (int) Math.ceil(floatNum-0.0000001);
			}
		return (int)Math.floor(floatNum +0.0000001);	
		}
		
		public static boolean isConnectedToServer(String url, int timeout) {
            try{
                URL myUrl = new URL(url);
                URLConnection connection = myUrl.openConnection();
               // connection.setConnectTimeout(timeout);
                connection.connect();
                return true;
            } catch (Exception e) {
                return false;
            }
        }
		public static void initCountryCodeMapping() {
		    String[] countries = Locale.getISOCountries();
		    localeMap = new HashMap<String, Locale>(countries.length);
		    for (String country : countries) {
		        Locale locale = new Locale("", country);
		        localeMap.put(locale.getISO3Country().toUpperCase(), locale);
		    }
		}
		
		public static String iso3CountryCodeToIso2CountryCode(String iso3CountryCode) {
		    return localeMap.get(iso3CountryCode).getCountry();
		}
		public static String getMimeType(String url)
	    {
	        String parts[]=url.split("\\.");
	        String extension=parts[parts.length-1];
	        String type = null;
	        if (extension != null) {
	            MimeTypeMap mime = MimeTypeMap.getSingleton();
	            type = mime.getMimeTypeFromExtension(extension);
	        }
	        return type;
	    }
		public static File createFileFromInputStream(InputStream inputStream) {

			   try{
			      File f = new File("manaul");
			      java.io.FileOutputStream outputStream = new java.io.FileOutputStream(f);
			      byte buffer[] = new byte[1024];
			      int length = 0;

			      while((length=inputStream.read(buffer)) > 0) {
			        outputStream.write(buffer,0,length);
			      }

			      outputStream.close();
			      inputStream.close();

			      return f;
			   }catch (IOException e) {
			        e.getMessage();
			   }

			   return null;
			}
		
		public static void copyFile(InputStream in, OutputStream out) throws IOException {
		    byte[] buffer = new byte[1024];
		    int read;
		    while((read = in.read(buffer)) != -1){
		      out.write(buffer, 0, read);
		    }
		}
	}

