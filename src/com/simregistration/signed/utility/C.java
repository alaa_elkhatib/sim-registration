package com.simregistration.signed.utility;
import java.util.ArrayList;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.simregistration.signed.MyPhoneStateListener;
import com.simregistration.signed.entity.User_Entity;

/*
 * Application level constant is decalred in this class
 */

public class C extends Application {
	public static MyPhoneStateListener myListener ;
	public static String TAG = "SIM Registration";
	public static String APPNAME = "SIM Registration";
	public static final String DATABASE_NAME = "simregistration";
	public final static String DATABASE_PATH = "data/data/com.simregistration.signed/databases/"; 
	public static String WSUserName="SIM";
	public static String WSPassword="123456";
	public static Integer AGE=18;
    //public static String USER_ID="" ;
	
	public static final String NOTIFICATION_TABLE = "Notification";
	public static final String ERRORLOG_TABLE = "ErrorLog";
	public static final String SIGNAL_TABLE = "Signal";
	public static final String COUNTRIES_TABLE = "Countries";
	public static final String USER_TABLE = "User";
	
	public static final String SIM = "SIM";
	public static final String DATETIME = "DateTime";
	public static final String MESSAGE = "Message";
	public static final String STATUS = "Status";
	public static final String TRANSID = "TransID";
	public static final String VARIFICATION = "Varification";
	public static final String ACTIVATION = "Activation";
	public static final String ID = "ID";
	public static final String NAME_ENG = "Name_Eng";
	public static final String NAME_AR= "Name_Ar";
	public static final String NAME= "Name";
	public static final String SHOPNAME= "ShopName";
	public static final String LOCATION= "Location";
	public static final String MOBILE= "Mobile";
	public static final String EMAIL= "Email";
	public static final String LANDMARK= "Landmark";
	public static final String CR_NUMBER= "CR_Number";
	public static final String DATE= "Date";
	public static final String LOCKED= "Locked";
	public static final String DISABLED= "Disabled";
	public static final String COMMISSION= "Commission";
	public static final String LANG= "Lang";
	public static final String LOGIN_DATETIME= "Login_DateTime";
	public static final String USERNAME= "UserName";
	public static final String PASSWORD= "Password";
	public static final String AUTO= "Auto";
	public static final String NDATE= "NDate";
	public static final String TYPE= "Type";	
	public static final String CLASSNAME= "ClassName";
	public static final String STACKTRACE= "StackTrace";
	public static final String DEVICEINFO= "DeviceInfo";
	public static final String SIGNAL= "Signal";
	public static final String ERRORFK= "Error_FK";
	public static final String USERID= "UserID";
	public static final String BANDWIDTH= "Bandwidth";
	public static final String BUNDLESALES= "BundleSales";
	public static SharedPreferences sharedPref;
   public static Editor editor;
	//public static ArrayList<User_Entity> alUserMainData= new ArrayList<User_Entity>();
	@Override
	public void onCreate() {
		super.onCreate();
		 myListener = new MyPhoneStateListener(this);
	     TelephonyManager telManager  = ( TelephonyManager )getSystemService(Context.TELEPHONY_SERVICE);
		 telManager.listen(myListener ,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		 initImageLoader(getApplicationContext());

	}
	
	public static void initImageLoader(Context context) {
		int memoryCacheSize;
		int memClass = ( ( ActivityManager )context.getSystemService( Context.ACTIVITY_SERVICE ) ).getMemoryClass();
		memoryCacheSize = 1024 * 1024 * memClass / 8;

		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
			.threadPriority(Thread.NORM_PRIORITY - 2)
			.memoryCacheSize(memoryCacheSize)
			.denyCacheImageMultipleSizesInMemory()
			.discCacheFileNameGenerator(new Md5FileNameGenerator())
			.tasksProcessingOrder(QueueProcessingType.LIFO)
			.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}
	 public static void UserPreferences(Context context) {	
			sharedPref 	  = context.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
			editor 		  = sharedPref.edit();}
	   
	public static void storeLastModificationDate(Context context,String strModificationDate) {
		UserPreferences(context);
		editor.putString("ModificationDate",strModificationDate);
		editor.commit();}
	
	
	public static String getLastModificationDate(Context context) {
		UserPreferences(context);
		return sharedPref.getString("ModificationDate","20110101000000");}
}

	
