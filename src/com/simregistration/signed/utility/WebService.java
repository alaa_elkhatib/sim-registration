package com.simregistration.signed.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ksoap2clone.HeaderProperty;
import org.ksoap2clone.SoapEnvelope;
import org.ksoap2clone.serialization.SoapObject;
import org.ksoap2clone.serialization.SoapPrimitive;
import org.ksoap2clone.serialization.SoapSerializationEnvelope;
import org.ksoap2clone.transport.HttpTransportSE;
import org.kxml2.kdom.Node;
import com.simregistration.signed.entity.BundleSalesResponse_Entity;
import com.simregistration.signed.entity.Bundle_Entity;
import com.simregistration.signed.entity.Bundles_Entity;
import com.simregistration.signed.entity.Countries_Entity;
import com.simregistration.signed.entity.Denomination_Entity;
import com.simregistration.signed.entity.Denominations_Entity;
import com.simregistration.signed.entity.Info_Entity;
import com.simregistration.signed.entity.Offers_Entity;
import com.simregistration.signed.entity.Register_Entity;
import com.simregistration.signed.entity.Result_Entity;
import com.simregistration.signed.entity.SIMCommission_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.entity.Stock_Entity;
import com.simregistration.signed.entity.Stocks_Entity;
import com.simregistration.signed.entity.User_Entity;



import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

/*
 * Application level constant is decalred in this class
 */

public class WebService {
	public static String strNamespace = "http://tempuri.org/";
//	public static String strURL ="http://94.128.16.54/SIMRegistration_BH_WS/service.asmx";
//	public static String strURL ="http://168.187.85.218/SIMRegistration_BH_WS/service.asmx";
	public static String strURL ="http://83.136.56.172/SIMRegistration_BH_WS/service.asmx";
	public static String strCredUser = "SIM";
	public static String strCredPass = "123456";
	public static String strMethodAddToken = "WS_SIM_AddToken";
	public static String strMethodLogin= "WS_SIM_CheckLogin_Accounts";
	public static String strMethodLocation="WS_SIM_LocationInformation";
	public static String strMethodErrorLog="WS_SIM_InsertErrorLog";
	public static String strMethodUploadImages="WS_SIM_UploadImages";
	public static String strMethodInsertSIMTransaction="WS_SIM_InsertSIMTransaction";
	public static String strMethodGetSIMTransaction="WS_SIM_GetSIMTransaction";
	public static String strMethodGetCountries="WS_SIM_GetCountry";
	public static String strMethodGetCountriesWithDate="WS_SIM_GetCountryWithDate";
	public static String strMethodGetSIMCommission="WS_SIM_GetCommission";
	public static String strMethodRegister="WS_SIM_RegisterDealer";
	public static String strMethodRegister2="WS_SIM_RegisterDealer_New";
	public static String strMethodForgetPassword="WS_SIM_AccountForgetPassword";
	public static String strMethodChangePassword="WS_SIM_AccountChangePassword";
	public static String strMethodAddAppVersion="WS_SIM_MobileApplicationVersion";
	public static String strMethodChangeComission="WS_SIM_AccountChangeCommissionMobileNo";
	public static String strMethodAuditTrail="WS_SIM_AuditTrail";
	public static String strMethodErrorLog2="WS_SIM_InsertErrorLog2";
	public static String strMethodGetBundlesSales="WS_SIM_BundleSales";
	public static String strMethodInsertBundleSales="WS_SIM_Insert_BundleSalesTransaction";
	public static String strMethodInsertCustomer_CodeBundle="WS_SIM_InsertCustomer_CodeBundle";
	public static String strMethodGetDenominations="WS_SIM_GetServices";
	public static String strMethodVoucher="WS_SIM_Voucher";
	public static String strMethodCreditTransfer="WS_SIM_CreditTransfer";
	public static String strMethodGetStocks="WS_SIM_GetAccountStock";
	public static String strMethodGetMyInfo="WS_SIM_MyAccountInfo";
	public static String strMethodGetOffers="WS_SIM_GetOffers";
	public static String strMethodGetAgeSetting="WS_SIM_GetAgeSetting";
	static DatabaseHelper dbHelper;
	static SQLiteDatabase db ;
	static ContentUpdater updater;
	
	
	


	public static org.kxml2.kdom.Element[] credentials = null;
	public static org.kxml2.kdom.Element[] getCredentials()
	{
		if (credentials == null)
		{
			org.kxml2.kdom.Element usernameElement = new org.kxml2.kdom.Element().createElement(strNamespace, "UserName");
	        usernameElement.addChild(Node.TEXT, strCredUser);
	        org.kxml2.kdom.Element passElement = new org.kxml2.kdom.Element().createElement(strNamespace, "Password");
	        passElement.addChild(Node.TEXT, strCredPass);
	        
	        org.kxml2.kdom.Element securityElement = new org.kxml2.kdom.Element().createElement(strNamespace, "UserCredentials");
	        securityElement.setPrefix("h", strNamespace);
	        securityElement.addChild(Node.ELEMENT, usernameElement);
	        securityElement.addChild(Node.ELEMENT, passElement);
	        
	        credentials = new org.kxml2.kdom.Element[] {securityElement};
		}
		
        return credentials;
	}
	
	/*public static SoapSerializationEnvelope prepareAndSendEnv2 (SoapObject obj)
	{
		
		try
		{
			SSLConnection.allowAllSSL(); 
			SoapSerializationEnvelope env = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			HttpTransportSE tr = null;
	    	env.dotNet = true;
	    	env.headerOut =  getCredentials();
	    	env.setOutputSoapObject(obj);
	    	
	    	if (tr == null){
	    		tr = new HttpTransportSE(strURL,60000);}
	    	
	    	//tr.reset();
	    	
	    	ArrayList<HeaderProperty> headerPropertyArrayList = new ArrayList<HeaderProperty>();
	    	headerPropertyArrayList.add(new HeaderProperty("Connection", "close"));
			tr.call(strNamespace + obj.getName(), env,headerPropertyArrayList);

			return env;
		}
		catch (Exception ex)
		{
			if(ex!=null){
			if(Utility.isConnected(context))
				 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
			}
			return null;
			}
	}*/
	
	
	public static SoapSerializationEnvelope prepareAndSendEnv (Context context,SoapObject obj)
	{
		
		SoapSerializationEnvelope env = null;
		try
		{
			//SSLConnection.allowAllSSL(); 
			env = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			HttpTransportSE tr = null;
	    	env.dotNet = true;
	    	env.headerOut =  getCredentials();
	    	env.setOutputSoapObject(obj);
	    	
	    	if (tr == null){
	    		tr = new HttpTransportSE(strURL,60000);}
	    	
	    	//tr.reset();
	    	ArrayList<HeaderProperty> headerPropertyArrayList = new ArrayList<HeaderProperty>();
	    	headerPropertyArrayList.add(new HeaderProperty("Connection", "close"));
	    	
			tr.call(strNamespace + obj.getName(), env,headerPropertyArrayList);

			return env;
		}
		catch (Exception ex)
		{ 
			ex.getMessage();
			SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
			prepareAndSendEnv (context,obj);
		}
		return env;
	}
	
	public static SoapObject createSoapObj (String strMethodName)
	{
		SoapObject obj = new SoapObject(strNamespace, strMethodName);
		return obj;
	}
	
	
	public static Bitmap decodeFile(int dstWidth,int dstHeight,String pathOfInputImage) {
        try {
        	try
        	{
        	    int inWidth = 0;
        	    int inHeight = 0;

        	    InputStream in = new FileInputStream(pathOfInputImage);

        	    // decode image size (decode metadata only, not the whole image)
        	    BitmapFactory.Options options = new BitmapFactory.Options();
        	    options.inJustDecodeBounds = true;
        	    BitmapFactory.decodeStream(in, null, options);
        	    in.close();
        	    in = null;

        	    // save width and height
        	    inWidth = options.outWidth;
        	    inHeight = options.outHeight;

        	    // decode full image pre-resized
        	    in = new FileInputStream(pathOfInputImage);
        	    options = new BitmapFactory.Options();
        	    // calc rought re-size (this is no exact resize)
        	    options.inSampleSize = Math.max(inWidth/dstWidth, inHeight/dstHeight);
        	    // decode full image
        	    Bitmap roughBitmap = BitmapFactory.decodeStream(in, null, options);

        	    // calc exact destination size
        	    Matrix m = new Matrix();
        	    RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
        	    RectF outRect = new RectF(0, 0, dstWidth, dstHeight);
        	    m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
        	    float[] values = new float[9];
        	    m.getValues(values);

        	    // resize bitmap
        	    Bitmap resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);

        	    // save image
        	    try
        	    {
        	        FileOutputStream out = new FileOutputStream(pathOfInputImage);
        	        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
        	        return resizedBitmap;
        	    }
        	    catch (Exception e)
        	    {
        	        Log.e("Image", e.getMessage(), e);
        	    }
        	}
        	catch (IOException e)
        	{
        	    Log.e("Image", e.getMessage(), e);
        	}} catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }
	

	 //////////////////////////////////////////////////////////////////////////////////////
	
	   public static Bitmap GetBitmap(byte[] cover ){
		   Options options = new BitmapFactory.Options(); 
		   options.inJustDecodeBounds = true; 
		   Bitmap img=null;
		   try{
		   ByteArrayInputStream is = new ByteArrayInputStream(cover);
		   BitmapFactory.decodeStream(is, null, options); 
		   Boolean scaleByHeight = Math.abs(options.outHeight - 65) >= Math.abs(options.outWidth - 65); 
		   img =BitmapFactory.decodeStream(is, null, options); 
		   if(options.outHeight * options.outWidth * 2 >= 200*200*2){ 
		          // Load, scaling to smallest power of 2 that'll get it <= desired dimensions 
		         double sampleSize = scaleByHeight 
		               ? options.outHeight / 65 
		               : options.outWidth / 65; 
		         options.inSampleSize =  
		               (int)Math.pow(2d, Math.floor( 
		               Math.log(sampleSize)/Math.log(2d))); 
		      } 
		  
		         // Do the actual decoding 
		         options.inJustDecodeBounds = false; 
		  
		         is.close();
		         is = new ByteArrayInputStream(cover);
		         img = BitmapFactory.decodeStream(is, null, options); 
		         is.close();}
		  catch(Exception ex){}
				return img; 
	   }
	
   //////////////////////////////////////////////////////////////
		 public static int AddToken(Context context,String RegistrationId,String iUserId)
			{
			    int iRes=-1;
				try
				{
					SoapObject obj = createSoapObj(strMethodAddToken);
					obj.addProperty("UserID",iUserId);
					obj.addProperty("UserType", "A");
					obj.addProperty("DeviceTokenID", RegistrationId);
					obj.addProperty("DeviceTokenType", "1");
					obj.addProperty("WSUserName",C.WSUserName);
					obj.addProperty("WSPassword",C.WSPassword);
					SoapSerializationEnvelope env= prepareAndSendEnv(context,obj);		
					iRes = Integer.parseInt(env.getResponse().toString());
									
				}
				catch (Exception ex){		
					SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
					Log.w("Error", ex.getCause());}

			return iRes;
			}
		 
		 /////////////////////////////////////////////////////
		 public static int Login (Context context,String strUserName,String strPassword,String strDeviceID,String strDeviceNumber, ArrayList<User_Entity> list)
			{
				int iRes = 0;
				
				try
				{
					SoapObject obj = createSoapObj(strMethodLogin);	
					obj.addProperty("UserName", strUserName);
					obj.addProperty("Password", strPassword);
					obj.addProperty("DeviceIMEI", strDeviceID);
					obj.addProperty("DeviceSIMMSISDN", "9731001001");
					
					obj.addProperty("DeviceType", "1");
					obj.addProperty("WSUserName",strCredUser);
					obj.addProperty("WSPassword",strCredPass);
				    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					User_Entity source;
					SoapObject sobj;
					SoapObject response = (SoapObject)env.getResponse();			
					iRes = response.getPropertyCount();
					        source= new User_Entity();
							if(response.hasProperty("AccountID"))
							source.setId(response.getProperty("AccountID").toString());
							if(response.hasProperty("AccountFullName"))
							source.setName(response.getProperty("AccountFullName").toString());
							if(response.hasProperty("ShopName"))
							source.setShopName(response.getProperty("ShopName").toString());
							if(response.hasProperty("Location"))
							source.setLocation(response.getProperty("Location").toString());
							if(response.hasProperty("MobileNumber"))
							source.setMobile(response.getProperty("MobileNumber").toString());
							if(response.hasProperty("Email"))
								source.setEmail((response.getProperty("Email")== null) ? "" : response.getProperty("Email").toString());	
							if(response.hasProperty("NearestLandmark"))
							source.setLandMark(response.getProperty("NearestLandmark").toString());
							if(response.hasProperty("CommercialRegistrationNumber"))
							source.setCRNumber(response.getProperty("CommercialRegistrationNumber").toString());
							if(response.getProperty("CreatedDate")!=null)
							source.setDate(response.getProperty("CreatedDate").toString());
							if(response.hasProperty("Locked"))
							source.setLocked(response.getProperty("Locked").toString());
							if(response.hasProperty("Disabled"))
							source.setDisabled(response.getProperty("Disabled").toString());
							if(response.hasProperty("COMMISSION"))
							source.setCommission(response.getProperty("COMMISSION").toString());
							if(response.hasProperty("Lang"))
							source.setLang(response.getProperty("Lang").toString());
							if(response.hasProperty("AccountType"))
								source.setType(response.getProperty("AccountType").toString());
							source.setBundleSales(Boolean.parseBoolean("True"));
							if(response.hasProperty("AccountBundelSales"))
								source.setBundleSales(Boolean.parseBoolean(response.getProperty("AccountBundelSales").toString()));
							if(response.hasProperty("ErrorDesc")){
								if(!response.getProperty("ErrorDesc").toString().equals("anyType{}"))
							       source.setError(response.getProperty("ErrorDesc").toString());}
							list.add(source);
							}
				catch (Exception ex)
				{
					Log.w(C.TAG, ex.getCause());
					SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
					iRes=-1;
				}
				
				return iRes;}
			
		 
		 
		 //////////////////////////////////////////////////////////////
		 public static int AddAppVersion(Context context,String iUserId,String strVersion)
			{
			    int iRes=-1;
				try
				{
					SoapObject obj = createSoapObj(strMethodAddAppVersion);
					obj.addProperty("UserID",iUserId);
					obj.addProperty("AppVersion", strVersion);
					obj.addProperty("WSUserName",C.WSUserName);
					obj.addProperty("WSPassword",C.WSPassword);
					SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);		
					if(env.getResponse()!=null){
						if(!env.getResponse().equals(""))
					iRes = Integer.parseInt(env.getResponse().toString());}
					
				}
				catch (Exception ex)
					{
					Log.w("Error", ex.getCause());
					SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
	
			}

			return iRes;
			}
		 
		 
		 //////////////////////////////////////////////////////////////
		 public static String LocationInformation(Context context,String strUserID,String strCountry, String strCountryCode,
				 String strCity,String strArea,String strStreetAddress,String strLongitude,String strLatitude)
			{
			    String strResutl="";
				try
				{
					SoapObject obj = createSoapObj(strMethodLocation);
					obj.addProperty("UserID", strUserID);
					obj.addProperty("Country", strCountry);
					obj.addProperty("CountryCode",strCountryCode);
					obj.addProperty("City",strCity);
					obj.addProperty("Governor",strArea);
					obj.addProperty("StreetAddress",strStreetAddress);
					obj.addProperty("Longitude",strLongitude);
					obj.addProperty("Latitude",strLatitude);
					obj.addProperty("WSUserName",C.WSUserName);
					obj.addProperty("WSPassword",C.WSPassword);
					SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);		
					SoapPrimitive response = (SoapPrimitive)env.getResponse();
						strResutl=(response.toString());
					
				}
				catch (Exception ex)
					{
					Log.w("Error", ex.getCause());
					SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
					
		        	
			}

			return strResutl;
			}
	
	/*	  //////////////////////////////////////////////////////////////
		 public static int AddErrorLog(String iUserId,String strScreenName,String strException,String strInnerException)
			{
			    int iRes=-1;
				try
				{
					SoapObject obj = createSoapObj(strMethodErrorLog);
					obj.addProperty("UserID",iUserId);
					obj.addProperty("UserType", "A");
					obj.addProperty("ScreenName", strScreenName);
					obj.addProperty("Exception",strException );
					obj.addProperty("InnerException",strInnerException );
					obj.addProperty("About",DeviceINfO());
					obj.addProperty("WSUserName",C.WSUserName);
					obj.addProperty("WSPassword",C.WSPassword);
					SoapSerializationEnvelope env = prepareAndSendEnv2(obj);		
					iRes = Integer.parseInt(env.getResponse().toString());
					
					
				}
				catch (Exception ex)
					{}

			return iRes;
			}
		 
		 */
		  //////////////////////////////////////////////////////////////
		 public static int AddErrorLog2(Context context,String iUserId,String strScreenName,String strStackTrace,String strDeviceInfo,String strDateTime,String strSingle
				 ,String strBandwidth)
			{
			    int iRes=-1;
				try
				{
					SoapObject obj = createSoapObj(strMethodErrorLog2);
					obj.addProperty("UserID",iUserId);
					obj.addProperty("UserType", "A");
					obj.addProperty("ScreenName", strScreenName);
					obj.addProperty("StackTrace", strStackTrace);
					obj.addProperty("DeviceInfo", strDeviceInfo);
					obj.addProperty("DateTime",  strDateTime);
					obj.addProperty("Signal", strSingle);
					obj.addProperty("Bandwidth", strBandwidth);
					obj.addProperty("WSUserName",C.WSUserName);
					obj.addProperty("WSPassword",C.WSPassword);
					SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);		
					SoapPrimitive response = (SoapPrimitive)env.getResponse();
					iRes = Integer.parseInt(response.toString());
					
					
				}
				catch (Exception ex)
					{}

			return iRes;
			}
		 
		 //////////////////////////////////////////////////////////////
		 public static String GetImageFromFile(Activity act,String strFileName){
	 try{
		 InputStream inputStream = new FileInputStream(strFileName);//You can get an inputStream using any IO API
		 byte[] bytes;
		 byte[] buffer = new byte[8192];
		 int bytesRead;
		 ByteArrayOutputStream output = new ByteArrayOutputStream(); 
		     while ((bytesRead = inputStream.read(buffer)) != -1) {
		     output.write(buffer, 0, bytesRead); }
		 bytes = output.toByteArray();
		 String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
		 inputStream.close();	 
		 output.close();
		 return encodedString;}catch(Exception ex){
			 SaveErrorLog(act,Utility.GetUserEntity(act).getId(),act.toString(),getStackTrace(ex),DeviceINfO());
		 }
			return "";}
		 
		 
 //////////////////////////////////////////////////////////////
			 public static String UploadImages(Context context,String iUserId,String strDocumentType,String strFolderName,String strImageName,String strImageData)
				{
				    String strResult="Error Uploading Images";
					try {
						SoapObject obj = createSoapObj(strMethodUploadImages);
						obj.addProperty("UserID",iUserId);
						obj.addProperty("DocumentType", strDocumentType);
						obj.addProperty("FolderName", strFolderName);
						obj.addProperty("ImageName",strImageName);
						obj.addProperty("ImageData",strImageData);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
				        if(env!=null)
						strResult = env.getResponse().toString();
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						strResult=ex.getMessage();}

				return strResult;
				}
		 
			 
			 //////////////////////////////////////////////////////////////
			 public static String InsertSIMTransaction(Context context,String iUserId,String strSIMMSISDN,String strCustomerName,
					 String strCustomerGender,String strCustomerNationality,String strDocumentType,String strCustomerIDNumber,
					 String strCustomerCountry,String strCustomerAddress,String strCustomerBirthday,String strCustomerExpityDate,String strFolderPath,String strImagePath)
				{
				  String strResult="Error";
					try {
						SoapObject obj = createSoapObj(strMethodInsertSIMTransaction);
						obj.addProperty("AccountID",iUserId);
						obj.addProperty("SIMMSISDN",strSIMMSISDN);
						obj.addProperty("CustomerName",strCustomerName);
						obj.addProperty("CustomerGender",strCustomerGender);
						obj.addProperty("CustomerNationality",strCustomerNationality);
						obj.addProperty("CustomerIDType", strDocumentType);
						obj.addProperty("CustomerIDNumber", strCustomerIDNumber);
						obj.addProperty("CustomerCountry", strCustomerCountry);
						obj.addProperty("CustomerAddress", strCustomerAddress);
						obj.addProperty("CustomerBirthday", strCustomerBirthday);
						obj.addProperty("CustomerIDExpiryDate", strCustomerExpityDate);
						obj.addProperty("ImagePath",strImagePath);	
						obj.addProperty("DocumentType",strDocumentType);
					    obj.addProperty("FolderPath", strFolderPath);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
						SoapObject response = (SoapObject)env.getResponse();			
						 if(response.hasProperty("Status")){
							   if(response.getProperty("Status").toString().equals("Fail")){
								   if(response.hasProperty("ErrorDesc")){
									if(!response.getProperty("ErrorDesc").toString().equals("anyType{}"))
										strResult=response.getProperty("ErrorDesc").toString();}}
							   else    
								strResult=response.getProperty("Status").toString();}
	
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						strResult=ex.getMessage();		
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
				}

				return strResult;
				}
			 
			 //////////////////////////////////////////////////////////////
			 public static String InsertSIMTransaction(Context context,String iUserId,String strDocumentType,String strICCID,String strFolderPath,String strImagePath)
				{
				    String strResult="Error";
					try {
						SoapObject obj = createSoapObj(strMethodInsertSIMTransaction);
						obj.addProperty("AccountID",iUserId);
						obj.addProperty("CustomerIDType", strDocumentType);
						obj.addProperty("DocumentType",strDocumentType);
						obj.addProperty("ImagePath",strImagePath);	
						obj.addProperty("SIMMSISDN",strICCID);
						obj.addProperty("FolderPath", strFolderPath);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
						SoapObject response = (SoapObject)env.getResponse();	
						
						 if(response.hasProperty("Status")){
							   if(response.getProperty("Status").toString().equals("Fail")){
								   if(response.hasProperty("ErrorDesc")){
									if(!response.getProperty("ErrorDesc").toString().equals("anyType{}"))
										strResult=response.getProperty("ErrorDesc").toString();}}
							   else    
								strResult=response.getProperty("Status").toString();}
						 
					
	
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
			        	
				}

				return strResult;
				}
			 
			 /////////////////////////////////////////////////////
			 public static int GetSimTransaction (Context context,String strTransactionID,String strAccountID,String strIDType,String strIDNumber,
					 String strSIMMSISDN,String strFromDate,String strToDate,String strVarificationStatus,String strActivationStatus,
					 String strDocumentType, ArrayList<SIMTransaction_Entity> list)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetSIMTransaction);	
						obj.addProperty("TransactionID", strTransactionID);
						obj.addProperty("AccountID", strAccountID);
						obj.addProperty("IDType", strIDType);
						obj.addProperty("IDNumber", strIDNumber);
						obj.addProperty("SIMMSISDN", strSIMMSISDN);
						obj.addProperty("FromDate", strFromDate);
						obj.addProperty("ToDate", strToDate);
						obj.addProperty("VerificationStatus", strVarificationStatus);
						obj.addProperty("ActivationStatus", strActivationStatus);
						obj.addProperty("DocumentType", strDocumentType);
						obj.addProperty("VerificationUserID", "-1");
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    SIMTransaction_Entity source;
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new SIMTransaction_Entity();
						if(response.hasProperty("ErrorDesc")){
								if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
									source.setError(response.getProperty("ErrorDesc").toString());
								    list.add(source);}}
						
						if(response.hasProperty("SIMInfo")){
							  sobj1 = (SoapObject)response.getProperty("SIMInfo");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source= new SIMTransaction_Entity();
										  
											if(sobj2.hasProperty("Transaction_ID")){
												if(sobj2.getProperty("Transaction_ID").toString().equals("anyType{}"))
													source.setTransactionID("");
												else
												source.setTransactionID(sobj2.getProperty("Transaction_ID").toString());}
											
											if(sobj2.hasProperty("Account_ID")){
												if(sobj2.getProperty("Account_ID").toString().equals("anyType{}"))
													source.setAccountID("");
												else
												source.setAccountID(sobj2.getProperty("Account_ID").toString());}
											
											if(sobj2.hasProperty("Accounts_FullName")){
												if(sobj2.getProperty("Accounts_FullName").toString().equals("anyType{}"))
													source.setAccountFullName("");
												else
												source.setAccountFullName(sobj2.getProperty("Accounts_FullName").toString());}
											

											if(sobj2.hasProperty("SIMMSISDN")){
												if(sobj2.getProperty("SIMMSISDN").toString().equals("anyType{}"))
													source.setSim("");
												else
												source.setSim(sobj2.getProperty("SIMMSISDN").toString());}
											
											if(sobj2.hasProperty("Customer_Name")){
												if(sobj2.getProperty("Customer_Name").toString().equals("anyType{}"))
													source.setName("");
												else
												source.setName(sobj2.getProperty("Customer_Name").toString());}
											

											if(sobj2.hasProperty("Customer_Gender")){
												if(sobj2.getProperty("Customer_Gender").toString().equals("anyType{}"))
													source.setGender("");
												else
												source.setGender(sobj2.getProperty("Customer_Gender").toString());}
											
										

											if(sobj2.hasProperty("Customer_Nationality")){
												if(sobj2.getProperty("Customer_Nationality").toString().equals("anyType{}"))
													source.setNationality("");
												else
												source.setNationality(sobj2.getProperty("Customer_Nationality").toString());}
											
											if(sobj2.hasProperty("Customer_IDType")){
												if(sobj2.getProperty("Customer_IDType").toString().equals("anyType{}"))
													source.setType("");
												else
												source.setType(sobj2.getProperty("Customer_IDType").toString());}
											
											if(sobj2.hasProperty("Customer_IDNumber")){
												if(sobj2.getProperty("Customer_IDNumber").toString().equals("anyType{}"))
													source.setIDNumber("");
												else
												source.setIDNumber(sobj2.getProperty("Customer_IDNumber").toString());}
										
											
											
												
												
											if(sobj2.hasProperty("Customer_Country")){
												if(sobj2.getProperty("Customer_Country").toString().equals("anyType{}"))
													source.setCountry("");
												else
												source.setCountry(sobj2.getProperty("Customer_Country").toString());}
											
											if(sobj2.hasProperty("Customer_Address")){
												if(sobj2.getProperty("Customer_Address").toString().equals("anyType{}"))
												source.setAddress("");
												else
												source.setAddress(sobj2.getProperty("Customer_Address").toString());}
											

											if(sobj2.hasProperty("Customer_Birthday")){
												if(sobj2.getProperty("Customer_Birthday").toString().equals("anyType{}"))
													source.setBirthDay("");
												else
												source.setBirthDay(sobj2.getProperty("Customer_Birthday").toString());}
											
											
											if(sobj2.hasProperty("Commission_Amount")){
												if(sobj2.getProperty("Commission_Amount").toString().equals("anyType{}"))
													source.setAmount("");
												else
												source.setAmount(sobj2.getProperty("Commission_Amount").toString());}
										
											
											if(sobj2.hasProperty("Transaction_DateTime")){
												if(sobj2.getProperty("Transaction_DateTime").toString().equals("anyType{}"))
													source.setDateTime("");
												else
												source.setDateTime(sobj2.getProperty("Transaction_DateTime").toString());}
										
											if(sobj2.hasProperty("UserID")){
												if(sobj2.getProperty("UserID").toString().equals("anyType{}"))
													source.setUserID("");
												else
												source.setUserID(sobj2.getProperty("UserID").toString());}
										
											
											if(sobj2.hasProperty("Users_FullName")){
												if(sobj2.getProperty("Users_FullName").toString().equals("anyType{}"))
													source.setUserFullName("");
												else
												source.setUserFullName(sobj2.getProperty("Users_FullName").toString());}
										
											
											if(sobj2.hasProperty("VerificationDateTime")){
												if(sobj2.getProperty("VerificationDateTime").toString().equals("anyType{}"))
													source.setVarification_DateTime("");
												else
												source.setVarification_DateTime(sobj2.getProperty("VerificationDateTime").toString());}
										

											if(sobj2.hasProperty("VerificationStatus")){
												if(sobj2.getProperty("VerificationStatus").toString().equals("anyType{}"))
													source.setVarification_Status("");
												else
												source.setVarification_Status(sobj2.getProperty("VerificationStatus").toString());}
										
	
											if(sobj2.hasProperty("VerificationRejectedReason")){
												if(sobj2.getProperty("VerificationRejectedReason").toString().equals("anyType{}"))
													source.setVarification_RejectedReason("");
												else
												source.setVarification_RejectedReason(sobj2.getProperty("VerificationRejectedReason").toString());}
										
	
											
											if(sobj2.hasProperty("ActivationStatus")){
												if(sobj2.getProperty("ActivationStatus").toString().equals("anyType{}"))
												source.setActivationStatus("");
												else
												source.setActivationStatus(sobj2.getProperty("ActivationStatus").toString());}
											
											if(sobj2.hasProperty("TABSError"))
												source.setTab_Error(sobj2.getProperty("TABSError").toString());
											
											if(sobj2.hasProperty("ContractNo")){
											if(sobj2.getProperty("ContractNo").toString().equals("anyType{}"))
												source.setContractNo("");
												else
												source.setContractNo(sobj2.getProperty("ContractNo").toString());}
										
											
											if(sobj2.hasProperty("TABSDateTime")){
												if(sobj2.getProperty("TABSDateTime").toString().equals("anyType{}"))
												source.setTabs_DateTime("");
												else
												source.setTabs_DateTime(sobj2.getProperty("TABSDateTime").toString());}
											
											
											if(sobj2.hasProperty("ImagePath"))
												source.setImagePath(sobj2.getProperty("ImagePath").toString());
											
											if(sobj2.hasProperty("DocumentType"))
												source.setDocument_Type(sobj2.getProperty("DocumentType").toString());
											
											if(sobj2.hasProperty("FolderPath"))
												source.setFolder_Path(sobj2.getProperty("FolderPath").toString());
											
											list.add(source);	}}}}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						iRes=-1;
					}
					
					return iRes;}
			 
			 
			 /////////////////////////////////////////////////////
			 public static int GetCountries (Context context,ArrayList<Countries_Entity> list,String strLastModificationDate)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetCountries);	
						obj.addProperty("ModificationDate",strLastModificationDate);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    Countries_Entity source;
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new Countries_Entity();
						if(response.hasProperty("ErrorDesc")){
							if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
								source.setError(response.getProperty("ErrorDesc").toString());
							    list.add(source);}}
						if(response.hasProperty("L_Entity")){
							  sobj1 = (SoapObject)response.getProperty("L_Entity");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source= new Countries_Entity();
										  if(sobj2.hasProperty("ID"))	  
											source.setID(sobj2.getProperty("ID").toString());
											if(sobj2.hasProperty("NameEng"))
											source.setNameEng(sobj2.getProperty("NameEng").toString());
											if(sobj2.hasProperty("NameAra"))
											source.setAction(sobj2.getProperty("NameAra").toString());	
											
											list.add(source);	}}}}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());	
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;}
			 
			 
			 /////////////////////////////////////////////////////
			 public static int GetCountriesWithDate (Context context,String strUserID,ArrayList<Countries_Entity> list,String strLastModificationDate)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetCountriesWithDate);	
						obj.addProperty("UserID",strUserID);
						obj.addProperty("ModDate",strLastModificationDate);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    Countries_Entity source;
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new Countries_Entity();
						if(response.hasProperty("ErrorDesc")){
							if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
								source.setError(response.getProperty("ErrorDesc").toString());
							    list.add(source);}}
						if(response.hasProperty("L_Entity")){
							  sobj1 = (SoapObject)response.getProperty("L_Entity");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source= new Countries_Entity();
										  if(sobj2.hasProperty("ID"))	  
											source.setID(sobj2.getProperty("ID").toString());
											if(sobj2.hasProperty("NameEng"))
											source.setNameEng(sobj2.getProperty("NameEng").toString());
											if(sobj2.hasProperty("NameAra"))
											source.setAction(sobj2.getProperty("NameAra").toString());	
											
											list.add(source);	}}}}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());	
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;}
			 
			 /////////////////////////////////////////////////////
			 public static int GetSimCommision (Context context,String strTransactionID,String strAccountID,String strIDType,String strIDNumber,
					 String strSIMMSISDN,String strFromDate,String strToDate, ArrayList<SIMCommission_Entity> list)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetSIMCommission);	
						obj.addProperty("TransactionID", strTransactionID);
						obj.addProperty("AccountID", strAccountID);
						obj.addProperty("IDType", strIDType);
						obj.addProperty("IDNumber", strIDNumber);
						obj.addProperty("SIMMSISDN", strSIMMSISDN);
						obj.addProperty("FromDate", strFromDate);
						obj.addProperty("ToDate", strToDate);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    SIMCommission_Entity source;
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new SIMCommission_Entity();
						if(response.hasProperty("ErrorDesc")){
								if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
									source.setError(response.getProperty("ErrorDesc").toString());
								    list.add(source);}}
						
						if(response.hasProperty("L_SIMCommissionDetails")){
							  sobj1 = (SoapObject)response.getProperty("L_SIMCommissionDetails");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source= new SIMCommission_Entity();
										  if(sobj2.hasProperty("MSISDN"))
											source.setIDType(sobj2.getProperty("MSISDN").toString());
											
										  if(sobj2.hasProperty("CommAmt"))
											source.setCommission(sobj2.getProperty("CommAmt").toString());
											
										  if(sobj2.hasProperty("CommType")){
												if(!sobj2.getProperty("CommType").toString().equals("anyType{}")){
													source.setCommType(sobj2.getProperty("CommType").toString());
												   }}
										  list.add(source);	
											}}}}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						iRes=-1;
					}
					
					return iRes;}	
			 
			 /////////////////////////////////////////////////////
			 public static int Register (Context context,String strFullName,String strShopName,String strLocation,String strMobileNumber,
					 String strEmail,String strLandMark,String strCRNumber,String strCPRNumber,String strSMSNumber,String strLanguage,String strCRImage,String strCPRImage,String strIMEI,Register_Entity register)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodRegister);	
						obj.addProperty("FullName", strFullName);
						obj.addProperty("ShopName", strShopName);
						obj.addProperty("Location", strLocation);
						obj.addProperty("MobileNumber", strMobileNumber);
						obj.addProperty("Email", strEmail);
						obj.addProperty("NearestLandmark", strLandMark);
						obj.addProperty("CRNumber", strCRNumber);
						obj.addProperty("CPRNumber", strCPRNumber);
						obj.addProperty("SMSNumber", strSMSNumber);
						obj.addProperty("Lang", strLanguage);
						obj.addProperty("CRImage", strCRImage);
						obj.addProperty("CPRImage", strCPRImage);
						obj.addProperty("IMEI", strIMEI);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
										  if(response.hasProperty("Status"))
											  register.setStatus(response.getProperty("Status").toString());
											
										  if(response.hasProperty("Description"))
											  register.setDescription(response.getProperty("Description").toString());
										
									}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						 iRes=-1;
					}
					
					return iRes;}	
			 
			 /////////////////////////////////////////////////////
			 public static int Register2 (Context context,String strFullName,String strShopName,String strLocation,String strMobileNumber,
					 String strEmail,String strLandMark,String strCRNumber,String strCPRNumber,String strSMSNumber,String strLanguage,String strCRImage,String strCPRImage,String strIMEI,Register_Entity register
					 ,String strWSUserName ,String strUSSD)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodRegister2);	
						obj.addProperty("FullName", strFullName);
						obj.addProperty("ShopName", strShopName);
						obj.addProperty("Location", strLocation);
						obj.addProperty("MobileNumber", strMobileNumber);
						obj.addProperty("Email", strEmail);
						obj.addProperty("NearestLandmark", strLandMark);
						obj.addProperty("CRNumber", strCRNumber);
						obj.addProperty("CPRNumber", strCPRNumber);
						obj.addProperty("SMSNumber", strSMSNumber);
						obj.addProperty("Lang", strLanguage);
						obj.addProperty("CRImage", strCRImage);
						obj.addProperty("CPRImage", strCPRImage);
						obj.addProperty("IMEI", strIMEI);
						obj.addProperty("USSDNo", strUSSD);
						obj.addProperty("WebUserName", strWSUserName);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
										  if(response.hasProperty("Status"))
											  register.setStatus(response.getProperty("Status").toString());
											
										  if(response.hasProperty("Description"))
											  register.setDescription(response.getProperty("Description").toString());
										
									}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;}	
			 //////////////////////////////////////////////////////////////
			 public static String ForgotPassword(Context context,String CPRNumber,String CRNumber,String strDeviceID)
				{
				    String strResul="";
					try
					{
						SoapObject obj = createSoapObj(strMethodForgetPassword);
						obj.addProperty("CPRNumber",CPRNumber);
						obj.addProperty("CRNumber", CRNumber);
						obj.addProperty("Token", strDeviceID);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);		
						strResul =env.getResponse().toString();
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						   
						
				}

				return strResul;
				}
			 
			 //////////////////////////////////////////////////////////////
			 public static String ChangePassword(Context context,String UserID,String CurrentPassword,String NewPassword)
				{
				    String strResul="";
					try
					{
						SoapObject obj = createSoapObj(strMethodChangePassword);
						obj.addProperty("AccountID",UserID);
						obj.addProperty("CurrentPassword", CurrentPassword);
						obj.addProperty("NewPassword", NewPassword);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);		
						strResul =env.getResponse().toString();
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						   
						
				}

				return strResul;
				}
			 
			 //////////////////////////////////////////////////////////////
			 public static String ChangeComission(Context context,String UserID,String strNewComission,String CurrentPassword)
				{
				    String strResul="";
					try
					{
						SoapObject obj = createSoapObj(strMethodChangeComission);
						obj.addProperty("AccountID",UserID);
						obj.addProperty("CurrentPassword", CurrentPassword);
						obj.addProperty("CommissionMobileNo", strNewComission);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						strResul =env.getResponse().toString();
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						}

				return strResul;
				}
			 
			 
			 
			 //////////////////////////////////////////////////////////////
			 public static String AuditTrail(Context context,String UserID,String strScreen,String strActionID
					 ,String strNotes)
				{
				    String strResul="";
					try
					{
						SoapObject obj = createSoapObj(strMethodAuditTrail);
						obj.addProperty("UserID",UserID);
						obj.addProperty("UserType","A");
						obj.addProperty("ScreenName", strScreen);
						obj.addProperty("ActionID", strActionID);
						obj.addProperty("Notes", strNotes);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						strResul =env.getResponse().toString();
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						}

				return strResul;
				}
			 	
		 
			 public static String DeviceINfO(){
				 String strAbout="";
				 strAbout=strAbout+" OS version: "+System.getProperty("os.version"); // OS version
				 strAbout=strAbout+" OS name: "+System.getProperty("os.name"); // OS name
				 strAbout=strAbout+"/ API Level: "+android.os.Build.VERSION.SDK;      // API Level
				 strAbout=strAbout+"/ Device: "+android.os.Build.DEVICE;         // Device
				 strAbout=strAbout+"/ Model: "+android.os.Build.MODEL;           // Model 
				 return strAbout=strAbout+"/ Product: "+android.os.Build.PRODUCT;  
				 
			 }
			 
			 public static long SaveErrorLog(Context context,String strUserID,String strClassName,String strStackTrace,String strDeviceInfo){
				  
				 SimpleDateFormat dateFormat= new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				  String strDateTime=dateFormat.format(new Date());
				  long id=0;
				  dbHelper = new DatabaseHelper(context);
				  db = dbHelper.getWritableDatabase();
				  updater = new ContentUpdater(context, db);
				  id=updater.saveContentsErrorLog(strUserID, strClassName, strStackTrace, strDeviceInfo,strDateTime);
				  ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                  //For 3G check
				  boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				            .isConnectedOrConnecting();	
				  if(is3g) {
					  updater.saveContentsSignal(Integer.toString( C.myListener.signalStrengthValue), Long.toString(id));}
				else{  
				  WifiReceiver receiverWifi = new WifiReceiver(id);
				  receiverWifi.mainWifi = (WifiManager)  context.getSystemService(Context.WIFI_SERVICE);
				     // Check for wifi is disabled
				     if (receiverWifi.mainWifi.isWifiEnabled() == false)
				            {  receiverWifi.mainWifi.setWifiEnabled(true); } 
				        
				      // Register broadcast receiver 
				       // Broacast receiver will automatically call when number of wifi connections changed
				   context.getApplicationContext().registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
				   receiverWifi.mainWifi.startScan();}
				   new DownloadImage().execute(new String[]{Long.toString(id)});
				
				return id;
				 
			 }
			 
			 public static long SaveErrorLogNoBandwidth(Context context,String strUserID,String strClassName,String strStackTrace,String strDeviceInfo){
				  
				 SimpleDateFormat dateFormat= new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				  String strDateTime=dateFormat.format(new Date());
				  long id=0;
				  dbHelper = new DatabaseHelper(context);
				  db = dbHelper.getWritableDatabase();
				  updater = new ContentUpdater(context, db);
				  id=updater.saveContentsErrorLog(strUserID, strClassName, strStackTrace, strDeviceInfo,strDateTime);
				  ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                  //For 3G check
				  boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				            .isConnectedOrConnecting();	
				  if(is3g) {
					  updater.saveContentsSignal(Integer.toString( C.myListener.signalStrengthValue), Long.toString(id));}
				else{  
				  WifiReceiver receiverWifi = new WifiReceiver(id);
				  receiverWifi.mainWifi = (WifiManager)  context.getSystemService(Context.WIFI_SERVICE);
				     // Check for wifi is disabled
				     if (receiverWifi.mainWifi.isWifiEnabled() == false)
				            {  receiverWifi.mainWifi.setWifiEnabled(true); } 
				        
				      // Register broadcast receiver 
				       // Broacast receiver will automatically call when number of wifi connections changed
				   context.getApplicationContext().registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
				   receiverWifi.mainWifi.startScan();}
				 return id;
				 
			 }
		 
			 public static String getStackTrace(Exception ex){
				 String s="";
				 try{
				 Writer writer = new StringWriter();
				 PrintWriter printWriter = new PrintWriter(writer);
				 ex.printStackTrace(printWriter);
				  s = writer.toString();}catch(Exception e){}
				return s;
			 }
			 
			
			 
			 private static class DownloadImage extends AsyncTask<String, Void, String> {
				  private String mURL = "http://168.187.85.218/Image(1MB).png";
				  private String id;
				    @Override
				    protected void onPreExecute() { }

				    @Override
				    protected String doInBackground(String... par) {
				    	id=par[0];
				    	try{
				        long startTime = System.currentTimeMillis();
				    	HttpGet httpRequest = new HttpGet(new URL(mURL).toURI());
				    	HttpClient httpClient = new DefaultHttpClient();
				    	HttpResponse response = (HttpResponse) httpClient.execute(httpRequest);
				    	long endTime = System.currentTimeMillis();

				    	HttpEntity entity = response.getEntity();
				    	BufferedHttpEntity bufHttpEntity;
				    	bufHttpEntity = new BufferedHttpEntity(entity);

				    	//You can re-check the size of your file
				    	final long contentLength = bufHttpEntity.getContentLength();

				    	// Log
				    	Log.d(C.TAG, "[BENCHMARK] Dowload time :"+(endTime-startTime)+" ms");

				    	// Bandwidth : size(KB)/time(s)
				    	float bandwidth = contentLength / ((endTime-startTime));
				    	return Integer.toString((int)bandwidth);}
				    	catch(Exception ex){}
				   
				      return "0";
				    }

				    @Override
				    protected void onPostExecute(String v) {
				    	updater.saveContentsErrorLogBandwidth(id, v);
				     }}
			 
			 
			 /////////////////////////////////////////////////////
			 public static int GetBundlesSales (Context context,String strUserID,String strSIMMSISDN,ArrayList<Bundle_Entity> list)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetBundlesSales);	
						obj.addProperty("UserID",strUserID);
						obj.addProperty("SIMMSISDN",strSIMMSISDN);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    Bundle_Entity source;
					    Bundles_Entity source2;
					    ArrayList<Bundles_Entity> list2=new  ArrayList<Bundles_Entity> ();
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new Bundle_Entity();
						if(response.hasProperty("ErrorDesc")){
							if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
								source.setError(response.getProperty("ErrorDesc").toString());
							    list.add(source);}}
						if(response.hasProperty("Bundle")){
							  sobj1 = (SoapObject)response.getProperty("Bundle");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source2= new Bundles_Entity();
										  if(sobj2.hasProperty("Package_ID"))	  
											source2.setPackageID(sobj2.getProperty("Package_ID").toString());
											if(sobj2.hasProperty("Bundle_ID"))
											source2.setID(sobj2.getProperty("Bundle_ID").toString());
											
											if(sobj2.hasProperty("PkgCode"))
											source2.setPackageCode(sobj2.getProperty("PkgCode").toString());	
											
											if(sobj2.hasProperty("Name"))
												source2.setName(sobj2.getProperty("Name").toString());	
											
											if(sobj2.hasProperty("Type"))
												source2.setType(sobj2.getProperty("Type").toString());	
											
											if(sobj2.hasProperty("Code"))
												source2.setCode(sobj2.getProperty("Code").toString());	
											
											if(sobj2.hasProperty("Command"))
												source2.setCommand(sobj2.getProperty("Command").toString());	
											
											if(sobj2.hasProperty("Action"))
												source2.setAction(sobj2.getProperty("Action").toString());	
											
											if(sobj2.hasProperty("BundlePrice"))
												source2.setPrice(sobj2.getProperty("BundlePrice").toString());	
											
											if(sobj2.hasProperty("BundleValidaty"))
												source2.setValidity(sobj2.getProperty("BundleValidaty").toString());	
											
											source2.setMobile(strSIMMSISDN);
											list2.add(source2);	}}}
						source.setBundles(list2);
						list.add(source);}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());	
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;}
			 
			 
			 //////////////////////////////////////////////////////////////
			 public static int InsertBundleSales(Context context,String iUserId,String strSIMMSISDN,String strPkgCode,String strPackageID,String strBundelID,
					String strType,String strCode,String strCommand,String strAction,String strActivationCode,BundleSalesResponse_Entity entity)
				{
				    int iRes=-1;
					try
					{
						SoapObject obj = createSoapObj(strMethodInsertBundleSales);
						obj.addProperty("UserID",iUserId);
						obj.addProperty("SIMMSISDN",strSIMMSISDN);
						obj.addProperty("PackageCode", strPkgCode);
						obj.addProperty("PackageID", strPackageID);
						obj.addProperty("BundleID", strBundelID);
					    obj.addProperty("BundleType", strType);
						obj.addProperty("BundleCode", strCode);
						obj.addProperty("BundleCommand", strCommand);
						obj.addProperty("BundleAction", strAction);	
						obj.addProperty("CustomerVerificationCode", strActivationCode);	
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						SoapObject response = (SoapObject)env.getResponse();
						iRes = response.getPropertyCount();
				        if(response.hasProperty("Code"))
							entity.setCode(response.getProperty("Code").toString());
						if(response.hasProperty("Message"))
							entity.setMsg(response.getProperty("Message").toString());
						if(response.hasProperty("flex1"))
							entity.setFlex1(response.getProperty("flex1").toString());
						if(response.hasProperty("flex2"))
							entity.setFlex2(response.getProperty("flex2").toString());
						if(response.hasProperty("flex3"))
							entity.setFlex3(response.getProperty("flex3").toString());
						if(response.hasProperty("flex4"))
							entity.setFlex4(response.getProperty("flex4").toString());
						if(response.hasProperty("ErrorDesc"))
							entity.setError(response.getProperty("ErrorDesc").toString());
							
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						   
						
				}

				return iRes;
				}
			 
		
			 //////////////////////////////////////////////////////////////
			 public static int InsertCustomer_CodeBundle(Context context,String UserID,String strBundleID,String strNumber)
				{
				    int iResul=0;
					try
					{
						SoapObject obj = createSoapObj(strMethodInsertCustomer_CodeBundle);
						obj.addProperty("UserID",UserID);
						obj.addProperty("BundleID",strBundleID);
						obj.addProperty("SubscriberNumber",strNumber);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						iResul =Integer.parseInt(env.getResponse().toString());
						
					}
					catch (Exception ex)
						{
						iResul=0;
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						}

				return iResul;
				}
			 	
		 

			 /////////////////////////////////////////////////////
			 public static int GetDenominations(Context context,String strUserID,ArrayList<Denomination_Entity> list,ArrayList<String> list3)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetDenominations);	
						obj.addProperty("UserID",strUserID);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    Denomination_Entity source;
					    Denominations_Entity source2;
					    ArrayList<Denominations_Entity> list2=new  ArrayList<Denominations_Entity> ();
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new Denomination_Entity();
						if(response.hasProperty("ErrorDesc")){
							if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
								source.setError(response.getProperty("ErrorDesc").toString());
							    list.add(source);}}
						if(response.hasProperty("Denominations")){
							  sobj1 = (SoapObject)response.getProperty("Denominations");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source2= new Denominations_Entity();
										  if(sobj2.hasProperty("ServiceID"))	  
											source2.setID(sobj2.getProperty("ServiceID").toString());
											if(sobj2.hasProperty("ServiceNameAr"))
											source2.setNameAR(sobj2.getProperty("ServiceNameAr").toString());
											
											if(sobj2.hasProperty("ServiceNameEn")){
											source2.setName(sobj2.getProperty("ServiceNameEn").toString());
											list3.add(sobj2.getProperty("ServiceNameEn").toString());}
											
											if(sobj2.hasProperty("ServiceAmount"))
												source2.setAmount(sobj2.getProperty("ServiceAmount").toString());	
											
											if(sobj2.hasProperty("ServiceProvider"))
												source2.setProvider(sobj2.getProperty("ServiceProvider").toString());	
										
											list2.add(source2);	}}}
						source.setDenominations(list2);
						list.add(source);}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());	
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;}
			 
			 
			 //////////////////////////////////////////////////////////////
			 public static int InsertVoucher(Context context,String iUserId,String strCustomerNumber,String strDenomination,Result_Entity entity)
				{
				    int iRes=-1;
					try
					{
						SoapObject obj = createSoapObj(strMethodVoucher);
						obj.addProperty("UserID",iUserId);
						obj.addProperty("subNo",strCustomerNumber);
						obj.addProperty("DenoID", strDenomination);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						SoapObject response = (SoapObject)env.getResponse();
						iRes = response.getPropertyCount();
				        if(response.hasProperty("Status"))
							entity.setStatus(response.getProperty("Status").toString());
						if(response.hasProperty("Description"))
							entity.setDescription(response.getProperty("Description").toString());
						if(response.hasProperty("Contract_No"));
						    entity.setContract(response.getProperty("Contract_No").toString());
							
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						   
						
				}

				return iRes;
				}
			 
			 //////////////////////////////////////////////////////////////
			 public static int InsertCreditTransfer(Context context,String iUserId,String strCustomerNumber,String strAmount,Result_Entity entity)
				{
				    int iRes=-1;
					try
					{
						SoapObject obj = createSoapObj(strMethodCreditTransfer);
						obj.addProperty("UserID",iUserId);
						obj.addProperty("subNo",strCustomerNumber);
						obj.addProperty("CreditAmount", strAmount);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						SoapObject response = (SoapObject)env.getResponse();
						iRes = response.getPropertyCount();
				        if(response.hasProperty("Status"))
							entity.setStatus(response.getProperty("Status").toString());
						if(response.hasProperty("Description"))
							entity.setDescription(response.getProperty("Description").toString());
						if(response.hasProperty("Contract_No"));
						   entity.setContract(response.getProperty("Contract_No").toString());
							
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						   
						
				}

				return iRes;
				}
			 /////////////////////////////////////////////////////
			 public static int GetMyStock(Context context,String strUserID,ArrayList<Stock_Entity> list)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetStocks);	
						obj.addProperty("UserID",strUserID);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    Stock_Entity source;
					    Stocks_Entity source2;
					    ArrayList<Stocks_Entity> list2=new  ArrayList<Stocks_Entity> ();
						SoapObject sobj1,sobj2;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						source= new Stock_Entity();
						if(response.hasProperty("ErrorDesc")){
							if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
								source.setError(response.getProperty("ErrorDesc").toString());
							    list.add(source);}}
						if(response.hasProperty("Stock")){
							  sobj1 = (SoapObject)response.getProperty("Stock");
							  iRes = sobj1.getPropertyCount();
								for (int i = 0; i < iRes; i++)
								{
									sobj2 = (SoapObject)sobj1.getProperty(i);
									  if (sobj2 != null)
									  {
										  source2= new Stocks_Entity();
										  if(sobj2.hasProperty("Denomination"))	  
											source2.setDenomination(sobj2.getProperty("Denomination").toString());
											
										  if(sobj2.hasProperty("Qty"))
											source2.setQty(sobj2.getProperty("Qty").toString());
											
										  if(sobj2.hasProperty("Amount"))
												source2.setAmount(sobj2.getProperty("Amount").toString());	
											
											list2.add(source2);	}}}
						source.setStocks(list2);
						list.add(source);}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());	
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;} 
			 
			 
			 //////////////////////////////////////////////////////////////
			 public static int GetMyInfo(Context context,String iUserId,Info_Entity entity)
				{
				    int iRes=-1;
					try
					{
						SoapObject obj = createSoapObj(strMethodGetMyInfo);
						obj.addProperty("UserID",iUserId);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);	
						SoapObject response = (SoapObject)env.getResponse();
						iRes = response.getPropertyCount();
						if(response.hasProperty("ErrorDesc")){
							if(!response.getProperty("ErrorDesc").toString().equals("anyType{}")){
								entity.setError(response.getProperty("ErrorDesc").toString());}}
				        if(response.hasProperty("ShopName"))
							entity.setShopName(response.getProperty("ShopName").toString());
						if(response.hasProperty("FullName"))
							entity.setFullName(response.getProperty("FullName").toString());
						
						if(response.hasProperty("Email"))
							entity.setEmail(response.getProperty("Email").toString());
						
						if(response.hasProperty("CommMobileNo"))
							entity.setComm(response.getProperty("CommMobileNo").toString());
						
						if(response.hasProperty("SMSMobileNo"))
							entity.setSMS(response.getProperty("SMSMobileNo").toString());
						
						if(response.hasProperty("MID"))
							entity.setMerchant(response.getProperty("MID").toString());
						
						if(response.hasProperty("CurrentBalance"))
							entity.setBalance(response.getProperty("CurrentBalance").toString());
							
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
						   
						
				}

				return iRes;
				}
			 
			 /////////////////////////////////////////////////////
			 public static int GetOffers(Context context,String strUserID,ArrayList<Offers_Entity> list)
				{
					int iRes = 0;
					
					try
					{
						SoapObject obj = createSoapObj(strMethodGetOffers);	
						obj.addProperty("UserID",strUserID);
						obj.addProperty("WSUserName",strCredUser);
						obj.addProperty("WSPassword",strCredPass);
					    SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);
					    Offers_Entity source = null;
					    SoapObject sobj1,sobj2,sobj3;
						SoapObject response = (SoapObject)env.getResponse();			
						iRes = response.getPropertyCount();
						for (int i = 0; i < iRes; i++)
						{
							 sobj1 = (SoapObject)response.getProperty(i);
							 if (sobj1 != null)
							  {
								 source = new Offers_Entity();
								 if(sobj1.hasProperty("ErrorDesc"))
									 source.setError(sobj1.getProperty("ErrorDesc").toString());
								 if(sobj1.hasProperty("OfferID"))
									 source.setID(sobj1.getProperty("OfferID").toString());
								 if(sobj1.hasProperty("OfferName"))
									 source.setName(sobj1.getProperty("OfferName").toString());
								 if(sobj1.hasProperty("OfferDescription"))
									 source.setDescription(sobj1.getProperty("OfferDescription").toString());
								 if(sobj1.hasProperty("count"))
									 source.setCount(sobj1.getProperty("count").toString());
								 ArrayList<String> Images = new  ArrayList<String>();
								 if(sobj1.hasProperty("_LOffers")){
									  sobj2 = (SoapObject)sobj1.getProperty("_LOffers");
								      int count = sobj2.getPropertyCount();
										for (int j = 0; j < count; j++){
											sobj3 = (SoapObject)sobj2.getProperty(j);
											  if (sobj3 != null)
											  {
												  if(sobj3.hasProperty("OfferURL")){
													  Images.add(sobj3.getProperty("OfferURL").toString());
													  if(j==0)
														  source.setImage(sobj3.getProperty("OfferURL").toString());
														  } }}
										source.setImages(Images);
										}}
							 list.add(source);}
						}
					catch (Exception ex)
					{
						Log.w(C.TAG, ex.getCause());	
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						iRes=-1;
					}
					
					return iRes;} 
			 
			 //////////////////////////////////////////////////////////////
			 public static int GetAgeSetting(Context context,String strUserID)
				{
				  int iResult=-1;
					try
					{
						SoapObject obj = createSoapObj(strMethodGetAgeSetting);
						obj.addProperty("UserID", strUserID);
						obj.addProperty("WSUserName",C.WSUserName);
						obj.addProperty("WSPassword",C.WSPassword);
						SoapSerializationEnvelope env = prepareAndSendEnv(context,obj);		
						SoapPrimitive response = (SoapPrimitive)env.getResponse();
						iResult=(Integer.parseInt(response.toString()));
						
					}
					catch (Exception ex)
						{
						Log.w("Error", ex.getCause());
						SaveErrorLog(context,Utility.GetUserEntity(context).getId(),context.toString(),getStackTrace(ex),DeviceINfO());
						
			        	
				}

				return iResult;
				}
}
	
