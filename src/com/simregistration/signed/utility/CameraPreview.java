package com.simregistration.signed.utility;

import java.io.ByteArrayOutputStream;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class CameraPreview extends SurfaceView implements
   SurfaceHolder.Callback {
	SurfaceHolder holder;
	public Camera camera;
	private int lastReportedWidth;
	private int lastReportedHeight;

	private static final String TAG = "CameraPreviewLog";
	private CameraPreviewProvider previewProvider;

	public CameraPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		Log.d(TAG, "Constructing CameraPreview");

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		holder = getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			previewProvider = new CameraPreviewGingerbread();
		} else {
			previewProvider = new CameraPreviewFroyo();
		}
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "SurfaceCreated");
		preparePreview();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		Log.d(TAG, "SurfaceDestroyed");
		stopPreview();
	}

	private void preparePreview() {
		Log.d(TAG, "preparePreview");
		// The Surface has been created, acquire the camera and tell it where
		// to draw.
		try {
			camera = previewProvider.openCamera();
			camera.setPreviewDisplay(holder);
		} catch (Exception exception) {
			if (camera != null) {
				camera.release();
			}
			camera = null;
		}
	}

	private void stopPreview() {
		Log.d(TAG, "stopPreview");
		if (camera != null) {
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		Log.d(TAG, "SurfaceChanged");
		this.lastReportedWidth = w;
		this.lastReportedHeight = h;
		if (camera != null) {
			startPreview();
		}
	}

	private void startPreview() {
		Log.d(TAG, "startPreview");
		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		Display display = ((WindowManager) getContext().getSystemService(
				Context.WINDOW_SERVICE)).getDefaultDisplay();
		previewProvider.startPreview(camera, lastReportedWidth,
				lastReportedHeight, display);
	}
	
	public Parameters getCameraParameters(){
		return camera.getParameters();
	}
	
	public void setFlash(boolean flash){
		Camera.Parameters parameters = camera.getParameters();
		if (flash){
			
			parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
			camera.setParameters(parameters);
		}
		else{
			parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
			camera.setParameters(parameters);
		}
	}
	
	public void setCameraFocus(AutoFocusCallback autoFocus){
		if (camera.getParameters().getFocusMode().equals(camera.getParameters().FOCUS_MODE_AUTO) ||
		        camera.getParameters().getFocusMode().equals(camera.getParameters().FOCUS_MODE_MACRO)){
		    camera.autoFocus(autoFocus);}
		
	}
	
	public Bitmap onPreviewFrame ( int x, int y, int width, int height, byte[] data, Camera arg1) {
		   Parameters mParameters= camera.getParameters();
		   int w = mParameters.getPreviewSize().width;
		   int h = mParameters.getPreviewSize().height;
		   int format = mParameters.getPreviewFormat();
		   YuvImage image = new YuvImage(data, format, w, h, null);

		   ByteArrayOutputStream out = new ByteArrayOutputStream();
		   Rect area = new Rect(x, y, width, height);
		   image.compressToJpeg(area, 100, out);
		   Bitmap bm = BitmapFactory.decodeByteArray(out.toByteArray(), 0, out.size());
	
			return bm;
			}

}
