package com.simregistration.signed.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.simregistration.signed.Login;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;


public class BitmapUtils {


    public static Bitmap getBitmap(InputStream stream, int scaleFactor) {
        BitmapFactory.Options factoryOptions = new BitmapFactory.Options();
        factoryOptions.inJustDecodeBounds = false;
        factoryOptions.inSampleSize = scaleFactor;
        Bitmap bitmap = BitmapFactory.decodeStream(stream, new Rect(-1, -1, -1, -1), factoryOptions);

        return bitmap;
    }

    public static Bitmap getBitmap(Context context, Uri uri, int scaleFactor) {
        InputStream stream = null;
        Bitmap bitmap = null;
        try {
            stream = context.getContentResolver().openInputStream(uri);
            bitmap = getBitmap(stream, scaleFactor);
        } catch (Exception ex) {
        	WebService.SaveErrorLogNoBandwidth(context,Utility.GetUserEntity(context).getId(),context.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());
        	/* if(Utility.isConnected(context))
				 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),ex.getMessage(),ex.getCause().toString()});
    	   	*/

        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception ex) {
            	WebService.SaveErrorLogNoBandwidth(context,Utility.GetUserEntity(context).getId(),context.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());
            	
            	//if(Utility.isConnected(context))
   				// new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(context).getId(),context.toString(),ex.getMessage(),ex.getCause().toString()});
       	   	
            }
        }
        return bitmap;
    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static InputStream getStream(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        return inputStream;
    }

    public static float exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static float getImageRotation(Context context, boolean isCamera, Uri imageUri) {
        float angle = 0f;
        if (isCamera) {
            ExifInterface exif;
            try {
                exif = new ExifInterface(imageUri.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                angle = exifOrientationToDegrees(orientation);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Cursor cursor = context.getContentResolver().query(imageUri, new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
            if (cursor.getCount() == 1) {
                cursor.moveToFirst();
                angle = cursor.getInt(0);
            }
            cursor.close();
        }

        return angle;
    }
}
