package com.simregistration.signed.utility;

import java.util.List;

import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.Display;

class CameraPreviewFroyo implements CameraPreviewProvider {

	public Camera openCamera() {
		return Camera.open();
	}
	
	public void startPreview(Camera camera, int previewWidth, int previewHeight, Display display) {
		Camera.Parameters parameters = camera.getParameters();
		List<Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
		Size optimalPreviewSize = CameraPreviewFroyo.getOptimalPreviewSize(supportedPreviewSizes, previewWidth, previewHeight);
		Log.d("CameraPreviewLog", "Optimal Size:" + optimalPreviewSize.width + ", " + optimalPreviewSize.height);
		if (optimalPreviewSize != null) {
			parameters.setPreviewSize(optimalPreviewSize.width, optimalPreviewSize.height);
			camera.setParameters(parameters);
			camera.startPreview();
		}
	}

	static Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		
		
		Size size1 = null;
		for (int i=0;i<sizes.size();i++){
			if(i==0)
				size1=sizes.get(i);
			else {
				if((sizes.get(i).height>size1.height) &&  (sizes.get(i).width>size1.width)) 
					size1=sizes.get(i);
			}}
			
		/* Camera.Size result=null;    
	       // Camera.Parameters p = camera.getParameters();
	        for (Camera.Size size : sizes) {
	            if (size.width<=w && size.height<=h) {
	                if (result==null) {
	                    result=size;
	                } else {
	                    int resultArea=result.width*result.height;
	                    int newArea=size.width*size.height;

	                    if (newArea>resultArea) {
	                        result=size;
	                    }
	                }
	            }
	        }*/
	    return size1;


	    
	    
		
		
	    /*final double ASPECT_TOLERANCE = 0.1;
	    final double MAX_DOWNSIZE = 1.5;
	    
	    double targetRatio = (double) w / h;
	    if (sizes == null) return null;
	
	    Size optimalSize = null;
	    double minDiff = Double.MAX_VALUE;
	
	    int targetHeight = h;
	
	    // Try to find an size match aspect ratio and size
	    for (Size size : sizes) {
	        double ratio = (double) size.width / size.height;
	        double downsize = (double) size.width / w;
	        if (downsize > MAX_DOWNSIZE) {
	        	//if the preview is a lot larger than our display surface ignore it
	        	//reason - on some phones there is not enough heap available to show the larger preview sizes 
	        	continue;
	        }
	        if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
	        if (Math.abs(size.height - targetHeight) < minDiff) {
	            optimalSize = size;
	            minDiff = Math.abs(size.height - targetHeight);
	        }
	    }
	    
	    // Cannot find the one match the aspect ratio, ignore the requirement
	    //keep the max_downsize requirement
	    if (optimalSize == null) {
	        minDiff = Double.MAX_VALUE;
	        for (Size size : sizes) {
	        	double downsize = (double) size.width / w;
		        if (downsize > MAX_DOWNSIZE) {
		        	continue;
		        }
	            if (Math.abs(size.height - targetHeight) < minDiff) {
	                optimalSize = size;
	                minDiff = Math.abs(size.height - targetHeight);
	            }
	        }
	    }
	    //everything else failed, just take the closest match
	    if (optimalSize == null) {
	        minDiff = Double.MAX_VALUE;
	        for (Size size : sizes) {
	            if (Math.abs(size.height - targetHeight) < minDiff) {
	                optimalSize = size;
	                minDiff = Math.abs(size.height - targetHeight);
	            }
	        }
	    }
	    
	    return optimalSize;*/
	}
	
}
