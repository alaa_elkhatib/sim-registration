package com.simregistration.signed;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.simregistration.signed.R;
import com.simregistration.signed.utility.Utility;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.app.Activity;
import android.content.Intent;
public class Splash_adv extends Activity {
	int iSplashTime = 5000;
	Handler exitHandler = null;
	Runnable exitRunnable = null; 
    ImageView iv_image,iv_type;
	View pd;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_adv);
        iv_image= (ImageView) findViewById(R.id.iv_image);
		pd =findViewById(R.id.screener_pd);
	    DiskCacheUtils.removeFromCache("http://83.136.56.172/SIM/FlashPosterMRS.png", ImageLoader.getInstance().getDiscCache());
		exitHandler = new Handler();
    	exitRunnable = new Runnable() {
    		public void run(){
    			
    		exitSplash();
    		}}; 
    		if(Utility.isConnected(this)){
    			new RetrieveImageTask().execute("http://83.136.56.172/SIM/FlashPosterMRS.png");} 	else{
    			  iv_image.setImageDrawable(getResources().getDrawable(R.drawable.splash));
 				    exitHandler.postDelayed(exitRunnable, iSplashTime);
 				
    		}
      	   		
    		}

    private void exitSplash(){
    	finish();
    	Intent intent = getIntent();
		startActivity(new Intent(this,Login.class).putExtra("redirect", intent.getStringExtra("redirect")));}
    	
    
  
    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	   if (requestCode == 0) {
    	      if (resultCode == RESULT_OK) {
    	         String contents = intent.getStringExtra("SCAN_RESULT");
    	         String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
    	         // Handle successful scan
    	      } else if (resultCode == RESULT_CANCELED) {
    	         // Handle cancel
    	      }
    	   }
    	}
 
	
    class RetrieveImageTask extends AsyncTask<String, Void, Boolean> {
        protected Boolean doInBackground(String... urls) {
            try {
            	return Utility.isConnectedToServer(urls[0],30);
            	} catch (Exception e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean connected) {
        	if(connected){
        		 Target target = new Target() {
           	      
 					@Override
 					public void onBitmapFailed(Drawable arg0) {
 						pd.setVisibility(View.GONE);
 						iv_image.setImageDrawable(getResources().getDrawable(R.drawable.splash));
 						exitHandler.postDelayed(exitRunnable, iSplashTime);
 						
 					}

 					@Override
 					public void onPrepareLoad(Drawable arg0) {
 						pd.setVisibility(View.VISIBLE);
 						
 					}

 					@Override
 					public void onBitmapLoaded(Bitmap bitmap, LoadedFrom arg1) {
 						pd.setVisibility(View.GONE);
 					   iv_image.setImageBitmap(bitmap);
 						exitHandler.postDelayed(exitRunnable, iSplashTime);} };
 						
 						
        		Picasso.with(Splash_adv.this)
        		    .load("http://83.136.56.172/SIM/FlashPosterMRS.png")
        		    .error(R.drawable.splash) 
        		    .memoryPolicy(MemoryPolicy.NO_CACHE )
        		    .networkPolicy(NetworkPolicy.NO_CACHE)
        		    .into(target);
        		   
        	 /*   
 			   imageLoader.displayImage("http://83.136.56.172/SIM/FlashPosterMRS.png",iv_image, options, new ImageLoadingListener() {
 	   				@Override
 	   				public void onLoadingStarted(String imageUri, View view) {
 	   					pd.setVisibility(View.VISIBLE);
 	   				}

 	   				@Override
 	   				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
 	   					pd.setVisibility(View.GONE);
 	   				    iv_image.setImageDrawable(getResources().getDrawable(R.drawable.splash));
 	   				    exitHandler.postDelayed(exitRunnable, iSplashTime);
 	   				}

 	   				@Override
 	   				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
 	   					pd.setVisibility(View.GONE);
 	   					exitHandler.postDelayed(exitRunnable, iSplashTime);
 	   				}

					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						
					}});*/
 			}
 			else {

   			  iv_image.setImageDrawable(getResources().getDrawable(R.drawable.splash));
				    exitHandler.postDelayed(exitRunnable, iSplashTime);
 			}
        }
    }
    	 
}
