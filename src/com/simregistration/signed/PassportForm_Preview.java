
package com.simregistration.signed;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.Passport_Navy_Entity;
import com.simregistration.signed.entity.User_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.ContentUpdater;
import com.simregistration.signed.utility.DatabaseHelper;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class PassportForm_Preview extends Activity  {
	private ImageView mFormImageViewer/*,mIDNoImageViewer,mNationalityImageViewer,mNameImageViewer,mExpiryImageViewer*/;
    private  File mFormLocation,mPssportBarInfoLocation,mPssportBarInfoLocation2,mSIMLocation/*,mSigneture1Location/*,mBarLocation,mIDNoLocation,mNationalityLocation,mNameLocation/*,mExpiryLocation,
    mStampLocation,mSigneture1Location,mSigneture2Location*/;
    private ProgressDialog progress = null;
    private ContentUpdater updater;
	private SQLiteDatabase db;
	private DatabaseHelper dbHelper;
   @Override
    public void onCreate(Bundle savedInstanceState) {
    	//Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.cpr_preview); 
        dbHelper = new DatabaseHelper(this);
		db = dbHelper.getWritableDatabase();
		updater = new ContentUpdater(this, db);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  
        mFormLocation = new File(Environment.getExternalStorageDirectory(),"Passport/Main.jpg");
        mPssportBarInfoLocation = new File(Environment.getExternalStorageDirectory(),"Passport/PassportBar.jpg");
        mPssportBarInfoLocation2 = new File(Environment.getExternalStorageDirectory(),"Passport/PassportBar2.jpg");
        mFormImageViewer = (ImageView) findViewById(R.id.iv_form); 
        if(mFormLocation.exists()){
        	final BitmapFactory.Options options = new BitmapFactory.Options();
        	//options.inSampleSize = 2;
            Bitmap myBitmap = BitmapFactory.decodeFile(mFormLocation.getAbsolutePath()/*,options*/);
            mFormImageViewer.setImageBitmap(myBitmap); }
       
         }
    
   
    
   
    
    public void onCancel(View v){
    	Intent intent = new Intent(PassportForm_Preview.this, PassportForm.class);
    	intent.putExtra("Passport_Navy_CR",  getIntent().getParcelableExtra("Passport_Navy_CR"));
		intent.putExtra("image", getIntent().getStringExtra("image"));
		intent.putExtra("ICCID", getIntent().getStringExtra("ICCID"));
		intent.putExtra("document", getIntent().getStringExtra("document"));
		startActivity(intent);
    	this.finish();
    }
    
    
    public void onSend(View v){
    	 if(Utility.isConnected(PassportForm_Preview.this))
    		 new UploadCPRImages().execute(new String[]{});
    	 else
	   		Utility.showToast(PassportForm_Preview.this, getString(R.string.alert_need_internet_connection)); 
    }
    
    
    
    private class UploadCPRImages extends AsyncTask<String, Void, String> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(PassportForm_Preview.this, getString(R.string.pleaseWait), "", true, false, null);

		}

		@Override
		protected String doInBackground(String... params) {
			String strResult="";
			try {
				
				  String strFolderName =  Utility.GetUserEntity(PassportForm_Preview.this).getId()+"_"+System.currentTimeMillis()+"_Passport";	 
				  String strImage=WebService.GetImageFromFile(PassportForm_Preview.this,mFormLocation.getPath());
				  if(strImage.equalsIgnoreCase(""))
						return "Error";
				  else{
				  strResult= WebService.UploadImages(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(), "Passport", strFolderName, "Main.png", strImage);
				  if(strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.no_account))|| !strResult.equalsIgnoreCase("Success"))
						return strResult;
				  else{
				  strImage="";
				  strImage=WebService.GetImageFromFile(PassportForm_Preview.this,mPssportBarInfoLocation2.getPath());
				  if(strImage.equalsIgnoreCase(""))
						return "Error";
				  else{
				  strResult="";
				  strResult=WebService.UploadImages(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(), "Passport", strFolderName, "Bar.png", strImage);
				  if(strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.no_account))|| !strResult.equalsIgnoreCase("Success"))
						return strResult;	 
				  else{
					  
					  String filename = getIntent().getStringExtra("image");  
					  byte[] byteArray = null;
						try {
						    FileInputStream is =PassportForm_Preview.this.openFileInput(filename);
						    Bitmap bmp = BitmapFactory.decodeStream(is);
						    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
						    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
							byteArray = byteArrayOutputStream .toByteArray();
						    is.close();
						} catch (Exception ex) {
							WebService.SaveErrorLogNoBandwidth(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  	
						//	if(Utility.isConnected(PassportForm_Preview.this))
	           			//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

						    ex.printStackTrace();
						}
					strImage="";
					if(byteArray!=null)
					 strImage=Base64.encodeToString(byteArray, Base64.DEFAULT); 
					 if(strImage.equalsIgnoreCase(""))
						return "Error";
				  else{
				  strResult="";
				  strResult=WebService.UploadImages(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(),"Passport", strFolderName, "Passport.png", strImage);
				  if(strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.disabled)) ||  strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.locked))||  strResult.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.no_account))|| !strResult.equalsIgnoreCase("Success"))
						return strResult;
				 
				  else{
					  strResult="";
					  strResult=WebService.InsertSIMTransaction(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(),"Passport",getIntent().getStringExtra("ICCID"),strFolderName,strFolderName+"/Main.png");
					  	 
				
				  }}
					 
					 
				
				  }}}}}catch (Exception ex) {
						WebService.SaveErrorLogNoBandwidth(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  	
						
					 // if(Utility.isConnected(PassportForm_Preview.this))
					//		 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
		      	   //	else
		      	   //		Utility.showToast(PassportForm_Preview.this, getString(R.string.alert_need_internet_connection)); 

				return "Error";
			}
			
				
			return strResult;
		}

		@Override
		protected void onPostExecute(String v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v.equalsIgnoreCase("Success"))
					Utility.alert("Your transaction has been sent successfully, waiting for verification and activation", PassportForm_Preview.this, onclick);	
				else if(v.equalsIgnoreCase("Error"))
					Utility.alert(getString(R.string.error),PassportForm_Preview.this,null);
				
				else if(v.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.disabled)) ||  v.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.locked))||  v.equalsIgnoreCase(PassportForm_Preview.this.getString(R.string.no_account)))
					Utility.alert(v, PassportForm_Preview.this, onclick3);
				
				else 
					Utility.alert(v, PassportForm_Preview.this, null);
			
			}catch(Exception ex){
				WebService.SaveErrorLogNoBandwidth(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  	
				
				// if(Utility.isConnected(PassportForm_Preview.this))
				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
      	   //	else
      	   //		Utility.showToast(PassportForm_Preview.this, getString(R.string.alert_need_internet_connection)); 
        
			
			}}}
    
    /*
    ----------------------------- LIFE CYCLE METHODS -----------------------------
    */
 
    
   
    
  
    
    DialogInterface.OnClickListener onclick = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			setResult(RESULT_OK, null);
			finish();
		}};
    
    
		
	  DialogInterface.OnClickListener onclick2 = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(PassportForm_Preview.this, PassportForm.class);
					intent.putExtra("Passport_Navy_CR",  getIntent().getParcelableExtra("Passport_Navy_CR"));
					intent.putExtra("image", getIntent().getStringExtra("image"));
					intent.putExtra("ICCID", getIntent().getStringExtra("ICCID"));
					intent.putExtra("document", getIntent().getStringExtra("document"));    	
					startActivity(intent);
					finish();
				}};
		    
				
				 DialogInterface.OnClickListener onclick3 = new DialogInterface.OnClickListener() {
	                  @Override
							public void onClick(DialogInterface dialog, int which) {
	                	    updater.deleteOldContentsUser();
	      			        Utility.ClearShared(PassportForm_Preview.this);
							Intent intent = new Intent(PassportForm_Preview.this, Login.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);}};
		    
  

		private void savePassportIDPhoto(Bitmap bm) {
			System.gc();
			FileOutputStream image = null;
			try {
				if(!mPssportBarInfoLocation2.exists())
					mPssportBarInfoLocation2.createNewFile();
				image = new FileOutputStream(mPssportBarInfoLocation2);
			} catch (Exception ex) {
				WebService.SaveErrorLogNoBandwidth(PassportForm_Preview.this,Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   							  	
				
				//if(Utility.isConnected(PassportForm_Preview.this))
  				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(PassportForm_Preview.this).getId(),PassportForm_Preview.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});

			}
			bm.compress(CompressFormat.JPEG, 100, image);
			bm.recycle();
			bm=null;
		}
		
		@Override
		public void onBackPressed() {
			Intent intent = new Intent(PassportForm_Preview.this, PassportForm.class);
			intent.putExtra("Passport_Navy_CR",  getIntent().getParcelableExtra("Passport_Navy_CR"));
			intent.putExtra("image", getIntent().getStringExtra("image"));
			intent.putExtra("ICCID", getIntent().getStringExtra("ICCID"));
			intent.putExtra("document", getIntent().getStringExtra("document"));
			startActivity(intent);
			finish();}

    
		
    

  

}
