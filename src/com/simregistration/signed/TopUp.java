package com.simregistration.signed;
import com.simregistration.signed.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

public class TopUp extends Activity {
	private Button btn_Logout,btn_Vouchers,btn_CreditTransfer;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup);
        btn_Logout = (Button)this.findViewById(R.id.btn_logout);
        btn_Logout.setVisibility(View.GONE);
       
       
       
        
        btn_Vouchers = (Button)this.findViewById(R.id.btn_vouchers);
        btn_Vouchers.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TopUp.this,Vouchers.class);
		    	startActivity(intent);
			}});
        
        btn_CreditTransfer = (Button)this.findViewById(R.id.btn_credittranfer);
        btn_CreditTransfer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TopUp.this,Credit_Transfer.class);
		    	startActivity(intent);
			}});
      
      
		
	  
       
   }
    
 
  }
