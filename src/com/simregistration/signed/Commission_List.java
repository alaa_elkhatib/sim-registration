package com.simregistration.signed;
import java.util.ArrayList;

import com.simregistration.signed.R;
import com.simregistration.signed.entity.SIMCommission_Entity;
import com.simregistration.signed.entity.SIMTransaction_Entity;
import com.simregistration.signed.utility.C;
import com.simregistration.signed.utility.CustomAdapter;
import com.simregistration.signed.utility.Utility;
import com.simregistration.signed.utility.WebService;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.ListView;
import android.app.Activity;
import android.app.ProgressDialog;

public class Commission_List extends Activity  implements CustomAdapter.ListFiller {
	private ArrayList<SIMCommission_Entity> alSimCommissionDataMain;
	ListView lvList;
	CustomAdapter adapter;
    private ProgressDialog progress = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commission_list);
        this.findViewById(R.id.btn_logout).setVisibility(View.GONE);
		lvList = (ListView)findViewById(R.id.lvList);
		alSimCommissionDataMain =new ArrayList<SIMCommission_Entity>();// getIntent().getParcelableArrayListExtra("Commission");
		adapter = new CustomAdapter(this,alSimCommissionDataMain, R.layout.commission_item, this);
		lvList.setAdapter(adapter);
		 if(Utility.isConnected(Commission_List.this))
       	  new GetSimCommission().execute(new SIMTransaction_Entity[]{new SIMTransaction_Entity()});
   	 else
	   		Utility.showToast(Commission_List.this, getString(R.string.alert_need_internet_connection)); 
   }
    
 
    private class GetSimCommission extends AsyncTask<SIMTransaction_Entity, Void, ArrayList<SIMCommission_Entity>> {
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = ProgressDialog.show(Commission_List.this, getString(R.string.pleaseWait), "", true, true, null);
			

		}

		@Override
		protected ArrayList<SIMCommission_Entity> doInBackground( SIMTransaction_Entity ... params) {
			ArrayList<SIMCommission_Entity> list= new  ArrayList<SIMCommission_Entity>();
		    	
			try {
				
				WebService.GetSimCommision(Commission_List.this,params[0].getTransactionID(), Utility.GetUserEntity(Commission_List.this).getId(),  params[0].getType(),
						 params[0].getIDNumber(),  params[0].getSim(),  params[0].getFromDate(),  params[0].getToDate(), list);
				 }catch (Exception ex) {
					  WebService.SaveErrorLogNoBandwidth(Commission_List.this,Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
					 //if(Utility.isConnected(Commission_List.this))
	 				//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	 	   	   	
				return null;
			}
			return list;
		}

		@Override
		protected void onPostExecute(ArrayList<SIMCommission_Entity> v) {
			super.onPostExecute(v);
			progress.dismiss();
			try{
				if(v.size()>0){
					SIMCommission_Entity entity= v.get(0);
					if(entity.getError()!=null)
						Utility.showToast(Commission_List.this, entity.getError());
				else{
					alSimCommissionDataMain=v;
					adapter.notifyDataSetChanged();
					// Intent intent = new Intent(Commission_List.this, Commission_List.class);
					// intent.putExtra("Commission",  v);
					// startActivity(intent);
				}}
			
				
				}catch(Exception ex){
					  WebService.SaveErrorLogNoBandwidth(Commission_List.this,Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
						
					 //if(Utility.isConnected(Commission_List.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	      	   	//else
	      	   	//	Utility.showToast(Commission_List.this, getString(R.string.alert_need_internet_connection)); 
	        
				}}}


	@Override
	public void fillListData(View v, int pos) {
		
	}


	@Override
	public boolean isEnabled(int pos) {
		return false;
	}


	@Override
	public boolean useExtGetView() {
		return true;
	}


	@Override
	public View getView(int pos, View contextView) {
try{
			
			if (alSimCommissionDataMain== null || alSimCommissionDataMain.size() == 0)
				return  contextView;
			
				 LayoutInflater inf = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
				 contextView = inf.inflate(R.layout.commission_item, null);
				 final ViewHolder holder = new ViewHolder();
				 holder.tv_commtype = (TextView) contextView.findViewById(R.id.tvCommType); 
				 holder.tv_idtype = (TextView) contextView.findViewById(R.id.tvIDType);
				 holder.tv_amount = (TextView) contextView.findViewById(R.id.tvCommissionAmount);
				  final SIMCommission_Entity entity = (SIMCommission_Entity) alSimCommissionDataMain.get(pos);
			    
			      
			    if (entity != null && entity.getIDType() != null)
					holder.tv_idtype.setText(entity.getIDType());
			    
			    if (entity != null && entity.getCommission() != null)
					holder.tv_amount.setText(entity.getCommission());
			    
			    
			    if (entity != null && entity.getCommType() != null)
					holder.tv_commtype.setText(entity.getCommType());
			    
			
				}catch (Exception ex){
					  WebService.SaveErrorLogNoBandwidth(Commission_List.this,Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),WebService.getStackTrace(ex),WebService.DeviceINfO());   
						
					// if(Utility.isConnected(Commission_List.this))
					//	 new Utility.ErrorLogLoader().execute(new String[] {Utility.GetUserEntity(Commission_List.this).getId(),Commission_List.this.toString(),(ex.getMessage() == null) ? "null" : ex.getMessage().toString(),(ex.getCause() == null) ? "null" : ex.getCause().toString()});
	      	   	//else
	      	   		//Utility.showToast(Commission_List.this, getString(R.string.alert_need_internet_connection)); 
	        
				
		}
return contextView;
	}

	
	class ViewHolder {
		private TextView tv_idtype,tv_amount,tv_commtype;
	}
	

	@Override
	public String getFilter() {
		return "";
	}


	@Override
	public ArrayList<SIMCommission_Entity> getFilteredList() {
		return alSimCommissionDataMain;
	}
    
    }
