package com.simregistration.signed.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Countries_Entity {

	private String  id,name_eng,name_ar,action,error;
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	public String getNameEng() {
		return name_eng;
	}

	public void setNameEng(String name_eng) {
		this.name_eng = name_eng;
	}
	
	public String getNameAr() {
		return name_ar;
	}

	public void setNameAr(String name_ar) {
		this.name_ar = name_ar;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	}
	

