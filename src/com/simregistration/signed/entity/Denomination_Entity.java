package com.simregistration.signed.entity;

import java.util.ArrayList;

public class Denomination_Entity {

	private String  error="";
	private ArrayList<Denominations_Entity> denominations;
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
	public ArrayList<Denominations_Entity> getDenominations() {
		return denominations;
	}

	public void setDenominations(ArrayList<Denominations_Entity> denominations) {
		this.denominations = denominations;
	}
	
	
	
	
	
	
}
