package com.simregistration.signed.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Notification_Entity  implements Parcelable {

	private String  sim="",datetime="",msg="",status="",transid="",varification="",activation="";
	public Notification_Entity(Parcel in) { 
		readFromParcel(in); } 
	
	public Notification_Entity( ) { } 
	public String getSIM() {
		return sim;
	}

	public void setSIM(String sim) {
		this.sim = sim;
	}
	
	public String getDateTime() {
		return datetime;
	}

	public void setDateTime(String datetime) {
		this.datetime = datetime;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getTransID() {
		return transid;
	}

	public void setTransID(String transid) {
		this.transid = transid;
	}
	
	public String getVarification() {
		return varification;
	}

	public void setVarification(String varification) {
		this.varification = varification;
	}
	
	
	public String getActivation() {
		return activation;
	}

	public void setActivation(String activation) {
		this.activation = activation;
	}
	

	@Override
	public int describeContents() {
		return 0;
	}
	
	 private void readFromParcel(Parcel in) {
		
	        this.sim = in.readString();
	        this.datetime = in.readString();
	        this.msg = in.readString();
	        this.transid = in.readString();
	        this.varification = in.readString();
	        this.activation = in.readString();
	        this.status = in.readString();}
	 
		
	 
	@Override
	public void writeToParcel(Parcel dest, int arg1) {
	     dest.writeString(sim);
	     dest.writeString(datetime);
	     dest.writeString(msg);
	     dest.writeString(transid);
	     dest.writeString(varification);
	     dest.writeString(activation);
	     dest.writeString(status);}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Notification_Entity createFromParcel(Parcel in) { 
			return new Notification_Entity(in); }   
		
		public Notification_Entity[] newArray(int size) { 
			return new Notification_Entity[size]; } };
}
