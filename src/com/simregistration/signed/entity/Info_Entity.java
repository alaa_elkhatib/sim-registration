package com.simregistration.signed.entity;

public class Info_Entity {

	private String  shopname="",fullname="",email="",comm="",sms="",
	error="",balance="",merchant="";
	
	
	public String getShopName(){
		return shopname;
	}

	public void setShopName(String shopname) {
	  this.shopname = shopname;
	}
	
	public String getFullName() {
		return fullname;
	}

	public void setFullName(String fullname) {
		this.fullname = fullname;
	}
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}
	
	public String getSMS() {
		return sms;
	}

	public void setSMS(String sms) {
		this.sms = sms;
	}
	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	
	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	

	
}
