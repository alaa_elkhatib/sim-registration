package com.simregistration.signed.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Register_Entity {

	private String  status,description,contract;
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	
	
	
	}
	

