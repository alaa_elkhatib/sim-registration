package com.simregistration.signed.entity;
import android.os.Parcel;
import android.os.Parcelable;

public class Stocks_Entity {

	private String  denomination="",qty="",amount="";
	public Stocks_Entity( ) { } 
	

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
	
	
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	

	
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
	

	

}
