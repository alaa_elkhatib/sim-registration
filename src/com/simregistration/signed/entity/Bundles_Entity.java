package com.simregistration.signed.entity;
import android.os.Parcel;
import android.os.Parcelable;

public class Bundles_Entity {

	private String  pkgcode="",pkgid,id="",name="",type="",code="",command="",action="",mobile="",
			validity="",price="";

	public Bundles_Entity( ) { } 
	

	public String getPackageCode() {
		return pkgcode;
	}

	public void setPackageCode(String pkgcode) {
		this.pkgcode = pkgcode;
	}
	
	
	public String getPackageID() {
		return pkgid;
	}

	public void setPackageID(String pkgid) {
		this.pkgid = pkgid;
	}
	
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
	
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}
	
	
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}


}
