package com.simregistration.signed.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Signal_Entity {

	private String  id,signal,errorid;
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	public String getSignal() {
		return signal;
	}

	public void setSignal(String signal) {
		this.signal = signal;
	}
	
	public String getErrorId() {
		return errorid;
	}

	public void setErrorId(String errorid) {
		this.errorid = errorid;
	}
	

	
	}
	

