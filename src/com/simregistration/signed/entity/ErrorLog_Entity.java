package com.simregistration.signed.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class ErrorLog_Entity {

	private String  id,classname,stacktrace,deviceInfo,userid,datetime,signal,signalid,bandwidth;
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	public String getClassName() {
		return classname;
	}

	public void setClassName(String classname) {
		this.classname = classname;
	}
	
	public String getStackTrace() {
		return stacktrace;
	}

	public void setStackTrace(String stacktrace) {
		this.stacktrace = stacktrace;
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	
	public String getUserID() {
		return userid;
	}

	public void setUserID(String userid) {
		this.userid = userid;
	}
	
	public String getdateTime() {
		return datetime;
	}

	public void setDateTime(String datetime) {
		this.datetime = datetime;
	}
	
	public String getSignal() {
		return signal;
	}

	public void setSignal(String signal) {
		this.signal = signal;
	}
	
	public String getSignalID() {
		return signalid;
	}

	public void setSignalID(String signalid) {
		this.signalid = signalid;
	}
	
	
	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}
	
	}
	

