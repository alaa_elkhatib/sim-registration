package com.simregistration.signed.entity;
import android.os.Parcel;
import android.os.Parcelable;

public class SIMTransaction_Entity implements Parcelable{

	private String  id_number="",transaction_id="-1",account_id="",account_fullname="",sim="",name="",gender="",
	nationality="",type="",number="",country="",address="",birthday="",amount="",datetime="",user_id="",user_fullname="",
	varification_datetime="",varification_status="",varification_rejectedreason="",activation_status="",tabs_error="",
	contractno="",tabs_datetime="",imagepath="",document_type="",folder_path="",
	from_date="",to_date="",error="";
	public SIMTransaction_Entity(Parcel in) { 
		readFromParcel(in); } 
	
	public SIMTransaction_Entity( ) { } 
	

	public String getIDNumber() {
		return id_number;
	}

	public void setIDNumber(String id_number) {
		this.id_number = id_number;
	}
	
	
	public String getTransactionID() {
		return transaction_id;
	}

	public void setTransactionID(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	
	public String getAccountID() {
		return account_id;
	}

	public void setAccountID(String account_id) {
		this.account_id = account_id;
	}
	
	
	public String getAccountFullName() {
		return account_fullname;
	}

	public void setAccountFullName(String account_fullname) {
		this.account_fullname = account_fullname;
	}
	
	
	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	

	
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthDay() {
		return birthday;
	}

	public void setBirthDay(String birthday) {
		this.birthday = birthday;
	}
	
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
	
	public String getDateTime() {
		return datetime;
	}

	public void setDateTime(String datetime) {
		this.datetime = datetime;
	}
	
	public String getUserID() {
		return user_id;
	}

	public void setUserID(String user_id) {
		this.user_id = user_id;
	}
	
	public String getUserFullName() {
		return user_fullname;
	}

	public void setUserFullName(String user_fullname) {
		this.user_fullname = user_fullname;
	}
	
	public String getVarification_DateTime() {
		return varification_datetime;
	}

	public void setVarification_DateTime(String varification_datetime) {
		this.varification_datetime = varification_datetime;
	}
	
	public String getVarification_Status() {
		return varification_status;
	}

	public void setVarification_Status(String varification_status) {
		this.varification_status = varification_status;
	}
	
	


	public String getVarification_RejectedReason() {
		return varification_rejectedreason;
	}

	public void setVarification_RejectedReason(String varification_rejectedreason) {
		this.varification_rejectedreason = varification_rejectedreason;
	}
	
	public String getActivationStatus() {
		return activation_status;
	}

	public void setActivationStatus(String activation_status) {
		this.activation_status = activation_status;
	}
	
	public String getTab_Error() {
		return tabs_error;
	}

	public void setTab_Error(String tabs_error) {
		this.tabs_error = tabs_error;
	}
	
	public String getContractNo() {
		return contractno;
	}

	public void setContractNo(String contractno) {
		this.contractno = contractno;
	}
	

	public String getTabs_DateTime() {
		return tabs_datetime;
	}

	public void setTabs_DateTime(String tabs_datetime) {
		this.tabs_datetime = tabs_datetime;
	}
	
	public String getImagePath() {
		return imagepath;
	}

	public void setImagePath(String imagepath) {
		this.imagepath = imagepath;
	}
	
	public String getDocument_Type() {
		return document_type;
	}

	public void setDocument_Type(String document_type) {
		this.document_type = document_type;
	}
	
	public String getFolder_Path() {
		return folder_path;
	}

	public void setFolder_Path(String folder_path) {
		this.folder_path = folder_path;
	}
	

	public String getFromDate() {
		return from_date;
	}

	public void setFromDate(String from_date) {
		this.from_date = from_date;
	}
	
	public String getToDate() {
		return to_date;
	}

	public void setToDate(String to_date) {
		this.to_date = to_date;
	}
	
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	 private void readFromParcel(Parcel in) {
		    this.id_number= in.readString();
	        this.transaction_id = in.readString();
	        this.account_id = in.readString();
	        this.account_fullname = in.readString();
	        this.sim = in.readString();
	        this.name = in.readString();
	        this.nationality = in.readString();
	        this.gender = in.readString();
	        this.type = in.readString(); 
	        this.number = in.readString();
	        this.country = in.readString();
	        this.address = in.readString();
	        this.birthday = in.readString();
	        this.amount = in.readString();
	        this.datetime = in.readString();
	        this.user_id = in.readString();
	        this.user_fullname = in.readString();
	        this.varification_datetime = in.readString();
	        this.varification_status = in.readString();
	        this.varification_rejectedreason = in.readString(); 
	        this.activation_status = in.readString();
	        this.tabs_error = in.readString();
	        this.contractno = in.readString();
	        this.tabs_datetime = in.readString();
	        this.imagepath = in.readString();
	        this.document_type = in.readString();
	        this.folder_path = in.readString();  
	        this.from_date = in.readString();
	        this.to_date = in.readString();
	        this.error = in.readString();}
	 
		
	 
	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		 dest.writeString(id_number);
		 dest.writeString(transaction_id);
	     dest.writeString(account_id);
	     dest.writeString(account_fullname);
	     dest.writeString(sim);
	     dest.writeString(name);
	     dest.writeString(nationality);
	     dest.writeString(gender);
	     dest.writeString(type);
	     dest.writeString(number);
	     dest.writeString(country);
	     dest.writeString(address);
	     dest.writeString(birthday);
	     dest.writeString(amount);
	     dest.writeString(datetime);
	     dest.writeString(user_id);
	     dest.writeString(user_fullname);
	     dest.writeString(varification_datetime);
	     dest.writeString(varification_status);
	     dest.writeString(varification_rejectedreason);
	     dest.writeString(activation_status);
	     dest.writeString(tabs_error);
	     dest.writeString(contractno);
	     dest.writeString(tabs_datetime);  
	     dest.writeString(imagepath);
	     dest.writeString(document_type);
	     dest.writeString(folder_path);
	     dest.writeString(from_date);
	     dest.writeString(to_date);
	     dest.writeString(error);}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public SIMTransaction_Entity createFromParcel(Parcel in) { 
			return new SIMTransaction_Entity(in); }   
		
		public SIMTransaction_Entity[] newArray(int size) { 
			return new SIMTransaction_Entity[size]; } };
}
