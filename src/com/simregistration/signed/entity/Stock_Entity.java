package com.simregistration.signed.entity;

import java.util.ArrayList;

public class Stock_Entity {

	private String  error="";
	private ArrayList<Stocks_Entity> stocks;
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
	public ArrayList<Stocks_Entity> getStocks() {
		return stocks;
	}

	public void setStocks(ArrayList<Stocks_Entity> stocks) {
		this.stocks = stocks;
	}
	
	
	
	
	
	
}
