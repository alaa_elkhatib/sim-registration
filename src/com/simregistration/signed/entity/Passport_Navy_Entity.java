package com.simregistration.signed.entity;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Passport_Navy_Entity implements Parcelable{

	private String  name,msisdn,gender,nationality,id,
	 country="",address="",birthday="",expirydate="";
	private Bitmap passport;

	public Passport_Navy_Entity(Parcel in) { 
		readFromParcel(in); } 
	
	public Passport_Navy_Entity( ) { } 
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getMSISDN() {
		return msisdn;
	}

	public void setMSISDN(String msisdn) {
		this.msisdn = msisdn;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	

	
	
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthDay() {
		return birthday;
	}

	public void setBirthDay(String birthday) {
		this.birthday = birthday;
	}
	
	public String getExpiryDate() {
		return expirydate;
	}

	public void setExpiryDate(String expirydate) {
		this.expirydate = expirydate;
	}
	
	
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	 private void readFromParcel(Parcel in) {
	        this.name = in.readString();
	        this.msisdn = in.readString();
	        this.gender = in.readString();
	        this.nationality = in.readString();
	        this.id = in.readString();
	        this.country = in.readString();
	        this.address = in.readString(); 
	        this.birthday = in.readString();
	        this.expirydate = in.readString();}
	 
	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		 dest.writeString(name);
	     dest.writeString(msisdn);
	     dest.writeString(gender);
	     dest.writeString(nationality);
	     dest.writeString(id);
	     dest.writeString(country);
	     dest.writeString(address);
	     dest.writeString(birthday);
	     dest.writeString(expirydate);}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Passport_Navy_Entity createFromParcel(Parcel in) { 
			return new Passport_Navy_Entity(in); }   
		
		public Passport_Navy_Entity[] newArray(int size) { 
			return new Passport_Navy_Entity[size]; } };
}
