package com.simregistration.signed.entity;
import android.os.Parcel;
import android.os.Parcelable;

public class Denominations_Entity {

	private String  id="",name="",name_ar="",amount="",provider="";
	public Denominations_Entity( ) { } 
	

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public String getNameAR() {
		return name_ar;
	}

	public void setNameAR(String name_ar) {
		this.name_ar = name_ar;
	}
	
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	

}
