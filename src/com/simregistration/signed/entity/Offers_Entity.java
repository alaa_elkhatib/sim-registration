package com.simregistration.signed.entity;

import java.util.ArrayList;

public class Offers_Entity {

	private String  id="",name="",description="",count="",error="",image="";
    private ArrayList<String> images;
	public Offers_Entity( ) { } 
	


	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public ArrayList<String> getImages() {
		return images;
	}

	public void setImages(ArrayList<String> images) {
		this.images = images;
	}
	
}
