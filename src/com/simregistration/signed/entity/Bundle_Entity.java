package com.simregistration.signed.entity;

import java.util.ArrayList;

public class Bundle_Entity {

	private String  error="";
	private ArrayList<Bundles_Entity> bundles;
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
	public ArrayList<Bundles_Entity> getBundles() {
		return bundles;
	}

	public void setBundles(ArrayList<Bundles_Entity> bundles) {
		this.bundles = bundles;
	}
	
	
	
	
	
	
}
