package com.simregistration.signed.entity;

public class User_Entity {

	private String  id="",name,shopname,location,mobile,email,landmark,cr_number,
	error,date,locked,disabled,commission,lang,username,password,type;
	private int auto;
	private boolean bundlesales;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
	  this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getShopName() {
		return shopname;
	}

	public void setShopName(String shopname) {
		this.shopname = shopname;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLandMark() {
		return landmark;
	}

	public void setLandMark(String landmark) {
		this.landmark = landmark;
	}
	
	public String getCRNumber() {
		return cr_number;
	}

	public void setCRNumber(String cr_number) {
		this.cr_number = cr_number;
	}
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	

	public String getLocked() {
		return locked;
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}
	
	
	public String getDisabled() {
		return disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
	
	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}
	
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
	
	
	public String getUserName() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}
	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	public int getAuto() {
		return auto;
	}

	public void setAuto(int auto) {
		this.auto = auto;
	}
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	public Boolean getBundleSales() {
		return bundlesales;
	}

	public void setBundleSales(Boolean bundlesales) {
		this.bundlesales = bundlesales;
	}
	
	
	
	
}
