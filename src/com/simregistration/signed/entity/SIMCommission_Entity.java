package com.simregistration.signed.entity;
import android.os.Parcel;
import android.os.Parcelable;

public class SIMCommission_Entity   implements Parcelable{

	private String  id_type,comm,comm_type,error;
	
	public SIMCommission_Entity(Parcel in) { 
		readFromParcel(in); } 
	
	public SIMCommission_Entity( ) { } 
	

	public String getIDType() {
		return id_type;
	}

	public void setIDType(String id_type) {
		this.id_type = id_type;
	}
	
	
	public String getCommission() {
		return comm;
	}

	public void setCommission(String comm) {
		this.comm = comm;
	}
	
	
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getCommType() {
		return comm_type;
	}

	public void setCommType(String comm_type) {
		this.comm_type = comm_type;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	 private void readFromParcel(Parcel in) {
		    this.id_type= in.readString();
	        this.comm = in.readString(); 
	        this.comm_type = in.readString(); 
	        this.error = in.readString();}
	 
		
	 
	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		 dest.writeString(id_type);
		 dest.writeString(comm);
		 dest.writeString(comm_type);
	     dest.writeString(error);}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public SIMCommission_Entity createFromParcel(Parcel in) { 
			return new SIMCommission_Entity(in); }   
		
		public SIMCommission_Entity[] newArray(int size) { 
			return new SIMCommission_Entity[size]; } };
}
